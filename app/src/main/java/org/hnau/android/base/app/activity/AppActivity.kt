package org.hnau.android.base.app.activity

import android.os.Bundle
import androidx.core.app.ComponentActivity
import org.hnau.android.base.extensions.applyEdgeToEdgeTheme
import org.hnau.android.base.extensions.prepareEdgeToEdge
import org.hnau.android.base.ui.lightness.Lightness


class AppActivity : ComponentActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        window.prepareEdgeToEdge()
        super.onCreate(savedInstanceState)
        setContentView(AppActivityView(this))
        window.applyEdgeToEdgeTheme(Lightness.light)
    }

}