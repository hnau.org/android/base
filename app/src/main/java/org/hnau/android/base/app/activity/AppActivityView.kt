package org.hnau.android.base.app.activity

import android.content.Context
import android.graphics.Color
import android.view.Gravity
import android.view.View
import android.widget.FrameLayout
import android.widget.LinearLayout
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import org.hnau.android.base.data.course.Course
import org.hnau.android.base.data.getter.px.dp
import org.hnau.android.base.data.insets.Insets
import org.hnau.android.base.extensions.color.replaceAlpha
import org.hnau.android.base.extensions.view.group.linear.course
import org.hnau.android.base.extensions.view.group.recycle.setLinearLayoutManager
import org.hnau.android.base.extensions.view.layout.params.linearParams
import org.hnau.android.base.extensions.view.layout.params.recycleParams
import org.hnau.android.base.ui.view.recycler.adapter.AdapterInfo
import org.hnau.android.base.ui.view.recycler.adapter.adapter
import org.hnau.android.base.ui.view.recycler.adapter.content.AdapterContent
import org.hnau.android.base.ui.view.recycler.adapter.content.static
import org.hnau.android.base.ui.view.recycler.adapter.item.ItemViewProvider
import org.hnau.android.base.ui.view.recycler.adapter.item.plain.plain
import org.hnau.android.base.ui.view.recycler.adapter.utils.separations.addBeforeAfterSeparations
import org.hnau.android.base.ui.view.utils.illuminator.ApplyInsetsContainer
import org.hnau.android.base.ui.view.utils.illuminator.ApplySelfInsetsContainer
import org.hnau.android.base.ui.view.utils.illuminator.IlluminatorContainerView
import org.hnau.android.base.ui.view.utils.illuminator.resolveInsets
import org.hnau.android.base.ui.view.utils.state.createStateEmitters
import org.hnau.emitter.extensions.map
import org.hnau.emitter.extensions.useWhen


fun AppActivityView(
        context: Context
) = FrameLayout(context).apply {
    val state = createStateEmitters()
    addView(
            IlluminatorContainerView(
                    context = context,
                    background = RecyclerView(context).apply {
                        setLinearLayoutManager(course = Course.vertical)
                        adapter(
                                info = resolveInsets(
                                        isActive = state.isActive,
                                        onLayout = state.onLayout
                                ).let { insets ->
                                    AdapterInfo(
                                            content = AdapterContent.static(
                                                    content = (0 until 100).toList()
                                            ),
                                            viewProvider = ItemViewProvider.plain { context, content ->
                                                TextView(context).apply {
                                                    recycleParams {
                                                        width = matchParent
                                                        height = 56.dp
                                                    }
                                                    textSize = 16f
                                                    gravity = Gravity.CENTER
                                                    content
                                                            .useWhen(createStateEmitters().isActive)
                                                            .observe {
                                                                text = it.toString()
                                                                if (it % 2 == 0) {
                                                                    setTextColor(Color.RED)
                                                                    setBackgroundColor(Color.WHITE)
                                                                } else {
                                                                    setTextColor(Color.WHITE)
                                                                    setBackgroundColor(Color.RED.replaceAlpha(0.5f))
                                                                }
                                                            }
                                                }
                                            }
                                    ).addBeforeAfterSeparations(
                                            separatorItemType = 1,
                                            before = insets.map(Insets<Int>::top),
                                            after = insets.map(Insets<Int>::bottom)
                                    )
                                }
                        )
                    },
                    buildForeground = { illuminatorView ->
                        ApplySelfInsetsContainer(
                                content = LinearLayout(context).apply {
                                    course = Course.vertical
                                    addView(
                                            View(context).apply {
                                                linearParams {
                                                    width = matchParent
                                                    height = 48.dp
                                                }
                                                setBackgroundColor(Color.MAGENTA.replaceAlpha(0.5f))
                                            }
                                    )
                                    addView(
                                            illuminatorView.apply {
                                                linearParams {
                                                    width = matchParent
                                                    height = zero
                                                    weight = 1
                                                }
                                            }
                                    )
                                    addView(
                                            View(context).apply {
                                                linearParams {
                                                    width = matchParent
                                                    height = 64.dp
                                                }
                                                setBackgroundColor(Color.CYAN.replaceAlpha(0.5f))
                                            }
                                    )
                                }
                        )
                    },
                    isActive = state.isActive,
                    onLayout = state.onLayout
            )
    )
}

