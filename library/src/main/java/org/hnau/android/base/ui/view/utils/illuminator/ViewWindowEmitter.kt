package org.hnau.android.base.ui.view.utils.illuminator

import android.view.View
import org.hnau.android.base.extensions.window
import org.hnau.base.extensions.boolean.ifTrue
import org.hnau.emitter.Emitter
import org.hnau.emitter.extensions.filterNotNull
import org.hnau.emitter.extensions.map


fun ViewWindowEmitter(
        view: View,
        isActive: Emitter<Boolean>
) = isActive
        .map { active ->
            active.ifTrue { view.context.window }
        }
        .filterNotNull()