package org.hnau.android.base.ui.color.material


enum class MaterialLightness {

    v50,
    v100,
    v200,
    v300,
    v400,
    v500,
    v600,
    v700,
    v800,
    v900,

    a100,
    a200,
    a400,
    a700

}