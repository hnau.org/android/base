package org.hnau.android.base.data.getter.tone

import android.content.Context
import android.graphics.Color
import androidx.annotation.ColorInt
import androidx.annotation.ColorRes
import androidx.core.content.ContextCompat
import org.hnau.android.base.data.getter.base.ContextGetter
import org.hnau.android.base.data.getter.base.CachedContextGetter


typealias Tone = ContextGetter<Int>

fun Tone(get: (Context) -> Int): Tone =
    CachedContextGetter(get)

object Tones {

    val transparent = fromColor(Color.TRANSPARENT)
    val black = fromColor(Color.BLACK)
    val white = fromColor(Color.WHITE)
    val darkGray = fromColor(Color.DKGRAY)
    val gray = fromColor(Color.GRAY)
    val lightGray = fromColor(Color.LTGRAY)
    val red = fromColor(Color.RED)
    val green = fromColor(Color.GREEN)
    val blue = fromColor(Color.BLUE)
    val yellow = fromColor(Color.YELLOW)
    val cyan = fromColor(Color.CYAN)
    val magenta = fromColor(Color.MAGENTA)

}

fun Tones.fromColor(@ColorInt color: Int) =
    Tone { color }

fun Tones.fromResId(@ColorRes colorResId: Int) =
    Tone { ContextCompat.getColor(it, colorResId) }

fun Tones.fromHex(hex: String) =
    Tone { Color.parseColor(hex) }

fun @receiver:ColorInt Int.toTone() = Tones.fromColor(this)