package org.hnau.android.base.ui.drawable.partout.calculators

import android.graphics.Point
import org.hnau.android.base.data.direction.Direction
import org.hnau.android.base.ui.drawable.partout.PartoutCalculator


open class ScalePartoutCalculator(
        scaleX: Number,
        scaleY: Number,
        direction: Direction = Direction.center
) : DirectionPartoutCalculator(
        direction = direction
) {

    constructor(
            scale: Number,
            direction: Direction = Direction.center
    ) : this(
            scaleX = scale,
            scaleY = scale,
            direction = direction
    )

    private val scaleX = scaleX.toFloat()
    private val scaleY = scaleY.toFloat()

    override fun calcContentSize(
            parentWidth: Int,
            parentHeight: Int,
            contentPreferredWidth: Int,
            contentPreferredHeight: Int,
            result: Point
    ) = result.set(
            (contentPreferredWidth * scaleX).toInt(),
            (contentPreferredHeight * scaleY).toInt()
    )

    override fun calcIntrinsicWrapperWidth(contentIntrinsicWidth: Int) =
            (contentIntrinsicWidth * scaleX).toInt()

    override fun calcIntrinsicWrapperHeight(contentIntrinsicHeight: Int) =
            (contentIntrinsicHeight * scaleY).toInt()

}

fun PartoutCalculator.Companion.scale(
        scaleX: Number,
        scaleY: Number,
        direction: Direction = Direction.center
) = ScalePartoutCalculator(
        scaleX = scaleX,
        scaleY = scaleY,
        direction = direction
)