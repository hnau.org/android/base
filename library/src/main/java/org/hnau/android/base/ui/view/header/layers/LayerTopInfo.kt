package org.hnau.android.base.ui.view.header.layers

import org.hnau.android.base.ui.lightness.Lightness
import org.hnau.android.base.ui.theme.ThemeGradient


sealed class LayerTopInfo {

    data class Background(
            val lightness: Lightness
    ) : LayerTopInfo()

    data class HeaderGradient(
            val gradient: ThemeGradient
    ) : LayerTopInfo()

}