package org.hnau.android.base.extensions.view.text

import android.widget.TextView
import org.hnau.android.base.data.direction.Direction
import org.hnau.android.base.data.direction.gravity
import org.hnau.base.exception.property.NoGetterException


var TextView.textViewDirection: Direction
    set(value) {
        gravity = value.gravity
    }
    @Deprecated(
            level = DeprecationLevel.HIDDEN,
            message = NoGetterException.message
    )
    get() = NoGetterException.doThrow()