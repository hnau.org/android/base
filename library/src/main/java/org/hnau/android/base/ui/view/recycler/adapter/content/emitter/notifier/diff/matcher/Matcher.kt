package org.hnau.android.base.ui.view.recycler.adapter.content.emitter.notifier.diff.matcher

import org.hnau.android.base.ui.view.recycler.adapter.content.emitter.notifier.diff.matcher.item.ContentsMatcher
import org.hnau.android.base.ui.view.recycler.adapter.content.emitter.notifier.diff.matcher.item.alwaysSame


data class Matcher<T>(
    val itemsMatcher: ItemsMatcher<T>,
    val contentsMatcher: ContentsMatcher<T>
) {

    companion object;

}

fun <T> Matcher.Companion.forImmutableItems(
    itemsMatcher: ItemsMatcher<T>
) = Matcher<T>(
    itemsMatcher = itemsMatcher,
    contentsMatcher = ContentsMatcher.alwaysSame()
)