package org.hnau.android.base.data.insets


inline fun <T> Insets<T>.horizontalSum(
        plus: (T, T) -> T
) = plus(left, right)

inline fun <T> Insets<T>.verticalSum(
        plus: (T, T) -> T
) = plus(top, bottom)