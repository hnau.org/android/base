package org.hnau.android.base.ui.view.waiter.material

import org.hnau.android.base.ui.view.waiter.material.params.MaterialWaiterAnimationParams


class MaterialWaiterHelper(
        private val animationParams: MaterialWaiterAnimationParams
) {

    companion object {

        private const val degreesInCircle = 360.0

    }

    data class ArcParams(
            var start: Float = 0f,
            var sweep: Float = 0f
    )

    fun calculateArcParams(arcParams: ArcParams) {
        val now = System.currentTimeMillis()
        val startFaze = (now % (animationParams.period.milliseconds)).toFloat() / animationParams.period.milliseconds.toDouble()
        val endFazeRaw = startFaze + animationParams.fazesDistancePercentage
        val endFaze = if (endFazeRaw > 1) endFazeRaw - 1 else endFazeRaw

        val degreesOffset = (now / animationParams.degreesOffsetFactor) % degreesInCircle

        val startAngleRaw = fazeToAngle(startFaze, degreesOffset)
        val endAngleRaw = fazeToAngle(endFaze, degreesOffset)

        arcParams.start = startAngleRaw.toFloat()
        arcParams.sweep = (endAngleRaw - startAngleRaw).let { if (it >= 0) it else it + degreesInCircle }.toFloat()

    }

    private fun fazeToAngle(faze: Double, degreesOffset: Double): Double {
        val value = faze % 1
        return animationParams.lineSidesCurver((value * value).toFloat()) * degreesInCircle + degreesOffset
    }


}