package org.hnau.android.base.ui.view.presenter.info.size.presenter

import org.hnau.android.base.ui.MeasureUtils
import org.hnau.android.base.ui.view.presenter.info.size.content.base.ContentSizeCalculator
import org.hnau.android.base.ui.view.presenter.info.size.content.wide
import org.hnau.android.base.ui.view.presenter.info.size.presenter.base.PresenterSizeCalculator
import org.hnau.android.base.ui.view.presenter.view.lifecycle.ShowingMeasurable
import org.hnau.android.base.ui.view.presenter.view.lifecycle.ViewMeasuredSizeGetter


class WrapContentPresenterSizeCalculator(
    private val contentSizeCalculator: ContentSizeCalculator
): PresenterSizeCalculator {

    override fun calcPresenterSize(
        measureSpec: Int,
        measureds: Collection<ShowingMeasurable>,
        measuredSizeExtractor: (ViewMeasuredSizeGetter) -> Int?
    ): Int {
        val contentSize = contentSizeCalculator.calcContentSize(measureds, measuredSizeExtractor)
        return MeasureUtils.fit(measureSpec, contentSize)
    }

}

fun PresenterSizeCalculator.Companion.wrapContent(
    contentSizeCalculator: ContentSizeCalculator = ContentSizeCalculator.wide()
) = WrapContentPresenterSizeCalculator(
    contentSizeCalculator = contentSizeCalculator
)