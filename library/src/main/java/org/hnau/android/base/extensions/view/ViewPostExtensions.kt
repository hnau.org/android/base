package org.hnau.android.base.extensions.view

import android.view.View
import org.hnau.base.data.time.Time


fun View.postDelayed(pause: Time, action: Runnable) =
    postDelayed(action, pause.milliseconds)

fun View.postDelayed(pause: Time, action: () -> Unit) =
    postDelayed(action, pause.milliseconds)