package org.hnau.android.base.extensions.view.base.edgesOld

import android.os.Build
import android.widget.ScrollView
import androidx.annotation.ColorInt
import org.hnau.android.base.data.getter.tone.Tone


private val scrollViewEdgeEffectsColorsSetter by lazy {
    SetEdgesOldColorUtils.createEdgeEffectsColorsSetter<ScrollView>(listOf("mEdgeGlowTop", "mEdgeGlowBottom"))
}

fun ScrollView.setEdgesOldColor(
        @ColorInt color: Int
) {
    if (Build.VERSION.SDK_INT < Build.VERSION_CODES.LOLLIPOP) {
        return
    }
    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.Q) {
        setEdgeEffectColor(color)
    } else {
        scrollViewEdgeEffectsColorsSetter(color)
    }
}

fun ScrollView.setEdgesOldTone(
        tone: Tone
) = setEdgesOldColor(
        color = tone(context)
)