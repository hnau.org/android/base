package org.hnau.android.base.ui.shape

import android.graphics.Canvas
import android.graphics.Paint
import org.hnau.android.base.data.edges.Edges
import org.hnau.emitter.Emitter


class RectCanvasShape : BoundsCanvasShape() {

    override fun draw(canvas: Canvas, paint: Paint) =
            canvas.drawRect(boundsRect, paint)

    override fun clip(canvas: Canvas) {
        canvas.clipRect(boundsRect)
    }

}

fun CanvasShape.Companion.rect() = RectCanvasShape()

fun CanvasShape.Companion.rect(bounds: Emitter<Edges<Int>>) =
        rect().withBounds(bounds)