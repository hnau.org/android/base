package org.hnau.android.base.ui.view.presenter.info.size.content

import org.hnau.android.base.ui.animation.curver.Curver
import org.hnau.android.base.ui.animation.curver.Curvers
import org.hnau.android.base.ui.view.presenter.info.size.content.base.ContentSizeCalculator


class SmoothContentSizeCalculator(
    curver: Curver = Curvers.accelerateDecelerate
) : CombiningContentSizeCalculator(
        curver = curver
) {

    override fun calculateViewSignificantSize(viewSize: Int?, percentage: Float) =
        (viewSize ?: 0) * percentage

}

fun ContentSizeCalculator.Companion.smooth(
    curver: Curver = Curvers.accelerateDecelerate
) = SmoothContentSizeCalculator(
    curver = curver
)