package org.hnau.android.base.extensions.color

import android.graphics.Color
import androidx.annotation.ColorInt
import androidx.annotation.FloatRange
import androidx.annotation.IntRange


typealias HSV = FloatArray

fun HSV(
    @FloatRange(from = 0.0, to = 360.0) hue: Float = 0f,
    @FloatRange(from = 0.0, to = 1.0) saturation: Float = 0f,
    @FloatRange(from = 0.0, to = 1.0) value: Float = 0f
): HSV = floatArrayOf(
    hue,
    saturation,
    value
)

fun HSV(
    @IntRange(from = 0L, to = 255L) hue: Int,
    @IntRange(from = 0L, to = 255L) saturation: Int,
    @IntRange(from = 0L, to = 255L) value: Int
): HSV = HSV(
    hue = hue / 255f,
    saturation = saturation / 255f,
    value = value / 255f
)

val HSV.hue get() = get(0)
val HSV.saturation get() = get(1)
val HSV.value get() = get(2)

fun @receiver:ColorInt Int.toHSV() =
    HSV().also { Color.RGBToHSV(red, green, blue, it) }

@ColorInt
fun HSV.toRGB(@IntRange(from = 0L, to = 255L) alpha: Int = 255) =
    Color.HSVToColor(alpha, this)