package org.hnau.android.base.extensions.view

import android.view.View
import org.hnau.android.base.data.getter.px.Px
import org.hnau.android.base.ui.MeasureUtils


val View.unspecifiedMeasureSpec: Int
    get() = MeasureUtils.unspecified

fun View.makeMeasureSpec(mode: Int, size: Number) = MeasureUtils.make(mode, size)
fun View.makeExactlyMeasureSpec(size: Number) = MeasureUtils.makeExactly(size)
fun View.makeAtMostMeasureSpec(size: Number) = MeasureUtils.makeAtMost(size)

fun View.makeMeasureSpec(mode: Int, size: Px) = MeasureUtils.make(context, mode, size)
fun View.makeExactlyMeasureSpec(size: Px) = MeasureUtils.makeExactly(context, size)
fun View.makeAtMostMeasureSpec(size: Px) = MeasureUtils.makeAtMost(context, size)

fun View.getMeasureSpecSize(measureSpec: Int) = MeasureUtils.getSize(measureSpec)
fun View.getMeasureSpecMode(measureSpec: Int) = MeasureUtils.getMode(measureSpec)
fun View.getMeasureSpecModeName(measureSpec: Int) = MeasureUtils.getModeName(measureSpec)

fun View.measureSpecMaxOrNull(measureSpec: Int) = MeasureUtils.maxOrNull(measureSpec)
fun View.measureSpecMax(measureSpec: Int, forUnspecified: Number = 0) = MeasureUtils.max(measureSpec, forUnspecified)
fun View.measureSpecMax(measureSpec: Int, forUnspecified: Px) =  MeasureUtils.max(context, measureSpec, forUnspecified)

fun View.measureSpecFit(measureSpec: Int, size: Number) = MeasureUtils.fit(measureSpec, size)
fun View.measureSpecFit(measureSpec: Int, size: Px) = MeasureUtils.fit(context, measureSpec, size)

inline fun View.mapMeasureSpec(measureSpec: Int, converter: (mode: Int, size: Int) -> Int) = MeasureUtils.map(measureSpec, converter)
inline fun View.mapMeasureSpecSize(measureSpec: Int, converter: (mode: Int, size: Int) -> Number) = MeasureUtils.mapSize(measureSpec, converter)
inline fun View.mapMeasureSpecSizePx(measureSpec: Int, converter: (mode: Int, size: Px) -> Px) = MeasureUtils.mapSize(context, measureSpec, converter)

fun View.changeMeasureSpecSize(measureSpec: Int, offset: Number) = MeasureUtils.changeSize(measureSpec, offset)
fun View.changeMeasureSpecSize(measureSpec: Int, offset: Px) = MeasureUtils.changeSize(context, measureSpec, offset)