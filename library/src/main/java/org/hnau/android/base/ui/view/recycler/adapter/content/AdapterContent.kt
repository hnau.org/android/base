package org.hnau.android.base.ui.view.recycler.adapter.content


interface AdapterContent<T> {

    companion object;

    val size: Int

    fun getItem(position: Int): T

}