package org.hnau.android.base.extensions

import android.graphics.Paint
import androidx.annotation.ColorInt

@Suppress("FunctionName")
fun paint(
        flags: Int = Paint.ANTI_ALIAS_FLAG,
        configure: Paint.() -> Unit = {}
) = Paint(
        flags
).apply(configure)

fun Paint.setShadowInfo(radius: Number, dx: Number, dy: Number, @ColorInt shadowColor: Int) =
        setShadowLayer(radius.toFloat(), dx.toFloat(), dy.toFloat(), shadowColor)

val Paint.FontMetrics.height
    get() = descent - ascent