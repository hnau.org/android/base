package org.hnau.android.base.data.insets

import org.hnau.android.base.data.edges.MutableEdges
import org.hnau.base.data.possible.lateinit.Lateinit
import org.hnau.base.data.possible.lateinit.unsafe
import org.hnau.base.data.possible.updateMutable
import org.hnau.emitter.Emitter
import org.hnau.emitter.extensions.map


inline fun <I, O> Insets<I>.map(
        transform: (I) -> O
) = Insets(
        left = transform(left),
        top = transform(top),
        right = transform(right),
        bottom = transform(bottom)
)

inline fun <I, O> Emitter<Insets<I>>.mapMemSave(
        crossinline transform: (I) -> O
): Emitter<Insets<O>> = Lateinit.unsafe<MutableInsets<O>>().let { resultCache ->
    map { source ->
        resultCache.updateMutable(
                createInitialValue = {
                    MutableInsets(
                            left = transform(source.left),
                            top = transform(source.top),
                            right = transform(source.right),
                            bottom = transform(source.bottom)
                    )
                },
                updateValue = {
                    left = transform(source.left)
                    top = transform(source.top)
                    right = transform(source.right)
                    bottom = transform(source.bottom)
                }
        )
    }
}