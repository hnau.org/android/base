package org.hnau.android.base.ui.path.bounded.drawable

import android.graphics.Canvas
import android.graphics.Paint
import org.hnau.android.base.extensions.inState
import org.hnau.android.base.ui.drawable.CustomDrawable
import org.hnau.android.base.ui.path.bounded.BoundedPath


abstract class BoundedPathDrawable(
        private val boundedPath: BoundedPath
) : CustomDrawable() {

    companion object;

    protected abstract val paint: Paint

    override fun draw(
            canvas: Canvas
    ) = canvas.inState {
        translate(
                bounds.left.toFloat(),
                bounds.top.toFloat()
        )
        scale(
                bounds.width() / boundedPath.size.x,
                bounds.height() / boundedPath.size.y
        )
        canvas.drawPath(boundedPath.path, paint)
    }

    override fun getIntrinsicWidth() =
            boundedPath.size.x.toInt()

    override fun getIntrinsicHeight() =
            boundedPath.size.y.toInt()

}