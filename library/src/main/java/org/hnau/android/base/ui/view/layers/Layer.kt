package org.hnau.android.base.ui.view.layers

import android.content.Context
import android.view.View
import org.hnau.android.base.ui.view.utils.GoBackHandler


interface Layer: GoBackHandler {

    fun createView(context: Context): View

    fun onCreate() {}
    fun onDestroy() {}

}