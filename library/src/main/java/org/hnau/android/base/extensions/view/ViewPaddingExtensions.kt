package org.hnau.android.base.extensions.view

import android.view.View


fun View.setPaddingLeft(paddingLeft: Number) =
        setPadding(paddingLeft.toInt(), paddingTop, paddingRight, paddingBottom)

fun View.setPaddingTop(paddingTop: Number) =
        setPadding(paddingLeft, paddingTop.toInt(), paddingRight, paddingBottom)

fun View.setPaddingRight(paddingRight: Number) =
        setPadding(paddingLeft, paddingTop, paddingRight.toInt(), paddingBottom)

fun View.setPaddingBottom(paddingBottom: Number) =
        setPadding(paddingLeft, paddingTop, paddingRight, paddingBottom.toInt())

fun View.setPaddingStart(paddingStart: Number) =
        setPaddingRelative(paddingStart.toInt(), paddingTop, paddingEnd, paddingBottom)

fun View.setPaddingEnd(paddingEnd: Number) =
        setPaddingRelative(paddingStart, paddingTop, paddingEnd.toInt(), paddingBottom)


fun View.setPaddingHorizontal(paddingHorizontal: Number) {
    val paddingHorizontalLocal = paddingHorizontal.toInt()
    setPadding(paddingHorizontalLocal, paddingTop, paddingHorizontalLocal, paddingBottom)
}

fun View.setPaddingVertical(paddingVertical: Number) {
    val paddingVerticalLocal = paddingVertical.toInt()
    setPadding(paddingVerticalLocal, paddingTop, paddingVerticalLocal, paddingBottom)
}