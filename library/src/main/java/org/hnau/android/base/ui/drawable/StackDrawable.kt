package org.hnau.android.base.ui.drawable

import android.graphics.Canvas
import android.graphics.ColorFilter
import android.graphics.PixelFormat
import android.graphics.drawable.Drawable
import kotlin.math.max
import kotlin.math.min


class StackDrawable(
    private val items: List<Drawable>
) : Drawable(), Drawable.Callback {

    interface WrapperSizeCalculator {

        fun calcIntrinsicWrapperWidth(contentIntrinsicWidth: Int): Int
        fun calcIntrinsicWrapperHeight(contentIntrinsicHeight: Int): Int

    }

    init {
        forEachItem { it.callback = this }
    }

    private inline fun forEachItem(action: (Drawable) -> Unit) =
        items.forEach(action)

    override fun getIntrinsicWidth() = run {
        var result = 0
        forEachItem { result = max(result, it.intrinsicWidth) }
        return@run result
    }

    override fun getIntrinsicHeight() = run {
        var result = 0
        forEachItem { result = max(result, it.intrinsicHeight) }
        return@run result
    }

    override fun setAlpha(alpha: Int) =
        forEachItem { it.setAlpha(alpha) }

    @Suppress("DEPRECATION")
    override fun getOpacity() = run {
        var result = PixelFormat.TRANSPARENT
        forEachItem { result = min(result, it.opacity) }
        return@run result
    }

    override fun setColorFilter(colorFilter: ColorFilter?) =
        forEachItem { it.setColorFilter(colorFilter) }

    override fun invalidateSelf() {
        super.invalidateSelf()
        forEachItem { it.invalidateSelf() }
    }

    override fun draw(canvas: Canvas) =
        forEachItem { it.draw(canvas) }

    override fun setBounds(left: Int, top: Int, right: Int, bottom: Int) {
        super.setBounds(left, top, right, bottom)
        forEachItem { it.setBounds(left, top, right, bottom) }
    }

    override fun unscheduleDrawable(who: Drawable, what: Runnable) {
        callback?.unscheduleDrawable(this, what)
    }

    override fun invalidateDrawable(who: Drawable) {
        callback?.invalidateDrawable(this)
    }

    override fun scheduleDrawable(who: Drawable, what: Runnable, time: Long) {
        callback?.scheduleDrawable(this, what, time)
    }

}