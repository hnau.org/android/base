package org.hnau.android.base.data.getter.px


class PxConstants {

    class Text {

        val sp16 = 16.sp

    }

    val text = Text()

    class Separations {

        val default = 16.dp
        val small = default / 2
        val large = default * 2

    }

    val separations = Separations()

    val headerHeight = 56.dp

}

private val pxConstants = PxConstants()
val Px.Companion.constants get() = pxConstants