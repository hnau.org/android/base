package org.hnau.android.base.ui.view.utils.input.filter


fun FilterInputFilter.Companion.alphabet(
        alphabet: Set<Char>
) = FilterInputFilter { char ->
    char in alphabet
}