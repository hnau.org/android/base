package org.hnau.android.base.extensions

import android.graphics.drawable.Drawable
import androidx.annotation.ColorInt
import androidx.core.graphics.drawable.DrawableCompat


fun Drawable.createdTinted(
        @ColorInt tint: Int
): Drawable = DrawableCompat.wrap(this).also { wrapperDrawable ->
    DrawableCompat.setTint(wrapperDrawable, tint)
}