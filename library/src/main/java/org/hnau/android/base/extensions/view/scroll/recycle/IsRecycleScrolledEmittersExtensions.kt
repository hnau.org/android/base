package org.hnau.android.base.extensions.view.scroll.recycle

import androidx.recyclerview.widget.RecyclerView
import org.hnau.emitter.Emitter
import org.hnau.emitter.extensions.map
import org.hnau.emitter.observing.notifier.Notifier


@Deprecated("Use RecyclerScrolledStateEmitters instead")
fun RecyclerView.createOnRecyclerScrolledEmitter() = object : Notifier() {

    private val onScrollListener =
        object : RecyclerView.OnScrollListener() {
            override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
                super.onScrolled(recyclerView, dx, dy)
                notifyObservers()
            }
        }

    init {
        addOnScrollListener(onScrollListener)
    }

}

@Deprecated("Use RecyclerScrolledStateEmitters instead")
fun RecyclerView.createRecycleIsScrolledToLeftEmitter(onRecycleScrolledEmitter: Emitter<Unit>) =
    onRecycleScrolledEmitter.map { isRecycleScrolledToLeft }

@Deprecated("Use RecyclerScrolledStateEmitters instead")
fun RecyclerView.createRecycleIsScrolledToTopEmitter(onRecycleScrolledEmitter: Emitter<Unit>) =
    onRecycleScrolledEmitter.map { isRecycleScrolledToTop }

@Deprecated("Use RecyclerScrolledStateEmitters instead")
fun RecyclerView.createRecycleIsScrolledToRightEmitter(onRecycleScrolledEmitter: Emitter<Unit>) =
    onRecycleScrolledEmitter.map { isRecycleScrolledToRight }

@Deprecated("Use RecyclerScrolledStateEmitters instead")
fun RecyclerView.createRecycleIsScrolledToBottomEmitter(onRecycleScrolledEmitter: Emitter<Unit>) =
    onRecycleScrolledEmitter.map { isRecycleScrolledToBottom }