package org.hnau.android.base.ui.view.button

import org.hnau.android.base.data.getter.px.Px
import org.hnau.android.base.data.getter.px.dp
import org.hnau.android.base.data.getter.tone.Tone
import org.hnau.android.base.ui.shadow.ButtonShadowInfo


data class MainButtonInfo(
        val background: Tone,
        val shadow: ButtonShadowInfo,
        val diameter: Px = 56.dp
)