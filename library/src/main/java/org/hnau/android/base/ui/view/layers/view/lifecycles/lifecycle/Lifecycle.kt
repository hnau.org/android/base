package org.hnau.android.base.ui.view.layers.view.lifecycles.lifecycle

import android.content.Context
import android.graphics.Canvas
import android.graphics.Rect
import android.view.View
import org.hnau.android.base.ui.animation.emergence.offset.EmergenceDirection
import org.hnau.android.base.ui.animation.emergence.offset.Offset
import org.hnau.android.base.ui.view.layers.view.lifecycles.LifecyclesParent
import org.hnau.android.base.ui.view.layers.view.lifecycles.transaction.Transaction
import org.hnau.android.base.ui.view.layers.view.presenting.PresentingLayer
import org.hnau.base.extensions.boolean.checkBoolean
import org.hnau.base.extensions.boolean.ifFalse
import org.hnau.base.extensions.boolean.ifTrue
import java.lang.ref.SoftReference


class Lifecycle(
    private val context: Context,
    val layer: PresentingLayer,
    private val lifecyclesParent: LifecyclesParent
) {

    var transaction: Transaction? = null

    private val layersStackDecoration =
        layer.stackDecoration

    private var viewCache: SoftReference<View>? = null
    private val existenceView get() = viewCache?.get()

    private val viewDrawer = ViewDrawer()

    private var visible = false

    fun setVisible(
        visible: Boolean
    ) {
        (this.visible == visible).ifTrue { return }
        this.visible = visible
        visible.checkBoolean(
            ifTrue = {
                val view = getView()
                lifecyclesParent.addLifecycleView(view)
            },
            ifFalse = {
                val view = existenceView ?: return@checkBoolean
                lifecyclesParent.removeLifecycleView(view)
            }
        )
    }

    private fun getView(): View {
        var result = existenceView
        if (result == null) {
            result = layer.createView(context)
            viewCache = SoftReference(result)
        }
        return result
    }

    fun setAlpha(
        alpha: Float
    ) {
        existenceView?.alpha = alpha
    }

    fun draw(
        canvas: Canvas,
        drawView: (Canvas, View) -> Unit,
        offset: Offset,
        direction: EmergenceDirection,
        offsetAmount: Float,
        rect: Rect
    ) {
        visible.ifFalse { return }

        (transaction != null || !layer.opaque).ifTrue {
            layersStackDecoration.drawLayersStackDecoration(
                context = context,
                canvas = canvas,
                direction = direction,
                offsetAmount = offsetAmount,
                rect = rect
            )
        }

        existenceView?.let { view ->
            viewDrawer.draw(
                view = view,
                canvas = canvas,
                drawView = drawView,
                offset = offset
            )
        }
    }

}