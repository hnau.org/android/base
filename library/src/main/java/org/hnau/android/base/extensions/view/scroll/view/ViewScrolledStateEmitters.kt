package org.hnau.android.base.extensions.view.scroll.view

import android.view.View
import androidx.recyclerview.widget.RecyclerView
import org.hnau.emitter.extensions.makeAlways


class ViewScrolledStateEmitters(
    view: View
) {

    val onScrolledNotifier by lazy {
        @Suppress("DEPRECATION")
        view.createOnScrolledNotifier()
    }

    private val onScrolledAlwaysNotifier by lazy {
        onScrolledNotifier.makeAlways { Unit }
    }

    val isScrolledToLeft by lazy {
        @Suppress("DEPRECATION")
        view.createIsScrolledToLeftEmitter(onScrolledAlwaysNotifier)
    }

    val isScrolledToTop by lazy {
        @Suppress("DEPRECATION")
        view.createIsScrolledToTopEmitter(onScrolledAlwaysNotifier)
    }

    val isScrolledToRight by lazy {
        @Suppress("DEPRECATION")
        view.createIsScrolledToRightEmitter(onScrolledAlwaysNotifier)
    }

    val isScrolledToBottom by lazy {
        @Suppress("DEPRECATION")
        view.createIsScrolledToBottomEmitter(onScrolledAlwaysNotifier)
    }

}

fun View.createScrolledStateEmitters() =
    ViewScrolledStateEmitters(this)


@Deprecated(
    message = "Use createRecyclerViewScrolledStateEmitters() instead",
    level = DeprecationLevel.ERROR,
    replaceWith = ReplaceWith(
        "this.createRecyclerScrolledStateEmitters()",
        "org.hnau.android.base.extensions.view.scroll.recycle.createRecyclerScrolledStateEmitters"
    )
)
fun RecyclerView.createScrolledStateEmitters() =
    ViewScrolledStateEmitters(this)