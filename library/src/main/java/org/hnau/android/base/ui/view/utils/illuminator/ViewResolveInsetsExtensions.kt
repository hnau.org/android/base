package org.hnau.android.base.ui.view.utils.illuminator

import android.view.View
import org.hnau.android.base.data.insets.Insets
import org.hnau.android.base.data.insets.combine
import org.hnau.android.base.data.insets.typed.uniqueInsets
import org.hnau.base.extensions.checkNullable
import org.hnau.emitter.Emitter
import org.hnau.emitter.extensions.callIfTrue
import org.hnau.emitter.extensions.map
import org.hnau.emitter.observing.push.lateinit.collapse


private val View.parentCustomInsetsProvider: CustomInsetsProvider?
    get() = parent.let { parent ->
        when (parent) {
            is CustomInsetsProvider -> parent
            is View -> parent.parentCustomInsetsProvider
            else -> null
        }
    }

private fun View.systemWindowToContentInsets(
        isActive: Emitter<Boolean>
) = InsetsFromWindowToSystemInsetsEmitter(
        view = this,
        isActive = isActive
)

fun View.resolveInsets(
        isActive: Emitter<Boolean>,
        onLayout: Emitter<Unit>
) = isActive.callIfTrue()
        .map {
            parentCustomInsetsProvider.checkNullable(
                    ifNotNull = CustomInsetsProvider::windowToContentInsets,
                    ifNull = { systemWindowToContentInsets(isActive) }
            )
        }
        .collapse()
        .let { windowToContentInsets ->
            Insets.combine(
                    first = windowToContentInsets,
                    second = InsetsFromWindowToViewBoundsEmitter(
                            view = this,
                            window = ViewWindowEmitter(
                                    view = this,
                                    isActive = isActive,
                            ),
                            onLayout = onLayout
                    )
            ) { windowToContent, windowToBounds ->
                (windowToContent - windowToBounds).coerceAtLeast(0)
            }
        }
        .uniqueInsets()