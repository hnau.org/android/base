package org.hnau.android.base.ui.view.recycler.adapter.item

import android.content.Context
import android.view.View
import org.hnau.emitter.Emitter


interface ItemViewProvider<T> {

    companion object;

    fun createItemView(
        context: Context,
        content: Emitter<T>,
        itemViewType: Int
    ): View

    fun getItemViewType(item: T) = 0

}