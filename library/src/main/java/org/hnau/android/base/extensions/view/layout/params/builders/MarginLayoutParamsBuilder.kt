package org.hnau.android.base.extensions.view.layout.params.builders

import android.content.Context
import android.view.ViewGroup
import org.hnau.android.base.data.getter.px.Px
import org.hnau.android.base.data.getter.px.pxInt
import org.hnau.base.exception.property.NoGetterException


open class MarginLayoutParamsBuilder(
        context: Context
) : LayoutParamsBuilder(
        context = context
) {

    var marginLeft = Px.zero
    var marginTop = Px.zero
    var marginRight = Px.zero
    var marginBottom = Px.zero

    var marginsHorizontal: Px
        @Deprecated(level = DeprecationLevel.HIDDEN, message = NoGetterException.message)
        get() = NoGetterException.doThrow()
        set(value) {
            marginLeft = value
            marginRight = value
        }

    var marginsVertical: Px
        @Deprecated(level = DeprecationLevel.HIDDEN, message = NoGetterException.message)
        get() = NoGetterException.doThrow()
        set(value) {
            marginTop = value
            marginBottom = value
        }

    var margins: Px
        @Deprecated(level = DeprecationLevel.HIDDEN, message = NoGetterException.message)
        get() = NoGetterException.doThrow()
        set(value) {
            marginsHorizontal = value
            marginsVertical = value
        }

    protected fun ViewGroup.MarginLayoutParams.applyMargins() = apply {
        setMargins(
                marginLeft.pxInt(context),
                marginTop.pxInt(context),
                marginRight.pxInt(context),
                marginBottom.pxInt(context)
        )
    }

    override fun build() =
            ViewGroup.MarginLayoutParams(widthValue, heightValue).applyMargins()

}