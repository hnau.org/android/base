package org.hnau.android.base.ui.view.utils.state.utils

import android.view.View
import org.hnau.base.extensions.setUnique
import org.hnau.emitter.observing.push.always.content.ContentEmitter


class ViewIsAttachedToWindowEmitter(
    private val view: View
) : ContentEmitter<Boolean>(
    initialValue = false
) {

    private val viewIsAttachedToWindowListener =
        object : View.OnAttachStateChangeListener {

            override fun onViewAttachedToWindow(v: View?)  =
                onAttachedStateChanged(true)

            override fun onViewDetachedFromWindow(v: View?) =
                onAttachedStateChanged(false)

        }

    init {
        view.addOnAttachStateChangeListener(viewIsAttachedToWindowListener)
    }

    private fun onAttachedStateChanged(isAttached: Boolean) {
        setUnique(value, isAttached) { value = it }
    }

}