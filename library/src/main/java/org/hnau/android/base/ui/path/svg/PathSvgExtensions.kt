package org.hnau.android.base.ui.path.svg

import android.graphics.Path


inline fun Path.svg(
        build: SvgCommandsReceiver.() -> Unit
) = object : SvgCommandsReceiver {

    override fun l(
            x: Number, y: Number
    ) = lineTo(
            x.toFloat(), y.toFloat()
    )

    override fun m(
            x: Number, y: Number
    ) = moveTo(
            x.toFloat(), y.toFloat()
    )

    override fun q(
            x1: Number, y1: Number,
            x: Number, y: Number
    ) = quadTo(
            x1.toFloat(), y1.toFloat(),
            x.toFloat(), y.toFloat()
    )

    override fun c(
            x1: Number, y1: Number,
            x2: Number, y2: Number,
            x: Number, y: Number
    ) = cubicTo(
            x1.toFloat(), y1.toFloat(),
            x2.toFloat(), y2.toFloat(),
            x.toFloat(), y.toFloat()
    )

}.build()

inline fun SvgPath(build: SvgCommandsReceiver.() -> Unit) =
        Path().apply { svg(build) }