package org.hnau.android.base.ui.view.header

import android.content.Context
import android.graphics.Color
import org.hnau.android.base.data.Side
import org.hnau.android.base.data.getter.picture.Picture
import org.hnau.android.base.data.getter.text.Text
import org.hnau.android.base.data.getter.tone.Tone
import org.hnau.android.base.data.insets.Insets
import org.hnau.android.base.extensions.color.shiftTo
import org.hnau.android.base.ui.animation.smooth
import org.hnau.android.base.ui.lightness.values.LightnessValues
import org.hnau.android.base.ui.rawcolor.RawColor
import org.hnau.android.base.ui.rawcolor.combine
import org.hnau.android.base.ui.theme.*
import org.hnau.android.base.ui.theme.drawable.ThemeGradientDrawable
import org.hnau.android.base.ui.view.utils.illuminator.ApplyInsetsContainer
import org.hnau.android.base.ui.view.utils.illuminator.ApplySelfInsetsContainer
import org.hnau.android.base.ui.view.utils.state.createStateEmitters
import org.hnau.base.extensions.elvis
import org.hnau.emitter.Emitter
import org.hnau.emitter.extensions.map
import org.hnau.emitter.extensions.unique
import org.hnau.emitter.extensions.useWhen


class HeaderView(
        context: Context,
        background: Emitter<ThemeGradient>,
        title: Emitter<Pair<Text, Side>>,
        foreground: Emitter<RawColor>,
        leftAction: Emitter<Triple<(RawColor) -> Picture, Text, () -> Unit>?>,
        rightAction: Emitter<Triple<(RawColor) -> Picture, Text, () -> Unit>?>
) : ApplySelfInsetsContainer(
        content = HeaderViewContent(
                context = context,
                title = title,
                foreground = foreground,
                leftAction = leftAction,
                rightAction = rightAction
        )
) {

    init {
        ThemeGradientDrawable(
                gradient = background
                        .unique(ThemeGradient.comparator)
                        .run {
                            MutableThemeGradient().let { cache ->
                                smooth(
                                        copy = ThemeGradient::copy,
                                        calcSmoothedValue = { from, to, percentage ->
                                            cache.apply {
                                                start = RawColor.combine(
                                                        first = from.start,
                                                        second = to.start
                                                ) { fromValue, toValue ->
                                                    fromValue.shiftTo(toValue, percentage)
                                                }
                                                end = RawColor.combine(
                                                        first = from.end,
                                                        second = to.end
                                                ) { fromValue, toValue ->
                                                    fromValue.shiftTo(toValue, percentage)
                                                }
                                            }
                                        }
                                )
                            }
                        }
        )
                .useWhen(state.isActive)
                .observe { backgroundDrawable ->
                    this.background = backgroundDrawable
                    invalidate()
                }


    }


}