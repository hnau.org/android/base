package org.hnau.android.base.ui.view.layers.view.lifecycles.decoration

import android.content.Context
import android.graphics.Canvas
import android.graphics.Rect
import org.hnau.android.base.ui.animation.emergence.offset.EmergenceDirection


class InvisibleLayersStackDecoration : LayersStackDecoration {

    override fun drawLayersStackDecoration(
        context: Context,
        canvas: Canvas,
        direction: EmergenceDirection,
        offsetAmount: Float,
        rect: Rect
    ) {
    }

}

fun LayersStackDecoration.Companion.invisible() = InvisibleLayersStackDecoration()