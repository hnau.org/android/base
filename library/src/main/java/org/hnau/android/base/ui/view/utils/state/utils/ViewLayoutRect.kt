package org.hnau.android.base.ui.view.utils.state.utils

import android.graphics.Rect
import android.view.View
import org.hnau.emitter.Emitter
import org.hnau.emitter.extensions.listen
import org.hnau.emitter.observing.push.always.content.ContentEmitter


class ViewLayoutRect(
        private val view: View,
        onLayout: Emitter<Unit>
) : ContentEmitter<Rect?>(null) {

    private val existenceValue = Rect()

    private fun actualize() {
        if (
                existenceValue.left != view.left ||
                existenceValue.top != view.top ||
                existenceValue.right != view.right ||
                existenceValue.bottom != view.bottom
        ) {
            value = existenceValue.apply {
                set(
                        view.left,
                        view.top,
                        view.right,
                        view.bottom
                )
            }
        }
    }

    init {
        onLayout.listen(::actualize)
        actualize()
    }

}