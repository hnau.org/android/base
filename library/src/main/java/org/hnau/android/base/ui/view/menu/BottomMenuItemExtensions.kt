package org.hnau.android.base.ui.view.menu

import org.hnau.emitter.extensions.map
import org.hnau.emitter.extensions.unique
import org.hnau.emitter.utils.mutable.MutableEmitter
import org.hnau.emitter.utils.mutable.map


fun <K> BottomMenuItem.Companion.switchable(
        designs: Iterable<Pair<K, BottomMenuItem.Design>>,
        activeItem: MutableEmitter<K>
) = designs.map { (key, design) ->
    BottomMenuItem(
            design = design,
            behavior = BottomMenuItem.Behavior(
                    state = activeItem.emitter.map { it == key }.unique().map(BottomMenuItem.State.Companion::get),
                    onClick = { activeItem.setValue(key) }
            )
    )
}