package org.hnau.android.base.ui.lightness.values


fun <T> LightnessValues(
        light: T,
        dark: T
) = object : LightnessValues<T> {
    override val light = light
    override val dark = dark
}