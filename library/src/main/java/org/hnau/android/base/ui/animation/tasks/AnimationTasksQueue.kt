package org.hnau.android.base.ui.animation.tasks

import java.util.*
import java.util.concurrent.ConcurrentLinkedQueue


class AnimationTasksQueue<T : AnimationTask>(
    initialTask: T
) {

    var task = initialTask
        private set

    private val queue: Queue<T> = ConcurrentLinkedQueue<T>()

    fun addTask(task: T) {
        queue.offer(task)
    }

    fun switchToNextTask() = synchronized<Boolean>(queue) {
        val nextTask = queue.poll() ?: return false
        task = nextTask
        return true
    }

    fun switchToLastTask() {
        @Suppress("ControlFlowWithEmptyBody")
        while (switchToNextTask()) {
        }
    }

}