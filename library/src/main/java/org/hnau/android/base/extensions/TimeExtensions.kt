package org.hnau.android.base.extensions

import android.os.SystemClock
import org.hnau.base.data.time.Time


fun Time.Companion.elapsedRealtime() =
    Time(SystemClock.elapsedRealtime())