package org.hnau.android.base.ui.view.waiter.material.params

import org.hnau.android.base.data.getter.tone.Tone
import org.hnau.android.base.data.getter.tone.Tones
import org.hnau.android.base.data.getter.tone.fromColor
import org.hnau.android.base.data.getter.tone.replaceAlpha
import org.hnau.android.base.ui.color.material.MaterialColor


data class MaterialWaiterColor(
        val foreground: Tone = Tones.fromColor(MaterialColor.blue.v700),
        val background: Tone = Tones.white.replaceAlpha(0.5f)
) {

    companion object;

}