package org.hnau.android.base.extensions.view.linear

import android.annotation.SuppressLint
import android.content.Context
import android.view.ViewGroup
import android.widget.LinearLayout
import org.hnau.android.base.data.course.Course
import org.hnau.android.base.extensions.view.group.children.view
import org.hnau.android.base.extensions.view.group.linear.course


@SuppressLint("ViewConstructor")
open class ManualLinearLayout(
        context: Context,
        course: Course
) : LinearLayout(
        context
) {

    init {
        this.course = course
    }

}

inline fun ViewGroup.linearLayout(
        course: Course,
        childConfigurator: ManualLinearLayout.() -> Unit = {}
) = view(
        view = ManualLinearLayout(
                context = context,
                course = course
        ),
        childConfigurator = childConfigurator
)