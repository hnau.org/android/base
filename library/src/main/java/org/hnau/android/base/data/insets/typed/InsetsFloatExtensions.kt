package org.hnau.android.base.data.insets.typed

import org.hnau.android.base.data.edges.Edges
import org.hnau.android.base.data.insets.Insets
import org.hnau.android.base.data.insets.applyInsets
import org.hnau.android.base.data.insets.horizontalSum
import org.hnau.android.base.data.insets.uniqueInsets
import org.hnau.android.base.data.insets.verticalSum
import org.hnau.emitter.Emitter


val Insets<Float>.horizontalSum
    get() = horizontalSum(Float::plus)

val Insets<Float>.verticalSum
    get() = verticalSum(Float::plus)

fun Edges<Float>.applyInsets(
        insets: Insets<Float>
) = applyInsets(
        insets = insets,
        plus = Float::plus,
        minus = Float::minus
)

fun Emitter<Edges<Float>>.applyInsets(
        insets: Insets<Float>
) = applyInsets(
        insets = insets,
        plus = Float::plus,
        minus = Float::minus
)

fun Edges<Float>.applyInsets(
        insets: Emitter<Insets<Float>>
) = applyInsets(
        insets = insets,
        plus = Float::plus,
        minus = Float::minus
)

fun Emitter<Edges<Float>>.applyInsets(
        insets: Emitter<Insets<Float>>
) = applyInsets(
        insets = insets,
        plus = Float::plus,
        minus = Float::minus
)

fun Emitter<Insets<Float>>.uniqueInsets() =
        uniqueInsets { first, second -> first == second }