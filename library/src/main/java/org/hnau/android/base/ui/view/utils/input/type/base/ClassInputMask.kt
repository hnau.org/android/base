package org.hnau.android.base.ui.view.utils.input.type.base

import org.hnau.base.extensions.number.useFlag


abstract class ClassInputMask(
    private val classMask: Int
) : InputMask {

    var variationMask = 0

    var flags = 0

    override val mask: Int
        get() = classMask + variationMask + flags
}

fun ClassInputMask.setFlag(flag: Int, add: Boolean) {
    flags = flags.useFlag(flag, add)
}

fun ClassInputMask.setVariation(variationMask: Int) {
    this.variationMask = variationMask
}