package org.hnau.android.base.ui.view.input.tone

import org.hnau.android.base.data.getter.tone.Tones
import org.hnau.base.extensions.boolean.checkBoolean


data class InputViewStateTones(
        val normal: InputViewTones = InputViewTones(Tones.black),
        val focused: InputViewTones = InputViewTones(Tones.blue)
) {

    companion object

    fun choose(
            focused: Boolean
    ) = focused.checkBoolean(
            ifTrue = { this.focused },
            ifFalse = { this.normal }
    )

}