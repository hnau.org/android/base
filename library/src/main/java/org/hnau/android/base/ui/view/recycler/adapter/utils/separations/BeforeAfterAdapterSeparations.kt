package org.hnau.android.base.ui.view.recycler.adapter.utils.separations

import android.content.Context
import androidx.recyclerview.widget.ListUpdateCallback
import org.hnau.android.base.data.getter.px.Px
import org.hnau.android.base.ui.view.recycler.adapter.AdapterInfo
import org.hnau.android.base.ui.view.recycler.adapter.content.AdapterContent
import org.hnau.android.base.ui.view.recycler.adapter.item.ItemViewProvider
import org.hnau.android.base.ui.view.recycler.adapter.item.various.ViewFactory
import org.hnau.android.base.ui.view.recycler.adapter.utils.offset
import org.hnau.emitter.Emitter
import org.hnau.emitter.extensions.map
import org.hnau.emitter.observing.push.lateinit.collapse


sealed class BeforeAfterSeparationsListItem<T> {

    class Separation<T>(
        val separation: Emitter<Int>
    ) : BeforeAfterSeparationsListItem<T>()

    class Value<T>(
        val value: T
    ) : BeforeAfterSeparationsListItem<T>()

}

fun <T> AdapterInfo<T>.addBeforeAfterSeparations(
    separatorItemType: Int,
    before: Emitter<Int>,
    after: Emitter<Int>,
    separatorItemViewFactory: ViewFactory<Int> = RecyclerViewSeparatorItemView
) = AdapterInfo<BeforeAfterSeparationsListItem<T>>(
    content = { listUpdateCallback ->
        object : AdapterContent<BeforeAfterSeparationsListItem<T>> {

            private val itemsCache = HashMap<Int, BeforeAfterSeparationsListItem<T>>()

            private val wrappedListUpdateCallback = object : ListUpdateCallback {

                override fun onChanged(position: Int, count: Int, payload: Any?) {
                    repeat(count) { i -> itemsCache.remove(position + i) }
                    listUpdateCallback.onChanged(position, count, payload)
                }

                override fun onMoved(fromPosition: Int, toPosition: Int) {
                    itemsCache.remove(fromPosition)?.let { movedItem ->
                        itemsCache[toPosition] = movedItem
                    }
                    listUpdateCallback.onMoved(fromPosition, toPosition)
                }

                override fun onInserted(position: Int, count: Int) {
                    listUpdateCallback.onInserted(position, count)
                }

                override fun onRemoved(position: Int, count: Int) {
                    itemsCache.remove(position)
                    listUpdateCallback.onRemoved(position, count)
                }

            }

            private val wrappedContent =
                this@addBeforeAfterSeparations.content(wrappedListUpdateCallback.offset(1))

            private var cachedSize = 0

            override val size
                get() = wrappedContent.size.plus(2).also { cachedSize = it }

            override fun getItem(position: Int): BeforeAfterSeparationsListItem<T> {
                var result = itemsCache[position]
                if (result == null) {
                    result = createItem(position)
                    itemsCache[position] = result
                }
                return result
            }

            private fun createItem(
                position: Int
            ) = when (position) {
                0 -> BeforeAfterSeparationsListItem.Separation<T>(before)
                cachedSize - 1 -> BeforeAfterSeparationsListItem.Separation<T>(after)
                else -> BeforeAfterSeparationsListItem.Value(wrappedContent.getItem(position - 1))
            }

        }
    },
    viewProvider = object : ItemViewProvider<BeforeAfterSeparationsListItem<T>> {

        private val wrappedViewProvider = this@addBeforeAfterSeparations.viewProvider

        @Suppress("UNCHECKED_CAST")
        override fun createItemView(
            context: Context,
            content: Emitter<BeforeAfterSeparationsListItem<T>>,
            itemViewType: Int
        ) = when (itemViewType) {
            separatorItemType -> separatorItemViewFactory.createView(
                context,
                (content as Emitter<BeforeAfterSeparationsListItem.Separation<T>>)
                    .map(BeforeAfterSeparationsListItem.Separation<T>::separation)
                    .collapse()
            )
            else -> wrappedViewProvider.createItemView(
                context = context,
                content = (content as Emitter<BeforeAfterSeparationsListItem.Value<T>>)
                    .map(BeforeAfterSeparationsListItem.Value<T>::value),
                itemViewType = itemViewType
            )
        }

        override fun getItemViewType(item: BeforeAfterSeparationsListItem<T>) = when (item) {
            is BeforeAfterSeparationsListItem.Separation -> separatorItemType
            is BeforeAfterSeparationsListItem.Value -> wrappedViewProvider.getItemViewType(item.value)
        }

    }
)