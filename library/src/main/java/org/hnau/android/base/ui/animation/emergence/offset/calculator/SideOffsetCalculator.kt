package org.hnau.android.base.ui.animation.emergence.offset.calculator

import org.hnau.android.base.data.Side
import org.hnau.android.base.ui.animation.emergence.offset.Offset


class SideOffsetCalculator(
    private val fromSide: Side
) : OffsetCalculator {

    override fun calcOffset(
        visibilityPercentage: Float,
        result: Offset
    ) {
        val offsetPercentage = 1 - visibilityPercentage
        result.set(
            dx = fromSide.singleOffset.x * offsetPercentage,
            dy = fromSide.singleOffset.y * offsetPercentage
        )
    }

}

fun OffsetCalculator.Companion.side(
    fromSide: Side = Side.right
) =
    SideOffsetCalculator(
        fromSide = fromSide
    )