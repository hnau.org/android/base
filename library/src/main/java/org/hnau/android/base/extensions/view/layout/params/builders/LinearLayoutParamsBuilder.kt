package org.hnau.android.base.extensions.view.layout.params.builders

import android.content.Context
import android.widget.LinearLayout
import org.hnau.android.base.data.direction.gravity


class LinearLayoutParamsBuilder(
        context: Context
) : MarginGravityLayoutParamsBuilder(
        context
) {

    var weight: Number = 0f

    override fun build() =
            LinearLayout.LayoutParams(widthValue, heightValue, weight.toFloat())
                    .apply {
                        applyMargins()
                        gravity = layoutParamsDirection.gravity
                    }

}