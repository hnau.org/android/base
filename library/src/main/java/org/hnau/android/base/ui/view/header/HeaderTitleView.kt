package org.hnau.android.base.ui.view.header

import android.content.Context
import android.widget.TextView
import org.hnau.android.base.data.Side
import org.hnau.android.base.data.direction.Direction
import org.hnau.android.base.data.getter.px.Px
import org.hnau.android.base.data.getter.px.constants
import org.hnau.android.base.data.getter.text.Text
import org.hnau.android.base.data.getter.tone.Tones
import org.hnau.android.base.ui.animation.emergence.EmergenceInfo
import org.hnau.android.base.ui.animation.emergence.offset.EmergencesInfo
import org.hnau.android.base.ui.animation.emergence.offset.calculator.OffsetCalculator
import org.hnau.android.base.ui.animation.emergence.offset.calculator.side
import org.hnau.android.base.ui.rawcolor.RawColor
import org.hnau.android.base.ui.view.AppTextView
import org.hnau.android.base.ui.view.presenter.Presenter
import org.hnau.android.base.ui.view.presenter.create
import org.hnau.android.base.ui.view.presenter.info.PresenterInfo
import org.hnau.android.base.ui.view.presenter.info.size.presenter.base.PresenterSizeCalculator
import org.hnau.android.base.ui.view.presenter.info.size.presenter.matchParent
import org.hnau.android.base.ui.view.presenter.transaction.Transaction
import org.hnau.android.base.ui.view.presenter.view.wrapper.existence.PresentingInfo
import org.hnau.android.base.ui.view.utils.state.createStateEmitters
import org.hnau.emitter.Emitter
import org.hnau.emitter.extensions.useWhen


class HeaderTitleView(
        context: Context,
        foreground: Emitter<RawColor>,
        title: Text
) : AppTextView(
        context = context,
        textColor = Tones.transparent,
        direction = Direction.center,
        textSize = Px.constants.text.sp16,
        text = title,
        singleLine = true
) {

    private val state = createStateEmitters()

    init {
        foreground
                .useWhen(state.isActive)
                .observe { foregroundColor ->
                    setTextColor(foregroundColor.value)
                }
    }

}

fun HeaderTitleViewPresenter(
        context: Context,
        foreground: Emitter<RawColor>,
        title: Emitter<Pair<Text, Side>>
) = Presenter.create(
        context = context,
        info = PresenterInfo(
                width = PresenterSizeCalculator.matchParent(),
                height = PresenterSizeCalculator.matchParent()
        ),
        values = title,
        createPresentValueTransaction = { (title, fromSide) ->
            Transaction(
                    view = HeaderTitleView(
                            context = context,
                            foreground = foreground,
                            title = title
                    ),
                    presentingInfo = PresentingInfo(
                            emergencesInfo = EmergencesInfo(
                                    show = EmergenceInfo(
                                            offsetCalculator = OffsetCalculator.side(fromSide)
                                    )
                            )
                    )
            )
        }
)