package org.hnau.android.base.ui.view.waiter.material.params

import android.graphics.Paint
import org.hnau.android.base.data.getter.px.Px
import org.hnau.android.base.data.getter.px.dp
import org.hnau.android.base.data.getter.px.plus
import org.hnau.android.base.data.getter.px.px
import org.hnau.android.base.data.getter.px.times


data class MaterialWaiterSize(
    val radius: Px,
    val lineWidthByRadius: Float = 0.2f,
    val lineCap: Paint.Cap = Paint.Cap.BUTT
) {

    val lineWidth = radius * lineWidthByRadius

    val preferredSize = radius * 2 + lineWidth + additionalSizeToPreferred

    companion object {

        private val additionalSizeToPreferred = 2.px

        val small = MaterialWaiterSize(radius = 12.dp)
        val medium = MaterialWaiterSize(radius = 16.dp)
        val large = MaterialWaiterSize(radius = 24.dp)

        val default = large

    }

}