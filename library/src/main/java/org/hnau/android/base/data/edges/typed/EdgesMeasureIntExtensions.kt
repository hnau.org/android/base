package org.hnau.android.base.data.edges.typed

import org.hnau.android.base.data.edges.Edges
import org.hnau.android.base.data.edges.centerX
import org.hnau.android.base.data.edges.centerY
import org.hnau.android.base.data.edges.height
import org.hnau.android.base.data.edges.width


val Edges<Int>.width
    get() = width(Int::minus)

val Edges<Int>.height
    get() = height(Int::minus)

val Edges<Int>.centerX
    get() = centerX(Int::plus) { it / 2 }

val Edges<Int>.centerY
    get() = centerY(Int::plus) { it / 2 }