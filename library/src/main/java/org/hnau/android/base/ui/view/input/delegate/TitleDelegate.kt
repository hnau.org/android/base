package org.hnau.android.base.ui.view.input.delegate

import android.content.Context
import android.graphics.Canvas
import android.graphics.Path
import android.graphics.PointF
import android.graphics.RectF
import org.hnau.android.base.data.edges.Edges
import org.hnau.android.base.data.getter.font.Font
import org.hnau.android.base.data.getter.text.Text
import org.hnau.android.base.data.getter.tone.Tone
import org.hnau.android.base.data.insets.Insets
import org.hnau.android.base.extensions.color.alpha
import org.hnau.android.base.extensions.color.clearAlpha
import org.hnau.android.base.extensions.height
import org.hnau.android.base.extensions.inState
import org.hnau.android.base.extensions.paint
import org.hnau.android.base.ui.animation.curver.interpolateAccelerateDecelerate
import org.hnau.android.base.ui.animation.smoothFloat
import org.hnau.android.base.ui.view.ViewScroll
import org.hnau.android.base.ui.view.input.info.InputViewTitleInfo
import org.hnau.android.base.ui.view.scroll
import org.hnau.base.extensions.number.asPercentageInter
import org.hnau.emitter.Emitter
import org.hnau.emitter.extensions.or
import org.hnau.emitter.extensions.toFloat
import org.hnau.emitter.extensions.useWhen
import org.hnau.emitter.observing.push.lateinit.combine


class TitleDelegate(
        context: Context,
        titleText: Text,
        font: Font,
        tone: Emitter<Tone>,
        bounds: Emitter<Edges<Int>>,
        getInsets: () -> Insets<Float>,
        getTextSize: () -> Float,
        isFocused: Emitter<Boolean>,
        textIsNotEmpty: Emitter<Boolean>,
        info: InputViewTitleInfo,
        private val invalidate: () -> Unit,
        isActive: Emitter<Boolean>,
        isForeground: Emitter<Boolean>,
        private val viewScroll: ViewScroll
) {

    private val title = titleText(context)

    private data class LayoutInfo(
            var toTopPercentage: Float = 0f,
            val inputBounds: RectF = RectF()
    )

    private val topTextSize = info.topTextSize(context)

    private val paint = paint {
        textSize = topTextSize
        typeface = font(context)
    }

    private var paintMaxAlpha = 1f
        set(value) {
            field = value
            updatePaintAlpha()
        }

    private var paintAlpha = 0f
        set(value) {
            field = value
            updatePaintAlpha()
        }

    private fun updatePaintAlpha() {
        paint.alpha = (paintMaxAlpha * paintAlpha * 255).toInt()
    }

    val topHeight =
            paint.fontMetrics.height

    val topWidth =
            paint.measureText(title)


    val toTopPercentageSafe =
            (isFocused or textIsNotEmpty)
                    .toFloat()
                    .smoothFloat(info.toTopAnimationTime)
                    .interpolateAccelerateDecelerate()
                    .useWhen(isForeground)

    private val drawClipPath = Path()
    private val drawPosition = PointF()

    init {
        tone
                .useWhen(isActive)
                .observe { titleTone ->
                    val color = titleTone(context)
                    paint.color = color.clearAlpha()
                    paintMaxAlpha = color.alpha / 255f
                    invalidate()
                }

        @Suppress("DEPRECATION")
        LayoutInfo()
                .let { layoutInfo ->
                    Emitter.combine(
                            firstSource = toTopPercentageSafe,
                            secondSource = bounds
                    ) { toTopPercentage, boundsLocal ->
                        layoutInfo.apply {
                            this.toTopPercentage = toTopPercentage
                            val insets = getInsets()
                            inputBounds.set(
                                    boundsLocal.xMin + insets.left,
                                    boundsLocal.yMin + insets.top,
                                    boundsLocal.xMax + insets.right,
                                    boundsLocal.yMax + insets.bottom
                            )
                        }
                    }
                }
                .observe { (toTopPercentage, inputBounds) ->

                    paintAlpha = toTopPercentage.asPercentageInter(info.bottomAlpha, 1f)

                    paint.textSize = toTopPercentage.asPercentageInter(
                            from = getTextSize(),
                            to = topTextSize
                    )

                    drawPosition.set(
                            inputBounds.left,
                            toTopPercentage.asPercentageInter(
                                    from = (inputBounds.top + inputBounds.bottom) / 2,
                                    to = inputBounds.top - topHeight / 2
                            ) + paint.fontMetrics.let { it.height / 2 - it.descent }
                    )

                    drawClipPath.reset()
                    drawClipPath.addRect(
                            inputBounds.left,
                            drawPosition.y + paint.fontMetrics.ascent,
                            inputBounds.right,
                            drawPosition.y + paint.fontMetrics.descent,
                            Path.Direction.CW
                    )

                    invalidate()

                }
    }

    fun draw(
            canvas: Canvas
    ) = canvas.inState {
        scroll(viewScroll)
        clipPath(drawClipPath)
        drawText(title, drawPosition.x, drawPosition.y, paint)
    }

}