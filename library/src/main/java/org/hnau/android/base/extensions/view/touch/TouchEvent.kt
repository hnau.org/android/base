package org.hnau.android.base.extensions.view.touch

import android.view.MotionEvent


class TouchEvent {

    var type = TouchEventType.cancel
        private set

    var x = 0f
        private set

    var y = 0f
        private set

    fun update(
        event: MotionEvent
    ) {
        x = event.x
        y = event.y
        type = when (event.action and MotionEvent.ACTION_MASK) {
            MotionEvent.ACTION_DOWN -> TouchEventType.down
            MotionEvent.ACTION_MOVE -> TouchEventType.move
            MotionEvent.ACTION_UP -> TouchEventType.up
            else -> TouchEventType.cancel
        }
    }

}