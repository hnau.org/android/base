package org.hnau.android.base.ui.drawable.partout.calculators.proportional

import android.graphics.Point
import org.hnau.android.base.data.direction.Direction
import org.hnau.android.base.ui.drawable.partout.calculators.DirectionPartoutCalculator
import org.hnau.android.base.ui.drawable.partout.calculators.NoResizeWrapperSizeCalculator


abstract class ProportionalPartoutCalculator(
    direction: Direction
) : DirectionPartoutCalculator(
    direction = direction
), NoResizeWrapperSizeCalculator {

    override fun calcContentSize(
        parentWidth: Int,
        parentHeight: Int,
        contentPreferredWidth: Int,
        contentPreferredHeight: Int,
        result: Point
    ) {
        val scaleFactor = calcScaleFactor(parentWidth, parentHeight, contentPreferredWidth, contentPreferredHeight)
        result.set(
            (contentPreferredWidth * scaleFactor).toInt(),
            (contentPreferredHeight * scaleFactor).toInt()
        )
    }

    protected abstract fun calcScaleFactor(
        parentWidth: Int,
        parentHeight: Int,
        contentPreferredWidth: Int,
        contentPreferredHeight: Int
    ): Float

}