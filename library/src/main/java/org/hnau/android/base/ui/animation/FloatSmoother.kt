package org.hnau.android.base.ui.animation

import org.hnau.base.data.time.Time
import org.hnau.base.extensions.boolean.checkBoolean
import org.hnau.base.extensions.boolean.ifFalse
import org.hnau.base.extensions.boolean.ifTrue
import org.hnau.base.extensions.number.asPercentageInter
import org.hnau.base.data.time.asMilliseconds
import org.hnau.base.data.time.now
import org.hnau.emitter.Emitter
import org.hnau.emitter.extensions.listen
import org.hnau.emitter.extensions.useWhen
import org.hnau.emitter.observing.push.always.AlwaysEmitter
import org.hnau.emitter.observing.push.always.content.ManualEmitter
import kotlin.math.abs


class FloatSmoother(
        source: Emitter<Float>,
        private val interStatesAnimationTimeMilliseconds: Double = interStatesAnimatingTime.milliseconds.toDouble()
) : AlwaysEmitter<Float>() {

    companion object {

        val interStatesAnimatingTime = Time.animation.default

    }

    constructor(
            source: Emitter<Float>,
            interStatesAnimationTime: Time = interStatesAnimatingTime
    ) : this(
            source = source,
            interStatesAnimationTimeMilliseconds = interStatesAnimationTime.milliseconds.toDouble()
    )

    private var lastTarget: Float? = null
    private var target = 0f
    private var targetTimestamp = Time.zero

    private val isObserving = ManualEmitter(false)
    private var isAnimationsAvailable = false

    private val animator = AnimationTicker()

    override val value get() = calcValue()

    init {
        source
                .useWhen(isObserving)
                .observe { newTarget ->
                    (target == newTarget).ifTrue { return@observe }
                    lastTarget = value
                    target = newTarget
                    targetTimestamp = Time.now()
                    isAnimationsAvailable.checkBoolean(
                            ifTrue = { animator.start() },
                            ifFalse = { onChanged() }
                    )

                }
        animator.listen(::onChanged)
    }

    private fun stop() {
        animator.stop()
        lastTarget = null
    }

    private fun calcValue(): Float {

        val lastTarget = lastTarget
        if (lastTarget == null) {
            stop()
            return target
        }

        val duration = calcDuration(lastTarget)
        if (duration <= Time.zero) {
            stop()
            return target
        }

        val time = Time.now() - targetTimestamp
        val percentage = (time / duration).toFloat()
        if (percentage >= 1) {
            stop()
            return target
        }

        return percentage.asPercentageInter(lastTarget, target)
    }

    private fun calcDuration(
            lastTarget: Float
    ): Time {
        isAnimationsAvailable.ifFalse { return Time.zero }
        val delta = abs(target - lastTarget)
        val milliseconds = delta * interStatesAnimationTimeMilliseconds
        return milliseconds.asMilliseconds
    }

    override fun beforeFirstAttached() {
        super.beforeFirstAttached()
        isObserving.value = true
        isAnimationsAvailable = true
    }

    override fun afterLastDetached() {
        super.afterLastDetached()
        isAnimationsAvailable = false
        isObserving.value = false
        stop()
    }

}

fun Emitter<Float>.smoothFloat(
        interStatesAnimatingMilliseconds: Double
): Emitter<Float> = FloatSmoother(
        source = this,
        interStatesAnimationTimeMilliseconds = interStatesAnimatingMilliseconds
)

fun Emitter<Float>.smoothFloat(
        interStatesAnimatingTime: Time = FloatSmoother.interStatesAnimatingTime
): Emitter<Float> = FloatSmoother(
        source = this,
        interStatesAnimationTime = interStatesAnimatingTime
)