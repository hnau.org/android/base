package org.hnau.android.base.ui.decoration

import android.graphics.Canvas


typealias ViewDecoration = (Canvas) -> Unit

object ViewDecorations