package org.hnau.android.base.extensions.view.base

import android.view.View
import org.hnau.base.extensions.boolean.checkBoolean

enum class VisibilityType(val code: Int) {

    visible(View.VISIBLE),
    invisible(View.INVISIBLE),
    gone(View.GONE);

    companion object {

        fun getByCode(code: Int) = values()
            .find { it.code == code }
            ?: throw IllegalArgumentException("Visibility type with code $code not found")

    }
}

var View.visibilityType: VisibilityType
    set(value) {
        visibility = value.code
    }
    get() = VisibilityType.getByCode(visibility)


fun View.setVisible() {
    visibilityType = VisibilityType.visible
}

fun View.setInvisible() {
    visibilityType = VisibilityType.invisible
}

fun View.setGone() {
    visibilityType = VisibilityType.gone
}

fun View.setVisibleOrInvisible(visible: Boolean) =
    visible.checkBoolean(ifTrue = ::setVisible, ifFalse = ::setInvisible)

fun View.setVisibleOrGone(visible: Boolean) =
    visible.checkBoolean(ifTrue = ::setVisible, ifFalse = ::setGone)

fun View.setInvisibleOrGone(invisible: Boolean) =
    invisible.checkBoolean(ifTrue = ::setInvisible, ifFalse = ::setGone)

val View.isVisible: Boolean
    get() = visibilityType == VisibilityType.visible

val View.isInvisible: Boolean
    get() = visibilityType == VisibilityType.invisible

val View.isGone: Boolean
    get() = visibilityType == VisibilityType.gone

val View.isInvisibleOrGone: Boolean
    get() = !isVisible

val View.isVisibleOrGone: Boolean
    get() = !isInvisible

val View.isVisibleOrInvisible: Boolean
    get() = !isGone