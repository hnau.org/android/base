package org.hnau.android.base.validating

import android.text.InputFilter
import org.hnau.android.base.ui.view.utils.input.filter.FilterInputFilter
import org.hnau.base.data.maybe.tryOrError
import org.hnau.base.extensions.cast
import org.hnau.base.utils.validator.PairValidator
import org.hnau.base.utils.validator.Validator
import org.hnau.base.utils.validator.charsequence.CharsValidator
import org.hnau.base.utils.validator.size.MaxSizeValidator
import org.hnau.emitter.Emitter
import org.hnau.emitter.extensions.map


class EditTextValidatingDelegate(
        val text: Emitter<String>,
        validator: Validator<CharSequence>
) {

    companion object

    val filters = run {

        fun getFilters(
                validator: Validator<CharSequence>
        ): List<InputFilter> = when (validator) {
            is MaxSizeValidator -> listOf(InputFilter.LengthFilter(validator.maxSize))
            is CharsValidator -> listOf(FilterInputFilter(validator.checkChar))
            is PairValidator<*, *, *> -> listOf(validator.first, validator.second)
                    .cast<List<Validator<CharSequence>>>()
                    .map(::getFilters)
                    .flatten()
            else -> emptyList()
        }

        getFilters(validator)

    }

    val validatedText = text.map { text ->
        tryOrError { validator.validate(text); text }
    }

}