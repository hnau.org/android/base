package org.hnau.android.base.extensions.color

import androidx.annotation.FloatRange
import androidx.annotation.IntRange


object ColorUtils {

    private const val channelMaxValue = 255

    fun percentageToValue(
        @FloatRange(from = 0.0, to = 1.0) percentage: Float
    ) = (percentage * channelMaxValue).toInt()

    fun valueToPercentage(
        @IntRange(from = 0L, to = 255L) value: Int
    ) = value.toFloat() / channelMaxValue

}