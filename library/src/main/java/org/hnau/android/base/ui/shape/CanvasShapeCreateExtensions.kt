package org.hnau.android.base.ui.shape

import android.graphics.Canvas
import android.graphics.Paint

inline fun CanvasShape.Companion.create(
        crossinline draw: (canvas: Canvas, paint: Paint) -> Unit,
        crossinline clip: (canvas: Canvas) -> Unit
) = object : CanvasShape {

    override fun draw(canvas: Canvas, paint: Paint) =
            draw.invoke(canvas, paint)

    override fun clip(canvas: Canvas) =
            clip.invoke(canvas)

}


