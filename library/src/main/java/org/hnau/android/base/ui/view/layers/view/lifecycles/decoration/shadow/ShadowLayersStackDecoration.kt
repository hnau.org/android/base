package org.hnau.android.base.ui.view.layers.view.lifecycles.decoration.shadow

import android.content.Context
import android.graphics.Canvas
import android.graphics.Paint
import android.graphics.Rect
import org.hnau.android.base.data.getter.tone.Tone
import org.hnau.android.base.data.getter.tone.Tones
import org.hnau.android.base.data.getter.tone.replaceAlpha
import org.hnau.android.base.extensions.color.alpha
import org.hnau.android.base.extensions.paint
import org.hnau.android.base.ui.animation.emergence.offset.EmergenceDirection
import org.hnau.android.base.ui.view.layers.view.lifecycles.decoration.LayersStackDecoration
import org.hnau.base.data.possible.getOrInitialize
import org.hnau.base.data.possible.lateinit.Lateinit
import org.hnau.base.data.possible.lateinit.sync
import org.hnau.base.utils.safe.SafeContext
import org.hnau.base.utils.safe.unsafe


class ShadowLayersStackDecoration(
        private val shadowTone: Tone = Tones.black.replaceAlpha(0.75f)
) : LayersStackDecoration {

    private val shadowPaint = Lateinit.sync<Paint>(SafeContext.unsafe)
    private val shadowAlpha = Lateinit.sync<Int>(SafeContext.unsafe)

    override fun drawLayersStackDecoration(
            context: Context,
            canvas: Canvas,
            direction: EmergenceDirection,
            offsetAmount: Float,
            rect: Rect
    ) {
        val paint = shadowPaint.getOrInitialize { paint { color = shadowTone(context) } }
        val alpha = shadowAlpha.getOrInitialize { shadowTone(context).alpha }
        paint.alpha = (when (direction) {
            EmergenceDirection.show -> offsetAmount
            EmergenceDirection.hide -> 1 - offsetAmount
        } * alpha).toInt()
        canvas.drawRect(rect, paint)
    }

}

fun LayersStackDecoration.Companion.shadow(
        shadowTone: Tone = Tones.black.replaceAlpha(0.75f)
) = ShadowLayersStackDecoration(
        shadowTone = shadowTone
)