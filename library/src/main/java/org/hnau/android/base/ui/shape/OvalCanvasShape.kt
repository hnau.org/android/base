package org.hnau.android.base.ui.shape

import android.graphics.Canvas
import android.graphics.Paint
import android.graphics.Path
import org.hnau.android.base.data.edges.Edges
import org.hnau.emitter.Emitter


class OvalCanvasShape : BoundsCanvasShape() {

    private val path = Path()

    private val pathCanvasShape = PathCanvasShape.create { path }

    override var bounds: Edges<Int>
        get() = super.bounds
        set(value) {
            super.bounds = value
            updatePath()
        }

    private fun updatePath() {
        path.reset()
        path.addOval(boundsRect, Path.Direction.CW)
    }

    override fun draw(canvas: Canvas, paint: Paint) =
            pathCanvasShape.draw(canvas, paint)

    override fun clip(canvas: Canvas) =
            pathCanvasShape.clip(canvas)

}

fun CanvasShape.Companion.oval() = OvalCanvasShape()

fun CanvasShape.Companion.oval(bounds: Emitter<Edges<Int>>) =
        oval().withBounds(bounds)