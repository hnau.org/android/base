package org.hnau.android.base.ui.animation.tasks


interface AnimationTaskPercentage<T : AnimationTask> {

    companion object;

    val task: T
    val percentage: Float

}

fun <T : AnimationTask> AnimationTaskPercentage.Companion.stub(
    task: T
) = object : AnimationTaskPercentage<T> {
    override val task = task
    override val percentage = 0f

}