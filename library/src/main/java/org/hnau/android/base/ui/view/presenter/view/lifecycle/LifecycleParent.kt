package org.hnau.android.base.ui.view.presenter.view.lifecycle

import org.hnau.emitter.Emitter


interface LifecycleParent {

    val isAnimationsAvailable: Emitter<Boolean>

    fun onLifecycleFinished(lifecycle: ViewLifecycle)
    fun onLifecycleChanged()

}