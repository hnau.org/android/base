package org.hnau.android.base.ui.view.recycler.decoration

import android.content.Context
import android.graphics.Canvas
import android.graphics.Rect
import org.hnau.android.base.data.course.Course
import org.hnau.android.base.data.getter.px.Px


interface SeparatorDrawer {

    companion object;

    val thickness: Px

    fun draw(
        context: Context,
        canvas: Canvas,
        bounds: Rect,
        course: Course
    )

}