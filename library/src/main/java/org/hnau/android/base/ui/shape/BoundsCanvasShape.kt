package org.hnau.android.base.ui.shape

import android.graphics.RectF
import org.hnau.android.base.data.edges.Edges
import org.hnau.android.base.ui.ContainsPointChecker


abstract class BoundsCanvasShape : CanvasShape, ContainsPointChecker {

    open var bounds: Edges<Int> = Edges(0)

    private val bondsRectInner = RectF()

    protected val boundsRect: RectF
        get() = bondsRectInner.apply {
            set(
                    bounds.xMin.toFloat(),
                    bounds.yMin.toFloat(),
                    bounds.xMax.toFloat(),
                    bounds.yMax.toFloat()
            )
        }

    override fun checkContainsPoint(
            x: Float,
            y: Float
    ) = x >= bounds.xMin &&
            y >= bounds.yMin &&
            x <= bounds.xMax &&
            y <= bounds.yMax

}