package org.hnau.android.base.ui.view.recycler.adapter.item.plain

import android.content.Context
import android.view.View
import org.hnau.android.base.ui.view.recycler.adapter.item.ItemViewProvider
import org.hnau.emitter.Emitter


class PlainItemViewProvider<T>(
    private val createItemView: (context: Context, content: Emitter<T>) -> View,
    private val itemViewType: Int = 0
) : ItemViewProvider<T> {

    override fun createItemView(context: Context, content: Emitter<T>, itemViewType: Int) =
        createItemView(context, content)

    override fun getItemViewType(item: T) = itemViewType

}

fun <T> ItemViewProvider.Companion.plain(
    itemViewType: Int = 0,
    createItemView: (context: Context, content: Emitter<T>) -> View
) = PlainItemViewProvider(
    createItemView = createItemView,
    itemViewType = itemViewType
)