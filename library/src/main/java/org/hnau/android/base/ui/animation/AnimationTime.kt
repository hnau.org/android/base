package org.hnau.android.base.ui.animation

import org.hnau.base.data.time.Time
import org.hnau.base.data.time.asSeconds


object AnimationTime {

    val default = 0.3.asSeconds
    val small = default * 0.5

}

val Time.Companion.animation get() = AnimationTime