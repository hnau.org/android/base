package org.hnau.android.base.ui.view.recycler.adapter

import androidx.recyclerview.widget.ListUpdateCallback
import org.hnau.android.base.ui.view.recycler.adapter.content.AdapterContent
import org.hnau.android.base.ui.view.recycler.adapter.item.ItemViewProvider


data class AdapterInfo<T>(
    val content: (ListUpdateCallback) -> AdapterContent<T>,
    val viewProvider: ItemViewProvider<T>
)