package org.hnau.android.base.extensions.view.text

import android.widget.TextView
import org.hnau.android.base.data.getter.text.Text
import org.hnau.android.base.ui.view.utils.input.ime.ActionImeMask


fun TextView.setImeActionLabel(imeActionLabel: Text, actionId: Int) =
    setImeActionLabel(imeActionLabel(context), actionId)

inline fun TextView.setImeAction(
    actionImeMask: ActionImeMask,
    noinline imeActionLabel: Text? = null,
    configurator: ActionImeMask.() -> Unit = {}
) {
    setImeOptions(actionImeMask.apply(configurator).mask)
    imeActionLabel?.let { setImeActionLabel(it, actionImeMask.actionId) }
}

inline fun TextView.setUnspecifiedIme(
    noinline imeActionLabel: Text? = null,
    configurator: ActionImeMask.() -> Unit = {}
) = setImeAction(
    actionImeMask = ActionImeMask.createUnspecified(),
    imeActionLabel = imeActionLabel,
    configurator = configurator
)

inline fun TextView.setDoneIme(
    noinline imeActionLabel: Text? = null,
    configurator: ActionImeMask.() -> Unit = {}
) = setImeAction(
    actionImeMask = ActionImeMask.createDone(),
    imeActionLabel = imeActionLabel,
    configurator = configurator
)

inline fun TextView.setGoIme(
    noinline imeActionLabel: Text? = null,
    configurator: ActionImeMask.() -> Unit = {}
) = setImeAction(
    actionImeMask = ActionImeMask.createGo(),
    imeActionLabel = imeActionLabel,
    configurator = configurator
)

inline fun TextView.setNextIme(
    noinline imeActionLabel: Text? = null,
    configurator: ActionImeMask.() -> Unit = {}
) = setImeAction(
    actionImeMask = ActionImeMask.createNext(),
    imeActionLabel = imeActionLabel,
    configurator = configurator
)

inline fun TextView.setNoneIme(
    noinline imeActionLabel: Text? = null,
    configurator: ActionImeMask.() -> Unit = {}
) = setImeAction(
    actionImeMask = ActionImeMask.createNone(),
    imeActionLabel = imeActionLabel,
    configurator = configurator
)

inline fun TextView.setPreviousIme(
    noinline imeActionLabel: Text? = null,
    configurator: ActionImeMask.() -> Unit = {}
) = setImeAction(
    actionImeMask = ActionImeMask.createPrevious(),
    imeActionLabel = imeActionLabel,
    configurator = configurator
)

inline fun TextView.setSearchIme(
    noinline imeActionLabel: Text? = null,
    configurator: ActionImeMask.() -> Unit = {}
) = setImeAction(
    actionImeMask = ActionImeMask.createSearch(),
    imeActionLabel = imeActionLabel,
    configurator = configurator
)

inline fun TextView.setSendIme(
    noinline imeActionLabel: Text? = null,
    configurator: ActionImeMask.() -> Unit = {}
) = setImeAction(
    actionImeMask = ActionImeMask.createSend(),
    imeActionLabel = imeActionLabel,
    configurator = configurator
)