package org.hnau.android.base.data

import android.view.View
import androidx.core.text.TextUtilsCompat
import androidx.core.view.ViewCompat
import org.hnau.base.extensions.boolean.checkBoolean
import java.util.*


@Suppress("EnumEntryName")
enum class LocaleDirection(
        val key: Int
) {

    ltr(View.LAYOUT_DIRECTION_LTR),
    rtl(View.LAYOUT_DIRECTION_RTL);

    companion object {

        @Suppress("ReplaceCallWithBinaryOperator")
        val current
            get() = TextUtilsCompat
                    .getLayoutDirectionFromLocale(Locale.getDefault())
                    .equals(ViewCompat.LAYOUT_DIRECTION_RTL)
                    .checkBoolean({ rtl }, { ltr })

        val isLTR = current == ltr
        val isRTL = !isLTR


        inline fun <T> chooseLeft(ifStart: () -> T, ifEnd: () -> T) = isLTR.checkBoolean(ifStart, ifEnd)
        inline fun <T> chooseRight(ifStart: () -> T, ifEnd: () -> T) = isLTR.checkBoolean(ifEnd, ifStart)
        inline fun <T> chooseStart(ifLeft: () -> T, ifRight: () -> T) = isLTR.checkBoolean(ifLeft, ifRight)
        inline fun <T> chooseEnd(ifLeft: () -> T, ifRight: () -> T) = isLTR.checkBoolean(ifRight, ifLeft)

    }

}