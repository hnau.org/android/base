package org.hnau.android.base.extensions.view.group.linear

import android.widget.LinearLayout
import org.hnau.android.base.data.direction.Direction
import org.hnau.android.base.data.direction.gravity
import org.hnau.base.exception.property.NoGetterException


var LinearLayout.linearLayoutDirection: Direction
    set(value) {
        gravity = value.gravity
    }
    @Deprecated(
            level = DeprecationLevel.HIDDEN,
            message = NoGetterException.message
    )
    get() = NoGetterException.doThrow()