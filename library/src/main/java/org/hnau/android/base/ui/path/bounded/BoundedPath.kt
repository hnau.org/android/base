package org.hnau.android.base.ui.path.bounded

import android.graphics.Path
import android.graphics.PointF


data class BoundedPath(
        val size: PointF,
        val path: Path
) {

    constructor(
            width: Number,
            height: Number,
            path: Path
    ) : this(
            size = PointF(width.toFloat(), height.toFloat()),
            path = path
    )

    constructor(
            size: Number,
            path: Path
    ) : this(
            width = size,
            height = size,
            path = path
    )

}