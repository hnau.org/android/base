package org.hnau.android.base.data.insets


fun <T> Insets(
        left: T,
        top: T,
        right: T,
        bottom: T,
) = object : Insets<T> {
    override val left get() = left
    override val top get() = top
    override val right get() = right
    override val bottom get() = bottom
}

fun <T> Insets(
        horizontal: T,
        vertical: T
) = Insets(
        left = horizontal,
        top = vertical,
        right = horizontal,
        bottom = vertical
)

fun <T> Insets(
        value: T
) = Insets(
        horizontal = value,
        vertical = value
)