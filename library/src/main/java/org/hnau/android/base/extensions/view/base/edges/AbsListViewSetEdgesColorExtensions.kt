package org.hnau.android.base.extensions.view.base.edgesOld

import android.os.Build
import android.widget.AbsListView
import androidx.annotation.ColorInt
import org.hnau.android.base.data.getter.tone.Tone

private val absListViewEdgeEffectsColorsSetter by lazy {
    SetEdgesOldColorUtils.createEdgeEffectsColorsSetter<AbsListView>(listOf("mEdgeGlowTop", "mEdgeGlowBottom"))
}

fun AbsListView.setEdgesOldColor(
        @ColorInt color: Int
) {
    if (Build.VERSION.SDK_INT < Build.VERSION_CODES.LOLLIPOP) {
        return
    }
    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.Q) {
        setEdgeEffectColor(color)
    } else {
        absListViewEdgeEffectsColorsSetter(color)
    }
}

fun AbsListView.setEdgesOldTone(
        tone: Tone
) = setEdgesOldColor(
        color = tone(context)
)
