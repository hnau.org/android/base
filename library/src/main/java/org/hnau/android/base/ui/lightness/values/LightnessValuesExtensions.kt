package org.hnau.android.base.ui.lightness.values

import org.hnau.android.base.ui.lightness.Lightness


operator fun <T> LightnessValues<T>.get(
        lightness: Lightness
) = when (lightness) {
    Lightness.light -> light
    Lightness.dark -> dark
}