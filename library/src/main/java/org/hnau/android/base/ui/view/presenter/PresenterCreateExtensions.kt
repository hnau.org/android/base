package org.hnau.android.base.ui.view.presenter

import android.content.Context
import org.hnau.android.base.ui.view.presenter.info.PresenterInfo
import org.hnau.android.base.ui.view.presenter.transaction.Transaction
import org.hnau.emitter.Emitter


fun <T> Presenter.Companion.create(
    context: Context,
    values: Emitter<T>,
    compareValues: (first: T, second: T) -> Boolean =
        { first, second -> first == second },
    createPresentValueTransaction: (value: T) -> Transaction,
    info: PresenterInfo = PresenterInfo()
) = object : Presenter<T>(
    context = context,
    values = values,
    info = info
) {

    override fun createPresentValueTransaction(value: T) =
        createPresentValueTransaction.invoke(value)

    override fun compareValues(first: T, second: T) =
        compareValues.invoke(first, second)

}