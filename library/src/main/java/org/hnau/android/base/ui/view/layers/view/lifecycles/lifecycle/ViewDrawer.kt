package org.hnau.android.base.ui.view.layers.view.lifecycles.lifecycle

import android.graphics.Canvas
import android.view.View
import org.hnau.android.base.extensions.inState
import org.hnau.android.base.ui.animation.emergence.offset.Offset


class ViewDrawer {

    fun draw(
        view: View,
        canvas: Canvas,
        drawView: (Canvas, View) -> Unit,
        offset: Offset
    ) {
        canvas.inState {
            translate(
                offset.dx * view.width,
                offset.dy * view.height
            )
            drawView(canvas, view)
        }
    }

}