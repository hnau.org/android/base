package org.hnau.android.base.extensions.view.group.linear

import android.widget.LinearLayout
import org.hnau.android.base.data.course.Course
import org.hnau.android.base.data.course.checkCourse
import org.hnau.base.extensions.boolean.checkBoolean

val Course.linearLayoutCourse
    get() = checkCourse({ LinearLayout.HORIZONTAL }, { LinearLayout.VERTICAL })

var LinearLayout.course: Course
    get() = (orientation == LinearLayout.HORIZONTAL).checkBoolean(
            ifTrue = { Course.horizontal },
            ifFalse = { Course.vertical }
    )
    set(value) {
        orientation = value.linearLayoutCourse
    }