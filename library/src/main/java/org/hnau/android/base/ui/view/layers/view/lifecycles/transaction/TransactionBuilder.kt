package org.hnau.android.base.ui.view.layers.view.lifecycles.transaction

import org.hnau.android.base.ui.animation.emergence.offset.EmergenceDirection
import org.hnau.android.base.ui.view.layers.view.lifecycles.LifecyclesParent
import org.hnau.android.base.ui.view.layers.view.lifecycles.lifecycle.Lifecycle
import org.hnau.base.extensions.checkNullable
import org.hnau.emitter.Emitter


class TransactionBuilder(
    private val isAnimationsAvailable: Emitter<Boolean>,
    private val direction: EmergenceDirection,
    private val onTransactionFinished: (Transaction) -> Unit,
    private val lifecyclesParent: LifecyclesParent
) {

    private var lifecycles: MutableList<Lifecycle>? = null

    fun addLifecycleToTransaction(
        lifecycle: Lifecycle
    ) = lifecycles.checkNullable<MutableList<Lifecycle>, Unit>(
        ifNotNull = { it.add(lifecycle) },
        ifNull = { lifecycles = arrayListOf(lifecycle) }
    )

    fun build() = lifecycles
        ?.groupBy {
            val animationInfo = it.layer.animationInfo
            when (direction) {
                EmergenceDirection.show -> animationInfo.show
                EmergenceDirection.hide -> animationInfo.hide
            }
        }
        ?.map { (emergenceInfo, lifecycles) ->
            Transaction(
                lifecycles = lifecycles,
                direction = direction,
                isAnimationsAvailable = isAnimationsAvailable,
                onFinished = onTransactionFinished,
                lifecyclesParent = lifecyclesParent,
                emergenceInfo = emergenceInfo
            )
        } ?: emptyList()

}