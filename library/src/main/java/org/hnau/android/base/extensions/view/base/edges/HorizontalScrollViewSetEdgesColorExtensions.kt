package org.hnau.android.base.extensions.view.base.edgesOld

import android.os.Build
import android.widget.HorizontalScrollView
import androidx.annotation.ColorInt
import org.hnau.android.base.data.getter.tone.Tone


private val horizontalScrollViewEdgeEffectsColorsSetter by lazy {
    SetEdgesOldColorUtils.createEdgeEffectsColorsSetter<HorizontalScrollView>(listOf("mEdgeGlowLeft", "mEdgeGlowRight"))
}

fun HorizontalScrollView.setEdgesOldColor(
        @ColorInt color: Int
) {
    if (Build.VERSION.SDK_INT < Build.VERSION_CODES.LOLLIPOP) {
        return
    }
    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.Q) {
        setEdgeEffectColor(color)
    } else {
        horizontalScrollViewEdgeEffectsColorsSetter(color)
    }
}

fun HorizontalScrollView.setEdgesOldTone(
        tone: Tone
) = setEdgesOldColor(
        color = tone(context)
)