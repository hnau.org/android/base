package org.hnau.android.base.extensions.view

import android.view.View
import org.hnau.android.base.data.getter.px.Px
import org.hnau.android.base.data.getter.px.pxInt
import org.hnau.android.base.data.insets.Insets


fun View.setInsets(insets: Insets<Number>) =
        setPadding(insets.left.toInt(), insets.top.toInt(), insets.right.toInt(), insets.bottom.toInt())

fun View.setPxInsets(insets: Insets<Px>) =
        setPadding(insets.left.pxInt(context), insets.top.pxInt(context), insets.right.pxInt(context), insets.bottom.pxInt(context))