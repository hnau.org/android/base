package org.hnau.android.base.ui.view.presenter

import android.graphics.Canvas
import android.graphics.Rect
import android.view.View
import org.hnau.android.base.ui.animation.emergence.offset.EmergencesInfo
import org.hnau.android.base.ui.view.presenter.view.lifecycle.ShowingMeasurable
import org.hnau.android.base.ui.view.presenter.view.lifecycle.ViewLifecycle


class Lifecycles {

    private val lifecycles = LinkedHashSet<ViewLifecycle>()

    val showingMeasurables: Collection<ShowingMeasurable> get() = lifecycles

    fun layout(layoutSlot: Rect) =
            lifecycles.forEach { it.layout(layoutSlot) }

    fun draw(
            slot: Rect,
            canvas: Canvas,
            emergencesInfo: EmergencesInfo,
            childDrawer: (canvas: Canvas, child: View) -> Unit
    ) = lifecycles.forEach { lifecycle ->
        lifecycle.draw(
                slot = slot,
                canvas = canvas,
                emergencesInfo = emergencesInfo,
                childDrawer = childDrawer
        )
    }

    fun add(lifecycle: ViewLifecycle) {
        lifecycles.add(lifecycle)
    }

    fun remove(lifecycle: ViewLifecycle) {
        lifecycles.remove(lifecycle)
    }

    fun hideAll() =
            lifecycles.forEach(ViewLifecycle::hide)

    fun measure(
            widthMeasureSpec: Int,
            heightMeasureSpec: Int
    ) = lifecycles.forEach {
        it.measure(widthMeasureSpec, heightMeasureSpec)
    }


}