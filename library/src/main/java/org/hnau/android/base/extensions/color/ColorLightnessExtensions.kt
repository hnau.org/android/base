package org.hnau.android.base.extensions.color

import androidx.annotation.ColorInt
import org.hnau.android.base.ui.lightness.Lightness
import org.hnau.base.extensions.boolean.checkBoolean


val @receiver:ColorInt Int.lightnessValue
    get() = (red + green + blue) / 3

val @receiver:ColorInt Int.lightness: Lightness
    get() = (lightnessValue > 127).checkBoolean(
        ifTrue = { Lightness.light },
        ifFalse = { Lightness.dark }
    )