package org.hnau.android.base.ui.theme

import android.graphics.Color
import org.hnau.android.base.ui.rawcolor.RawColor


data class MutableThemeGradient(
        override var start: RawColor = RawColor(Color.TRANSPARENT),
        override var end: RawColor = RawColor(Color.TRANSPARENT)
) : ThemeGradient