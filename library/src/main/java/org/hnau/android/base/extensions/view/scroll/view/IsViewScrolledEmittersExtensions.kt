package org.hnau.android.base.extensions.view.scroll.view

import android.os.Build
import android.view.View
import androidx.recyclerview.widget.RecyclerView
import org.hnau.emitter.Emitter
import org.hnau.emitter.extensions.map
import org.hnau.emitter.observing.notifier.Notifier


@Deprecated("Use ViewScrolledStateEmitters instead")
fun View.createOnScrolledNotifier() =
    object : Notifier() {

        init {
            addOnScrollChangedListener()
        }

        private fun addOnScrollChangedListener() {

            if (Build.VERSION.SDK_INT < Build.VERSION_CODES.M) {
                viewTreeObserver.addOnScrollChangedListener(::notifyObservers)
                return
            }

            setOnScrollChangeListener(::onScrollChanged)
        }

        @Suppress("UNUSED_PARAMETER")
        private fun onScrollChanged(
            view: View,
            scrollX: Int,
            scrollY: Int,
            oldScrollX: Int,
            oldScrollY: Int
        ) = notifyObservers()

    }

@Deprecated("Use ViewScrolledStateEmitters instead")
fun View.createIsScrolledToLeftEmitter(onScrolledEmitter: Emitter<Unit>) =
    onScrolledEmitter.map { isScrolledToLeft }

@Deprecated("Use ViewScrolledStateEmitters instead")
fun View.createIsScrolledToTopEmitter(onScrolledEmitter: Emitter<Unit>) =
    onScrolledEmitter.map { isScrolledToTop }

@Deprecated("Use ViewScrolledStateEmitters instead")
fun View.createIsScrolledToRightEmitter(onScrolledEmitter: Emitter<Unit>) =
    onScrolledEmitter.map { isScrolledToRight}

@Deprecated("Use ViewScrolledStateEmitters instead")
fun View.createIsScrolledToBottomEmitter(onScrolledEmitter: Emitter<Unit>) =
    onScrolledEmitter.map { isScrolledToBottom }

@Suppress("unused")
@Deprecated(
    message = "Use createOnRecyclerViewScrolledEmitter() instead",
    level = DeprecationLevel.ERROR,
    replaceWith = ReplaceWith(
        "this.createOnRecyclerViewScrolledEmitter()",
        "org.hnau.android.base.extensions.view.scroll.recycle.createOnRecyclerScrolledEmitter"
    )
)
fun RecyclerView.createOnScrolledEmitter() {
}

@Suppress("unused")
@Deprecated(
    message = "Use createIsRecyclerViewScrolledToStartEmitter(onScrolledEmitter: Emitter<Unit>) instead",
    level = DeprecationLevel.ERROR,
    replaceWith = ReplaceWith(
        "this.createRecycleViewIsScrolledToStartEmitter(onScrolledEmitter)",
        "org.hnau.android.base.extensions.view.scroll.recycle.createRecycleIsScrolledToStartEmitter"
    )
)
fun RecyclerView.createIsScrolledToStartEmitter(onScrolledEmitter: Emitter<Unit>) {
}

@Suppress("unused")
@Deprecated(
    message = "Use createIsRecyclerViewScrolledToTopEmitter(onScrolledEmitter: Emitter<Unit>) instead",
    level = DeprecationLevel.ERROR,
    replaceWith = ReplaceWith(
        "this.createRecycleViewIsScrolledToTopEmitter(onScrolledEmitter)",
        "org.hnau.android.base.extensions.view.scroll.recycle.createRecycleIsScrolledToTopEmitter"
    )
)
fun RecyclerView.createIsScrolledToTopEmitter(onScrolledEmitter: Emitter<Unit>) {
}

@Suppress("unused")
@Deprecated(
    message = "Use createIsRecyclerViewScrolledToEndEmitter(onScrolledEmitter: Emitter<Unit>) instead",
    level = DeprecationLevel.ERROR,
    replaceWith = ReplaceWith(
        "this.createRecycleViewIsScrolledToEndEmitter(onScrolledEmitter)",
        "org.hnau.android.base.extensions.view.scroll.recycle.createRecycleIsScrolledToEndEmitter"
    )
)
fun RecyclerView.createIsScrolledToEndEmitter(onScrolledEmitter: Emitter<Unit>) {
}

@Suppress("unused")
@Deprecated(
    message = "Use createIsRecyclerViewScrolledToBottomEmitter(onScrolledEmitter: Emitter<Unit>) instead",
    level = DeprecationLevel.ERROR,
    replaceWith = ReplaceWith(
        "this.createRecycleViewIsScrolledToBottomEmitter(onScrolledEmitter)",
        "org.hnau.android.base.extensions.view.scroll.recycle.createRecycleIsScrolledToBottomEmitter"
    )
)
fun RecyclerView.createIsScrolledToBottomEmitter(onScrolledEmitter: Emitter<Unit>) {
}