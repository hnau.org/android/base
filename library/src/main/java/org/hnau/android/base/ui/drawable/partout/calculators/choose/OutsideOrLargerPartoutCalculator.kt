package org.hnau.android.base.ui.drawable.partout.calculators.choose

import org.hnau.android.base.data.direction.Direction
import org.hnau.android.base.ui.drawable.partout.PartoutCalculator
import org.hnau.android.base.ui.drawable.partout.calculators.IndependentPartoutCalculator
import org.hnau.android.base.ui.drawable.partout.calculators.proportional.OutsidePartoutCalculator
import org.hnau.base.extensions.boolean.checkBoolean


class OutsideOrLargerPartoutCalculator(
        direction: Direction = Direction.center
) : ChoosePartoutCalculator() {

    private val independent
            by lazy { IndependentPartoutCalculator(direction) }

    private val outside
            by lazy { OutsidePartoutCalculator(direction) }

    override fun choosePartoutCalculator(
            parentWidth: Int,
            parentHeight: Int,
            contentPreferredWidth: Int,
            contentPreferredHeight: Int
    ) = (contentPreferredWidth > parentWidth && contentPreferredHeight > parentHeight)
            .checkBoolean({ independent }, { outside })

}

fun PartoutCalculator.Companion.outsideOrLarge(
        direction: Direction = Direction.center
) = OutsideOrLargerPartoutCalculator(
        direction = direction
)