package org.hnau.android.base.data.direction

import android.graphics.Rect


private fun calcChildBegin(
        value: DirectionValue,
        childSize: Number,
        parentBegin: Number,
        parentEnd: Number
): Int {
    val parentBeginInt = parentBegin.toInt()
    val parentEndInt = parentEnd.toInt()
    val childSizeInt = childSize.toInt()
    val parentSize = parentEndInt - parentBeginInt
    return parentBeginInt + when (value) {
        DirectionValue.start -> 0
        DirectionValue.center -> (parentSize - childSizeInt) / 2
        DirectionValue.end -> parentSize - childSizeInt
    }
}

fun Direction.apply(
        width: Int, height: Int,
        parentLeft: Int, parentTop: Int,
        parentRight: Int, parentBottom: Int,
        result: Rect
) {
    val left = calcChildBegin(
            value = horizontal,
            childSize = width,
            parentBegin = parentLeft,
            parentEnd = parentRight
    )
    val top = calcChildBegin(
            value = vertical,
            childSize = height,
            parentBegin = parentTop,
            parentEnd = parentBottom
    )
    result.set(
            left,
            top,
            left + width,
            top + height
    )
}