package org.hnau.android.base.data.getter.tone

import androidx.annotation.FloatRange
import androidx.annotation.IntRange
import org.hnau.android.base.data.getter.base.combine
import org.hnau.android.base.data.getter.base.map
import org.hnau.android.base.extensions.color.clearAlpha
import org.hnau.android.base.extensions.color.clearBlue
import org.hnau.android.base.extensions.color.clearGreen
import org.hnau.android.base.extensions.color.clearRed
import org.hnau.android.base.extensions.color.fullAlpha
import org.hnau.android.base.extensions.color.fullBlue
import org.hnau.android.base.extensions.color.fullGreen
import org.hnau.android.base.extensions.color.fullRed
import org.hnau.android.base.extensions.color.mapAlpha
import org.hnau.android.base.extensions.color.mapAlphaFloat
import org.hnau.android.base.extensions.color.mapBlue
import org.hnau.android.base.extensions.color.mapBlueFloat
import org.hnau.android.base.extensions.color.mapGreen
import org.hnau.android.base.extensions.color.mapGreenFloat
import org.hnau.android.base.extensions.color.mapRed
import org.hnau.android.base.extensions.color.mapRedFloat
import org.hnau.android.base.extensions.color.replaceAlpha
import org.hnau.android.base.extensions.color.replaceBlue
import org.hnau.android.base.extensions.color.replaceGreen
import org.hnau.android.base.extensions.color.replaceRed
import org.hnau.android.base.extensions.color.scaleAlpha
import org.hnau.android.base.extensions.color.scaleBlue
import org.hnau.android.base.extensions.color.scaleGreen
import org.hnau.android.base.extensions.color.scaleRed
import org.hnau.android.base.extensions.color.shiftTo


fun Tone.shiftTo(
        target: Tone,
        @FloatRange(from = 0.0, to = 1.0) percentage: Float
): Tone = combine(target) { from, to ->
    from.shiftTo(to, percentage)
}

inline fun Tone.mapAlpha(crossinline converter: (Int) -> Int): Tone =
        map { color -> color.mapAlpha(converter) }

inline fun Tone.mapAlphaFloat(crossinline converter: (Float) -> Float): Tone =
        map { color -> color.mapAlphaFloat(converter) }

fun Tone.replaceAlpha(@IntRange(from = 0L, to = 255L) newAlpha: Int): Tone =
        map { color -> color.replaceAlpha(newAlpha) }

fun Tone.replaceAlpha(@FloatRange(from = 0.0, to = 1.0) newAlpha: Float): Tone =
        map { color -> color.replaceAlpha(newAlpha) }

fun Tone.scaleAlpha(factor: Number): Tone =
        map { color -> color.scaleAlpha(factor) }

fun Tone.clearAlpha(): Tone =
        map { color -> color.clearAlpha() }

fun Tone.fullAlpha(): Tone =
        map { color -> color.fullAlpha() }


inline fun Tone.mapRed(crossinline converter: (Int) -> Int): Tone =
        map { color -> color.mapRed(converter) }

inline fun Tone.mapRedFloat(crossinline converter: (Float) -> Float): Tone =
        map { color -> color.mapRedFloat(converter) }

fun Tone.applyRed(@IntRange(from = 0L, to = 255L) newRed: Int): Tone =
        map { color -> color.replaceRed(newRed) }

fun Tone.applyRed(@FloatRange(from = 0.0, to = 1.0) newRed: Float): Tone =
        map { color -> color.replaceRed(newRed) }

fun Tone.scaleRed(factor: Number): Tone =
        map { color -> color.scaleRed(factor) }

fun Tone.clearRed(): Tone =
        map { color -> color.clearRed() }

fun Tone.fullRed(): Tone =
        map { color -> color.fullRed() }


inline fun Tone.mapGreen(crossinline converter: (Int) -> Int): Tone =
        map { color -> color.mapGreen(converter) }

inline fun Tone.mapGreenFloat(crossinline converter: (Float) -> Float): Tone =
        map { color -> color.mapGreenFloat(converter) }

fun Tone.replaceGreen(@IntRange(from = 0L, to = 255L) newGreen: Int): Tone =
        map { color -> color.replaceGreen(newGreen) }

fun Tone.replaceGreen(@FloatRange(from = 0.0, to = 1.0) newGreen: Float): Tone =
        map { color -> color.replaceGreen(newGreen) }

fun Tone.scaleGreen(factor: Number): Tone =
        map { color -> color.scaleGreen(factor) }

fun Tone.clearGreen(): Tone =
        map { color -> color.clearGreen() }

fun Tone.fullGreen(): Tone =
        map { color -> color.fullGreen() }


inline fun Tone.mapBlue(crossinline converter: (Int) -> Int): Tone =
        map { color -> color.mapBlue(converter) }

inline fun Tone.mapBlueFloat(crossinline converter: (Float) -> Float): Tone =
        map { color -> color.mapBlueFloat(converter) }

fun Tone.replaceBlue(@IntRange(from = 0L, to = 255L) newBlue: Int): Tone =
        map { color -> color.replaceBlue(newBlue) }

fun Tone.replaceBlue(@FloatRange(from = 0.0, to = 1.0) newBlue: Float): Tone =
        map { color -> color.replaceBlue(newBlue) }

fun Tone.scaleBlue(factor: Number): Tone =
        map { color -> color.scaleBlue(factor) }

fun Tone.clearBlue(): Tone =
        map { color -> color.clearBlue() }

fun Tone.fullBlue(): Tone =
        map { color -> color.fullBlue() }