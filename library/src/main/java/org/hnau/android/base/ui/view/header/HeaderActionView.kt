package org.hnau.android.base.ui.view.header

import android.annotation.SuppressLint
import android.content.Context
import android.graphics.Canvas
import android.view.View
import org.hnau.android.base.data.Side
import org.hnau.android.base.data.getter.picture.Picture
import org.hnau.android.base.data.getter.px.Px
import org.hnau.android.base.data.getter.px.constants
import org.hnau.android.base.data.getter.px.pxInt
import org.hnau.android.base.data.getter.text.Text
import org.hnau.android.base.data.getter.tone.Tones
import org.hnau.android.base.data.getter.tone.fromColor
import org.hnau.android.base.data.insets.Insets
import org.hnau.android.base.data.insets.typed.applyInsets
import org.hnau.android.base.extensions.view.createBounds
import org.hnau.android.base.extensions.view.touch.createTouchEmitter
import org.hnau.android.base.extensions.view.touch.mapOnClick
import org.hnau.android.base.ui.animation.emergence.EmergenceInfo
import org.hnau.android.base.ui.animation.emergence.offset.EmergencesInfo
import org.hnau.android.base.ui.animation.emergence.offset.calculator.OffsetCalculator
import org.hnau.android.base.ui.animation.emergence.offset.calculator.side
import org.hnau.android.base.ui.animation.emergence.offset.symetric
import org.hnau.android.base.ui.decoration.ViewDecorations
import org.hnau.android.base.ui.decoration.composite
import org.hnau.android.base.ui.decoration.drawable
import org.hnau.android.base.ui.decoration.receivingViewDecoration
import org.hnau.android.base.ui.decoration.ripple.RippleInfo
import org.hnau.android.base.ui.decoration.ripple.replaceAlphaForRipple
import org.hnau.android.base.ui.decoration.ripple.ripple
import org.hnau.android.base.ui.decoration.ripple.shape.RippleShape
import org.hnau.android.base.ui.decoration.ripple.shape.borderless
import org.hnau.android.base.ui.rawcolor.RawColor
import org.hnau.android.base.ui.shape.CanvasShape
import org.hnau.android.base.ui.shape.rect
import org.hnau.android.base.ui.view.presenter.Presenter
import org.hnau.android.base.ui.view.presenter.create
import org.hnau.android.base.ui.view.presenter.info.PresenterInfo
import org.hnau.android.base.ui.view.presenter.info.size.presenter.base.PresenterSizeCalculator
import org.hnau.android.base.ui.view.presenter.info.size.presenter.fixed
import org.hnau.android.base.ui.view.presenter.info.size.presenter.matchParent
import org.hnau.android.base.ui.view.presenter.transaction.Transaction
import org.hnau.android.base.ui.view.presenter.view.wrapper.existence.PresentingInfo
import org.hnau.android.base.ui.view.utils.state.createStateEmitters
import org.hnau.emitter.Emitter
import org.hnau.emitter.extensions.listen
import org.hnau.emitter.extensions.map
import org.hnau.emitter.observing.push.lateinit.collapse


@SuppressLint("ViewConstructor")
class HeaderActionView(
        context: Context,
        foreground: Emitter<RawColor>,
        icon: (RawColor) -> Picture,
        hint: Text,
        onClick: () -> Unit
) : View(
        context
) {

    private val bounds = createBounds()

    private val touchEvents = createTouchEmitter()

    private val state = createStateEmitters()

    private val decorations = receivingViewDecoration(
            isActive = state.isActive,
            decorationEmitter = foreground
                    .map { foreground ->
                        ViewDecorations.composite(
                                ViewDecorations.drawable(
                                        bounds = bounds,
                                        drawable = icon(foreground)(context)
                                ),
                                ViewDecorations.ripple(
                                        context = context,
                                        info = RippleInfo(Tones.fromColor(foreground.replaceAlphaForRipple().value)),
                                        touchEvents = touchEvents,
                                        shape = RippleShape.borderless(bounds.applyInsets(Insets(Px.constants.separations.large.pxInt(context))))
                                )
                        )
                    }
                    .collapse()
    )

    init {

        touchEvents
                .mapOnClick(CanvasShape.rect(bounds))
                .listen(onClick)

    }

    override fun draw(canvas: Canvas) {
        super.draw(canvas)
        decorations(canvas)
    }


}

fun HeaderActionViewPresenter(
        context: Context,
        foreground: Emitter<RawColor>,
        action: Emitter<Triple<(RawColor) -> Picture, Text, () -> Unit>?>,
        emergenceSide: Side
) = Presenter.create(
        context = context,
        info = PresenterInfo(
                width = PresenterSizeCalculator.fixed(context, Px.constants.headerHeight),
                height = PresenterSizeCalculator.matchParent()
        ),
        values = action,
        createPresentValueTransaction = { actionOrNull ->
            Transaction(
                    view = actionOrNull?.let { (icon, hint, onClick) ->
                        HeaderActionView(
                                context = context,
                                foreground = foreground,
                                icon = icon,
                                hint = hint,
                                onClick = onClick
                        )
                    },
                    presentingInfo = PresentingInfo(
                            emergencesInfo = EmergencesInfo.symetric(
                                    info = EmergenceInfo(
                                            offsetCalculator = OffsetCalculator.side(emergenceSide)
                                    )
                            )
                    )
            )
        }

)