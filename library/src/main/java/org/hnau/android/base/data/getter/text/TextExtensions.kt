package org.hnau.android.base.data.getter.text

import org.hnau.android.base.data.getter.base.combine
import org.hnau.android.base.data.getter.base.map


operator fun Text.plus(other: Text): Text =
    combine(other, String::plus)

private fun Text.plusStringable(other: Any): Text =
    map { value -> value + other.toString() }

operator fun Text.plus(other: String) = plusStringable(other)
operator fun Text.plus(other: Char) = plusStringable(other)
operator fun Text.plus(other: Byte) = plusStringable(other)
operator fun Text.plus(other: Short) = plusStringable(other)
operator fun Text.plus(other: Int) = plusStringable(other)
operator fun Text.plus(other: Float) = plusStringable(other)
operator fun Text.plus(other: Double) = plusStringable(other)
operator fun Text.plus(other: Boolean) = plusStringable(other)

private fun Text.toUpperCase(): Text = map(String::toUpperCase)
private fun Text.toLowerCase(): Text = map(String::toLowerCase)
private fun Text.capitalize(): Text = map(String::capitalize)