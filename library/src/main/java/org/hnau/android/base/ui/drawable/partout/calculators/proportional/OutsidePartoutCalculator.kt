package org.hnau.android.base.ui.drawable.partout.calculators.proportional

import org.hnau.android.base.data.direction.Direction
import org.hnau.android.base.ui.drawable.partout.PartoutCalculator
import org.hnau.base.extensions.number.takeIfPositive
import kotlin.math.max


class OutsidePartoutCalculator(
    direction: Direction = Direction.center
) : ProportionalPartoutCalculator(direction) {

    override fun calcScaleFactor(
        parentWidth: Int,
        parentHeight: Int,
        contentPreferredWidth: Int,
        contentPreferredHeight: Int
    ): Float {
        val contentWidth = contentPreferredWidth.takeIfPositive()?.toFloat() ?: return 1f
        val contentHeight = contentPreferredHeight.takeIfPositive()?.toFloat() ?: return 1f
        val width = parentWidth.takeIfPositive()?.toFloat() ?: return 1f
        val height = parentHeight.takeIfPositive()?.toFloat() ?: return 1f
        return max(width / contentWidth, height / contentHeight)
    }

}

fun PartoutCalculator.Companion.outside(
        direction: Direction = Direction.center
) = OutsidePartoutCalculator(
        direction = direction
)