package org.hnau.android.base.extensions.view.touch

import android.annotation.SuppressLint
import android.view.MotionEvent
import android.view.View
import org.hnau.emitter.Emitter
import org.hnau.emitter.observing.ObservingEmitter


class TouchEventEmitter : ObservingEmitter<TouchEvent>() {

    companion object;

    private val value = TouchEvent()

    fun onMotionEvent(motionEvent: MotionEvent) {
        value.update(motionEvent)
        call(value)
    }

}

@SuppressLint("ClickableViewAccessibility")
fun TouchEventEmitter.Companion.create(
    view: View
): Emitter<TouchEvent> = TouchEventEmitter().apply {
    view.setOnTouchListener { _, event ->
        onMotionEvent(event)
        return@setOnTouchListener true
    }
}

fun View.createTouchEmitter() =
    TouchEventEmitter.create(this)