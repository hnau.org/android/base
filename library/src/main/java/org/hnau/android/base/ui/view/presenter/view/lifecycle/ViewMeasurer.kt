package org.hnau.android.base.ui.view.presenter.view.lifecycle


interface ViewMeasurer {

    fun measure(widthMeasureSpec: Int, heightMeasureSpec: Int)

}