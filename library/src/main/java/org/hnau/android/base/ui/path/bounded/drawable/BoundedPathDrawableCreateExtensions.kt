package org.hnau.android.base.ui.path.bounded.drawable

import android.graphics.Paint
import org.hnau.android.base.ui.path.bounded.BoundedPath
import org.hnau.base.data.possible.lateinit.Lateinit
import org.hnau.base.data.possible.lateinit.unsafe
import org.hnau.base.data.possible.valueOrThrow
import org.hnau.emitter.Emitter
import org.hnau.emitter.extensions.map


fun BoundedPathDrawable.Companion.create(
        boundedPath: BoundedPath,
        getPaint: () -> Paint
) = object : BoundedPathDrawable(
        boundedPath = boundedPath
) {

    override val paint get() = getPaint()

}

fun BoundedPathDrawable.Companion.create(
        boundedPath: BoundedPath,
        paint: Paint
) = create(
        boundedPath = boundedPath,
        getPaint = { paint }
)

fun BoundedPathDrawable.Companion.create(
        boundedPath: BoundedPath,
        paint: Emitter<Paint>
) = Lateinit.unsafe<Paint>().let { paintCache ->
    create(
            boundedPath = boundedPath,
            getPaint = paintCache::valueOrThrow
    ).let { drawable ->
        paint.map { paint ->
            paintCache.set(paint)
            drawable
        }
    }
}