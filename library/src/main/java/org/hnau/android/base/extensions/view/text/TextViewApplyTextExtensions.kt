package org.hnau.android.base.extensions.view.text

import android.util.TypedValue
import android.widget.TextView
import org.hnau.android.base.data.getter.font.Font
import org.hnau.android.base.data.getter.px.Px
import org.hnau.android.base.data.getter.text.Text
import org.hnau.android.base.data.getter.tone.Tone


fun TextView.setText(text: Text) =
    setText(text(context))

fun TextView.setTextSize(textSize: Number) {
     setTextSize(TypedValue.COMPLEX_UNIT_PX, textSize.toFloat())
}

fun TextView.setTextSize(textSize: Px) =
    setTextSize(textSize(context) as Number)

fun TextView.setFont(font: Font?) =
    setTypeface(font?.invoke(context))

fun TextView.setTextTone(textTone: Tone) =
    setTextColor(textTone(context))