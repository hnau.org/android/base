package org.hnau.android.base.ui.view.presenter.view.wrapper

import android.graphics.Canvas
import android.graphics.Rect
import android.view.View
import org.hnau.android.base.ui.animation.emergence.EmergenceInfo
import org.hnau.android.base.ui.animation.emergence.offset.EmergenceDirection
import org.hnau.android.base.ui.animation.emergence.offset.EmergencesInfo
import org.hnau.android.base.ui.animation.emergence.offset.calculator.OffsetCalculator
import org.hnau.android.base.ui.view.presenter.view.lifecycle.ChildViewAttacher
import org.hnau.android.base.ui.view.presenter.view.lifecycle.ViewMeasuredSizeGetter
import org.hnau.android.base.ui.view.presenter.view.lifecycle.ViewMeasurer
import org.hnau.android.base.ui.view.presenter.view.wrapper.existence.PresentingInfo
import org.hnau.android.base.ui.view.presenter.view.wrapper.existence.view
import org.hnau.base.extensions.checkNullable


interface ViewWrapper : ViewMeasurer, ViewMeasuredSizeGetter, ChildViewAttacher {

    companion object;

    val emergencesInfo: EmergencesInfo

    fun layout(
        slot: Rect
    )

    fun setAlpha(
        direction: EmergenceDirection,
        offsetAmount: Float
    )

    fun draw(
            slot: Rect,
            canvas: Canvas,
            childDrawer: (canvas: Canvas, child: View) -> Unit,
            emergenceInfo: EmergenceInfo,
            visibilityPercentage: Float
    )

}

fun ViewWrapper.Companion.viewOrEmpty(
    view: View?,
    presentingInfo: PresentingInfo = PresentingInfo()
) = view.checkNullable(
    ifNotNull = { ViewWrapper.view(it, presentingInfo) },
    ifNull = { ViewWrapper.empty }
)