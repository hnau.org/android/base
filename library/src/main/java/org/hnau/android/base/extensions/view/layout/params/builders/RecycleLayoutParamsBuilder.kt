package org.hnau.android.base.extensions.view.layout.params.builders

import android.content.Context
import androidx.recyclerview.widget.RecyclerView


class RecycleLayoutParamsBuilder(
        context: Context
) : MarginLayoutParamsBuilder(
        context
) {

    override fun build() =
            RecyclerView.LayoutParams(widthValue, heightValue).applyMargins()

}