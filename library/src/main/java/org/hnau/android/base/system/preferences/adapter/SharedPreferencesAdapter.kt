package org.hnau.android.base.system.preferences.adapter

import android.content.SharedPreferences


data class SharedPreferencesAdapter<T>(
        val read: SharedPreferences.(key: String) -> T,
        val write: SharedPreferences.Editor.(key: String, value: T) -> Unit
) {

    companion object;

}