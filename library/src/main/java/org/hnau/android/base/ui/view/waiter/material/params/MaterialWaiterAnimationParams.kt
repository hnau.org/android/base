package org.hnau.android.base.ui.view.waiter.material.params

import org.hnau.android.base.ui.animation.animation
import org.hnau.android.base.ui.animation.curver.Curver
import org.hnau.android.base.ui.animation.curver.Curvers
import org.hnau.base.data.time.Time
import org.hnau.base.data.time.asSeconds


data class MaterialWaiterAnimationParams(
        val period: Time = 1.7.asSeconds,
        val degreesOffsetFactor: Double = 6.0,
        val fazesDistancePercentage: Double = 0.4,
        val lineSidesCurver: Curver = Curvers.accelerateDecelerate,
        val visibilitySwitchingTime: Time = Time.animation.default
)