package org.hnau.android.base.data.edges


fun <T> Edges(
        xMin: T,
        yMin: T,
        xMax: T,
        yMax: T,
) = object : Edges<T> {
    override val xMin get() = xMin
    override val yMin get() = yMin
    override val xMax get() = xMax
    override val yMax get() = yMax
}

fun <T> Edges(
        horizontal: T,
        vertical: T
) = Edges(
        xMin = horizontal,
        yMin = vertical,
        xMax = horizontal,
        yMax = vertical
)

fun <T> Edges(
        value: T
) = Edges(
        horizontal = value,
        vertical = value
)