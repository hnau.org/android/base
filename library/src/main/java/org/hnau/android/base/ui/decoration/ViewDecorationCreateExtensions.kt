package org.hnau.android.base.ui.decoration

import android.graphics.Canvas
import org.hnau.base.data.possible.lateinit.Lateinit
import org.hnau.base.data.possible.lateinit.unsafe
import org.hnau.base.data.possible.valueOrThrow
import org.hnau.emitter.Emitter
import org.hnau.emitter.extensions.map


fun ViewDecorations.create(draw: Canvas.() -> Unit): ViewDecoration = draw

