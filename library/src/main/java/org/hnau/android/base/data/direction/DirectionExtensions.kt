package org.hnau.android.base.data.direction

import org.hnau.android.base.data.Side


fun Direction.Companion.find(
    horizontal: DirectionValue = DirectionValue.center,
    vertical: DirectionValue = DirectionValue.center
) = when (horizontal) {
    DirectionValue.start -> when (vertical) {
        DirectionValue.start -> Direction.leftTop
        DirectionValue.center -> Direction.left
        DirectionValue.end -> Direction.leftBottom
    }
    DirectionValue.center -> when (vertical) {
        DirectionValue.start -> Direction.top
        DirectionValue.center -> Direction.center
        DirectionValue.end -> Direction.bottom
    }
    DirectionValue.end -> when (vertical) {
        DirectionValue.start -> Direction.rightTop
        DirectionValue.center -> Direction.right
        DirectionValue.end -> Direction.rightBottom
    }
}

val Side.direction
    get() = when (this) {
        Side.left -> Direction.left
        Side.top -> Direction.top
        Side.right -> Direction.right
        Side.bottom -> Direction.bottom
    }

operator fun Direction.plus(
    other: Direction
) = Direction.find(
    horizontal = this.horizontal + other.horizontal,
    vertical = this.vertical + other.vertical
)
