package org.hnau.android.base.ui.drawable.partout.calculators.choose

import org.hnau.android.base.data.direction.Direction
import org.hnau.android.base.ui.drawable.partout.PartoutCalculator
import org.hnau.android.base.ui.drawable.partout.calculators.IndependentPartoutCalculator
import org.hnau.android.base.ui.drawable.partout.calculators.proportional.InsidePartoutCalculator
import org.hnau.base.extensions.boolean.checkBoolean


class InsideOrLargerPartoutCalculator(
        direction: Direction = Direction.center
) : ChoosePartoutCalculator() {

    private val independent
            by lazy { IndependentPartoutCalculator(direction) }

    private val inside
            by lazy { InsidePartoutCalculator(direction) }


    override fun choosePartoutCalculator(
            parentWidth: Int,
            parentHeight: Int,
            contentPreferredWidth: Int,
            contentPreferredHeight: Int
    ) = (contentPreferredWidth > parentWidth || contentPreferredHeight > parentHeight)
            .checkBoolean({ independent }, { inside })

}

fun PartoutCalculator.Companion.insideOrLarge(
        direction: Direction = Direction.center
) = InsideOrLargerPartoutCalculator(
        direction = direction
)