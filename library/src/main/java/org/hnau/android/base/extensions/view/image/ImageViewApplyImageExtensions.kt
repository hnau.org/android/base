package org.hnau.android.base.extensions.view.image

import android.widget.ImageView
import org.hnau.android.base.data.getter.picture.Picture
import org.hnau.android.base.data.getter.picture.toPicture
import org.hnau.android.base.data.getter.tone.Tone

fun ImageView.setImagePicture(picture: Picture?) =
    setImageDrawable(picture?.invoke(context))

fun ImageView.setImageTone(tone: Tone?) =
    setImagePicture(tone?.toPicture())