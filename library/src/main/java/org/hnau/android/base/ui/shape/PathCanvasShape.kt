package org.hnau.android.base.ui.shape

import android.graphics.Canvas
import android.graphics.Paint
import android.graphics.Path


abstract class PathCanvasShape : CanvasShape {

    companion object;

    protected abstract val path: Path

    override fun draw(canvas: Canvas, paint: Paint) =
            canvas.drawPath(path, paint)

    override fun clip(canvas: Canvas) {
        canvas.clipPath(path)
    }

}