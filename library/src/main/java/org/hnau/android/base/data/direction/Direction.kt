package org.hnau.android.base.data.direction


@Suppress("EnumEntryName")
enum class Direction(
    val horizontal: DirectionValue = DirectionValue.center,
    val vertical: DirectionValue = DirectionValue.center
) {

    center,
    left(horizontal = DirectionValue.start),
    right(horizontal = DirectionValue.end),
    top(vertical = DirectionValue.start),
    bottom(vertical = DirectionValue.end),
    leftTop(horizontal = DirectionValue.start, vertical = DirectionValue.start),
    leftBottom(horizontal = DirectionValue.start, vertical = DirectionValue.end),
    rightTop(horizontal = DirectionValue.end, vertical = DirectionValue.start),
    rightBottom(horizontal = DirectionValue.end, vertical = DirectionValue.end);

    companion object

}