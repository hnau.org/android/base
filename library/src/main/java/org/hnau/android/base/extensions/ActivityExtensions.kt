package org.hnau.android.base.extensions

import android.app.Activity
import android.graphics.Color
import android.os.Build
import android.view.View
import android.view.Window
import android.view.WindowManager
import androidx.annotation.RequiresApi

@RequiresApi(Build.VERSION_CODES.JELLY_BEAN)
private const val transparentStatusBarBaseFlags = View.SYSTEM_UI_FLAG_LAYOUT_STABLE or View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN

@RequiresApi(Build.VERSION_CODES.JELLY_BEAN)
private const val transparentStatusBarLightIconsFlags = transparentStatusBarBaseFlags

@RequiresApi(Build.VERSION_CODES.M)
private const val transparentStatusBarDarkIconsFlags = transparentStatusBarBaseFlags or View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR

@RequiresApi(Build.VERSION_CODES.JELLY_BEAN)
private fun getTransparentStatusBarBaseFlags(lightStatusBarIcons: Boolean) =
        if (lightStatusBarIcons || Build.VERSION.SDK_INT < Build.VERSION_CODES.M) {
            transparentStatusBarLightIconsFlags
        } else {
            transparentStatusBarDarkIconsFlags
        }

@Deprecated("Use TransparentStatusBarActivity instead")
fun Activity.initializeTransparentStatusBar(lightStatusBarIcons: Boolean) {
    if (Build.VERSION.SDK_INT < Build.VERSION_CODES.LOLLIPOP) return
    window.statusBarColor = Color.TRANSPARENT
    window.decorView.systemUiVisibility = getTransparentStatusBarBaseFlags(lightStatusBarIcons)
    window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS)
    requestWindowFeature(Window.FEATURE_NO_TITLE)
}