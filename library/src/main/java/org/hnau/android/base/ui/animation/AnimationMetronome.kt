package org.hnau.android.base.ui.animation

import android.view.Choreographer
import org.hnau.android.base.ui.UiThreadUtils
import org.hnau.base.extensions.boolean.checkBoolean
import org.hnau.emitter.observing.notifier.Notifier


object AnimationMetronome : Notifier() {

    private val choreographer
            by lazy { UiThreadUtils.runUiOrThrow { Choreographer.getInstance()!! } }

    private var attached = false

    private lateinit var callback: Choreographer.FrameCallback

    init {

        callback = Choreographer.FrameCallback {
            synchronized(this) {
                attached.checkBoolean(
                    ifTrue = { choreographer.postFrameCallback(callback) },
                    ifFalse = { return@FrameCallback }
                )
            }
            notifyObservers()
        }

    }

    override fun beforeFirstAttached() {
        super.beforeFirstAttached()
        synchronized(this) {
            attached = true
            choreographer.postFrameCallback(callback)
        }
    }

    override fun afterLastDetached() {
        synchronized(this) {
            choreographer.removeFrameCallback(callback)
            attached = false
        }
        super.afterLastDetached()
    }

}