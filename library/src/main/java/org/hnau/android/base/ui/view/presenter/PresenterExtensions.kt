package org.hnau.android.base.ui.view.presenter

import android.view.ViewGroup
import org.hnau.android.base.extensions.view.group.children.view
import org.hnau.android.base.ui.view.presenter.info.PresenterInfo
import org.hnau.android.base.ui.view.presenter.transaction.Transaction
import org.hnau.emitter.Emitter


inline fun <T> ViewGroup.presenter(
        values: Emitter<T>,
        noinline compareValues: (first: T, second: T) -> Boolean =
        { first, second -> first == second },
        noinline createPresentValueTransaction: (value: T) -> Transaction,
        info: PresenterInfo = PresenterInfo(),
        childConfigurator: Presenter<T>.() -> Unit = {}
) = view(
    Presenter.create(
        context = context,
        values = values,
        info = info,
        compareValues = compareValues,
        createPresentValueTransaction = createPresentValueTransaction
    ),
    childConfigurator
)