package org.hnau.android.base.ui.rawcolor


inline fun RawColor.map(
        transform: Int.() -> Int
) = RawColor(
        value = transform(value)
)

inline fun RawColor.Companion.combine(
        first: RawColor,
        second: RawColor,
        combinator: (Int, Int) -> Int
) = RawColor(
        value = combinator(first.value, second.value)
)

