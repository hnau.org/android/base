package org.hnau.android.base.ui.view.input.info

import org.hnau.android.base.data.getter.px.Px
import org.hnau.android.base.data.getter.px.dp


data class InputViewBorderInfo(
        val width: Px = 1.dp,
        val titleSeparation: Px = 4.dp
) {

    companion object

}