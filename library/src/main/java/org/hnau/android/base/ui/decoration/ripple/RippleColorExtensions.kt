package org.hnau.android.base.ui.decoration.ripple

import androidx.annotation.ColorInt
import org.hnau.android.base.data.getter.tone.Tone
import org.hnau.android.base.data.getter.tone.replaceAlpha
import org.hnau.android.base.extensions.color.replaceAlpha
import org.hnau.android.base.ui.rawcolor.RawColor
import org.hnau.android.base.ui.rawcolor.map

private const val rippleAlpha = 0.5f

fun @receiver:ColorInt Int.replaceAlphaForRipple() = replaceAlpha(rippleAlpha)

fun Tone.replaceAlphaForRipple() = replaceAlpha(rippleAlpha)

fun RawColor.replaceAlphaForRipple() = map(Int::replaceAlphaForRipple)