package org.hnau.android.base.extensions.view

import android.view.View
import org.hnau.android.base.data.edges.Edges
import org.hnau.android.base.data.edges.MutableEdges
import org.hnau.emitter.observing.push.always.AlwaysEmitter


fun View.createBounds() = object : AlwaysEmitter<Edges<Int>>() {

    private val valueInner =
            MutableEdges(0)

    override val value: Edges<Int>
        get() = valueInner

    private val listener = View.OnLayoutChangeListener { _, _, _, _, _, _, _, _, _ ->
        valueInner.set(0, 0, width, height)
        onChanged()
    }

    override fun beforeFirstAttached() {
        super.beforeFirstAttached()
        addOnLayoutChangeListener(listener)
    }

    override fun afterLastDetached() {
        removeOnLayoutChangeListener(listener)
        super.afterLastDetached()
    }

}