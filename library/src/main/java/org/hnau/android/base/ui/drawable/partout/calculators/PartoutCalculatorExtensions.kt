package org.hnau.android.base.ui.drawable.partout.calculators

import android.graphics.Rect
import org.hnau.android.base.ui.drawable.partout.PartoutCalculator


operator fun PartoutCalculator.plus(
    other: PartoutCalculator
) = this.let { self ->
    object : PartoutCalculator {

        override fun calcContentBounds(
            parentWidth: Int,
            parentHeight: Int,
            contentPreferredWidth: Int,
            contentPreferredHeight: Int,
            result: Rect
        ) {
            other.calcContentBounds(
                parentWidth = parentWidth,
                parentHeight = parentHeight,
                contentPreferredWidth = self.calcIntrinsicWrapperWidth(contentPreferredWidth),
                contentPreferredHeight = self.calcIntrinsicWrapperHeight(contentPreferredHeight),
                result = result
            )
            val selfLeft = result.left
            val selfTop = result.top
            val selfWidth = result.width()
            val selfHeight = result.height()
            self.calcContentBounds(
                parentWidth = selfWidth,
                parentHeight = selfHeight,
                contentPreferredWidth = contentPreferredWidth,
                contentPreferredHeight = contentPreferredHeight,
                result = result
            )
            result.offset(selfLeft, selfTop)
        }

        override fun calcIntrinsicWrapperWidth(contentIntrinsicWidth: Int) =
            other.calcIntrinsicWrapperWidth(self.calcIntrinsicWrapperWidth(contentIntrinsicWidth))

        override fun calcIntrinsicWrapperHeight(contentIntrinsicHeight: Int) =
            other.calcIntrinsicWrapperHeight(self.calcIntrinsicWrapperHeight(contentIntrinsicHeight))

    }
}