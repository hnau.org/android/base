package org.hnau.android.base.ui.animation.tasks

import org.hnau.android.base.ui.animation.AnimationTicker
import org.hnau.base.data.time.Time
import org.hnau.base.extensions.boolean.checkBoolean
import org.hnau.base.extensions.boolean.ifFalse
import org.hnau.base.extensions.ifNotNull
import org.hnau.base.data.time.now
import org.hnau.emitter.Emitter
import org.hnau.emitter.extensions.listen
import org.hnau.emitter.extensions.mapIsNotNull
import org.hnau.emitter.observing.notifier.Notifier
import org.hnau.emitter.observing.notifier.NotifierSimple
import org.hnau.emitter.observing.push.always.AlwaysEmitter
import org.hnau.emitter.observing.push.always.content.ManualEmitter


class AnimationTasks<T : AnimationTask>(
    initialTask: T,
    isAnimateAvailable: Emitter<Boolean>
) : AlwaysEmitter<AnimationTaskPercentage<T>>(), AnimationTaskPercentage<T> {

    override val value = this

    private val queue = AnimationTasksQueue<T>(initialTask)

    override val task get() = queue.task
    override val percentage
        get() = calculatePercentage()

    private val lastTaskStarted = ManualEmitter<Time?>(null)

    private val onFinishedInner = NotifierSimple()
    val onFinished: Notifier get() = onFinishedInner

    val active = lastTaskStarted.mapIsNotNull()

    private var animationAvailable = false

    private val animator = AnimationTicker()

    init {
        isAnimateAvailable.observe {
            animationAvailable = it
        }

        animator.listen(::onAnimationTic)

        animationAvailable.checkBoolean(
            ifTrue = ::onTaskStarted,
            ifFalse = ::onTasksFinished
        )
    }

    fun addTask(
        task: T
    ) {
        queue.addTask(task)
        tryStartNextTask()
    }

    private fun tryStartNextTask() = synchronized<Unit>(this) {

        lastTaskStarted.value.ifNotNull { return }
        (queue.switchToNextTask() && animationAvailable).checkBoolean(
            ifTrue = ::onTaskStarted,
            ifFalse = ::onTasksFinished
        )
    }

    private fun onTaskStarted() {
        lastTaskStarted::value.set(Time.now())
        animator.start()
    }

    private fun onTasksFinished() {
        animator.stop()
        onChanged()
        onFinishedInner.notifyObservers()
    }

    private fun onAnimationTic() {
        onChanged()
        if (percentage >= 1) {
            lastTaskStarted::value.set(null)
            tryStartNextTask()
        }
    }

    private fun calculatePercentage(): Float {

        animationAvailable.ifFalse { return 1f }

        val lastTaskStarted = lastTaskStarted.value
            ?: return 1f

        val duration = task.duration
            .takeIf { it > Time.zero }
            ?: return 1f

        val rawPercentage = ((Time.now() - lastTaskStarted) / duration).toFloat()

        return rawPercentage.coerceAtMost(1f)
    }

}