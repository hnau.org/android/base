package org.hnau.android.base.data.insets

import org.hnau.android.base.data.edges.MutableEdges
import org.hnau.base.data.possible.lateinit.Lateinit
import org.hnau.base.data.possible.lateinit.unsafe
import org.hnau.base.data.possible.updateMutable
import org.hnau.emitter.Emitter
import org.hnau.emitter.observing.push.lateinit.combine


inline fun <A, B, R> Insets.Companion.combine(
        first: Insets<A>,
        second: Insets<B>,
        combinator: (A, B) -> R
) = Insets(
        left = combinator(first.left, second.left),
        top = combinator(first.top, second.top),
        right = combinator(first.right, second.right),
        bottom = combinator(first.bottom, second.bottom)
)

inline fun <A, B, R> Insets.Companion.combine(
        first: Emitter<Insets<A>>,
        second: Emitter<Insets<B>>,
        crossinline combinator: (A, B) -> R
): Emitter<Insets<R>> = Lateinit.unsafe<MutableInsets<R>>().let { resultCache ->
    Emitter.combine(
            firstSource = first,
            secondSource = second
    ) { insetsA, insetsB ->
        val left = combinator(insetsA.left, insetsB.left)
        val top = combinator(insetsA.top, insetsB.top)
        val right = combinator(insetsA.right, insetsB.right)
        val bottom = combinator(insetsA.bottom, insetsB.bottom)
        resultCache.updateMutable(
                createInitialValue = { MutableInsets(left, top, right, bottom) },
                updateValue = { set(left, top, right, bottom) }
        )
    }
}