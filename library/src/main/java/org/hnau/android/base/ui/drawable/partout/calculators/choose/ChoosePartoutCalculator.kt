package org.hnau.android.base.ui.drawable.partout.calculators.choose

import android.graphics.Rect
import org.hnau.android.base.ui.drawable.partout.PartoutCalculator
import org.hnau.android.base.ui.drawable.partout.calculators.NoResizeWrapperSizeCalculator


abstract class ChoosePartoutCalculator : PartoutCalculator, NoResizeWrapperSizeCalculator {

    override fun calcContentBounds(
        parentWidth: Int,
        parentHeight: Int,
        contentPreferredWidth: Int,
        contentPreferredHeight: Int,
        result: Rect
    ) = choosePartoutCalculator(
        parentWidth = parentWidth,
        parentHeight = parentHeight,
        contentPreferredWidth = contentPreferredWidth,
        contentPreferredHeight = contentPreferredHeight
    ).calcContentBounds(
        parentWidth = parentWidth,
        parentHeight = parentHeight,
        contentPreferredWidth = contentPreferredWidth,
        contentPreferredHeight = contentPreferredHeight,
        result = result
    )

    protected abstract fun choosePartoutCalculator(
        parentWidth: Int,
        parentHeight: Int,
        contentPreferredWidth: Int,
        contentPreferredHeight: Int
    ): PartoutCalculator

}