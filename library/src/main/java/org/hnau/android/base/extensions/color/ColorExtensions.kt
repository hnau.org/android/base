package org.hnau.android.base.extensions.color

import android.graphics.Color
import androidx.annotation.ColorInt
import androidx.annotation.FloatRange


@ColorInt
fun @receiver:ColorInt Int.shiftTo(
        @ColorInt target: Int,
        @FloatRange(from = 0.0, to = 1.0) percentage: Float
) = Color.argb(
        alpha + ((target.alpha - alpha) * percentage).toInt(),
        red + ((target.red - red) * percentage).toInt(),
        green + ((target.green - green) * percentage).toInt(),
        blue + ((target.blue - blue) * percentage).toInt()
)