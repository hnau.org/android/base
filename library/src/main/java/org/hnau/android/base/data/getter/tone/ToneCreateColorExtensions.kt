package org.hnau.android.base.data.getter.tone

import androidx.annotation.FloatRange
import androidx.annotation.IntRange
import org.hnau.android.base.extensions.color.HSV
import org.hnau.android.base.extensions.color.argb
import org.hnau.android.base.extensions.color.grey
import org.hnau.android.base.extensions.color.rgb
import org.hnau.android.base.extensions.color.toRGB


fun Tones.createARGB(
    @IntRange(from = 0L, to = 255L) alpha: Int,
    @IntRange(from = 0L, to = 255L) red: Int,
    @IntRange(from = 0L, to = 255L) green: Int,
    @IntRange(from = 0L, to = 255L) blue: Int
) = argb(alpha, red, green, blue).toTone()

fun Tones.createARGB(
    @FloatRange(from = 0.0, to = 1.0) alpha: Float,
    @FloatRange(from = 0.0, to = 1.0) red: Float,
    @FloatRange(from = 0.0, to = 1.0) green: Float,
    @FloatRange(from = 0.0, to = 1.0) blue: Float
) = argb(alpha, red, green, blue).toTone()

fun Tones.createRGB(
    @IntRange(from = 0L, to = 255L) red: Int,
    @IntRange(from = 0L, to = 255L) green: Int,
    @IntRange(from = 0L, to = 255L) blue: Int
) = rgb(red, green, blue).toTone()

fun Tones.createRGB(
    @FloatRange(from = 0.0, to = 1.0) red: Float,
    @FloatRange(from = 0.0, to = 1.0) green: Float,
    @FloatRange(from = 0.0, to = 1.0) blue: Float
) = rgb(red, green, blue).toTone()

fun Tones.createHSV(
    @FloatRange(from = 0.0, to = 1.0) hue: Float,
    @FloatRange(from = 0.0, to = 1.0) saturation: Float,
    @FloatRange(from = 0.0, to = 1.0) light: Float
) = HSV(hue, saturation, light).toRGB().toTone()

fun Tones.createHSV(
    @IntRange(from = 0L, to = 255L) hue: Int,
    @IntRange(from = 0L, to = 255L) saturation: Int,
    @IntRange(from = 0L, to = 255L) value: Int
) = HSV(hue, saturation, value).toRGB().toTone()

fun Tones.createGrey(
    @IntRange(from = 0L, to = 255L) alpha: Int,
    @IntRange(from = 0L, to = 255L) lightness: Int
) = grey(alpha, lightness).toTone()

fun Tones.createGrey(
    @FloatRange(from = 0.0, to = 1.0) alpha: Float,
    @FloatRange(from = 0.0, to = 1.0) lightness: Float
) = grey(alpha, lightness).toTone()

fun Tones.createGrey(@IntRange(from = 0L, to = 255L) lightness: Int) =
    grey(lightness).toTone()

fun Tones.createGrey(@FloatRange(from = 0.0, to = 1.0) lightness: Float) =
    grey(lightness).toTone()