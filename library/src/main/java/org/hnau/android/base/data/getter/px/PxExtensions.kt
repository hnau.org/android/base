package org.hnau.android.base.data.getter.px

import android.content.Context
import kotlin.math.max
import kotlin.math.min

inline fun Px.mapFull(
        crossinline converter: (Context, Float) -> Float
) = Px { i ->
    val io = this.getRawPx(i)
    converter(i, io)
}

inline fun Px.map(
        crossinline converter: (Float) -> Float
) = mapFull { _, io ->
    converter(io)
}

inline fun Px.combineFull(
        other: Px,
        crossinline combinator: (Context, Float, Float) -> Float
) = Px { context ->
    val a = this.getRawPx(context)
    val b = other.getRawPx(context)
    combinator(context, a, b)
}

inline fun Px.combine(
        other: Px,
        crossinline combinator: (Float, Float) -> Float
) = combineFull(other) { _, a, b ->
    combinator(a, b)
}

operator fun Px.plus(other: Px): Px =
        combine(other, Float::plus)

operator fun Px.minus(other: Px): Px =
        combine(other, Float::minus)

operator fun Px.unaryMinus(): Px =
        map(Float::unaryMinus)

operator fun Px.unaryPlus() =
        this


operator fun Px.times(factor: Number): Px =
        map { value -> value * factor.toFloat() }

operator fun Px.div(factor: Number): Px =
        map { value -> value / factor.toFloat() }

operator fun Px.rem(factor: Number): Px =
        map { value -> value % factor.toFloat() }


fun Px.coerceAtLeast(minimumValue: Px): Px =
        combine(minimumValue, Float::coerceAtLeast)

fun Px.coerceAtMost(maximumValue: Px): Px =
        combine(maximumValue, Float::coerceAtMost)

fun Px.coerceIn(minimumValue: Px, maximumValue: Px): Px =
        mapFull { context, value ->
            value.coerceIn(minimumValue(context), maximumValue(context))
        }

fun Px.Companion.max(first: Px, second: Px): Px =
        first.combine(second) { a, b -> max(a, b) }

fun Px.Companion.min(first: Px, second: Px): Px =
        first.combine(second) { a, b -> min(a, b) }

fun Px.max(other: Px) = Px.Companion.max(this, other)
fun Px.min(other: Px) = Px.Companion.min(this, other)