package org.hnau.android.base.extensions.view.text

import android.text.Editable
import android.widget.TextView
import org.hnau.base.extensions.boolean.ifFalse
import org.hnau.base.extensions.it
import org.hnau.emitter.Emitter
import org.hnau.emitter.extensions.useWhen
import org.hnau.emitter.observing.push.always.AlwaysEmitter
import org.hnau.emitter.utils.mutable.MutableEmitter


fun TextView.createTextEmitter() =
        object : AlwaysEmitter<CharSequence>() {

            override val value: CharSequence
                get() = text

            init {
                addTextChangedListener(
                        TextWatcher(afterTextChanged = { onChanged() })
                )
            }

        }

inline fun <T> TextView.connectEmitter(
        isActive: Emitter<Boolean>,
        emitter: MutableEmitter<T>,
        crossinline setText: TextView.(T) -> Unit,
        crossinline resolveEditable: (Editable) -> T,
        crossinline compare: (CharSequence, T) -> Boolean
) {
    emitter.emitter
            .useWhen(isActive)
            .observe { newText ->
                (compare(text, newText)).ifFalse { setText(newText) }
            }
    addTextChangedListener(
            TextWatcher(afterTextChanged = { emitter.setValue(resolveEditable(it)) })
    )
}

fun TextView.connectStringEmitter(
        isActive: Emitter<Boolean>,
        emitter: MutableEmitter<String>
) = connectEmitter(
        isActive = isActive,
        emitter = emitter,
        resolveEditable = Editable::toString,
        setText = TextView::setText,
        compare = { old, new -> old.toString() == new }
)

fun TextView.connectEditableEmitter(
        isActive: Emitter<Boolean>,
        emitter: MutableEmitter<Editable>
) = connectEmitter(
        isActive = isActive,
        emitter = emitter,
        resolveEditable = ::it,
        setText = TextView::setText,
        compare = { old, new -> old.toString() == new.toString() }
)