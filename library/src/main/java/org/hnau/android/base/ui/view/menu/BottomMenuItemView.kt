package org.hnau.android.base.ui.view.menu

import android.content.Context
import android.graphics.Canvas
import android.graphics.Paint
import android.graphics.Point
import android.graphics.PointF
import android.view.View
import org.hnau.android.base.data.edges.typed.centerX
import org.hnau.android.base.data.edges.typed.centerY
import org.hnau.android.base.data.getter.picture.Picture
import org.hnau.android.base.data.getter.px.sp
import org.hnau.android.base.data.getter.px.unaryMinus
import org.hnau.android.base.extensions.paint
import org.hnau.android.base.extensions.setBoundsByCenter
import org.hnau.android.base.extensions.view.createBounds
import org.hnau.android.base.extensions.view.touch.TouchEventType
import org.hnau.android.base.extensions.view.touch.createTouchEmitter
import org.hnau.android.base.extensions.view.touch.mapOnClick
import org.hnau.android.base.ui.animation.smoothFloat
import org.hnau.android.base.ui.decoration.ViewDecorations
import org.hnau.android.base.ui.decoration.composite
import org.hnau.android.base.ui.decoration.create
import org.hnau.android.base.ui.decoration.receivingViewDecoration
import org.hnau.android.base.ui.decoration.ripple.ripple
import org.hnau.android.base.ui.decoration.ripple.shape.RippleShape
import org.hnau.android.base.ui.decoration.ripple.shape.borderless
import org.hnau.android.base.ui.shape.CanvasShape
import org.hnau.android.base.ui.shape.rect
import org.hnau.android.base.ui.view.utils.state.createStateEmitters
import org.hnau.base.data.associator.Associator
import org.hnau.base.data.associator.byHashMap
import org.hnau.base.data.associator.resolveWeakReference
import org.hnau.base.data.associator.toAutoAssociator
import org.hnau.base.data.delegate.mutable.MutableDelegate
import org.hnau.base.data.delegate.mutable.cache
import org.hnau.base.extensions.boolean.ifFalse
import org.hnau.base.extensions.checkNullable
import org.hnau.base.extensions.number.asPercentageInter
import org.hnau.emitter.Emitter
import org.hnau.emitter.extensions.filter
import org.hnau.emitter.extensions.map
import org.hnau.emitter.extensions.toFloat
import org.hnau.emitter.extensions.useWhen
import org.hnau.emitter.observing.push.lateinit.combine
import java.lang.ref.WeakReference


class BottomMenuItemView(
        context: Context,
        private val item: BottomMenuItem,
        private val info: BottomMenuViewInfo
) : View(
        context
) {

    private val titleTextSize = 12.sp
    private val titleOffsetY = 4.sp

    private val iconOffsetY = -8.sp

    private val titlePaint = paint {
        textSize = titleTextSize(context)
        color = item.design.title.tone(context)
        textAlign = Paint.Align.CENTER
    }

    private val iconProvider = Associator
            .byHashMap<BottomMenuItem.State, WeakReference<Picture>>()
            .resolveWeakReference()
            .toAutoAssociator { state -> item.design.createIcon(state) }

    private val bounds = createBounds()

    private val canvasShape = CanvasShape.rect(bounds)

    private val state = createStateEmitters()

    private val itemState = item
            .behavior
            .state

    private var currentItemState = BottomMenuItem.State.inactive

    val activePercentage = itemState
            .map(BottomMenuItem.State::isActive)
            .toFloat()
            .smoothFloat(info.switchingStateDuration)

    private val touchEvents = createTouchEmitter()
            .filter { touchEvent ->
                touchEvent.type != TouchEventType.down || currentItemState != BottomMenuItem.State.active
            }

    private val decorations = receivingViewDecoration(
            isActive = state.isActive,
            decorationEmitter = ViewDecorations.composite(
                    ViewDecorations.ripple(
                            context = context,
                            info = info.rippleInfo,
                            touchEvents = touchEvents,
                            shape = RippleShape.borderless(bounds)
                    ),
                    Point().let { iconCenter ->
                        MutableDelegate.cache<BottomMenuItem.State>().let { stateCache ->
                            ViewDecorations.create {
                                iconProvider(stateCache.get())(context).run {
                                    setBoundsByCenter(iconCenter.x, iconCenter.y)
                                    draw(this@create)
                                }
                            }.let { iconDecoration ->
                                Emitter.combine(
                                        firstSource = bounds,
                                        secondSource = activePercentage
                                ) { bounds, activePercentage ->
                                    stateCache.set(BottomMenuItem.State[activePercentage > 0])
                                    iconCenter.set(
                                            bounds.centerX,
                                            bounds.centerY + (iconOffsetY(context) * activePercentage).toInt()
                                    )
                                    iconDecoration
                                }
                            }
                        }
                    },
                    PointF().let { textDrawPoint ->
                        ViewDecorations.create {
                            textDrawPoint.y.isNaN().ifFalse {
                                drawText(item.design.title.text(context), textDrawPoint.x, textDrawPoint.y, titlePaint)
                            }
                        }.let { iconDecoration ->
                            Emitter.combine(
                                    firstSource = bounds,
                                    secondSource = activePercentage
                            ) { bounds, activePercentage ->
                                activePercentage
                                        .takeIf { it > 0 }
                                        .checkNullable(
                                                ifNotNull = { positiveActivePercentage ->
                                                    val titleHeight = titlePaint.fontMetrics.run { descent - ascent }
                                                    val titleActiveY = bounds.centerY + titleOffsetY(context) + titleHeight
                                                    val titleInactiveY = bounds.yMax + titleHeight
                                                    textDrawPoint.set(
                                                            bounds.centerX.toFloat(),
                                                            positiveActivePercentage.asPercentageInter(titleInactiveY, titleActiveY)
                                                    )
                                                },
                                                ifNull = {
                                                    textDrawPoint.set(Float.NaN, Float.NaN)
                                                }
                                        )
                                iconDecoration
                            }
                        }
                    }
            )
    )

    init {

        touchEvents
                .mapOnClick(canvasShape)
                .observe { item.behavior.onClick() }

        itemState
                .useWhen(state.isActive)
                .observe { currentItemState = it }

    }

    override fun draw(canvas: Canvas) {
        super.draw(canvas)
        decorations(canvas)
    }

}