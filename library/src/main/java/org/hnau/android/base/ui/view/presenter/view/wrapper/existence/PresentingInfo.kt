package org.hnau.android.base.ui.view.presenter.view.wrapper.existence

import org.hnau.android.base.data.direction.Direction
import org.hnau.android.base.ui.animation.emergence.offset.EmergencesInfo


data class PresentingInfo(
    val direction: Direction = Direction.center,
    val emergencesInfo: EmergencesInfo = EmergencesInfo()
) {

    companion object;

}