package org.hnau.android.base.ui.view.utils


interface GoBackHandler {

    fun handleGoBack() = false

}