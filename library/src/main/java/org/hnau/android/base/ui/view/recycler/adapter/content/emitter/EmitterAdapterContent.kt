package org.hnau.android.base.ui.view.recycler.adapter.content.emitter

import androidx.recyclerview.widget.ListUpdateCallback
import org.hnau.android.base.ui.view.recycler.adapter.content.AdapterContent
import org.hnau.android.base.ui.view.recycler.adapter.content.emitter.notifier.NewContentNotifier
import org.hnau.emitter.Emitter
import org.hnau.emitter.extensions.useWhen


class EmitterAdapterContent<T>(
    adapter: ListUpdateCallback,
    viewIsActive: Emitter<Boolean>,
    emitter: Emitter<List<T>>,
    newContentNotifier: (ListUpdateCallback) -> NewContentNotifier<T>
) : AdapterContent<T> {

    companion object;

    private val newContentNotifier = newContentNotifier(adapter)

    private var content: List<T> = emptyList()
        set(value) {
            val oldValue = field
            field = value
            newContentNotifier.onContentChanged(oldValue, value)
        }

    init {
        emitter.useWhen(viewIsActive).observe { content = it }
    }

    override val size
        get() = content.size

    override fun getItem(position: Int) =
        content[position]

}

fun <T> AdapterContent.Companion.emitter(
    viewIsActive: Emitter<Boolean>,
    emitter: Emitter<List<T>>,
    newContentNotifier: (ListUpdateCallback) -> NewContentNotifier<T>
): (ListUpdateCallback) -> AdapterContent<T> = { adapter ->
    EmitterAdapterContent(
        adapter = adapter,
        viewIsActive = viewIsActive,
        emitter = emitter,
        newContentNotifier = newContentNotifier
    )
}