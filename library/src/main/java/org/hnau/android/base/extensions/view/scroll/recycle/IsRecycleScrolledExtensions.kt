package org.hnau.android.base.extensions.view.scroll.recycle

import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import org.hnau.android.base.extensions.view.group.recycle.linearLayoutManager
import org.hnau.base.extensions.number.takeIfPositive


val RecyclerView.isRecycleScrolledToLeft: Boolean
    get() {
        val itemsCount = itemsCount.takeIfPositive() ?: return false
        val layoutManager = linearLayoutManager
        val firstItemChild = layoutManager.getFirstChildIfItIsForFirstItem(itemsCount - 1)
            ?: return true
        return layoutManager.getDecoratedLeft(firstItemChild) < 0
    }

val RecyclerView.isRecycleScrolledToTop: Boolean
    get() {
        val itemsCount = itemsCount.takeIfPositive() ?: return false
        val layoutManager = linearLayoutManager
        val firstItemChild = layoutManager.getFirstChildIfItIsForFirstItem(itemsCount - 1)
            ?: return true
        return layoutManager.getDecoratedTop(firstItemChild) < 0
    }

val RecyclerView.isRecycleScrolledToRight: Boolean
    get() {
        val itemsCount = itemsCount.takeIfPositive() ?: return false
        val layoutManager = linearLayoutManager
        val lastItemChild = layoutManager.getLastChildIfItIsForLastItem(itemsCount - 1)
            ?: return true
        return layoutManager.getDecoratedRight(lastItemChild) > width
    }

val RecyclerView.isRecycleScrolledToBottom: Boolean
    get() {
        val itemsCount = itemsCount.takeIfPositive() ?: return false
        val layoutManager = linearLayoutManager
        val lastItemChild = layoutManager.getLastChildIfItIsForLastItem(itemsCount - 1)
            ?: return true
        return layoutManager.getDecoratedBottom(lastItemChild) > height
    }

private fun LinearLayoutManager.getFirstChildIfItIsForFirstItem(lastItemPosition: Int) =
    if (reverseLayout) getRightOrBottomChildIfItIsForLastItem(lastItemPosition) else getLeftOtTopChildIfItIsForFirstItem()

private fun LinearLayoutManager.getLastChildIfItIsForLastItem(lastItemPosition: Int) =
    if (reverseLayout) getLeftOtTopChildIfItIsForFirstItem() else getRightOrBottomChildIfItIsForLastItem(lastItemPosition)

private fun LinearLayoutManager.getLeftOtTopChildIfItIsForFirstItem() =
    if (findFirstVisibleItemPosition() > 0) null else findViewByPosition(0)

private fun LinearLayoutManager.getRightOrBottomChildIfItIsForLastItem(lastItemPosition: Int) =
    if (findLastVisibleItemPosition() < lastItemPosition) null else findViewByPosition(lastItemPosition)

private val RecyclerView.itemsCount: Int
    get() = adapter?.itemCount ?: 0