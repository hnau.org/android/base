package org.hnau.android.base.extensions

import android.util.DisplayMetrics


fun DisplayMetrics.clone() =
    DisplayMetrics().apply { setTo(this@clone) }

@Suppress("ReplaceWithOperatorAssignment")
fun DisplayMetrics.scale(
    scaleFactor: Float
) = clone().apply {
    density = density * scaleFactor
    densityDpi = (densityDpi * scaleFactor).toInt()
    scaledDensity = scaledDensity * scaleFactor
    xdpi = xdpi * scaleFactor
    ydpi = ydpi * scaleFactor
}