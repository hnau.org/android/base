package org.hnau.android.base.data.insets

import org.hnau.android.base.data.edges.Edges


class MutableInsets<T>(
        override var left: T,
        override var top: T,
        override var right: T,
        override var bottom: T
) : Insets<T> {

    constructor(
            source: Insets<T>
    ) : this(
            left = source.left,
            top = source.top,
            right = source.right,
            bottom = source.bottom
    )

    constructor(
            horizontal: T,
            vertical: T
    ) : this(
            left = horizontal,
            top = vertical,
            right = horizontal,
            bottom = vertical
    )

    constructor(
            value: T
    ) : this(
            horizontal = value,
            vertical = value
    )

    fun set(
            left: T,
            top: T,
            right: T,
            bottom: T,
    ) {
        this.left = left
        this.top = top
        this.right = right
        this.bottom = bottom
    }

    fun set(
            source: Insets<T>
    ) = set(
            left = source.left,
            top = source.top,
            right = source.right,
            bottom = source.bottom
    )

    fun set(
            horizontal: T,
            vertical: T
    ) = set(
            left = horizontal,
            top = vertical,
            right = horizontal,
            bottom = vertical
    )

    fun set(
            value: T
    ) = set(
            horizontal = value,
            vertical = value
    )

}