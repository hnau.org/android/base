package org.hnau.android.base.ui.animation.curver

import org.hnau.emitter.Emitter
import org.hnau.emitter.extensions.map


inline fun Emitter<Float>.interpolate(
        crossinline curver: Curver
) = map { value ->
    curver(value)
}

fun Emitter<Float>.interpolateAccelerateDecelerate() =
        interpolate(Curvers.accelerateDecelerate)

fun Emitter<Float>.interpolateAccelerate() =
        interpolate(Curvers.accelerate)

fun Emitter<Float>.interpolateDecelerate() =
        interpolate(Curvers.decelerate)

fun Emitter<Float>.interpolateOvershoot() =
        interpolate(Curvers.overshoot)

fun Emitter<Float>.interpolateBounce() =
        interpolate(Curvers.bounce)