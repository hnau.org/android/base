package org.hnau.android.base.extensions.view

import android.view.View


val View.paddingHorizontalSum
    get() = paddingLeft + paddingRight

val View.paddingVerticalSum
    get() = paddingTop + paddingBottom