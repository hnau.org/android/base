package org.hnau.android.base.data.insets

import org.hnau.android.base.data.edges.Edges
import org.hnau.android.base.data.edges.MutableEdges
import org.hnau.base.data.possible.lateinit.Lateinit
import org.hnau.base.data.possible.lateinit.unsafe
import org.hnau.base.data.possible.updateMutable
import org.hnau.emitter.Emitter
import org.hnau.emitter.extensions.map
import org.hnau.emitter.observing.push.lateinit.combineWith


inline fun <T> Edges<T>.applyInsets(
        insets: Insets<T>,
        plus: (T, T) -> T,
        minus: (T, T) -> T
) = Edges(
        xMin = plus(xMin, insets.left),
        yMin = plus(yMin, insets.top),
        xMax = minus(xMax, insets.right),
        yMax = minus(yMax, insets.bottom)
)

@PublishedApi
internal inline fun <T> InsetsApplier(
        crossinline plus: (T, T) -> T,
        crossinline minus: (T, T) -> T
): (Edges<T>, Insets<T>) -> Edges<T> = Lateinit.unsafe<MutableEdges<T>>().let { resultCache ->
    val result: (Edges<T>, Insets<T>) -> Edges<T> = { edges, insets ->
        resultCache.updateMutable(
                createInitialValue = {
                    MutableEdges(
                            xMin = plus(edges.xMin, insets.left),
                            yMin = plus(edges.yMin, insets.top),
                            xMax = minus(edges.xMax, insets.right),
                            yMax = minus(edges.yMax, insets.bottom)
                    )
                },
                updateValue = {
                    xMin = plus(edges.xMin, insets.left)
                    yMin = plus(edges.yMin, insets.top)
                    xMax = minus(edges.xMax, insets.right)
                    yMax = minus(edges.yMax, insets.bottom)
                }
        )
    }
    result
}

@Suppress("ComplexRedundantLet")
inline fun <T> Emitter<Edges<T>>.applyInsets(
        insets: Insets<T>,
        crossinline plus: (T, T) -> T,
        crossinline minus: (T, T) -> T
) = InsetsApplier(plus, minus).let { applier ->
    map { edges -> applier(edges, insets) }
}

@Suppress("ComplexRedundantLet")
inline fun <T> Emitter<Edges<T>>.applyInsets(
        insets: Emitter<Insets<T>>,
        crossinline plus: (T, T) -> T,
        crossinline minus: (T, T) -> T
) = InsetsApplier(plus, minus).let { applier ->
    combineWith(insets) { edges, insetsValue ->
        applier(edges, insetsValue)
    }
}

@Suppress("ComplexRedundantLet")
inline fun <T> Edges<T>.applyInsets(
        insets: Emitter<Insets<T>>,
        crossinline plus: (T, T) -> T,
        crossinline minus: (T, T) -> T
) = InsetsApplier(plus, minus).let { applier ->
    insets.map { insetsValue -> applier(this, insetsValue) }
}