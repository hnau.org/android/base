package org.hnau.android.base.ui.shadow

import android.content.Context
import android.graphics.Paint
import org.hnau.android.base.extensions.setShadowInfo
import org.hnau.emitter.Emitter
import org.hnau.emitter.extensions.map
import org.hnau.emitter.observing.push.lateinit.combineWith


fun Paint.setShadowInfo(
        context: Context,
        shadowInfo: ButtonShadowInfo,
        pressedPercentage: Float
) = with(shadowInfo) {
    setShadowInfo(
            radius = getRadius(context, pressedPercentage),
            dx = getDx(context, pressedPercentage),
            dy = getDy(context, pressedPercentage),
            shadowColor = getShadowColor(context, pressedPercentage)
    )
}

fun Paint.setShadowInfo(
        context: Context,
        shadowInfo: ButtonShadowInfo,
        pressedPercentageEmitter: Emitter<Float>
) = pressedPercentageEmitter.map { pressedPercentage ->
    apply { setShadowInfo(context, shadowInfo, pressedPercentage) }
}

fun Emitter<Paint>.setShadowInfo(
        context: Context,
        shadowInfo: ButtonShadowInfo,
        pressedPercentageEmitter: Emitter<Float>
) = combineWith(pressedPercentageEmitter) { paint, pressedPercentage ->
    paint.apply { setShadowInfo(context, shadowInfo, pressedPercentage) }
}
