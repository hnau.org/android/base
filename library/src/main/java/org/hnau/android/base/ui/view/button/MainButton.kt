package org.hnau.android.base.ui.view.button

import android.annotation.SuppressLint
import android.content.Context
import android.graphics.Canvas
import android.graphics.Point
import android.view.View
import android.view.ViewGroup
import org.hnau.android.base.data.getter.picture.Picture
import org.hnau.android.base.data.getter.px.pxInt
import org.hnau.android.base.data.insets.map
import org.hnau.android.base.data.insets.typed.applyInsets
import org.hnau.android.base.data.insets.typed.horizontalSum
import org.hnau.android.base.data.insets.typed.verticalSum
import org.hnau.android.base.extensions.paint
import org.hnau.android.base.extensions.view.base.setLayerTypeSoftware
import org.hnau.android.base.extensions.view.createBounds
import org.hnau.android.base.extensions.view.group.children.view
import org.hnau.android.base.extensions.view.measureSpecFit
import org.hnau.android.base.extensions.view.paddingHorizontalSum
import org.hnau.android.base.extensions.view.paddingVerticalSum
import org.hnau.android.base.extensions.view.touch.createTouchEmitter
import org.hnau.android.base.extensions.view.touch.mapIsPressed
import org.hnau.android.base.extensions.view.touch.mapOnClick
import org.hnau.android.base.ui.animation.smoothFloat
import org.hnau.android.base.ui.decoration.ViewDecorations
import org.hnau.android.base.ui.decoration.composite
import org.hnau.android.base.ui.decoration.drawable
import org.hnau.android.base.ui.decoration.paint
import org.hnau.android.base.ui.decoration.receivingViewDecoration
import org.hnau.android.base.ui.shadow.setShadowInfo
import org.hnau.android.base.ui.shape.CanvasShape
import org.hnau.android.base.ui.shape.oval
import org.hnau.android.base.ui.view.utils.state.createStateEmitters
import org.hnau.emitter.extensions.listen
import org.hnau.emitter.extensions.map
import org.hnau.emitter.extensions.toFloat


@SuppressLint("ViewConstructor")
class MainButton(
        context: Context,
        picture: Picture,
        info: MainButtonInfo,
        onClick: () -> Unit
) : View(
        context
) {

    private val preferredSize = info.shadow.insets.map { it(context) }.let { insets ->
        info.diameter(context).let { diameter ->
            Point(
                    (diameter + insets.horizontalSum).toInt(),
                    (diameter + insets.verticalSum).toInt()
            )
        }
    }

    private val contentBounds = createBounds().run {
        info.shadow.insets.map { it.pxInt(context) }.let { shadowInsets ->
            map { it.applyInsets(shadowInsets) }
        }
    }

    private val canvasShape = CanvasShape.oval(
            bounds = contentBounds
    )

    private val touchEvents = createTouchEmitter()
    private val pressedPercentage = touchEvents.mapIsPressed(canvasShape).toFloat().smoothFloat()

    private val state = createStateEmitters()

    private val decorations = receivingViewDecoration(
            isActive = state.isActive,
            decorationEmitter = ViewDecorations.composite(
                    ViewDecorations.paint(
                            paint = paint {
                                color = info.background(context)
                            }.setShadowInfo(
                                    context = context,
                                    shadowInfo = info.shadow,
                                    pressedPercentageEmitter = pressedPercentage
                            ),
                            shape = canvasShape
                    ),
                    ViewDecorations.drawable(
                            bounds = contentBounds,
                            drawable = picture(context)
                    )
            )
    )

    init {
        setLayerTypeSoftware()

        touchEvents
                .mapOnClick(canvasShape)
                .listen(onClick)

    }

    override fun draw(canvas: Canvas) {
        super.draw(canvas)
        decorations(canvas)
    }

    override fun onMeasure(
            widthMeasureSpec: Int,
            heightMeasureSpec: Int
    ) = setMeasuredDimension(
            measureSpecFit(
                    measureSpec = widthMeasureSpec,
                    size = preferredSize.x + paddingHorizontalSum
            ),
            measureSpecFit(
                    measureSpec = heightMeasureSpec,
                    size = preferredSize.y + paddingVerticalSum
            )
    )

}

inline fun ViewGroup.mainButton(
        noinline picture: Picture,
        info: MainButtonInfo,
        noinline onClick: () -> Unit,
        childConfigurator: MainButton.() -> Unit = {}
) = view(
        MainButton(
                context = context,
                picture = picture,
                info = info,
                onClick = onClick
        ),
        childConfigurator
)
