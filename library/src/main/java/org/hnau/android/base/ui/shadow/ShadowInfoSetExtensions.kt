package org.hnau.android.base.ui.shadow

import android.content.Context
import android.graphics.Paint
import org.hnau.android.base.extensions.setShadowInfo


fun Paint.setShadowInfo(
        context: Context,
        shadowInfo: ShadowInfo
) = with(shadowInfo) {
    setShadowInfo(
            radius = radius(context),
            dx = dx(context),
            dy = dy(context),
            shadowColor = tone(context)
    )
}