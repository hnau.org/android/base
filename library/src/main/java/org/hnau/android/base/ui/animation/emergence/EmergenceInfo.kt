package org.hnau.android.base.ui.animation.emergence

import org.hnau.android.base.ui.animation.animation
import org.hnau.android.base.ui.animation.emergence.offset.calculator.OffsetCalculator
import org.hnau.android.base.ui.animation.emergence.offset.calculator.side
import org.hnau.base.data.time.Time


data class EmergenceInfo(
    val duration: Time = Time.animation.default,
    val offsetCalculator: OffsetCalculator = OffsetCalculator.side(),
    val fade: EmergenceFadeInfo = EmergenceFadeInfo()
) {

    companion object

    fun reverse() =
        copy(offsetCalculator = offsetCalculator.reverse())

}