package org.hnau.android.base.ui.color.material

import android.graphics.Color


open class MaterialList(
    val hue: MaterialHue,
    vararg values: Pair<MaterialLightness, String>
) {

    val values =
        values.associate { (lightness, colorCode) -> lightness to Color.parseColor(colorCode) }

    operator fun get(lightness: MaterialLightness) =
        values[lightness] ?: throw IllegalArgumentException("There is no $lightness lightness for $hue hue")

    val v50 get() = get(MaterialLightness.v50)
    val v100 get() = get(MaterialLightness.v100)
    val v200 get() = get(MaterialLightness.v200)
    val v300 get() = get(MaterialLightness.v300)
    val v400 get() = get(MaterialLightness.v400)
    val v500 get() = get(MaterialLightness.v500)
    val v600 get() = get(MaterialLightness.v600)
    val v700 get() = get(MaterialLightness.v700)
    val v800 get() = get(MaterialLightness.v800)
    val v900 get() = get(MaterialLightness.v900)
    val a100 get() = get(MaterialLightness.a100)
    val a200 get() = get(MaterialLightness.a200)
    val a400 get() = get(MaterialLightness.a400)
    val a700 get() = get(MaterialLightness.a700)

}