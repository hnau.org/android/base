package org.hnau.android.base.extensions.color

import android.graphics.Color
import androidx.annotation.ColorInt
import androidx.annotation.FloatRange
import androidx.annotation.IntRange


@ColorInt
fun argb(
    @IntRange(from = 0L, to = 255L) alpha: Int,
    @IntRange(from = 0L, to = 255L) red: Int,
    @IntRange(from = 0L, to = 255L) green: Int,
    @IntRange(from = 0L, to = 255L) blue: Int
) = Color.argb(alpha, red, green, blue)

@ColorInt
fun argb(
    @FloatRange(from = 0.0, to = 1.0) alpha: Float,
    @FloatRange(from = 0.0, to = 1.0) red: Float,
    @FloatRange(from = 0.0, to = 1.0) green: Float,
    @FloatRange(from = 0.0, to = 1.0) blue: Float
) = argb(
    alpha = ColorUtils.percentageToValue(alpha),
    red = ColorUtils.percentageToValue(red),
    green = ColorUtils.percentageToValue(green),
    blue = ColorUtils.percentageToValue(blue)
)

@ColorInt
fun rgb(
    @IntRange(from = 0L, to = 255L) red: Int,
    @IntRange(from = 0L, to = 255L) green: Int,
    @IntRange(from = 0L, to = 255L) blue: Int
) = Color.rgb(red, green, blue)

@ColorInt
fun rgb(
    @FloatRange(from = 0.0, to = 1.0) red: Float,
    @FloatRange(from = 0.0, to = 1.0) green: Float,
    @FloatRange(from = 0.0, to = 1.0) blue: Float
) = rgb(
    red = ColorUtils.percentageToValue(red),
    green = ColorUtils.percentageToValue(green),
    blue = ColorUtils.percentageToValue(blue)
)