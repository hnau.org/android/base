package org.hnau.android.base.ui.decoration.ripple.shape

import android.graphics.Canvas
import android.graphics.PointF
import org.hnau.android.base.ui.shape.CanvasShape
import org.hnau.base.extensions.number.asPercentageInter
import kotlin.math.sqrt


data class RippleShape(
        val calcCircleCenter: (touchPoint: PointF, growingPercentage: Float, result: PointF) -> Unit,
        val getMaxRadius: () -> Float,
        val clip: Canvas.() -> Unit,
        val checkContains: (x: Float, y: Float) -> Boolean
) {

    companion object

}