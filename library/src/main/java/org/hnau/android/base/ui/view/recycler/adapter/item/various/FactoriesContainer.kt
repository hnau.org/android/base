package org.hnau.android.base.ui.view.recycler.adapter.item.various

import org.hnau.base.extensions.boolean.ifTrue


class FactoriesContainer<T>(
    private val reservedItemViewTypesCount: Int
) {

    private val factories = ArrayList<ViewFactory<T>>(1)

    fun put(
        factory: ViewFactory<*>
    ) = synchronized(factories) {

        factories.forEachIndexed { index, viewFactory ->

            (factory === viewFactory)
                .ifTrue { return@synchronized index }

            (factory.javaClass == viewFactory.javaClass)
                .ifTrue { throw IllegalStateException("Duplicated instance of ${factory.javaClass.name}. Only one instance of each ViewFactory class expected to recycle View correct.") }

        }

        @Suppress("UNCHECKED_CAST")
        factories.add(factory as ViewFactory<T>)

        return@synchronized factories.size - 1 + reservedItemViewTypesCount
    }

    fun get(
        itemViewType: Int
    ) = synchronized(factories) {
        factories.getOrNull(itemViewType - reservedItemViewTypesCount)
            ?: throw IllegalStateException("ViewFactory for itemViewType $itemViewType not found")
    }

}