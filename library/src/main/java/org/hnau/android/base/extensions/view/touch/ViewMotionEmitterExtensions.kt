package org.hnau.android.base.extensions.view.touch

import android.view.MotionEvent
import org.hnau.android.base.ui.ContainsPointChecker
import org.hnau.emitter.Emitter
import org.hnau.emitter.extensions.callIfTrue
import org.hnau.emitter.extensions.map
import org.hnau.emitter.extensions.unique
import org.hnau.emitter.observing.push.lateinit.combineWith

@Suppress("DEPRECATION")
fun Emitter<MotionEvent>.toTouch() =
        TouchEvent().let { touchEvent ->
            map { motionEvent ->
                touchEvent.apply { update(motionEvent) }
            }
        }

fun Emitter<TouchEvent>.mapIsPressed(
        containsCheckerEmitter: Emitter<ContainsPointChecker>
): Emitter<Boolean> {

    var value = false

    return combineWith(containsCheckerEmitter) { event, containsChecker ->

        when (event.type) {
            TouchEventType.up,
            TouchEventType.cancel -> false
            TouchEventType.move -> value
            TouchEventType.down -> containsChecker.checkContainsPoint(event.x, event.y)
        }
                .also { value = it }

    }.unique()

}

fun Emitter<TouchEvent>.mapOnClick(
        containsCheckerEmitter: Emitter<ContainsPointChecker>
): Emitter<Unit> {

    var wasPressed = false

    return combineWith(containsCheckerEmitter) { event, containsChecker ->

        when (event.type) {
            TouchEventType.cancel -> {
                wasPressed = false
                false
            }
            TouchEventType.down -> {
                wasPressed = containsChecker.checkContainsPoint(event.x, event.y)
                false
            }
            TouchEventType.up -> {
                (wasPressed && containsChecker.checkContainsPoint(event.x, event.y)).also {
                    wasPressed = false
                }
            }
            TouchEventType.move -> false
        }

    }.callIfTrue()

}