package org.hnau.android.base.ui.animation
import org.hnau.base.extensions.setUnique
import org.hnau.emitter.extensions.listen
import org.hnau.emitter.extensions.useWhen
import org.hnau.emitter.observing.notifier.Notifier
import org.hnau.emitter.observing.push.always.content.ManualEmitter


class AnimationTicker : Notifier() {

    private val isAnimating =
        ManualEmitter(false)

    init {
        AnimationMetronome.useWhen(isAnimating).listen(::notifyObservers)
    }

    fun start() = isAnimating::value.setUnique(true)
    fun stop() = isAnimating::value.setUnique(false)

}