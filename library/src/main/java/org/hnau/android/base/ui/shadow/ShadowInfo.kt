package org.hnau.android.base.ui.shadow

import org.hnau.android.base.data.getter.px.Px
import org.hnau.android.base.data.getter.px.minus
import org.hnau.android.base.data.getter.px.plus
import org.hnau.android.base.data.getter.tone.Tone
import org.hnau.android.base.data.insets.Insets


data class ShadowInfo(
        val dx: Px,
        val dy: Px,
        val radius: Px,
        val tone: Tone
) {

    val insets = Insets(
            left = radius - dx,
            top = radius - dy,
            right = radius + dx,
            bottom = radius + dy
    )

}