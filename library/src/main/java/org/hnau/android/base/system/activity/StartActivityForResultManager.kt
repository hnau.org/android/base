package org.hnau.android.base.system.activity

import android.app.Activity
import android.content.Intent
import kotlinx.coroutines.CompletableDeferred
import org.hnau.android.base.AndroidUtils
import org.hnau.base.data.AutoKeyCache
import org.hnau.base.extensions.checkNullable


class StartActivityForResultManager(
        private val getActivity: () -> Activity,
        private val onHandlerNotFound: (
                requestCode: Int,
                resultCode: Int,
                data: Intent?
        ) -> Unit
) {

    data class Response(
            val result: Int,
            val data: Intent?
    )

    @PublishedApi
    internal val handlers = AutoKeyCache<Int, CompletableDeferred<Response>>(
            AndroidUtils::generateId
    )

    suspend fun startActivityForResult(
            intent: Intent
    ): Response {
        val deferredResponse = CompletableDeferred<Response>()
        val requestCode = handlers.put(deferredResponse)
                ?: throw error("Unable generate request code")
        getActivity().startActivityForResult(intent, requestCode)
        return deferredResponse.await()
    }

    fun onActivityResult(
            requestCode: Int,
            resultCode: Int,
            data: Intent?
    ) {
        handlers.get(requestCode).checkNullable(
                ifNull = { onHandlerNotFound(requestCode, resultCode, data) },
                ifNotNull = { it.complete(Response(resultCode, data)) }
        )
    }

}