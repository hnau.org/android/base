package org.hnau.android.base.ui.view.input.tone

import org.hnau.android.base.data.getter.tone.Tone


data class InputViewTones(
        val text: Tone,
        val title: Tone,
        val border: Tone
) {

    companion object;

    constructor(
            tone: Tone
    ) : this(
            text = tone,
            title = tone,
            border = tone
    )

}