package org.hnau.android.base.ui.lightness.values


interface LightnessValues<T> {

    companion object;

    val light: T
    val dark: T
}