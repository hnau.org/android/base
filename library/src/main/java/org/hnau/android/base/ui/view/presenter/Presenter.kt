package org.hnau.android.base.ui.view.presenter

import android.content.Context
import android.graphics.Canvas
import android.graphics.Rect
import android.view.View
import android.view.ViewGroup
import org.hnau.android.base.extensions.view.changeMeasureSpecSize
import org.hnau.android.base.extensions.view.paddingHorizontalSum
import org.hnau.android.base.extensions.view.paddingVerticalSum
import org.hnau.android.base.ui.animation.emergence.offset.EmergencesInfo
import org.hnau.android.base.ui.view.presenter.info.PresenterInfo
import org.hnau.android.base.ui.view.presenter.transaction.Transaction
import org.hnau.android.base.ui.view.presenter.view.lifecycle.LifecycleParent
import org.hnau.android.base.ui.view.presenter.view.lifecycle.ViewLifecycle
import org.hnau.android.base.ui.view.presenter.view.lifecycle.ViewMeasuredSizeGetter
import org.hnau.android.base.ui.view.utils.state.createStateEmitters
import org.hnau.base.extensions.boolean.checkBoolean
import org.hnau.emitter.Emitter
import org.hnau.emitter.extensions.unique
import org.hnau.emitter.extensions.useWhen


abstract class Presenter<T>(
        context: Context,
        values: Emitter<T>,
        private val info: PresenterInfo = PresenterInfo()
) : ViewGroup(context), LifecycleParent {

    companion object;

    private val lifecycles = Lifecycles()
    private val layoutSlot = Rect()

    private val state = createStateEmitters()

    private lateinit var currentEmergencesInfo: EmergencesInfo

    override val isAnimationsAvailable
        get() = state.isForeground

    init {
        clipToPadding = false
        values
                .useWhen(state.isActive)
                .unique(::compareValues)
                .observe { value ->
                    val transaction = createPresentValueTransaction(value)
                    currentEmergencesInfo = transaction.view.emergencesInfo
                    present(transaction)
                }
    }

    protected open fun compareValues(first: T, second: T) = first == second

    protected abstract fun createPresentValueTransaction(value: T): Transaction

    private fun present(
            transaction: Transaction
    ) {
        lifecycles.hideAll()
        lifecycles.add(ViewLifecycle.create(this@Presenter, transaction))
        transaction.view.addView(this)
    }

    override fun onLifecycleFinished(lifecycle: ViewLifecycle) {
        lifecycle.removeView(this)
        lifecycles.remove(lifecycle)
    }

    override fun onLifecycleChanged() =
            info.contentSizeIndependent.checkBoolean(
                    ifTrue = ::invalidate,
                    ifFalse = ::requestLayout
            )

    private val drawChild: (Canvas, View) -> Unit =
            { canvas, child -> drawChild(canvas, child, drawingTime) }

    override fun dispatchDraw(canvas: Canvas) =
            lifecycles.draw(layoutSlot, canvas, currentEmergencesInfo, drawChild)

    override fun onLayout(changed: Boolean, l: Int, t: Int, r: Int, b: Int) {
        layoutSlot.set(
                paddingLeft,
                paddingTop,
                width - paddingRight,
                height - paddingBottom
        )
        lifecycles.layout(layoutSlot)
    }

    override fun onMeasure(widthMeasureSpec: Int, heightMeasureSpec: Int) {

        val childWidth = changeMeasureSpecSize(widthMeasureSpec, -paddingHorizontalSum)
        val childHeight = changeMeasureSpecSize(heightMeasureSpec, -paddingVerticalSum)
        lifecycles.measure(childWidth, childHeight)

        setMeasuredDimension(
                info.width.calcPresenterSize(
                        measureSpec = childWidth,
                        measureds = lifecycles.showingMeasurables,
                        measuredSizeExtractor = ViewMeasuredSizeGetter::measuredWidth
                ) - paddingHorizontalSum,
                info.height.calcPresenterSize(
                        measureSpec = childHeight,
                        measureds = lifecycles.showingMeasurables,
                        measuredSizeExtractor = ViewMeasuredSizeGetter::measuredHeight
                ) - paddingVerticalSum
        )

    }

}