package org.hnau.android.base.security

import android.security.keystore.KeyProperties
import org.hnau.base.data.mapper.Mapper
import java.io.ByteArrayInputStream
import java.io.ByteArrayOutputStream
import java.security.KeyStore
import javax.crypto.Cipher
import javax.crypto.CipherInputStream
import javax.crypto.CipherOutputStream


object RSAEncryptor {

    const val algorithm = KeyProperties.KEY_ALGORITHM_RSA
    const val blockMode = KeyProperties.BLOCK_MODE_ECB
    const val padding = KeyProperties.ENCRYPTION_PADDING_RSA_PKCS1

}

private const val transformation =
    "${RSAEncryptor.algorithm}/${RSAEncryptor.blockMode}/${RSAEncryptor.padding}"

fun Mapper.Companion.createRSAEncryptor(
    key: KeyStore.PrivateKeyEntry
) = Mapper<ByteArray, ByteArray>(
    direct = { encrypted ->
        Cipher.getInstance(transformation).run {
            init(Cipher.DECRYPT_MODE, key.privateKey)
            val cipherInputStream = CipherInputStream(
                ByteArrayInputStream(encrypted),
                this
            )
            val values: ArrayList<Byte> = ArrayList()
            var nextByte: Int
            while (cipherInputStream.read().also { nextByte = it } != -1) {
                values.add(nextByte.toByte())
            }
            val bytes = ByteArray(values.size)
            for (i in bytes.indices) {
                bytes[i] = values[i]
            }
            bytes
        }
    },
    reverse = { decrypted ->
        Cipher.getInstance(transformation).run {
            init(Cipher.ENCRYPT_MODE, key.certificate.publicKey)
            val outputStream = ByteArrayOutputStream()
            CipherOutputStream(outputStream, this).apply {
                write(decrypted)
                close()
            }
            outputStream.toByteArray()
        }
    }
)