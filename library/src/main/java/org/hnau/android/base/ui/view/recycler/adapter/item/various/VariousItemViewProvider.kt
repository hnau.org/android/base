package org.hnau.android.base.ui.view.recycler.adapter.item.various

import android.content.Context
import org.hnau.android.base.ui.view.recycler.adapter.item.ItemViewProvider
import org.hnau.emitter.Emitter


class VariousItemViewProvider<T>(
    private val getItemViewFactory: (item: T) -> ViewFactory<*>,
    reservedItemViewTypesCount: Int = 0
) : ItemViewProvider<T> {

    private val factories = FactoriesContainer<T>(
        reservedItemViewTypesCount = reservedItemViewTypesCount
    )

    override fun createItemView(
        context: Context,
        content: Emitter<T>,
        itemViewType: Int
    ) = factories
        .get(itemViewType)
        .createView(context, content)

    override fun getItemViewType(item: T): Int {
        val factory = getItemViewFactory(item)
        return factories.put(factory)
    }

}

fun <T> ItemViewProvider.Companion.various(
    getItemViewFactory: (item: T) -> ViewFactory<*>,
    reservedItemViewTypesCount: Int = 0
) = VariousItemViewProvider(
    getItemViewFactory = getItemViewFactory,
    reservedItemViewTypesCount = reservedItemViewTypesCount
)