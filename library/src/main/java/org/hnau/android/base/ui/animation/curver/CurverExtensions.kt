package org.hnau.android.base.ui.animation.curver

import android.view.animation.Interpolator


val Interpolator.curver: Curver
    get() = ::getInterpolation

val Curver.inverted: Curver
    get() = { percentage -> 1f - invoke(1f - percentage) }