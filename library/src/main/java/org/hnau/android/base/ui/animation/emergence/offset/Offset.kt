package org.hnau.android.base.ui.animation.emergence.offset


data class Offset(
    var dx: Float = 0f,
    var dy: Float = 0f
) {

    fun set(
        dx: Float = this.dx,
        dy: Float = this.dy
    ) {
        this.dx = dx
        this.dy = dy
    }

}