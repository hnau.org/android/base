package org.hnau.android.base.extensions

import android.graphics.Color
import android.os.Build
import android.view.View
import android.view.Window
import org.hnau.android.base.ui.lightness.Lightness

fun Window.prepareEdgeToEdge() {
    if (Build.VERSION.SDK_INT < Build.VERSION_CODES.LOLLIPOP) return
    statusBarColor = Color.TRANSPARENT
    navigationBarColor = Color.TRANSPARENT
    decorView.fitsSystemWindows = true
}

private fun Window.getEdgeToEdgeThemeFlag(
        lightness: Lightness
) = when (lightness) {
    Lightness.light -> {

        val lightStatusBarFlag = if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR
        } else {
            0
        }

        val lightNavigationBarFlag = if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            View.SYSTEM_UI_FLAG_LIGHT_NAVIGATION_BAR
        } else {
            0
        }

        lightStatusBarFlag or lightNavigationBarFlag

    }
    Lightness.dark -> 0
}

fun Window.applyEdgeToEdgeTheme(
        lightness: Lightness
) {
    if (Build.VERSION.SDK_INT < Build.VERSION_CODES.LOLLIPOP) return
    decorView.systemUiVisibility = View.SYSTEM_UI_FLAG_LAYOUT_STABLE or
            View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION or
            getEdgeToEdgeThemeFlag(lightness)
}