package org.hnau.android.base.ui.view.layers.view.presenting

import org.hnau.android.base.ui.animation.emergence.EmergenceInfo
import org.hnau.android.base.ui.animation.emergence.offset.EmergencesInfo
import org.hnau.android.base.ui.view.layers.Layer
import org.hnau.android.base.ui.view.layers.view.lifecycles.decoration.LayersStackDecoration
import org.hnau.android.base.ui.view.layers.view.lifecycles.decoration.shadow.shadow


interface PresentingLayer : Layer {

    companion object {

        private val defaultAnimationInfo = EmergencesInfo(
            show = EmergenceInfo(),
            hide = EmergenceInfo()
        )

        private val defaultStackDecoration =
            LayersStackDecoration.shadow()

    }

    val opaque get() = true

    val animationInfo: EmergencesInfo
        get() = defaultAnimationInfo

    val stackDecoration: LayersStackDecoration
        get() = defaultStackDecoration

}