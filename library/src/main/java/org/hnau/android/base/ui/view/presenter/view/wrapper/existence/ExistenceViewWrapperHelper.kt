package org.hnau.android.base.ui.view.presenter.view.wrapper.existence

import android.graphics.Canvas
import android.graphics.Rect
import android.view.View
import org.hnau.android.base.data.direction.apply
import org.hnau.android.base.extensions.inState
import org.hnau.android.base.ui.animation.emergence.EmergenceInfo
import org.hnau.android.base.ui.animation.emergence.offset.EmergenceDirection
import org.hnau.android.base.ui.animation.emergence.offset.Offset
import org.hnau.android.base.ui.animation.emergence.offset.calculator.OffsetCalculator
import org.hnau.base.extensions.number.asPercentageInter
import kotlin.math.max


class ExistenceViewWrapperHelper(
        private val view: View,
        private val presentingInfo: PresentingInfo
) {

    private val layoutRect = Rect()

    fun layout(slot: Rect) {
        presentingInfo.direction.apply(
                width = view.measuredWidth,
                height = view.measuredHeight,
                parentLeft = slot.left,
                parentTop = slot.top,
                parentRight = slot.right,
                parentBottom = slot.bottom,
                result = layoutRect
        )
        view.layout(
                layoutRect.left,
                layoutRect.top,
                layoutRect.right,
                layoutRect.bottom
        )
    }

    private val offset = Offset()

    fun draw(
            slot: Rect,
            canvas: Canvas,
            childDrawer: (canvas: Canvas, child: View) -> Unit,
            emergenceInfo: EmergenceInfo,
            visibilityPercentage: Float
    ) {
        val maxXOffset = max(slot.width(), view.measuredWidth)
        val maxYOffset = max(slot.height(), view.measuredHeight)
        emergenceInfo.offsetCalculator.calcOffset(visibilityPercentage, offset)
        val xOffset = maxXOffset * offset.dx * emergenceInfo.fade.offsetFactor
        val yOffset = maxYOffset * offset.dy * emergenceInfo.fade.offsetFactor
        canvas.inState {
            translate(xOffset, yOffset)
            childDrawer(canvas, view)
        }
    }

    fun setAlpha(
            offsetAmount: Float,
            direction: EmergenceDirection
    ) {
        val centerProximity = when (direction) {
            EmergenceDirection.show -> offsetAmount
            EmergenceDirection.hide -> 1 - offsetAmount
        }
        val emergenceInfo = presentingInfo.emergencesInfo[direction]
        view.alpha = centerProximity.asPercentageInter(
                from = emergenceInfo.fade.minAlpha,
                to = 1f
        )
    }

}