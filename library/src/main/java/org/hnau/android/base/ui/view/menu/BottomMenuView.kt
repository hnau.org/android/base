package org.hnau.android.base.ui.view.menu

import android.content.Context
import org.hnau.android.base.data.getter.picture.Picture
import org.hnau.android.base.data.getter.px.dp
import org.hnau.android.base.data.getter.text.Text
import org.hnau.android.base.extensions.view.layout.params.builders.LinearLayoutParamsBuilder
import org.hnau.android.base.extensions.view.layout.params.linearParams
import org.hnau.android.base.extensions.view.linear.RowLayout
import org.hnau.android.base.extensions.view.makeExactlyMeasureSpec
import org.hnau.android.base.extensions.view.measureSpecFit
import org.hnau.android.base.extensions.view.measureSpecMax
import org.hnau.android.base.ui.view.utils.state.createStateEmitters
import org.hnau.base.extensions.number.asPercentageInter
import org.hnau.emitter.Emitter
import org.hnau.emitter.extensions.useWhen


class BottomMenuView(
        context: Context,
        items: List<BottomMenuItem>,
        info: BottomMenuViewInfo
) : RowLayout(
        context = context
) {

    companion object {

        private const val activeItemWeight = 1.25f

    }

    val preferredHeight = 56.dp

    private val state = createStateEmitters()

    init {
        clipChildren = false
        items.forEach { item ->
            addView(
                    BottomMenuItemView(
                            context = context,
                            item = item,
                            info = info
                    ).apply {
                        val layoutParams = LinearLayoutParamsBuilder(context)
                                .apply {
                                    height = matchParent
                                    width = zero
                                }
                                .build()
                        activePercentage
                                .useWhen(state.isActive)
                                .observe { activePercentage ->
                                    this.layoutParams = layoutParams.apply {
                                        weight = activePercentage.asPercentageInter(1f, activeItemWeight)
                                    }
                                }
                    }
            )
        }
    }

    override fun onMeasure(
            widthMeasureSpec: Int,
            heightMeasureSpec: Int
    ) = super.onMeasure(
            makeExactlyMeasureSpec(measureSpecMax(widthMeasureSpec)),
            makeExactlyMeasureSpec(measureSpecFit(heightMeasureSpec, preferredHeight))
    )

}