package org.hnau.android.base.ui.view.presenter.info.size.content

import org.hnau.android.base.ui.animation.curver.Curver
import org.hnau.android.base.ui.animation.curver.Curvers
import org.hnau.android.base.ui.view.presenter.info.size.content.base.ContentSizeCalculator

/**
 * Like SmoothContentSizeCalculator but uses in size calculating only existece views
 */
class WideContentSizeCalculator(
    curver: Curver = Curvers.accelerateDecelerate
) : CombiningContentSizeCalculator(
    curver = curver
) {

    override fun calculateViewSignificantSize(viewSize: Int?, percentage: Float) =
        viewSize?.times(percentage)

}

fun ContentSizeCalculator.Companion.wide(
    curver: Curver = Curvers.accelerateDecelerate
) = WideContentSizeCalculator(
    curver = curver
)