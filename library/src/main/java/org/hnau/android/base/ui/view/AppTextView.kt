package org.hnau.android.base.ui.view

import android.content.Context
import android.util.TypedValue
import android.widget.TextView
import org.hnau.android.base.data.direction.Direction
import org.hnau.android.base.data.getter.font.Font
import org.hnau.android.base.data.getter.font.Fonts
import org.hnau.android.base.data.getter.px.Px
import org.hnau.android.base.data.getter.text.Text
import org.hnau.android.base.data.getter.tone.Tone
import org.hnau.android.base.extensions.view.text.textViewDirection
import org.hnau.base.extensions.boolean.ifTrue


open class AppTextView(
        context: Context,
        text: Text,
        textSize: Px,
        textColor: Tone,
        font: Font = Fonts.default,
        singleLine: Boolean = true,
        direction: Direction = Direction.left
): TextView(
        context
) {

    init {
        setText(text(context))
        setTextSize(TypedValue.COMPLEX_UNIT_PX, textSize(context))
        setTextColor(textColor(context))
        this.setTypeface(font(context))
        singleLine.ifTrue { maxLines = 1 }
        textViewDirection = direction
    }

}