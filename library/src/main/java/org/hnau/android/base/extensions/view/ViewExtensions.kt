package org.hnau.android.base.extensions.view

import android.view.View

@Suppress("UNUSED_PARAMETER")
fun <T> View.invalidate(param: T) = invalidate()

@Suppress("UNUSED_PARAMETER")
fun <T> View.requestLayout(param: T) = requestLayout()