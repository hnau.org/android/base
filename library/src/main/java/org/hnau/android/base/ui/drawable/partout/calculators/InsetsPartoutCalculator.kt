package org.hnau.android.base.ui.drawable.partout.calculators

import android.content.Context
import android.graphics.Rect
import org.hnau.android.base.data.getter.px.Px
import org.hnau.android.base.data.getter.px.pxInt
import org.hnau.android.base.data.insets.Insets
import org.hnau.android.base.ui.drawable.partout.PartoutCalculator


class InsetsPartoutCalculator(
        context: Context,
        insets: Insets<Px>
) : PartoutCalculator {

    private val left = insets.left.pxInt(context)
    private val top = insets.top.pxInt(context)
    private val right = -insets.right.pxInt(context)
    private val bottom = -insets.bottom.pxInt(context)

    override fun calcContentBounds(
            parentWidth: Int,
            parentHeight: Int,
            contentPreferredWidth: Int,
            contentPreferredHeight: Int,
            result: Rect
    ) = result.set(
            left,
            top,
            parentWidth - right,
            parentHeight - bottom
    )

    override fun calcIntrinsicWrapperWidth(contentIntrinsicWidth: Int) =
            contentIntrinsicWidth + left + right

    override fun calcIntrinsicWrapperHeight(contentIntrinsicHeight: Int) =
            contentIntrinsicHeight + top + bottom

}