package org.hnau.android.base.extensions.color

import androidx.annotation.ColorInt
import androidx.annotation.FloatRange
import androidx.annotation.IntRange


@ColorInt
inline fun @receiver:ColorInt Int.mapAlpha(
    converter: (Int) -> Int
) = argb(
    alpha = converter(alpha),
    red = red,
    green = green,
    blue = blue
)

@ColorInt
inline fun @receiver:ColorInt Int.mapAlphaFloat(
    converter: (Float) -> Float
) = mapAlpha {
    val percentage = ColorUtils.valueToPercentage(alpha)
    val newPercentage = converter(percentage)
    ColorUtils.percentageToValue(newPercentage)
}

@ColorInt
fun @receiver:ColorInt Int.replaceAlpha(
    @IntRange(from = 0L, to = 255L) newAlpha: Int
) = mapAlpha { newAlpha }

@ColorInt
fun @receiver:ColorInt Int.replaceAlpha(
    @FloatRange(from = 0.0, to = 1.0) newAlpha: Float
) = mapAlphaFloat { newAlpha }

@ColorInt
fun @receiver:ColorInt Int.scaleAlpha(factor: Number) =
    mapAlphaFloat { (it * factor.toFloat()).coerceIn(0f, 1f) }

@ColorInt
fun @receiver:ColorInt Int.clearAlpha() =
    replaceAlpha(0)

@ColorInt
fun @receiver:ColorInt Int.fullAlpha() =
    replaceAlpha(255)


@ColorInt
inline fun @receiver:ColorInt Int.mapRed(
    converter: (Int) -> Int
) = argb(
    alpha = alpha,
    red = converter(red),
    green = green,
    blue = blue
)

@ColorInt
inline fun @receiver:ColorInt Int.mapRedFloat(
    converter: (Float) -> Float
) = mapRed {
    val percentage = ColorUtils.valueToPercentage(red)
    val newPercentage = converter(percentage)
    ColorUtils.percentageToValue(newPercentage)
}

@ColorInt
fun @receiver:ColorInt Int.replaceRed(
    @IntRange(from = 0L, to = 255L) newRed: Int
) = mapRed { newRed }

@ColorInt
fun @receiver:ColorInt Int.replaceRed(
    @FloatRange(from = 0.0, to = 1.0) newRed: Float
) = mapRedFloat { newRed }

@ColorInt
fun @receiver:ColorInt Int.scaleRed(factor: Number) =
    mapRedFloat { (it * factor.toFloat()).coerceIn(0f, 1f) }

@ColorInt
fun @receiver:ColorInt Int.clearRed() =
    replaceRed(0)

@ColorInt
fun @receiver:ColorInt Int.fullRed() =
    replaceRed(255)


@ColorInt
inline fun @receiver:ColorInt Int.mapGreen(
    converter: (Int) -> Int
) = argb(
    red = red,
    alpha = alpha,
    green = converter(green),
    blue = blue
)

@ColorInt
inline fun @receiver:ColorInt Int.mapGreenFloat(
    converter: (Float) -> Float
) = mapGreen {
    val percentage = ColorUtils.valueToPercentage(green)
    val newPercentage = converter(percentage)
    ColorUtils.percentageToValue(newPercentage)
}

@ColorInt
fun @receiver:ColorInt Int.replaceGreen(
    @IntRange(from = 0L, to = 255L) newGreen: Int
) = mapGreen { newGreen }

@ColorInt
fun @receiver:ColorInt Int.replaceGreen(
    @FloatRange(from = 0.0, to = 1.0) newGreen: Float
) = mapGreenFloat { newGreen }

@ColorInt
fun @receiver:ColorInt Int.scaleGreen(factor: Number) =
    mapGreenFloat { (it * factor.toFloat()).coerceIn(0f, 1f) }

@ColorInt
fun @receiver:ColorInt Int.clearGreen() =
    replaceGreen(0)

@ColorInt
fun @receiver:ColorInt Int.fullGreen() =
    replaceGreen(255)


@ColorInt
inline fun @receiver:ColorInt Int.mapBlue(
    converter: (Int) -> Int
) = argb(
    alpha = alpha,
    red = red,
    green = green,
    blue = converter(blue)
)

@ColorInt
inline fun @receiver:ColorInt Int.mapBlueFloat(
    converter: (Float) -> Float
) = mapBlue {
    val percentage = ColorUtils.valueToPercentage(blue)
    val newPercentage = converter(percentage)
    ColorUtils.percentageToValue(newPercentage)
}

@ColorInt
fun @receiver:ColorInt Int.replaceBlue(
    @IntRange(from = 0L, to = 255L) newBlue: Int
) = mapBlue { newBlue }

@ColorInt
fun @receiver:ColorInt Int.replaceBlue(
    @FloatRange(from = 0.0, to = 1.0) newBlue: Float
) = mapBlueFloat { newBlue }

@ColorInt
fun @receiver:ColorInt Int.scaleBlue(factor: Number) =
    mapBlueFloat { (it * factor.toFloat()).coerceIn(0f, 1f) }

@ColorInt
fun @receiver:ColorInt Int.clearBlue() =
    replaceBlue(0)

@ColorInt
fun @receiver:ColorInt Int.fullBlue() =
    replaceBlue(255)
    