package org.hnau.android.base.data.getter.base

import android.content.Context
import org.hnau.base.data.possible.getOrInitialize
import org.hnau.base.data.possible.lateinit.Lateinit
import org.hnau.base.data.possible.lateinit.sync
import org.hnau.base.utils.safe.SafeContext
import org.hnau.base.utils.safe.synchronized


typealias ContextGetter<T> = (Context) -> T

@Suppress("FunctionName")
inline fun <T> CachedContextGetter(
        crossinline get: (Context) -> T
): ContextGetter<T> = Lateinit
        .sync<T>(SafeContext.synchronized())
        .let { lateinit ->
            { dependency ->
                lateinit.getOrInitialize { get(dependency) }
            }
        }