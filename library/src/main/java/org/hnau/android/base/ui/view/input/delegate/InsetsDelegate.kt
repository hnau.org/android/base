package org.hnau.android.base.ui.view.input.delegate

import android.content.Context
import org.hnau.android.base.data.edges.Edges
import org.hnau.android.base.data.getter.px.Px
import org.hnau.android.base.data.insets.Insets
import org.hnau.android.base.data.insets.map


class InsetsDelegate(
        context: Context,
        borderWidth: Float,
        titleTopHeight: Float,
        horizontalSpace: Px
) : Insets<Float> {

    private val horizontal = horizontalSpace(context)

    override val left: Float
        get() = horizontal

    override val top =
            titleTopHeight

    override val right: Float
        get() = horizontal

    override val bottom =
            (titleTopHeight + borderWidth) / 2

    val int by lazy { map(Float::toInt) }


}