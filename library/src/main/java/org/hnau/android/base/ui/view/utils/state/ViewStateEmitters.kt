package org.hnau.android.base.ui.view.utils.state

import android.graphics.Rect
import android.view.View
import org.hnau.android.base.ui.view.utils.state.utils.ViewIsActiveEmitter
import org.hnau.android.base.ui.view.utils.state.utils.ViewIsAttachedToWindowEmitter
import org.hnau.android.base.ui.view.utils.state.utils.ViewIsForegroundEmitter
import org.hnau.android.base.ui.view.utils.state.utils.ViewLayoutRect
import org.hnau.android.base.ui.view.utils.state.utils.ViewLifecycleStateEmitter
import org.hnau.android.base.ui.view.utils.state.utils.ViewOnLayoutEmitter
import org.hnau.emitter.Emitter


class ViewStateEmitters(
        view: View
) {

    val onLayout by lazy {
        ViewOnLayoutEmitter(view)
    }

    val layoutRect: Emitter<Rect?>
            by lazy { ViewLayoutRect(view, onLayout) }

    private val lifecycleState
            by lazy { ViewLifecycleStateEmitter(view) }

    private val isAttached: Emitter<Boolean>
            by lazy { ViewIsAttachedToWindowEmitter(view) }

    val isActive
            by lazy { ViewIsActiveEmitter(lifecycleState, isAttached) }

    val isForeground
            by lazy { ViewIsForegroundEmitter(lifecycleState, isAttached, layoutRect) }

}

fun View.createStateEmitters() = ViewStateEmitters(this)