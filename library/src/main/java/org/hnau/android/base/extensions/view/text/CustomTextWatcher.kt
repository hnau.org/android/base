package org.hnau.android.base.extensions.view.text

import android.text.Editable
import android.text.TextWatcher


fun TextWatcher(
        beforeTextChanged: ((s: CharSequence, start: Int, count: Int, after: Int) -> Unit)? = null,
        onTextChanged: ((s: CharSequence, start: Int, before: Int, count: Int) -> Unit)? = null,
        afterTextChanged: ((s: Editable) -> Unit)? = null
) = object : TextWatcher {
    override fun afterTextChanged(s: Editable) {
        afterTextChanged?.invoke(s)
    }

    override fun beforeTextChanged(s: CharSequence, start: Int, count: Int, after: Int) {
        beforeTextChanged?.invoke(s, start, count, after)
    }

    override fun onTextChanged(s: CharSequence, start: Int, before: Int, count: Int) {
        onTextChanged?.invoke(s, start, before, count)
    }
}