package org.hnau.android.base.extensions

import android.graphics.RectF
import android.graphics.drawable.Drawable
import org.hnau.android.base.data.edges.Edges


fun Drawable.setBounds(left: Number, top: Number, right: Number, bottom: Number) =
        setBounds(left.toInt(), top.toInt(), right.toInt(), bottom.toInt())

fun Drawable.setBounds(bounds: RectF) =
        setBounds(bounds.left, bounds.top, bounds.right, bounds.bottom)

fun Drawable.setBounds(bounds: Edges<Number>) =
        setBounds(bounds.xMin, bounds.yMin, bounds.xMax, bounds.yMax)

fun Drawable.setBoundsByCenter(
        centerX: Number,
        centerY: Number,
        width: Int = intrinsicWidth,
        height: Int = intrinsicHeight
) = setBounds(
        centerX.toInt() - width / 2,
        centerY.toInt() - height / 2,
        centerX.toInt() + (width - width / 2),
        centerY.toInt() + (height - height / 2),
)