package org.hnau.android.base.extensions.view.group.recycle

import androidx.recyclerview.widget.RecyclerView
import org.hnau.android.base.data.course.checkCourse
import org.hnau.android.base.extensions.view.scroll.recycle.RecyclerScrolledStateEmitters
import org.hnau.base.extensions.number.takeIfNotNegative
import org.hnau.base.extensions.setUnique
import org.hnau.emitter.Emitter
import org.hnau.emitter.extensions.listen
import org.hnau.emitter.extensions.useWhen


fun RecyclerView.setPositionEmitter(
        viewIsActive: Emitter<Boolean>,
        positionEmitter: Emitter<Int>,
        onPositionChanged: (Int) -> Unit
) {

    var lastPosition: Int? = null

    fun actualize(
            newPosition: Int,
            onNeedUpdate: (Int) -> Unit
    ) = setUnique(lastPosition, newPosition) {
        lastPosition = newPosition
        onNeedUpdate(newPosition)
    }

    val linearLayoutManager = this.linearLayoutManager
    positionEmitter
            .useWhen(viewIsActive)
            .observe { newPosition ->
                actualize(newPosition) {
                    val offset = linearLayoutManager.course.checkCourse({ paddingLeft }, { paddingTop })
                    linearLayoutManager.scrollToPositionWithOffset(it, offset)
                }
            }

    RecyclerScrolledStateEmitters(this)
            .onScrolledNotifier
            .listen {
                val newPosition = linearLayoutManager.findFirstCompletelyVisibleItemPosition()
                        .takeIfNotNegative() ?: return@listen
                actualize(newPosition, onPositionChanged)
            }
}