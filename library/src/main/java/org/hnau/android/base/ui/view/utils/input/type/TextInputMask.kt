package org.hnau.android.base.ui.view.utils.input.type

import android.text.InputType
import org.hnau.android.base.ui.view.utils.input.type.base.ClassInputMask
import org.hnau.android.base.ui.view.utils.input.type.base.setFlag
import org.hnau.android.base.ui.view.utils.input.type.base.setVariation


class TextInputMask() : ClassInputMask(InputType.TYPE_CLASS_TEXT) {

    fun setEmailAddressVariation() =
        setVariation(InputType.TYPE_TEXT_VARIATION_EMAIL_ADDRESS)

    fun setEmailSubjectVariation() =
        setVariation(InputType.TYPE_TEXT_VARIATION_EMAIL_SUBJECT)

    fun setFilterVariation() =
        setVariation(InputType.TYPE_TEXT_VARIATION_FILTER)

    fun setLongMessageVariation() =
        setVariation(InputType.TYPE_TEXT_VARIATION_LONG_MESSAGE)

    fun setPasswordVariation() =
        setVariation(InputType.TYPE_TEXT_VARIATION_PASSWORD)

    fun setPersonNameVariation() =
        setVariation(InputType.TYPE_TEXT_VARIATION_PERSON_NAME)

    fun setPhoneticVariation() =
        setVariation(InputType.TYPE_TEXT_VARIATION_PHONETIC)

    fun setPostalAddressVariation() =
        setVariation(InputType.TYPE_TEXT_VARIATION_POSTAL_ADDRESS)

    fun setShortMessageVariation() =
        setVariation(InputType.TYPE_TEXT_VARIATION_SHORT_MESSAGE)

    fun setUriVariation() =
        setVariation(InputType.TYPE_TEXT_VARIATION_URI)

    fun setVisiblePasswordVariation() =
        setVariation(InputType.TYPE_TEXT_VARIATION_VISIBLE_PASSWORD)

    fun setWebEditTextVariation() =
        setVariation(InputType.TYPE_TEXT_VARIATION_WEB_EDIT_TEXT)

    fun setWebEmailAddressVariation() =
        setVariation(InputType.TYPE_TEXT_VARIATION_WEB_EMAIL_ADDRESS)

    fun setWebPasswordVariation() =
        setVariation(InputType.TYPE_TEXT_VARIATION_WEB_PASSWORD)

    fun setAutoCompleteFlag(add: Boolean = true) =
        setFlag(InputType.TYPE_TEXT_FLAG_AUTO_COMPLETE, add)

    fun setAutoCorrectFlag(add: Boolean = true) =
        setFlag(InputType.TYPE_TEXT_FLAG_AUTO_CORRECT, add)

    fun setCapCharactersFlag(add: Boolean = true) =
        setFlag(InputType.TYPE_TEXT_FLAG_CAP_CHARACTERS, add)

    fun setCapSentencesFlag(add: Boolean = true) =
        setFlag(InputType.TYPE_TEXT_FLAG_CAP_SENTENCES, add)

    fun setCapWordsFlag(add: Boolean = true) =
        setFlag(InputType.TYPE_TEXT_FLAG_CAP_WORDS, add)

    fun setImeMultiLineFlag(add: Boolean = true) =
        setFlag(InputType.TYPE_TEXT_FLAG_IME_MULTI_LINE, add)

    fun setMultiLineFlag(add: Boolean = true) =
        setFlag(InputType.TYPE_TEXT_FLAG_MULTI_LINE, add)

    fun setNoSuggestionsFlag(add: Boolean = true) =
        setFlag(InputType.TYPE_TEXT_FLAG_NO_SUGGESTIONS, add)

}