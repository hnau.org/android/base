package org.hnau.android.base.extensions.view.group.recycle

import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.RecyclerView
import org.hnau.android.base.data.course.Course
import org.hnau.android.base.data.course.checkCourse
import org.hnau.base.extensions.boolean.checkBoolean

val Course.gridLayoutManagerOrientation
    get() = checkCourse({ GridLayoutManager.HORIZONTAL }, { GridLayoutManager.VERTICAL })

var GridLayoutManager.course: Course
    get() = (orientation == GridLayoutManager.HORIZONTAL).checkBoolean(
            ifTrue = { Course.horizontal },
            ifFalse = { Course.vertical }
    )
    set(value) {
        orientation = value.gridLayoutManagerOrientation
    }

inline fun RecyclerView.setGridLayoutManager(
        spanCount: Int,
        orientation: Course,
        reverseLayout: Boolean = false,
        configurator: GridLayoutManager.() -> Unit = {}
) = setLayoutManager(
        layoutManager = GridLayoutManager(context, spanCount, orientation.gridLayoutManagerOrientation, reverseLayout),
        configurator = configurator
)

val RecyclerView.gridLayoutManager
    get() = existenceLayoutManager as? GridLayoutManager
            ?: throw IllegalStateException("layoutManager is not GridLayoutManager")