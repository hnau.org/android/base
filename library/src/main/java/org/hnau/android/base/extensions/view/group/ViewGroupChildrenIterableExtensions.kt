package org.hnau.android.base.extensions.view.group

import android.view.View
import android.view.ViewGroup


inline fun ViewGroup.forEach(action: (View) -> Unit) =
        repeat(childCount) { action(getChildAt(it)) }