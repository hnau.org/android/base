package org.hnau.android.base.ui.theme

import org.hnau.android.base.extensions.color.shiftTo
import org.hnau.android.base.ui.rawcolor.RawColor
import org.hnau.android.base.ui.rawcolor.combine


val ThemeGradient.center
    get() = RawColor.combine(start, end) { start, end ->
        start.shiftTo(end, 0.5f)
    }

private val themeGradientsComparator: (ThemeGradient, ThemeGradient) -> Boolean = { first, second ->
    first.start == second.start && first.end == second.end
}

val ThemeGradient.Companion.comparator
    get() = themeGradientsComparator

fun ThemeGradient.copy() = object : ThemeGradient {
    override val start = this@copy.start
    override val end = this@copy.end

}