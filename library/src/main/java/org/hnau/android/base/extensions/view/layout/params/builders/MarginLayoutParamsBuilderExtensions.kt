package org.hnau.android.base.extensions.view.layout.params.builders
import org.hnau.android.base.data.getter.px.Px
import org.hnau.android.base.data.insets.Insets


fun MarginLayoutParamsBuilder.setInsetsAsMargins(
       insets: Insets<Px>
) {
    marginLeft = insets.left
    marginTop = insets.top
    marginTop = insets.right
    marginBottom = insets.bottom
}