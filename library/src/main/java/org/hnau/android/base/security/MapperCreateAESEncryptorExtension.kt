package org.hnau.android.base.security

import android.os.Build
import android.security.keystore.KeyProperties
import org.hnau.base.data.mapper.Mapper
import java.security.Key
import javax.crypto.Cipher
import javax.crypto.spec.GCMParameterSpec
import javax.crypto.spec.IvParameterSpec


object AESEncryptor {

    const val algorithm = KeyProperties.KEY_ALGORITHM_AES
    const val blockMode = KeyProperties.BLOCK_MODE_GCM
    const val padding = KeyProperties.ENCRYPTION_PADDING_NONE

}

private const val tagLength = 128

private val gcmParameterSpec =
    intArrayOf(0x64, 0x3f, 0x5b, 0xfa, 0xdf, 0x1d, 0x4e, 0x6a, 0xa6, 0xb3, 0xf5, 0x42)
        .map(Int::toByte)
        .toByteArray()
        .let { bytes ->
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                GCMParameterSpec(tagLength, bytes)
            } else {
                IvParameterSpec(bytes, 0, bytes.size)
            }
        }

private const val transformation =
    "${AESEncryptor.algorithm}/${AESEncryptor.blockMode}/${AESEncryptor.padding}"

fun Mapper.Companion.createAESEncryptor(
    key: Key
): Mapper<ByteArray, ByteArray> {

    fun execute(
        source: ByteArray,
        mode: Int
    ): ByteArray = Cipher.getInstance(
        transformation
    ).run {
        init(mode, key, gcmParameterSpec)
        doFinal(source)
    }

    return Mapper(
        direct = { execute(it, Cipher.DECRYPT_MODE) },
        reverse = { execute(it, Cipher.ENCRYPT_MODE) }
    )

}