package org.hnau.android.base.ui.view.utils.state.utils

import android.graphics.Rect
import org.hnau.android.base.system.lifecycle.LifecycleState
import org.hnau.emitter.Emitter
import org.hnau.emitter.extensions.alwaysFalse
import org.hnau.emitter.extensions.and
import org.hnau.emitter.extensions.map
import org.hnau.emitter.extensions.mapIsNotNull
import org.hnau.emitter.extensions.unique
import org.hnau.emitter.extensions.useWhen


fun ViewIsForegroundEmitter(
    lifecycleState: Emitter<LifecycleState>,
    isAttachedToWindow: Emitter<Boolean>,
    layoutRect: Emitter<Rect?>
) = lifecycleState
    .map { it.level >= LifecycleState.started.level }
    .and(layoutRect.mapIsNotNull())
    .useWhen(
        isNeedUse = isAttachedToWindow,
        placeholder = Emitter.alwaysFalse
    )
    .unique()