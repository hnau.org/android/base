package org.hnau.android.base.ui.view.utils.state.utils

import android.content.Context
import android.view.ContextThemeWrapper
import android.view.View
import androidx.lifecycle.LifecycleOwner
import org.hnau.android.base.system.lifecycle.LifecycleState
import org.hnau.android.base.system.lifecycle.LifecycleStateEmitter
import org.hnau.emitter.Emitter


private val Context.lifecycleOwner: LifecycleOwner
    get() = when (this) {
        is LifecycleOwner -> this
        is ContextThemeWrapper -> baseContext.lifecycleOwner
        else -> throw IllegalArgumentException("'${javaClass.name}' not implements android.arch.lifecycle.LifecycleOwner")
    }

fun ViewLifecycleStateEmitter(
    view: View
): Emitter<LifecycleState> = LifecycleStateEmitter(
    lifecycle = view.context.lifecycleOwner.lifecycle
)