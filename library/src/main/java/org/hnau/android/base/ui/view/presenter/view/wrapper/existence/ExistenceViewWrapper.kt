package org.hnau.android.base.ui.view.presenter.view.wrapper.existence

import android.graphics.Canvas
import android.graphics.Rect
import android.view.View
import android.view.ViewGroup
import org.hnau.android.base.ui.animation.emergence.EmergenceInfo
import org.hnau.android.base.ui.animation.emergence.offset.EmergenceDirection
import org.hnau.android.base.ui.animation.emergence.offset.EmergencesInfo
import org.hnau.android.base.ui.animation.emergence.offset.calculator.OffsetCalculator
import org.hnau.android.base.ui.view.presenter.view.wrapper.ViewWrapper


class ExistenceViewWrapper(
        private val view: View,
        private val presentingInfo: PresentingInfo
) : ViewWrapper {

    override val emergencesInfo: EmergencesInfo
        get() = presentingInfo.emergencesInfo

    private val helper = ExistenceViewWrapperHelper(
            view = view,
            presentingInfo = presentingInfo
    )

    override val measuredWidth get() = view.measuredWidth
    override val measuredHeight get() = view.measuredHeight

    override fun measure(widthMeasureSpec: Int, heightMeasureSpec: Int) =
            view.measure(widthMeasureSpec, heightMeasureSpec)

    override fun layout(slot: Rect) = helper.layout(slot)

    override fun setAlpha(
            direction: EmergenceDirection,
            offsetAmount: Float
    ) = helper.setAlpha(
            direction = direction,
            offsetAmount = offsetAmount
    )

    override fun draw(
            slot: Rect,
            canvas: Canvas,
            childDrawer: (canvas: Canvas, child: View) -> Unit,
            emergenceInfo: EmergenceInfo,
            visibilityPercentage: Float
    ) = helper.draw(
            slot = slot,
            canvas = canvas,
            childDrawer = childDrawer,
            emergenceInfo = emergenceInfo,
            visibilityPercentage = visibilityPercentage
    )

    override fun addView(parent: ViewGroup) = parent.addView(view)
    override fun removeView(parent: ViewGroup) = parent.removeView(view)

}

fun ViewWrapper.Companion.view(
        view: View,
        presentingInfo: PresentingInfo = PresentingInfo()
) = ExistenceViewWrapper(
        view = view,
        presentingInfo = presentingInfo
)