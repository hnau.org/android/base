package org.hnau.android.base.ui.decoration.ripple

import org.hnau.android.base.ui.animation.AnimationTicker
import org.hnau.base.data.time.Time
import org.hnau.base.extensions.boolean.checkBoolean
import org.hnau.base.extensions.boolean.ifTrue
import org.hnau.base.data.time.now
import org.hnau.emitter.extensions.listen
import org.hnau.emitter.observing.notifier.Notifier


class RippleState(
        private val info: RippleInfo
) : Notifier() {

    private val animationTicker = AnimationTicker()

    private var cancelThen = false
    var cancelled = true
        private set

    private var started = Time.zero

    fun start() {
        cancelled = false
        started = Time.now()
        animationTicker.start()
    }

    fun cancel() {
        cancelled.ifTrue { return }
        (stepPercentage < 1).ifTrue {
            cancelThen = true
            return
        }
        cancelled = true
        started = Time.now()
        animationTicker.start()
    }

    val stepPercentage: Float
        get() = (Time.now() - started).let { duration ->
            cancelled
                    .checkBoolean(
                            ifFalse = { duration / info.growingDuration },
                            ifTrue = { duration / info.fadingDuration }
                    )
                    .toFloat()
        }

    val finished
        get() = cancelled && stepPercentage >= 1

    init {
        animationTicker.listen {
            notifyObservers()
            if (stepPercentage >= 1) {
                (!cancelled && cancelThen).checkBoolean(
                        ifTrue = {
                            cancelThen = false
                            cancel()
                        },
                        ifFalse = {
                            animationTicker.stop()
                        }
                )

            }
        }
    }

    override fun afterLastDetached() {
        super.afterLastDetached()
        cancelled = true
        started = Time.zero
        animationTicker.stop()
    }

}