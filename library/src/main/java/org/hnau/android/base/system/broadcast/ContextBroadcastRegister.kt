package org.hnau.android.base.system.broadcast

import android.content.BroadcastReceiver
import android.content.Context
import android.content.IntentFilter


class ContextBroadcastRegister(
    private val context: Context
) : BroadcastRegister {

    override fun register(
        broadcastReceiver: BroadcastReceiver,
        intentFilter: IntentFilter
    ) {
        context.registerReceiver(
            broadcastReceiver,
            intentFilter
        )
    }

    override fun unregister(
        broadcastReceiver: BroadcastReceiver
    ) {
        context.unregisterReceiver(
            broadcastReceiver
        )
    }

}

fun BroadcastRegister.Companion.context(
    context: Context
) = ContextBroadcastRegister(
    context = context
)