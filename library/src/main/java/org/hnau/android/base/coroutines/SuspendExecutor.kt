package org.hnau.android.base.coroutines

import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.Job
import kotlinx.coroutines.SupervisorJob
import kotlinx.coroutines.launch
import org.hnau.base.extensions.boolean.ifTrue
import org.hnau.emitter.Emitter


typealias SuspendExecutor = (suspend CoroutineScope.() -> Unit) -> Unit

fun SuspendExecutor(
        isActive: Emitter<Boolean>
): SuspendExecutor {
    var job: Job? = null
    isActive.observe { active ->
        synchronized(isActive) {
            job?.cancel()
            job = active.ifTrue { SupervisorJob() }
        }
    }
    return { block ->
        job?.let { GlobalScope.launch(it, block = block) }
    }
}