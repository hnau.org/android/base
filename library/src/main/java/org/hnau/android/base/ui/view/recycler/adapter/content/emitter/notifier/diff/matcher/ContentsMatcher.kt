package org.hnau.android.base.ui.view.recycler.adapter.content.emitter.notifier.diff.matcher.item


interface ContentsMatcher<T> {

    companion object;

    fun contentsAreSame(item1: T, item2: T): Boolean

}

fun <T> ContentsMatcher.Companion.create(
    contentsAreSame: (item1: T, item2: T) -> Boolean
) = object : ContentsMatcher<T> {

    override fun contentsAreSame(item1: T, item2: T) =
        contentsAreSame.invoke(item1, item2)

}

fun <T> ContentsMatcher.Companion.alwaysSame() =
    create<T> { _, _ -> true }

fun <T> ContentsMatcher.Companion.byEquals() =
    create<T> { item1, item2 -> item1 == item2 }

inline fun <T> ContentsMatcher.Companion.byValue(
    crossinline extractContent: T.() -> Any?
) = create<T> { item1, item2 ->
    item1.extractContent() == item2.extractContent()
}