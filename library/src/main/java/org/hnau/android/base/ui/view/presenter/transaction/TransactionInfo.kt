package org.hnau.android.base.ui.view.presenter.transaction

import org.hnau.android.base.ui.animation.animation
import org.hnau.base.data.time.Time


data class TransactionInfo(
    val duration: Time = Time.animation.default
)