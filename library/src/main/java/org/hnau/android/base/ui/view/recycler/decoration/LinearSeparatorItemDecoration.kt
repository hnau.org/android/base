package org.hnau.android.base.ui.view.recycler.decoration

import android.graphics.Canvas
import android.graphics.Rect
import android.view.View
import androidx.recyclerview.widget.RecyclerView
import org.hnau.android.base.data.course.checkCourse
import org.hnau.android.base.data.getter.px.pxInt
import org.hnau.android.base.extensions.inState
import org.hnau.android.base.extensions.view.group.forEach
import org.hnau.android.base.extensions.view.group.recycle.course
import org.hnau.android.base.extensions.view.group.recycle.linearLayoutManager


class LinearSeparatorItemDecoration(
    private val separatorDrawer: SeparatorDrawer
) : RecyclerView.ItemDecoration() {

    private val drawTempRect = Rect()

    override fun getItemOffsets(
        outRect: Rect,
        view: View,
        parent: RecyclerView,
        state: RecyclerView.State
    ) = parent.linearLayoutManager.course.checkCourse(
        ifHorizontal = { outRect.set(0, 0, separatorDrawer.thickness.pxInt(view.context), 0) },
        ifVertical = { outRect.set(0, 0, 0, separatorDrawer.thickness.pxInt(view.context)) }
    )

    override fun onDraw(c: Canvas, parent: RecyclerView, state: RecyclerView.State) {
        super.onDraw(c, parent, state)
        val context = parent.context
        val course = parent.linearLayoutManager.course
        c.inState {
            var acrossStart = 0
            var acrossEnd = course.checkCourse({ parent.height }, { parent.width })
            if (parent.clipToPadding) {
                acrossStart += course.checkCourse({ parent.paddingTop }, { parent.paddingLeft })
                acrossEnd -= course.checkCourse({ parent.paddingBottom }, { parent.paddingRight })
                c.clipRect(
                    parent.paddingLeft,
                    parent.paddingTop,
                    parent.width - parent.paddingRight,
                    parent.height - parent.paddingBottom
                )
            }
            parent.forEach { child ->
                parent.getDecoratedBoundsWithMargins(child, drawTempRect)
                val alongEnd = course.checkCourse(
                    ifHorizontal = { drawTempRect.right + child.translationX.toInt() },
                    ifVertical = { drawTempRect.bottom + child.translationY.toInt() }
                )
                val alongStart = alongEnd - separatorDrawer.thickness.pxInt(context)
                drawTempRect.set(
                    course.checkCourse({ alongStart }, { acrossStart }),
                    course.checkCourse({ acrossStart }, { alongStart }),
                    course.checkCourse({ alongEnd }, { acrossEnd }),
                    course.checkCourse({ acrossEnd }, { alongEnd })
                )
                separatorDrawer.draw(context, c, drawTempRect, course)
            }
        }
    }

}