package org.hnau.android.base.ui.shape
import org.hnau.android.base.data.edges.Edges
import org.hnau.emitter.Emitter
import org.hnau.emitter.extensions.map


fun <C : BoundsCanvasShape> C.withBounds(
        bounds: Emitter<Edges<Int>>
) = bounds.map { boundsEdgesOld ->
    apply { this.bounds = boundsEdgesOld }
}