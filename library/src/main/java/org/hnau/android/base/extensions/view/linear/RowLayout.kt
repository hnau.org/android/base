package org.hnau.android.base.extensions.view.linear

import android.content.Context
import android.view.ViewGroup
import org.hnau.android.base.data.course.Course
import org.hnau.android.base.extensions.view.group.children.view


open class RowLayout(
        context: Context
) : ManualLinearLayout(
        context = context,
        course = Course.horizontal
)

inline fun ViewGroup.row(
        childConfigurator: RowLayout.() -> Unit = {}
) = view(
        view = RowLayout(context),
        childConfigurator = childConfigurator
)