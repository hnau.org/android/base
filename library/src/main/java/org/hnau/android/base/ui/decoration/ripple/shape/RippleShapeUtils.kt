package org.hnau.android.base.ui.decoration.ripple.shape

import org.hnau.android.base.data.edges.Edges
import org.hnau.android.base.data.edges.typed.height
import org.hnau.android.base.data.edges.typed.width
import kotlin.math.sqrt


object RippleShapeUtils {

    fun calcBoundsDiagonal(
            bounds: Edges<Int>
    ) = sqrt(
            bounds.width.toFloat().let { it * it } +
                    bounds.height.toFloat().let { it * it }
    )

}