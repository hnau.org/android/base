package org.hnau.android.base.ui.shape

import android.graphics.Canvas
import android.graphics.Paint


interface CanvasShape {

    companion object {

        val empty = CanvasShape.create(
                draw = { _, _ -> },
                clip = {}
        )

    }

    fun draw(canvas: Canvas, paint: Paint)

    fun clip(canvas: Canvas)

}