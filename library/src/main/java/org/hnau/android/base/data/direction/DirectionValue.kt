package org.hnau.android.base.data.direction

import org.hnau.base.extensions.boolean.checkBoolean


enum class DirectionValue {

    start,
    center,
    end;

    val opposite: DirectionValue
        get() = when (this) {
            start -> end
            center -> center
            end -> start
        }

    operator fun plus(other: DirectionValue) =
        (other == center).checkBoolean(ifTrue = { this }, ifFalse = { other })

}