package org.hnau.android.base.data.direction

import android.annotation.SuppressLint
import android.view.Gravity


val Direction.gravity
    get() = horizontalGravity or verticalGravity

val Direction.verticalGravity
    get() = when (vertical) {
        DirectionValue.start -> Gravity.TOP
        DirectionValue.end -> Gravity.BOTTOM
        DirectionValue.center -> Gravity.CENTER_VERTICAL
    }

val Direction.horizontalGravity
    @SuppressLint("RtlHardcoded")
    get() = when (horizontal) {
        DirectionValue.start -> Gravity.LEFT
        DirectionValue.end -> Gravity.RIGHT
        DirectionValue.center -> Gravity.CENTER_HORIZONTAL
    }