package org.hnau.android.base.ui.view.menu

import org.hnau.android.base.data.getter.tone.Tone
import org.hnau.android.base.ui.animation.animation
import org.hnau.android.base.ui.decoration.ripple.RippleInfo
import org.hnau.base.data.time.Time


data class BottomMenuViewInfo(
        val rippleInfo: RippleInfo,
        val switchingStateDuration: Time = Time.animation.small
) {

    companion object;

}