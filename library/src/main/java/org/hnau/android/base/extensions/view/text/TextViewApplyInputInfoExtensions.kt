package org.hnau.android.base.extensions.view.text

import android.widget.TextView
import org.hnau.android.base.ui.view.utils.input.type.DateTimeInputMask
import org.hnau.android.base.ui.view.utils.input.type.NullInputMask
import org.hnau.android.base.ui.view.utils.input.type.NumberInputMask
import org.hnau.android.base.ui.view.utils.input.type.PhoneInputMask
import org.hnau.android.base.ui.view.utils.input.type.TextInputMask
import org.hnau.android.base.ui.view.utils.input.type.base.InputMask

inline fun <IM : InputMask> TextView.setInputMask(inputMask: IM, inputMaskConfigurator: IM.() -> Unit = {}) =
    setInputType(inputMask.apply(inputMaskConfigurator).mask)

inline fun TextView.setTextInputMask(inputMaskConfigurator: TextInputMask.() -> Unit = {}) =
    setInputMask(TextInputMask(), inputMaskConfigurator)

fun TextView.setPhoneInputMask() =
    setInputMask(PhoneInputMask)

fun TextView.setNullInputMask() =
    setInputMask(NullInputMask)

inline fun TextView.setNumberInputMask(inputMaskConfigurator: NumberInputMask.() -> Unit = {}) =
    setInputMask(NumberInputMask(), inputMaskConfigurator)

inline fun TextView.setDateTimeInputMask(inputMaskConfigurator: DateTimeInputMask.() -> Unit = {}) =
    setInputMask(DateTimeInputMask(), inputMaskConfigurator)

inline fun <IM : InputMask> TextView.setRawInputMask(
    rawInputMask: IM,
    rawInputMaskConfigurator: IM.() -> Unit = {}
) = setRawInputType(
    rawInputMask.apply(rawInputMaskConfigurator).mask
)

inline fun TextView.setRawTextInputMask(rawInputMaskConfigurator: TextInputMask.() -> Unit = {}) =
    setRawInputMask(TextInputMask(), rawInputMaskConfigurator)

fun TextView.setRawPhoneInputMask() =
    setRawInputMask(PhoneInputMask)

fun TextView.setRawNullInputMask() =
    setRawInputMask(NullInputMask)

inline fun TextView.setRawNumberInputMask(rawInputMaskConfigurator: NumberInputMask.() -> Unit = {}) =
    setRawInputMask(NumberInputMask(), rawInputMaskConfigurator)

inline fun TextView.setRawDateTimeInputMask(rawInputMaskConfigurator: DateTimeInputMask.() -> Unit = {}) =
    setRawInputMask(DateTimeInputMask(), rawInputMaskConfigurator)