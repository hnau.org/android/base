package org.hnau.android.base.ui.rawcolor

import android.content.Context
import androidx.annotation.ColorInt
import org.hnau.android.base.data.getter.tone.Tone
import org.hnau.android.base.data.getter.tone.toTone


@Suppress("EXPERIMENTAL_FEATURE_WARNING")
inline class RawColor(
        @ColorInt val value: Int
) {

    companion object;

}

