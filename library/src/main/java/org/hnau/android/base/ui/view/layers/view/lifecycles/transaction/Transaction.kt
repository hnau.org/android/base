package org.hnau.android.base.ui.view.layers.view.lifecycles.transaction

import android.graphics.Canvas
import android.graphics.Rect
import android.view.View
import org.hnau.android.base.ui.animation.AnimationTicker
import org.hnau.android.base.ui.animation.emergence.EmergenceInfo
import org.hnau.android.base.ui.animation.emergence.offset.EmergenceDirection
import org.hnau.android.base.ui.animation.emergence.offset.Offset
import org.hnau.android.base.ui.view.layers.view.lifecycles.LifecyclesParent
import org.hnau.android.base.ui.view.layers.view.lifecycles.lifecycle.Lifecycle
import org.hnau.base.data.time.Time
import org.hnau.base.extensions.boolean.checkBoolean
import org.hnau.base.extensions.boolean.ifFalse
import org.hnau.base.extensions.boolean.ifTrue
import org.hnau.base.extensions.ifLessThan
import org.hnau.base.extensions.ifNotLessThan
import org.hnau.base.extensions.number.asPercentageInter
import org.hnau.base.extensions.setUnique
import org.hnau.base.extensions.takeIfLargeThan
import org.hnau.base.data.time.now
import org.hnau.emitter.Emitter
import org.hnau.emitter.extensions.listen
import org.hnau.emitter.extensions.useWhen
import org.hnau.emitter.observing.push.always.content.ManualEmitter


class Transaction(
        val direction: EmergenceDirection,
        private val lifecyclesParent: LifecyclesParent,
        private val emergenceInfo: EmergenceInfo,
        val lifecycles: List<Lifecycle>,
        isAnimationsAvailable: Emitter<Boolean>,
        private val onFinished: (Transaction) -> Unit
) {

    private val isActive = ManualEmitter(true)

    private val offset = Offset()

    private var animationsAvailable = false

    private val animationTicker = AnimationTicker()

    private val started = Time.now()

    private val offsetAmount: Float
        get() {
            val duration = emergenceInfo.duration.takeIfLargeThan(Time.zero) ?: return 1f
            val percentage = (Time.now() - started) / duration
            return percentage.toFloat().coerceAtMost(1f)
        }

    private val visibilityPercentage
        get() = when (direction) {
            EmergenceDirection.show -> offsetAmount
            EmergenceDirection.hide -> 1 - offsetAmount
        }

    init {

        lifecycles
                .forEach { it.transaction = this }

        isAnimationsAvailable
                .useWhen(isActive)
                .observe { this.animationsAvailable = it }

    }

    fun start() {
        this.animationsAvailable.checkBoolean(
                ifTrue = {
                    animationTicker.listen(::onAnimationTic)
                    animationTicker.start()
                },
                ifFalse = { onFinished() }
        )
    }

    private fun onAnimationTic() {

        checkNeedFinish().ifTrue {
            onFinished()
            return
        }

        emergenceInfo.fade.minAlpha.ifLessThan(1f) {
            val alpha = calcAlpha()
            lifecycles.forEach { lifecycle ->
                lifecycle.setAlpha(alpha)
            }
        }
        lifecyclesParent.onLifecycleLayout()

    }

    private fun calcAlpha() = visibilityPercentage.asPercentageInter(
            from = emergenceInfo.fade.minAlpha,
            to = 1f
    )

    private fun checkNeedFinish(): Boolean {
        animationsAvailable.ifFalse { return true }
        offsetAmount.ifNotLessThan(1f) { return true }
        return false
    }

    private fun onFinished() {
        isActive::value.setUnique(false)
        animationTicker.stop()
        lifecycles.forEach { it.transaction = null }
        onFinished.invoke(this)
    }

    fun prepareOffset() {
        emergenceInfo.offsetCalculator.calcOffset(
                visibilityPercentage = visibilityPercentage,
                result = offset
        )
        offset.set(
                offset.dx * emergenceInfo.fade.offsetFactor,
                offset.dy * emergenceInfo.fade.offsetFactor
        )
    }

    fun draw(
            canvas: Canvas,
            drawView: (Canvas, View) -> Unit,
            lifecycle: Lifecycle,
            rect: Rect
    ) = lifecycle.draw(
            canvas = canvas,
            drawView = drawView,
            offset = offset,
            rect = rect,
            direction = direction,
            offsetAmount = offsetAmount
    )

}