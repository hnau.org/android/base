package org.hnau.android.base.extensions.view.layout.params.builders

import android.content.Context
import org.hnau.android.base.data.direction.Direction


abstract class MarginGravityLayoutParamsBuilder(
    context: Context
) : MarginLayoutParamsBuilder(
    context = context
) {

    var layoutParamsDirection = Direction.center

}