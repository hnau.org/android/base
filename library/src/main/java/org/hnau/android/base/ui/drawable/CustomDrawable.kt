package org.hnau.android.base.ui.drawable

import android.graphics.ColorFilter
import android.graphics.PixelFormat
import android.graphics.drawable.Drawable


abstract class CustomDrawable : Drawable() {

    override fun setAlpha(alpha: Int) {}

    override fun getOpacity() = PixelFormat.TRANSLUCENT

    override fun setColorFilter(colorFilter: ColorFilter?) {}

}