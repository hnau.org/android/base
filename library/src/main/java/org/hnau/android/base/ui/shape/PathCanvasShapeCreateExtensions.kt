package org.hnau.android.base.ui.shape

import android.graphics.Path


fun PathCanvasShape.Companion.create(
        getPath: () -> Path
) = object : PathCanvasShape() {

    override val path get() = getPath()

}