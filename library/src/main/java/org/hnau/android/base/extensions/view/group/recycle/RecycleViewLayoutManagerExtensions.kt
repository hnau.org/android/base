package org.hnau.android.base.extensions.view.group.recycle

import androidx.recyclerview.widget.RecyclerView


inline fun <LM : RecyclerView.LayoutManager> RecyclerView.setLayoutManager(
        layoutManager: LM,
        configurator: LM.() -> Unit = {}
) = layoutManager
        .apply(configurator)
        .also(::setLayoutManager)

var RecyclerView.existenceLayoutManager: RecyclerView.LayoutManager
    get() = layoutManager ?: throw IllegalStateException("RecyclerView has no layout manager")
    set(value) {
        layoutManager = value
    }