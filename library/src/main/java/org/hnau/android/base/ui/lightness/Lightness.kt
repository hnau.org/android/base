package org.hnau.android.base.ui.lightness

enum class Lightness {
    light,
    dark;

    val opposite
        get() = when (this) {
            light -> dark
            dark -> light
        }

}