package org.hnau.android.base.extensions

import android.app.Activity
import android.content.Context
import android.content.ContextWrapper
import android.content.Intent
import android.os.Bundle
import android.view.Window
import org.hnau.base.utils.tryOrFalse


inline fun <reified T : Activity> Context.createStartActivityIntent(intentConfigurator: Intent.() -> Unit = {}) =
        Intent(this, T::class.java).apply(intentConfigurator)

fun Context.tryStartActivity(intent: Intent, options: Bundle? = null) =
        tryOrFalse { startActivity(intent, options) }

inline fun <reified T : Activity> Context.tryStartActivity(options: Bundle? = null, intentConfigurator: Intent.() -> Unit = {}) =
        tryStartActivity(createStartActivityIntent<T>(intentConfigurator), options)

val Context.window: Window?
    get() = when (this) {
        is Activity -> window
        is ContextWrapper -> baseContext.window
        else -> null
    }