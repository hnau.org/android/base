package org.hnau.android.base.ui.view.presenter.transaction

import org.hnau.android.base.ui.animation.emergence.offset.EmergenceDirection
import org.hnau.android.base.ui.animation.tasks.AnimationTask
import org.hnau.base.data.time.Time


data class OffsetTask(
    override val duration: Time,
    val direction: EmergenceDirection
) : AnimationTask