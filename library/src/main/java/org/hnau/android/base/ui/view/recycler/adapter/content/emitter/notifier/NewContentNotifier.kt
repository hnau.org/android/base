package org.hnau.android.base.ui.view.recycler.adapter.content.emitter.notifier

import androidx.recyclerview.widget.ListUpdateCallback


abstract class NewContentNotifier<T>(
    protected val adapter: ListUpdateCallback
) {

    companion object {

        inline fun <T> create(
            crossinline notify: ListUpdateCallback.(oldContent: List<T>, newContent: List<T>) -> Unit
        ): (ListUpdateCallback) -> NewContentNotifier<T> = { adapter ->
            object : NewContentNotifier<T>(adapter) {
                override fun onContentChanged(oldContent: List<T>, newContent: List<T>) =
                    notify(adapter, oldContent, newContent)
            }
        }

    }

    abstract fun onContentChanged(oldContent: List<T>, newContent: List<T>)

}

fun <T> NewContentNotifier.Companion.simple() =
    create<T> { oldValues, _ ->
        onChanged(0, oldValues.size, null)
    }