package org.hnau.android.base.data.getter.px

import android.os.Build


object ScreenManager {

    val width = Px { context ->
        context.resources.displayMetrics.widthPixels.toFloat()
    }

    val height = Px { context ->
        context.resources.displayMetrics.heightPixels.toFloat()
    }

}

val Px.screen get() = ScreenManager