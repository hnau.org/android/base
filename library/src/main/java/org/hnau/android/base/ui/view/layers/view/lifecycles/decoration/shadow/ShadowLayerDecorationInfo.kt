package org.hnau.android.base.ui.view.layers.view.lifecycles.decoration.shadow

import org.hnau.android.base.data.getter.tone.Tone
import org.hnau.android.base.data.getter.tone.Tones
import org.hnau.android.base.data.getter.tone.replaceAlpha


data class ShadowLayerDecorationInfo(
    val background: Tone = Tones.white,
    val shadow: Tone = Tones.black.replaceAlpha(0.5f)
)