package org.hnau.android.base.data.edges


interface Edges<out T> {
    val xMin: T
    val yMin: T
    val xMax: T
    val yMax: T
}