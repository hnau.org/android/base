package org.hnau.android.base.ui.view.utils.input.filter

import android.text.InputFilter
import android.text.SpannableStringBuilder
import android.text.Spanned


open class FilterInputFilter(
        private val isAllowed: (Char) -> Boolean
) : InputFilter {

    companion object;

    override fun filter(
            source: CharSequence, start: Int, end: Int,
            dest: Spanned, dstart: Int, dend: Int
    ): CharSequence? {
        var modification: SpannableStringBuilder? = null
        var modoff = 0
        for (i in start until end) {
            val c = source[i]
            if (isAllowed(c)) {
                modoff++
            } else {
                if (modification == null) {
                    modification = SpannableStringBuilder(source, start, end)
                    modoff = i - start
                }
                modification.delete(modoff, modoff + 1)
            }
        }
        return modification
    }
}