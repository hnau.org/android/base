package org.hnau.android.base.ui.animation.emergence.offset.calculator

import org.hnau.android.base.ui.animation.emergence.offset.Offset


inline fun OffsetCalculator.Companion.create(
    crossinline calcOffset: (visibilityPercentage: Float, result: Offset) -> Unit
) = object : OffsetCalculator {
    override fun calcOffset(visibilityPercentage: Float, result: Offset) =
        calcOffset.invoke(visibilityPercentage, result)
}