package org.hnau.android.base.ui.decoration.ripple

import android.graphics.Canvas
import android.graphics.PointF
import androidx.annotation.ColorInt
import org.hnau.android.base.extensions.paint
import org.hnau.android.base.ui.decoration.ripple.shape.RippleShape
import org.hnau.base.extensions.boolean.ifFalse
import org.hnau.base.extensions.boolean.ifTrue
import org.hnau.base.extensions.number.asPercentageInter
import org.hnau.emitter.observing.notifier.Notifier


class Ripple(
        private val maxAlpha: Int,
        @ColorInt colorWithoutAlpha: Int,
        private val info: RippleInfo
) {

    private val paint = paint { color = colorWithoutAlpha }

    private val touchPoint = PointF()
    private val drawPoint = PointF()

    private val state = RippleState(info)

    val needRefreshNotifier: Notifier
        get() = state

    fun tryStart(x: Float, y: Float): Boolean {
        state.finished.ifFalse { return false }
        touchPoint.x = x
        touchPoint.y = y
        state.start()
        return true
    }

    fun cancel() = state.cancel()

    fun draw(
            canvas: Canvas,
            shape: RippleShape,
            maxRadius: Float
    ) {
        state.finished.ifTrue { return }
        val alphaFactor: Float
        val radius: Float
        val growingPercentage: Float
        if (state.cancelled) {
            radius = maxRadius
            alphaFactor = 1 - state.stepPercentage.coerceAtMost(1f)
            growingPercentage = 1f
        } else {
            val radiusPercentage = info.growingSizeCurver(state.stepPercentage)
            radius = maxRadius * radiusPercentage.asPercentageInter(info.minRadiusPercentage, 1f)
            alphaFactor = 1f
            growingPercentage = radiusPercentage
        }
        shape.calcCircleCenter(touchPoint, growingPercentage, drawPoint)
        paint.alpha = (maxAlpha * alphaFactor).toInt()
        canvas.drawCircle(drawPoint.x, drawPoint.y, radius, paint)
    }

}