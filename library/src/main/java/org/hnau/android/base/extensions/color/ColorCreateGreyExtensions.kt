package org.hnau.android.base.extensions.color

import androidx.annotation.ColorInt
import androidx.annotation.FloatRange
import androidx.annotation.IntRange


@ColorInt
fun grey(
    @IntRange(from = 0L, to = 255L) alpha: Int,
    @IntRange(from = 0L, to = 255L) lightness: Int
) = argb(alpha, lightness, lightness, lightness)

@ColorInt
fun grey(
    @FloatRange(from = 0.0, to = 1.0) alpha: Float,
    @FloatRange(from = 0.0, to = 1.0) lightness: Float
) = grey(
    alpha = ColorUtils.percentageToValue(alpha),
    lightness = ColorUtils.percentageToValue(lightness)
)

@ColorInt
fun grey(@IntRange(from = 0L, to = 255L) lightness: Int) =
    grey(255, lightness)

@ColorInt
fun grey(@FloatRange(from = 0.0, to = 1.0) lightness: Float) =
    grey(1f, lightness)

val @receiver:IntRange(from = 0, to = 255) Int.asLightness: Int
    @ColorInt get() = grey(this)

val @receiver:FloatRange(from = 0.0, to = 1.0) Float.asLightness: Int
    @ColorInt get() = grey(this)