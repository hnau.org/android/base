package org.hnau.android.base.extensions.path

import android.graphics.Path


//From https://stackoverflow.com/questions/1734745/how-to-create-circle-with-b%C3%A9zier-curves
//private val CUBIC_FOR_CIRCLE_FACTOR = 4 * (sqrt(2f) - 1) / 3
private val CUBIC_FOR_CIRCLE_FACTOR = 0.5522847498307933984f

fun Path.ellipseArcQuarterTo(
        currentX: Number, currentY: Number,
        toX: Number, toY: Number,
        startByX: Boolean
) {
    val toXFloat = toX.toFloat()
    val toYFloat = toY.toFloat()
    val dx = toXFloat - currentX.toFloat()
    val dy = toYFloat - currentY.toFloat()

    if (startByX) {
        rCubicTo(
                dx * CUBIC_FOR_CIRCLE_FACTOR, 0f,
                dx, dy * (1 - CUBIC_FOR_CIRCLE_FACTOR),
                dx, dy
        )
    } else {
        rCubicTo(
                0f, dy * CUBIC_FOR_CIRCLE_FACTOR,
                dx * (1 - CUBIC_FOR_CIRCLE_FACTOR), dy,
                dx, dy
        )
    }
}