package org.hnau.android.base.ui

import android.content.Context
import android.view.View
import org.hnau.android.base.data.getter.px.Px
import org.hnau.android.base.data.getter.px.px
import org.hnau.android.base.data.getter.px.pxInt
import kotlin.math.min


object MeasureUtils {

    val unspecified = make(View.MeasureSpec.UNSPECIFIED, 0)

    fun make(mode: Int, size: Number) = View.MeasureSpec.makeMeasureSpec(size.toInt(), mode)
    fun makeExactly(size: Number) = make(View.MeasureSpec.EXACTLY, size)
    fun makeAtMost(size: Number) = make(View.MeasureSpec.AT_MOST, size)

    fun make(context: Context, mode: Int, size: Px) = make(mode, size(context))
    fun makeExactly(context: Context, size: Px) = make(context, View.MeasureSpec.EXACTLY, size)
    fun makeAtMost(context: Context, size: Px) = make(context, View.MeasureSpec.AT_MOST, size)

    fun getSize(measureSpec: Int) = View.MeasureSpec.getSize(measureSpec)
    fun getMode(measureSpec: Int) = View.MeasureSpec.getMode(measureSpec)
    fun getModeName(measureSpec: Int) = when (val mode = getMode(measureSpec)) {
        View.MeasureSpec.EXACTLY -> "EXACTLY"
        View.MeasureSpec.UNSPECIFIED -> "UNSPECIFIED"
        View.MeasureSpec.AT_MOST -> "AT_MOST"
        else -> "unknown[$mode]"
    }

    fun maxOrNull(measureSpec: Int) = when (getMode(measureSpec)) {
        View.MeasureSpec.UNSPECIFIED -> null
        else -> getSize(measureSpec)
    }

    fun max(measureSpec: Int, forUnspecified: Number = 0) = maxOrNull(
            measureSpec = measureSpec
    ) ?: forUnspecified.toInt()

    fun max(context: Context, measureSpec: Int, forUnspecified: Px) = max(measureSpec, forUnspecified(context))

    fun fit(measureSpec: Int, size: Number) = when (getMode(measureSpec)) {
        View.MeasureSpec.AT_MOST -> min(size.toInt(), getSize(measureSpec))
        View.MeasureSpec.EXACTLY -> getSize(measureSpec)
        else -> size.toInt()
    }

    fun fit(context: Context, measureSpec: Int, size: Px) = fit(measureSpec, size(context))

    inline fun map(measureSpec: Int, converter: (mode: Int, size: Int) -> Int) =
        converter(getMode(measureSpec), getSize(measureSpec))

    inline fun mapSize(measureSpec: Int, converter: (mode: Int, size: Int) -> Number): Int {
        val mode = getMode(measureSpec).takeIf { it != View.MeasureSpec.UNSPECIFIED } ?: return measureSpec
        val size = getSize(measureSpec)
        val convertedSize = converter(mode, size)
        return make(mode, convertedSize)
    }

    inline fun mapSize(context: Context, measureSpec: Int, converter: (mode: Int, size: Px) -> Px) =
            mapSize(measureSpec) { mode, size -> converter(mode, size.px)(context) }

    fun changeSize(measureSpec: Int, offset: Number) =
            mapSize(measureSpec) { _, oldSize -> oldSize.toInt() + offset.toInt() }

    fun changeSize(context: Context, measureSpec: Int, offset: Px) =
            mapSize(measureSpec) { _, oldSize -> oldSize.toInt() + offset.pxInt(context) }

}