package org.hnau.android.base.ui.animation.emergence.offset.calculator

import org.hnau.android.base.ui.animation.emergence.offset.Offset


interface OffsetCalculator {

    companion object;

    fun calcOffset(visibilityPercentage: Float, result: Offset)

    fun reverse() =
        OffsetCalculator.create { visibilityPercentage, result ->
            calcOffset(visibilityPercentage, result)
            result.dx = -result.dx
            result.dy = -result.dy
        }

}