package org.hnau.android.base.ui.decoration

import android.graphics.Canvas
import android.view.View
import org.hnau.base.data.possible.lateinit.Lateinit
import org.hnau.base.data.possible.lateinit.unsafe
import org.hnau.base.data.possible.valueOrThrow
import org.hnau.base.extensions.pass
import org.hnau.emitter.Emitter
import org.hnau.emitter.extensions.useWhen


inline fun ViewDecorations.receiving(
        isActive: Emitter<Boolean>,
        decorationEmitter: Emitter<ViewDecoration>,
        crossinline invalidate: () -> Unit
) = Lateinit
        .unsafe<ViewDecoration>()
        .let { viewDecorationCache ->
            decorationEmitter
                    .useWhen(isActive)
                    .observe { viewDecorationValue ->
                        viewDecorationCache.set(viewDecorationValue)
                        invalidate()
                    }
            ViewDecorations.create {
                viewDecorationCache.checkPossible(
                        ifValueNotExists = ::pass,
                        ifValueExists = { existenceViewDecoration ->
                            existenceViewDecoration(this)
                        }
                )
            }
        }

fun View.receivingViewDecoration(
        isActive: Emitter<Boolean>,
        decorationEmitter: Emitter<ViewDecoration>
): ViewDecoration = ViewDecorations.receiving(
        isActive = isActive,
        decorationEmitter = decorationEmitter,
        invalidate = ::invalidate
)