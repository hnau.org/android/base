package org.hnau.android.base.ui.view.layers.view

import android.content.Context
import android.graphics.Canvas
import android.graphics.Rect
import android.view.MotionEvent
import android.view.View
import android.view.ViewGroup
import org.hnau.android.base.extensions.view.group.children.view
import org.hnau.android.base.extensions.view.group.forEach
import org.hnau.android.base.extensions.view.makeExactlyMeasureSpec
import org.hnau.android.base.extensions.view.measureSpecMax
import org.hnau.android.base.extensions.view.paddingHorizontalSum
import org.hnau.android.base.extensions.view.paddingVerticalSum
import org.hnau.android.base.ui.view.layers.view.lifecycles.Lifecycles
import org.hnau.android.base.ui.view.layers.view.lifecycles.LifecyclesParent
import org.hnau.android.base.ui.view.layers.view.presenting.PresentingLayer
import org.hnau.android.base.ui.view.utils.state.createStateEmitters
import org.hnau.base.extensions.boolean.ifTrue
import org.hnau.emitter.Emitter
import org.hnau.emitter.extensions.useWhen


class LayersView(
        context: Context,
        private val layers: Emitter<List<PresentingLayer>>
) : ViewGroup(
        context
), LifecyclesParent {

    private val state = createStateEmitters()

    private val lifecycles = Lifecycles(
            context = context,
            isAnimationsAvailable = state.isForeground,
            lifecyclesParent = this,
            layers = layers.useWhen(state.isActive)
    )

    private val layoutRect = Rect()

    override fun onLifecycleLayout() =
            invalidate()

    override fun onLayout(p0: Boolean, p1: Int, p2: Int, p3: Int, p4: Int) {
        layoutRect.set(
                paddingLeft, paddingTop,
                width - paddingRight, height - paddingBottom
        )
        forEach { child ->
            child.layout(layoutRect.left, layoutRect.top, layoutRect.right, layoutRect.bottom)
        }
    }

    override fun dispatchTouchEvent(motionEvent: MotionEvent): Boolean {
        lifecycles.isAnimating.ifTrue { return true }
        return super.dispatchTouchEvent(motionEvent)
    }

    override fun refresh() = invalidate()

    private val drawChild: (Canvas, View) -> Unit = { canvas, child ->
        drawChild(canvas, child, drawingTime)
    }

    override fun dispatchDraw(canvas: Canvas) =
            lifecycles.draw(canvas, drawChild, layoutRect)

    override fun addLifecycleView(view: View) = addView(view)
    override fun removeLifecycleView(view: View) = removeView(view)

    override fun onMeasure(widthMeasureSpec: Int, heightMeasureSpec: Int) {
        val maxWidth = measureSpecMax(widthMeasureSpec)
        val maxHeight = measureSpecMax(heightMeasureSpec)
        val childWidthMeasureSpec = makeExactlyMeasureSpec(maxWidth + paddingHorizontalSum)
        val childHeightMeasureSpec = makeExactlyMeasureSpec(maxHeight + paddingVerticalSum)
        forEach { child ->
            child.measure(childWidthMeasureSpec, childHeightMeasureSpec)
        }
        setMeasuredDimension(maxWidth, maxHeight)
    }

}

inline fun ViewGroup.layers(
        layers: Emitter<List<PresentingLayer>>,
        childConfigurator: LayersView.() -> Unit = {}
) = view(
        LayersView(
                context = context,
                layers = layers
        ),
        childConfigurator
)