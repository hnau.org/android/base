package org.hnau.android.base.data.getter.picture

import android.content.Context
import android.graphics.drawable.ColorDrawable
import android.graphics.drawable.Drawable
import androidx.annotation.ColorInt
import androidx.annotation.DrawableRes
import androidx.core.content.ContextCompat
import org.hnau.android.base.data.getter.tone.Tone
import org.hnau.base.extensions.sureNotNull

fun Pictures.create(
    calcValue: (context: Context) -> Drawable
) = Picture { context ->
    calcValue(context)
}

@Suppress("FunctionName")
fun Picture(drawable: Drawable) =
    Pictures.create { drawable }

@Suppress("FunctionName")
fun Picture(
    @DrawableRes drawableResId: Int
) = Pictures.create { context ->
    ContextCompat.getDrawable(context, drawableResId).sureNotNull()
}

fun Pictures.fromColor(@ColorInt color: Int) =
    Picture(ColorDrawable(color))

fun Pictures.fromTone(tone: Tone) =
    Picture { context -> ColorDrawable(tone(context)) }

fun Drawable.toPicture() =
    Picture(this)

fun @receiver:ColorInt Int.toPicture() =
    Pictures.fromColor(this)

fun Tone.toPicture() =
    Pictures.fromTone(this)