package org.hnau.android.base.ui.view.utils.input.ime

import android.os.Build
import android.view.inputmethod.EditorInfo
import androidx.annotation.RequiresApi
import org.hnau.base.extensions.number.useFlag


class ActionImeMask private constructor(
    val actionId: Int
) : ImeMask {

    companion object {

        fun createUnspecified() = ActionImeMask(EditorInfo.IME_ACTION_UNSPECIFIED)
        fun createNone() = ActionImeMask(EditorInfo.IME_ACTION_NONE)
        fun createGo() = ActionImeMask(EditorInfo.IME_ACTION_GO)
        fun createSearch() = ActionImeMask(EditorInfo.IME_ACTION_SEARCH)
        fun createSend() = ActionImeMask(EditorInfo.IME_ACTION_SEND)
        fun createNext() = ActionImeMask(EditorInfo.IME_ACTION_NEXT)
        fun createDone() = ActionImeMask(EditorInfo.IME_ACTION_DONE)
        fun createPrevious() = ActionImeMask(EditorInfo.IME_ACTION_PREVIOUS)

    }

    private var flags = 0

    private fun useFlag(flag: Int, add: Boolean = true) =
        apply { flags = flags.useFlag(flag, add) }

    @RequiresApi(Build.VERSION_CODES.O)
    fun applyNoPersonalizedLearningFlag(add: Boolean = true) =
        useFlag(EditorInfo.IME_FLAG_NO_PERSONALIZED_LEARNING, add)

    fun applyNoFullscreenFlag(add: Boolean = true) =
        useFlag(EditorInfo.IME_FLAG_NO_FULLSCREEN, add)

    fun applyNavigatePreviousFlag(add: Boolean = true) =
        useFlag(EditorInfo.IME_FLAG_NAVIGATE_PREVIOUS, add)

    fun applyNavigateNextFlag(add: Boolean = true) =
        useFlag(EditorInfo.IME_FLAG_NAVIGATE_NEXT, add)

    fun applyNoExtractUiFlag(add: Boolean = true) =
        useFlag(EditorInfo.IME_FLAG_NO_EXTRACT_UI, add)

    fun applyNoAccessoryActionFlag(add: Boolean = true) =
        useFlag(EditorInfo.IME_FLAG_NO_ACCESSORY_ACTION, add)

    fun applyNoEnterActionFlag(add: Boolean = true) =
        useFlag(EditorInfo.IME_FLAG_NO_ENTER_ACTION, add)

    fun applyForceAsciiFlag(add: Boolean = true) =
        useFlag(EditorInfo.IME_FLAG_FORCE_ASCII, add)

    override val mask: Int
        get() = actionId + flags
}
