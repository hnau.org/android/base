package org.hnau.android.base.ui.animation.tasks

import org.hnau.base.data.time.Time


interface AnimationTask {

    val duration: Time

}