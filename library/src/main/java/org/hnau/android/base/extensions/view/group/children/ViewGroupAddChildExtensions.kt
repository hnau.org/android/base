package org.hnau.android.base.extensions.view.group.children

import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.EditText
import android.widget.FrameLayout
import android.widget.HorizontalScrollView
import android.widget.ImageView
import android.widget.ScrollView
import android.widget.TextView
import androidx.core.widget.NestedScrollView
import androidx.recyclerview.widget.RecyclerView

inline fun ViewGroup.view(childConfigurator: View.() -> Unit = {}) =
    view(View(context), childConfigurator)

inline fun ViewGroup.frame(childConfigurator: FrameLayout.() -> Unit = {}) =
    view(FrameLayout(context), childConfigurator)

inline fun ViewGroup.image(childConfigurator: ImageView.() -> Unit = {}) =
    view(ImageView(context), childConfigurator)

inline fun ViewGroup.edit(childConfigurator: EditText.() -> Unit = {}) =
    view(EditText(context), childConfigurator)

inline fun ViewGroup.text(childConfigurator: TextView.() -> Unit = {}) =
    view(TextView(context), childConfigurator)

inline fun ViewGroup.scroll(childConfigurator: ScrollView.() -> Unit = {}) =
    view(ScrollView(context), childConfigurator)

inline fun ViewGroup.horizontalScroll(childConfigurator: HorizontalScrollView.() -> Unit = {}) =
    view(HorizontalScrollView(context), childConfigurator)

inline fun ViewGroup.nestedScroll(childConfigurator: NestedScrollView.() -> Unit = {}) =
    view(NestedScrollView(context), childConfigurator)

inline fun ViewGroup.recycler(childConfigurator: RecyclerView.() -> Unit = {}) =
    view(RecyclerView(context), childConfigurator)

inline fun ViewGroup.button(childConfigurator: Button.() -> Unit = {}) =
    view(Button(context), childConfigurator)
