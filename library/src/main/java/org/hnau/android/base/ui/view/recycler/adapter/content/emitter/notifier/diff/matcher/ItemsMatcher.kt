package org.hnau.android.base.ui.view.recycler.adapter.content.emitter.notifier.diff.matcher


interface ItemsMatcher<T> {

    companion object;

    fun itemsAreSame(item1: T, item2: T): Boolean

}

fun <T> ItemsMatcher.Companion.create(
    itemsAreSame: (item1: T, item2: T) -> Boolean
) = object :
    ItemsMatcher<T> {

    override fun itemsAreSame(item1: T, item2: T) =
        itemsAreSame.invoke(item1, item2)

}

fun <T> ItemsMatcher.Companion.byId(
    extractId: T.() -> Any
) = create<T> { item1, item2 ->
    item1.extractId() == item2.extractId()
}

fun <T> ItemsMatcher.Companion.byEquals() =
    create<T> { item1, item2 -> item1 == item2 }

fun <T> ItemsMatcher.Companion.byReference() =
    create<T> { item1, item2 -> item1 === item2 }