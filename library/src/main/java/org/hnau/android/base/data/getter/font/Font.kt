package org.hnau.android.base.data.getter.font

import android.content.Context
import android.graphics.Typeface
import org.hnau.android.base.data.getter.base.ContextGetter
import org.hnau.android.base.data.getter.base.CachedContextGetter


typealias Font = ContextGetter<Typeface>

fun Font(get: (Context) -> Typeface): Font =
    CachedContextGetter(get)

object Fonts {

    val default = Font(Typeface.DEFAULT)
    val bold = Font(Typeface.DEFAULT_BOLD)
    val mono = Font(Typeface.MONOSPACE)
    val sans = Font(Typeface.SANS_SERIF)
    val serif = Font(Typeface.SERIF)

}