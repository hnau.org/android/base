package org.hnau.android.base.ui.animation.emergence.offset


enum class EmergenceDirection {

    show,
    hide;

    companion object;

}