package org.hnau.android.base.ui.drawable.partout.calculators

import android.content.Context
import android.graphics.Point
import org.hnau.android.base.data.direction.Direction
import org.hnau.android.base.data.getter.px.Px
import org.hnau.android.base.data.getter.px.pxInt
import org.hnau.android.base.ui.drawable.partout.PartoutCalculator


open class SizePartoutCalculator(
        context: Context,
        width: Px,
        height: Px,
        direction: Direction = Direction.center
) : DirectionPartoutCalculator(
        direction = direction
) {

    constructor(
            context: Context,
            size: Px,
            direction: Direction = Direction.center
    ) : this(
            context = context,
            width = size,
            height = size,
            direction = direction
    )

    private val width = width.pxInt(context)
    private val height = height.pxInt(context)

    override fun calcContentSize(
            parentWidth: Int,
            parentHeight: Int,
            contentPreferredWidth: Int,
            contentPreferredHeight: Int,
            result: Point
    ) = result.set(
            width,
            height
    )

    override fun calcIntrinsicWrapperWidth(contentIntrinsicWidth: Int) = width
    override fun calcIntrinsicWrapperHeight(contentIntrinsicHeight: Int) = height

}

fun PartoutCalculator.Companion.size(
        context: Context,
        width: Px,
        height: Px,
        direction: Direction = Direction.center
) = SizePartoutCalculator(
        context = context,
        width = width,
        height = height,
        direction = direction
)