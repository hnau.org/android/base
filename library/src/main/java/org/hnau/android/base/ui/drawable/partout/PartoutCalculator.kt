package org.hnau.android.base.ui.drawable.partout

import android.graphics.Rect
import org.hnau.android.base.ui.drawable.DrawableWrapper


interface PartoutCalculator : DrawableWrapper.WrapperSizeCalculator {

     companion object

    fun calcContentBounds(
        parentWidth: Int,
        parentHeight: Int,
        contentPreferredWidth: Int,
        contentPreferredHeight: Int,
        result: Rect
    )

}