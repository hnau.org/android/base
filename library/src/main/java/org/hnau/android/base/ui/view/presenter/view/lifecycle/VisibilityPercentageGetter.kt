package org.hnau.android.base.ui.view.presenter.view.lifecycle


interface VisibilityPercentageGetter {

    val visibilityPercentage: Float

}