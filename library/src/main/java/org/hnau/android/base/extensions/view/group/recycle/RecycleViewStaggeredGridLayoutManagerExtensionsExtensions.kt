package org.hnau.android.base.extensions.view.group.recycle

import androidx.recyclerview.widget.RecyclerView
import androidx.recyclerview.widget.StaggeredGridLayoutManager
import org.hnau.android.base.data.course.Course
import org.hnau.android.base.data.course.checkCourse
import org.hnau.base.extensions.boolean.checkBoolean


val Course.staggeredGridLayoutManagerOrientation
    get() = checkCourse({ StaggeredGridLayoutManager.HORIZONTAL }, { StaggeredGridLayoutManager.VERTICAL })

var StaggeredGridLayoutManager.course: Course
    get() = (orientation == StaggeredGridLayoutManager.HORIZONTAL).checkBoolean(
            ifTrue = { Course.horizontal },
            ifFalse = { Course.vertical }
    )
    set(value) {
        orientation = value.gridLayoutManagerOrientation
    }

inline fun RecyclerView.setStaggeredGridLayoutManager(
        spanCount: Int,
        orientation: Course,
        reverseLayout: Boolean = false,
        configurator: StaggeredGridLayoutManager.() -> Unit = {}
) = setLayoutManager(
        layoutManager = StaggeredGridLayoutManager(spanCount, orientation.gridLayoutManagerOrientation),
        configurator = configurator
)

val RecyclerView.staggeredGridLayoutManager
    get() = existenceLayoutManager as? StaggeredGridLayoutManager
            ?: throw IllegalStateException("layoutManager is not StaggeredGridLayoutManager")