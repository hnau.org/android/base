package org.hnau.android.base.ui.view.input.info

import org.hnau.android.base.data.getter.font.Font
import org.hnau.android.base.data.getter.font.Fonts
import org.hnau.android.base.data.getter.px.Px
import org.hnau.android.base.data.getter.px.dp


data class InputViewInfo(
        val title: InputViewTitleInfo = InputViewTitleInfo(),
        val border: InputViewBorderInfo = InputViewBorderInfo(),
        val horizontalSpace: Px = 16.dp,
        val font: Font = Fonts.default
) {

    companion object

}