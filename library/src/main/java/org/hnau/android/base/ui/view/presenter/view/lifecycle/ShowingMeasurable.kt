package org.hnau.android.base.ui.view.presenter.view.lifecycle


interface ShowingMeasurable : VisibilityPercentageGetter, ViewMeasuredSizeGetter