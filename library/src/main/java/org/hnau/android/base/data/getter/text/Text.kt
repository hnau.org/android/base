package org.hnau.android.base.data.getter.text

import android.content.Context
import org.hnau.android.base.data.getter.base.ContextGetter
import org.hnau.android.base.data.getter.base.CachedContextGetter

typealias Text = ContextGetter<String>

fun Text(get: (Context) -> String): Text =
    CachedContextGetter(get)

object Texts {

    val empty = Text("")

}