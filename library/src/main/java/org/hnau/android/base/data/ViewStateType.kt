package org.hnau.android.base.data


enum class ViewStateType(
    val key: Int
) {

    aboveAnchor(android.R.attr.state_above_anchor),
    accelerated(android.R.attr.state_accelerated),
    activated(android.R.attr.state_activated),
    active(android.R.attr.state_active),
    checkable(android.R.attr.state_checkable),
    checked(android.R.attr.state_checked),
    dragCanAccept(android.R.attr.state_drag_can_accept),
    dragHovered(android.R.attr.state_drag_hovered),
    empty(android.R.attr.state_empty),
    enabled(android.R.attr.state_enabled),
    expanded(android.R.attr.state_expanded),
    first(android.R.attr.state_first),
    focused(android.R.attr.state_focused),
    hovered(android.R.attr.state_hovered),
    last(android.R.attr.state_last),
    middle(android.R.attr.state_middle),
    multiline(android.R.attr.state_multiline),
    pressed(android.R.attr.state_pressed),
    selected(android.R.attr.state_selected),
    single(android.R.attr.state_single),
    windowFocused(android.R.attr.state_window_focused)

}