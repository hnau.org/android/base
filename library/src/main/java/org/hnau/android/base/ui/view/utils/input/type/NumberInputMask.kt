package org.hnau.android.base.ui.view.utils.input.type

import android.text.InputType
import org.hnau.android.base.ui.view.utils.input.type.base.ClassInputMask
import org.hnau.android.base.ui.view.utils.input.type.base.setFlag
import org.hnau.android.base.ui.view.utils.input.type.base.setVariation


class NumberInputMask() : ClassInputMask(InputType.TYPE_CLASS_NUMBER) {

    fun setTimeVariation() =
        setVariation(InputType.TYPE_NUMBER_VARIATION_PASSWORD)

    fun setSignedFlag(add: Boolean = true) =
        setFlag(InputType.TYPE_NUMBER_FLAG_SIGNED, add)

    fun setDecimalFlag(add: Boolean = true) =
        setFlag(InputType.TYPE_NUMBER_FLAG_DECIMAL, add)

}