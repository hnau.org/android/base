package org.hnau.android.base.ui.drawable.partout.calculators

import org.hnau.android.base.data.direction.Direction
import org.hnau.android.base.ui.drawable.partout.PartoutCalculator


class IndependentPartoutCalculator(
    direction: Direction = Direction.center
) : ScalePartoutCalculator(
    scale = 1,
    direction = direction
)

fun PartoutCalculator.Companion.independent(
        direction: Direction = Direction.center
) = IndependentPartoutCalculator(
        direction = direction
)