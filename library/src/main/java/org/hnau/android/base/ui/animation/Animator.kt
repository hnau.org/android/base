package org.hnau.android.base.ui.animation

import org.hnau.base.data.time.Time
import org.hnau.base.data.time.now
import org.hnau.emitter.extensions.observeWithEmitterDetacher


object Animator {

    var maxSupportedHeaviness = AnimationHeaviness.hard

    fun doAnimation(
            duration: Time,
            heaviness: AnimationHeaviness = AnimationHeaviness.medium,
            onBegin: (() -> Unit)? = null,
            onEnd: (() -> Unit)? = null,
            onProgress: (Float) -> Unit
    ) {

        onBegin?.invoke()
        onProgress.invoke(0f)

        val normalizedDuration = if (maxSupportedHeaviness.value < heaviness.value) Time.zero else duration
        if (normalizedDuration <= Time.zero) {
            onProgress.invoke(1f)
            onEnd?.invoke()
            return
        }

        val start = Time.now()

        AnimationMetronome.observeWithEmitterDetacher { _, detacher ->

            val percentage = (Time.now() - start) / normalizedDuration
            onProgress(percentage.coerceAtMost(1.0).toFloat())

            if (percentage >= 1) {
                onEnd?.invoke()
                detacher()
            }
        }

    }

}