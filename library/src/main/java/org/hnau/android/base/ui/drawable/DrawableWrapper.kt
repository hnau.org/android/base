package org.hnau.android.base.ui.drawable

import android.graphics.ColorFilter
import android.graphics.PixelFormat
import android.graphics.drawable.Drawable
import org.hnau.base.extensions.boolean.checkBoolean


abstract class DrawableWrapper(
        protected val content: Drawable,
        private val wrapperSizeCalculator: WrapperSizeCalculator = WrapperSizeCalculator.default
) : Drawable(), Drawable.Callback {

    interface WrapperSizeCalculator {

        companion object {
            val default = object : WrapperSizeCalculator {
                override fun calcIntrinsicWrapperWidth(contentIntrinsicWidth: Int) =
                        contentIntrinsicWidth

                override fun calcIntrinsicWrapperHeight(contentIntrinsicHeight: Int) =
                        contentIntrinsicHeight
            }
        }

        fun calcIntrinsicWrapperWidth(contentIntrinsicWidth: Int): Int
        fun calcIntrinsicWrapperHeight(contentIntrinsicHeight: Int): Int

    }

    init {
        content.callback = this
    }

    override fun getIntrinsicWidth() =
            wrapperSizeCalculator.calcIntrinsicWrapperWidth(content.intrinsicWidth)

    override fun getIntrinsicHeight() =
            wrapperSizeCalculator.calcIntrinsicWrapperHeight(content.intrinsicHeight)

    @Suppress("UsePropertyAccessSyntax")
    override fun setAlpha(alpha: Int) =
            content.setAlpha(alpha)

    @Suppress("DEPRECATION")
    override fun getOpacity(): Int {
        val contentOpacity = content.opacity
        return (contentOpacity == PixelFormat.OPAQUE)
                .checkBoolean(
                        { PixelFormat.TRANSPARENT },
                        { contentOpacity }
                )
    }

    override fun setColorFilter(colorFilter: ColorFilter?) =
            content.setColorFilter(colorFilter)

    override fun invalidateSelf() {
        super.invalidateSelf()
        content.invalidateSelf()
    }

    override fun unscheduleDrawable(who: Drawable, what: Runnable) {
        callback?.unscheduleDrawable(this, what)
    }

    override fun invalidateDrawable(who: Drawable) {
        callback?.invalidateDrawable(this)
    }

    override fun scheduleDrawable(who: Drawable, what: Runnable, time: Long) {
        callback?.scheduleDrawable(this, what, time)
    }

}