package org.hnau.android.base.extensions.view.base.edgesOld

import android.os.Build
import android.widget.EdgeEffect
import androidx.annotation.ColorInt
import androidx.annotation.RequiresApi
import androidx.recyclerview.widget.RecyclerView
import org.hnau.android.base.data.getter.tone.Tone

fun RecyclerView.setEdgesOldColor(
        @ColorInt color: Int
) {
    if (Build.VERSION.SDK_INT < Build.VERSION_CODES.LOLLIPOP) {
        return
    }
    @RequiresApi(Build.VERSION_CODES.LOLLIPOP)
    edgeEffectFactory = object : RecyclerView.EdgeEffectFactory() {
        override fun createEdgeEffect(view: RecyclerView, direction: Int) =
                EdgeEffect(context).apply { this.color = color }
    }
}

fun RecyclerView.setEdgesOldTone(
        tone: Tone
) = setEdgesOldColor(
        color = tone(context)
)


