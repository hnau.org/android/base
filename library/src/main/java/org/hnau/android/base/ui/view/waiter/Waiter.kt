package org.hnau.android.base.ui.view.waiter

import android.content.Context
import android.graphics.Canvas
import android.view.MotionEvent
import android.view.View
import org.hnau.android.base.data.getter.px.Px
import org.hnau.android.base.data.getter.tone.Tone
import org.hnau.android.base.extensions.color.alpha
import org.hnau.android.base.extensions.paint
import org.hnau.android.base.extensions.view.measureSpecFit
import org.hnau.android.base.extensions.view.paddingHorizontalSum
import org.hnau.android.base.extensions.view.paddingVerticalSum
import org.hnau.android.base.ui.animation.AnimationMetronome
import org.hnau.android.base.ui.animation.smoothFloat
import org.hnau.android.base.ui.view.utils.state.createStateEmitters
import org.hnau.base.data.time.Time
import org.hnau.emitter.Emitter
import org.hnau.emitter.extensions.and
import org.hnau.emitter.extensions.listen
import org.hnau.emitter.extensions.mapIsLargeThan
import org.hnau.emitter.extensions.toFloat
import org.hnau.emitter.extensions.useWhen


abstract class Waiter(
        context: Context,
        isVisible: Emitter<Boolean>,
        visibilitySwitchingTime: Time,
        backgroundTone: Tone,
        private val preferredWidth: Px,
        private val preferredHeight: Px
) : View(
        context
) {

    companion object;

    private val backgroundPaint = paint {
        color = backgroundTone(context)
    }

    private val backgroundAlpha =
            backgroundTone(context).alpha

    private val state = createStateEmitters()

    private val visibilityPercentage =
            isVisible
                    .toFloat()
                    .smoothFloat(visibilitySwitchingTime)

    private var visibilityPercentageValue = 0f
        set(value) {
            field = value
            invalidate()
        }

    private val isAnimating =
            state.isForeground and visibilityPercentage.mapIsLargeThan(0f)

    init {

        visibilityPercentage
                .useWhen(state.isForeground)
                .observe { visibilityPercentageValue = it }

        AnimationMetronome
                .useWhen(isAnimating)
                .listen(::invalidate)
    }

    override fun onTouchEvent(event: MotionEvent?) =
            visibilityPercentageValue > 0.01

    override fun draw(canvas: Canvas) {
        super.draw(canvas)
        val percentage = visibilityPercentageValue
        backgroundPaint.setAlpha((percentage * backgroundAlpha).toInt())
        canvas.drawRect(0f, 0f, width.toFloat(), height.toFloat(), backgroundPaint)
        drawContent(canvas, percentage)
    }

    protected abstract fun drawContent(
            canvas: Canvas,
            visibilityPercentage: Float
    )

    override fun onMeasure(
            widthMeasureSpec: Int,
            heightMeasureSpec: Int
    ) = setMeasuredDimension(
            measureSpecFit(
                    measureSpec = widthMeasureSpec,
                    size = preferredWidth(context) + paddingHorizontalSum + 2
            ),
            measureSpecFit(
                    measureSpec = heightMeasureSpec,
                    size = preferredHeight(context) + paddingVerticalSum + 2
            )
    )

}