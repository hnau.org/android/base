package org.hnau.android.base.system.broadcast

import android.content.BroadcastReceiver
import android.content.IntentFilter


interface BroadcastRegister {

    companion object;

    fun register(
        broadcastReceiver: BroadcastReceiver,
        intentFilter: IntentFilter
    )

    fun unregister(
        broadcastReceiver: BroadcastReceiver
    )

}