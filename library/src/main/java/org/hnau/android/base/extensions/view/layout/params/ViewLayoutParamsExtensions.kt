package org.hnau.android.base.extensions.view.layout.params

import android.view.View
import org.hnau.android.base.extensions.view.layout.params.builders.FrameLayoutParamsBuilder
import org.hnau.android.base.extensions.view.layout.params.builders.LayoutParamsBuilder
import org.hnau.android.base.extensions.view.layout.params.builders.LinearLayoutParamsBuilder
import org.hnau.android.base.extensions.view.layout.params.builders.MarginLayoutParamsBuilder
import org.hnau.android.base.extensions.view.layout.params.builders.RecycleLayoutParamsBuilder


inline fun <LPB: LayoutParamsBuilder> View.setLayoutParams(layoutParamsBuilder: LPB, configurator: LPB.() -> Unit = {}) =
    setLayoutParams(layoutParamsBuilder.apply(configurator).build())

inline fun View.layoutParams(configurator: LayoutParamsBuilder.() -> Unit = {}) =
    setLayoutParams(LayoutParamsBuilder(context), configurator)

inline fun View.marginParams(configurator: MarginLayoutParamsBuilder.() -> Unit = {}) =
    setLayoutParams(MarginLayoutParamsBuilder(context), configurator)

inline fun View.linearParams(configurator: LinearLayoutParamsBuilder.() -> Unit = {}) =
    setLayoutParams(LinearLayoutParamsBuilder(context), configurator)

inline fun View.frameParams(configurator: FrameLayoutParamsBuilder.() -> Unit = {}) =
    setLayoutParams(FrameLayoutParamsBuilder(context), configurator)

inline fun View.recycleParams(configurator: RecycleLayoutParamsBuilder.() -> Unit = {}) =
    setLayoutParams(RecycleLayoutParamsBuilder(context), configurator)