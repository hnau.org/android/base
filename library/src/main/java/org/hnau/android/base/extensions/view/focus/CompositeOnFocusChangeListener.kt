package org.hnau.android.base.extensions.view.focus

import android.view.View


class CompositeOnFocusChangeListener : View.OnFocusChangeListener {

    private val listeners = ArrayList<View.OnFocusChangeListener>()

    val isEmpty get() = listeners.isEmpty()

    fun add(listener: View.OnFocusChangeListener) {
        listeners.add(listener)
    }

    fun remove(listener: View.OnFocusChangeListener) {
        listeners.remove(listener)
    }

    override fun onFocusChange(
            v: View?,
            hasFocus: Boolean
    ) = listeners.forEach { listener ->
        listener.onFocusChange(v, hasFocus)
    }

}