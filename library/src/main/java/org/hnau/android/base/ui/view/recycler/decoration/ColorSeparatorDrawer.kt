package org.hnau.android.base.ui.view.recycler.decoration

import android.content.Context
import android.graphics.Canvas
import android.graphics.Paint
import android.graphics.Rect
import org.hnau.android.base.data.course.Course
import org.hnau.android.base.data.getter.px.Px
import org.hnau.android.base.data.getter.tone.Tone
import org.hnau.android.base.extensions.paint
import org.hnau.base.data.possible.getOrInitialize
import org.hnau.base.data.possible.lateinit.Lateinit
import org.hnau.base.data.possible.lateinit.sync
import org.hnau.base.utils.safe.SafeContext
import org.hnau.base.utils.safe.unsafe


class ColorSeparatorDrawer(
        tone: Tone,
        override val thickness: Px
) : SeparatorDrawer {

    private val paint: (Context) -> Paint =
            Lateinit.sync<Paint>(SafeContext.unsafe).let { lateinit ->
                { context ->
                    lateinit.getOrInitialize { paint { color = tone(context) } }
                }
            }

    override fun draw(context: Context, canvas: Canvas, bounds: Rect, course: Course) =
            canvas.drawRect(bounds, paint(context))

}

fun SeparatorDrawer.Companion.color(
        tone: Tone,
        thickness: Px
) = ColorSeparatorDrawer(
        tone = tone,
        thickness = thickness
)

