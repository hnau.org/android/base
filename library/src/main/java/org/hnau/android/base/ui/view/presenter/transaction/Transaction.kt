package org.hnau.android.base.ui.view.presenter.transaction

import android.view.View
import org.hnau.android.base.ui.view.presenter.view.wrapper.NullViewWrapper
import org.hnau.android.base.ui.view.presenter.view.wrapper.ViewWrapper
import org.hnau.android.base.ui.view.presenter.view.wrapper.existence.PresentingInfo
import org.hnau.android.base.ui.view.presenter.view.wrapper.viewOrEmpty


data class Transaction(
    val view: ViewWrapper = NullViewWrapper
) {

    constructor(
        view: View?,
        presentingInfo: PresentingInfo = PresentingInfo()
    ) : this(
        view = ViewWrapper.viewOrEmpty(view, presentingInfo)
    )

}