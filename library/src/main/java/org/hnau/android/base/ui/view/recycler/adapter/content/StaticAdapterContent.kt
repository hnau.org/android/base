package org.hnau.android.base.ui.view.recycler.adapter.content

import androidx.recyclerview.widget.ListUpdateCallback


class StaticAdapterContent<T>(
    private val content: List<T>
) : AdapterContent<T> {

    override val size
        get() = content.size

    override fun getItem(position: Int) =
        content[position]

}

fun <T> AdapterContent.Companion.static(
    content: List<T>
): (ListUpdateCallback) -> AdapterContent<T> = {
    StaticAdapterContent(content)
}