package org.hnau.android.base.ui.view.input.delegate

import android.content.Context
import android.graphics.Canvas
import android.graphics.Paint
import android.graphics.Path
import android.graphics.RectF
import org.hnau.android.base.data.edges.Edges
import org.hnau.android.base.data.edges.typed.width
import org.hnau.android.base.data.getter.tone.Tone
import org.hnau.android.base.data.insets.Insets
import org.hnau.android.base.data.insets.horizontalSum
import org.hnau.android.base.data.insets.typed.horizontalSum
import org.hnau.android.base.extensions.path.addRoundCornerRect
import org.hnau.android.base.extensions.clipPathOut
import org.hnau.android.base.extensions.inState
import org.hnau.android.base.extensions.paint
import org.hnau.android.base.extensions.path.addConvexSidesRect
import org.hnau.android.base.ui.view.ViewScroll
import org.hnau.android.base.ui.view.input.info.InputViewBorderInfo
import org.hnau.android.base.ui.view.scroll
import org.hnau.emitter.Emitter
import org.hnau.emitter.extensions.useWhen
import org.hnau.emitter.observing.push.lateinit.combine
import kotlin.math.min
import kotlin.math.round


class BorderDelegate(
        context: Context,
        borderInfo: InputViewBorderInfo,
        boundsEmitter: Emitter<Edges<Int>>,
        getInsets: () -> Insets<Float>,
        titleTopHeight: Float,
        tone: Emitter<Tone>,
        isActive: Emitter<Boolean>,
        invalidate: () -> Unit,
        titleToTopPercentageSafe: Emitter<Float>,
        titleTopWidth: Float,
        private val viewScroll: ViewScroll
) {

    private data class LayoutInfo(
            var bounds: Edges<Int> = Edges(0),
            var titleToTopPercentage: Float = 0f
    )

    private val titleSeparation = borderInfo.titleSeparation(context)

    val borderWidth = round(borderInfo.width(context) / 2) * 2

    private val borderWidthDiv2 = this.borderWidth / 2f
    private val titleTopHeightDiv2 = titleTopHeight / 2f

    private val paint = paint {
        style = Paint.Style.STROKE
        strokeWidth = borderWidth
    }
    private val borderPath = Path()
    private val clipOutPath = Path()

    init {

        @Suppress("DEPRECATION")
        LayoutInfo()
                .let { layerInfo ->
                    Emitter.combine(
                            firstSource = boundsEmitter,
                            secondSource = titleToTopPercentageSafe
                    ) { bounds, titleToTopPercentage ->
                        layerInfo.apply {
                            this.bounds = bounds
                            this.titleToTopPercentage = titleToTopPercentage
                        }
                    }
                }
                .observe { (bounds, titleToTopPercentage) ->

                    val top = bounds.yMin + titleTopHeightDiv2

                    run {
                        borderPath.reset()
                        borderPath.addConvexSidesRect(
                                left = bounds.xMin + borderWidthDiv2,
                                top = top,
                                right = bounds.xMax - borderWidthDiv2,
                                bottom = bounds.yMax - borderWidthDiv2
                        )
                    }

                    run {
                        val insets = getInsets()
                        val clipOutMaxWidth = min(
                                titleSeparation + titleTopWidth + titleSeparation,
                                bounds.width - insets.horizontalSum + titleSeparation + titleSeparation
                        )
                        val clipOutWidth = clipOutMaxWidth * titleToTopPercentage
                        val clipOutCenter = bounds.xMin + insets.left + titleTopWidth / 2
                        clipOutPath.reset()
                        clipOutPath.addRect(
                                clipOutCenter - clipOutWidth / 2,
                                top - borderWidthDiv2 - 1f,
                                clipOutCenter + clipOutWidth / 2,
                                top + borderWidthDiv2 + 1f,
                                Path.Direction.CW
                        )
                    }

                    invalidate()
                }
        tone
                .useWhen(isActive)
                .observe {
                    paint.color = it(context)
                    invalidate()
                }
    }

    fun draw(
            canvas: Canvas
    ) = canvas.inState {
        scroll(viewScroll)
        clipPathOut(clipOutPath)
        drawPath(borderPath, paint)
    }

}