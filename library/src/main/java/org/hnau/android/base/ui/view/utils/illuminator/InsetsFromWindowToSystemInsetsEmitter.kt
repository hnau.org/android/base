package org.hnau.android.base.ui.view.utils.illuminator

import android.os.Build
import android.view.View
import org.hnau.android.base.data.insets.Insets
import org.hnau.android.base.data.insets.MutableInsets
import org.hnau.base.extensions.boolean.ifFalse
import org.hnau.emitter.Emitter
import org.hnau.emitter.extensions.callIfTrue
import org.hnau.emitter.observing.push.lateinit.LateinitEmitter


fun InsetsFromWindowToSystemInsetsEmitter(
        view: View,
        isActive: Emitter<Boolean>
): Emitter<Insets<Int>> = object : LateinitEmitter<Insets<Int>>() {

    private val value = MutableInsets(0)

    init {
        initialize(
                view = view,
                isActive = isActive
        )
    }

    private fun initialize(
            view: View,
            isActive: Emitter<Boolean>
    ) {

        (Build.VERSION.SDK_INT >= 21).ifFalse {
            set(value)
            return
        }

        isActive
                .callIfTrue()
                .observe { view.requestApplyInsets() }

        view.setOnApplyWindowInsetsListener { _, insets ->
            value.left = insets.systemWindowInsetLeft
            value.top = insets.systemWindowInsetTop
            value.right = insets.systemWindowInsetRight
            value.bottom = insets.systemWindowInsetBottom
            set(value)
            insets
        }

    }


}