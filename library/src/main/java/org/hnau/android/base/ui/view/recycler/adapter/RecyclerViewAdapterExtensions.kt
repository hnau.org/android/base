package org.hnau.android.base.ui.view.recycler.adapter

import android.view.ViewGroup
import androidx.recyclerview.widget.ListUpdateCallback
import androidx.recyclerview.widget.RecyclerView
import org.hnau.android.base.ui.view.lister.adapter.AutoViewHolder
import org.hnau.android.base.ui.view.recycler.adapter.content.AdapterContent
import org.hnau.android.base.ui.view.recycler.adapter.item.ItemViewProvider


fun <T> RecyclerView.adapter(
    info: AdapterInfo<T>
) {
    adapter = object : RecyclerView.Adapter<AutoViewHolder<T>>() {

        private val content = info.content(createListUpdateCallback())
        private val viewProvider = info.viewProvider

        override fun onCreateViewHolder(
            parent: ViewGroup,
            viewType: Int
        ) = AutoViewHolder.create<T> { content ->
            viewProvider.createItemView(
                context = parent.context,
                itemViewType = viewType,
                content = content
            )
        }

        override fun getItemViewType(
            position: Int
        ) = viewProvider.getItemViewType(
            item = content.getItem(position)
        )

        override fun getItemCount() = content.size

        override fun onBindViewHolder(holder: AutoViewHolder<T>, position: Int) {
            val item = content.getItem(position)
            holder.setContent(item)
        }

    }
}

fun <T> RecyclerView.adapter(
    content: (ListUpdateCallback) -> AdapterContent<T>,
    viewProvider: ItemViewProvider<T>
) = adapter(
    info = AdapterInfo(
        content = content,
        viewProvider = viewProvider
    )
)