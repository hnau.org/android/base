package org.hnau.android.base.system.preferences

import android.content.SharedPreferences
import org.hnau.android.base.system.preferences.adapter.SharedPreferencesAdapter
import org.hnau.android.base.system.preferences.adapter.boolean
import org.hnau.android.base.system.preferences.adapter.float
import org.hnau.android.base.system.preferences.adapter.int
import org.hnau.android.base.system.preferences.adapter.long
import org.hnau.android.base.system.preferences.adapter.string
import org.hnau.android.base.system.preferences.adapter.stringSet
import org.hnau.base.data.delegate.mutable.MutableDelegate
import org.hnau.base.data.delegate.mutable.create
import org.hnau.base.data.possible.getOrInitialize
import org.hnau.base.data.possible.lateinit.Lateinit
import org.hnau.base.data.possible.lateinit.synchronized
import org.hnau.base.extensions.boolean.ifTrue


fun <T> SharedPreferences.createPropertyDelegate(
        key: String,
        adapter: SharedPreferencesAdapter<T>
) = MutableDelegate
        .create(
                get = { adapter.read(this, key) },
                set = { value -> edit().apply { adapter.write(this, key, value) }.commit() }
        )
        .let { rawDelegate ->
            val cache = Lateinit.synchronized<T>()
            MutableDelegate.create(
                    get = { cache.getOrInitialize { rawDelegate.get() } },
                    set = { value ->
                        cache
                                .checkPossible(
                                        ifValueExists = { oldValue -> oldValue != value },
                                        ifValueNotExists = { true }
                                )
                                .ifTrue { rawDelegate.set(value) }
                    }
            )
        }

fun SharedPreferences.int(
        key: String
) = createPropertyDelegate(
        key = key,
        adapter = SharedPreferencesAdapter.int
)

fun SharedPreferences.long(
        key: String
) = createPropertyDelegate(
        key = key,
        adapter = SharedPreferencesAdapter.long
)

fun SharedPreferences.float(
        key: String
) = createPropertyDelegate(
        key = key,
        adapter = SharedPreferencesAdapter.float
)

fun SharedPreferences.boolean(
        key: String
) = createPropertyDelegate(
        key = key,
        adapter = SharedPreferencesAdapter.boolean
)

fun SharedPreferences.string(
        key: String
) = createPropertyDelegate(
        key = key,
        adapter = SharedPreferencesAdapter.string
)

fun SharedPreferences.stringSet(
        key: String
) = createPropertyDelegate(
        key = key,
        adapter = SharedPreferencesAdapter.stringSet
)