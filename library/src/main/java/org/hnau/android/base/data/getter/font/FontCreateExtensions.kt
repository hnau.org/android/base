package org.hnau.android.base.data.getter.font

import android.graphics.Typeface


fun Font(typeface: Typeface) =
    Font({ typeface })

fun Fonts.fromAssets(assertFontName: String) =
    Font { context -> Typeface.createFromAsset(context.assets, assertFontName) }

fun Typeface.toFont() = Font(this)