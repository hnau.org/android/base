package org.hnau.android.base.extensions.view.layout.params.builders

import org.hnau.android.base.data.course.Course
import org.hnau.android.base.data.course.checkCourse
import org.hnau.android.base.data.getter.px.Px


var LayoutParamsBuilder.size: Px
    get() = width
    set(value) {
        width = value
        height = value
    }

fun LayoutParamsBuilder.get(course: Course) =
        course.checkCourse({ width }, { height })