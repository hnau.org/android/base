package  org.hnau.android.base.ui.drawable

import android.graphics.Canvas
import android.graphics.ColorFilter
import android.graphics.PixelFormat
import android.graphics.drawable.Drawable


object EmptyDrawable : Drawable() {

    override fun draw(canvas: Canvas) {}

    override fun getOpacity() = PixelFormat.TRANSPARENT

    override fun setAlpha(alpha: Int) {}

    override fun setColorFilter(colorFilter: ColorFilter?) {}

}