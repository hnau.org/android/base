package org.hnau.android.base.system.broadcast

import android.content.Intent
import android.content.IntentFilter


class ActionsBroadcastEmitter(
    private val actions: Iterable<String>,
    register: BroadcastRegister
) : BroadcastEmitter(
    intentFilter = IntentFilter().apply { actions.forEach(::addAction) },
    register = register
) {

    override fun validateBroadcast(intent: Intent) =
        actions.any { it == intent.action }

}

fun ActionBroadcastEmitter(
    action: String,
    register: BroadcastRegister
) = ActionsBroadcastEmitter(
    actions = listOf(action),
    register = register
)