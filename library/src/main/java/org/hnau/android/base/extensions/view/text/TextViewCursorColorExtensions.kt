package org.hnau.android.base.extensions.view.text

import android.graphics.PorterDuff
import android.graphics.drawable.Drawable
import android.os.Build
import android.widget.TextView
import androidx.annotation.ColorInt
import androidx.annotation.RequiresApi
import androidx.core.content.res.ResourcesCompat
import org.hnau.android.base.extensions.createdTinted
import org.hnau.base.extensions.boolean.ifTrue
import org.hnau.base.extensions.cast
import org.hnau.base.extensions.sureNotNull

fun TextView.setCursorColor(
        @ColorInt color: Int
) {
    val wasFocused = isFocused
    wasFocused.ifTrue { clearFocus() }
    setCursorColorOnUnfocused(color)
    wasFocused.ifTrue { requestFocus() }
}

private fun TextView.setCursorColorOnUnfocused(
        @ColorInt color: Int
) {
    if (Build.VERSION.SDK_INT < Build.VERSION_CODES.P) {
        setCursorColorBeforeP(color)
        return
    }
    //Unable to set cursor color on Android P
    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.Q) {
        setCursorColorAfterQ(color)
        return
    }
}

@RequiresApi(Build.VERSION_CODES.Q)
private fun TextView.setCursorColorAfterQ(
        @ColorInt color: Int
) {
    textSelectHandle?.let { textSelectHandle ->
        setTextSelectHandle(textSelectHandle.createdTinted(color))
    }
    textSelectHandleLeft?.let { textSelectHandleLeft ->
        setTextSelectHandleLeft(textSelectHandleLeft.createdTinted(color))
    }
    textSelectHandleRight?.let { textSelectHandleRight ->
        setTextSelectHandleRight(textSelectHandleRight.createdTinted(color))
    }
    textCursorDrawable?.let { textCursorDrawable ->
        this.textCursorDrawable = textCursorDrawable.createdTinted(color)
    }
}

private fun TextView.setCursorColorBeforeP(
        @ColorInt color: Int
) {

    val drawablesToUpdate: List<Triple<String, String, (() -> Drawable) -> Any>> = listOf(
            Triple(
                    "mCursorDrawableRes",
                    "mCursorDrawable",
                    { getDrawable -> Array(2) { getDrawable() } }
            ),
            Triple(
                    "mTextSelectHandleLeftRes",
                    "mSelectHandleLeft",
                    { getDrawable -> getDrawable() }
            ),
            Triple(
                    "mTextSelectHandleRightRes",
                    "mSelectHandleRight",
                    { getDrawable -> getDrawable() }
            ),
            Triple(
                    "mTextSelectHandleRes",
                    "mSelectHandleCenter",
                    { getDrawable -> getDrawable() }
            )
    )

    val textViewClass = TextView::class.java

    val editor = TextView::class.java
            .getDeclaredField("mEditor")
            .apply { isAccessible = true }
            .get(this)
            .cast<Any>()

    val editorClass = editor.javaClass

    drawablesToUpdate.forEach { (textViewResIdFieldName, editorDrawableFieldName, createValueForEditor) ->
        textViewClass
                .getDeclaredField(textViewResIdFieldName)
                .apply { isAccessible = true }
                .getInt(this)
                .let { drawableResId ->
                    editorClass
                            .getDeclaredField(editorDrawableFieldName)
                            .apply { isAccessible = true }
                            .set(
                                    editor,
                                    createValueForEditor {
                                        ResourcesCompat
                                                .getDrawable(context.resources, drawableResId, context.theme)
                                                .sureNotNull()
                                                .apply {
                                                    @Suppress("DEPRECATION")
                                                    setColorFilter(color, PorterDuff.Mode.SRC_IN)
                                                }
                                    }
                            )
                }
    }
}