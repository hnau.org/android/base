package org.hnau.android.base.data

import android.util.Base64
import org.hnau.base.data.mapper.Mapper


object Base64Android {

    private val base64Flags = Base64.NO_CLOSE or Base64.NO_PADDING or Base64.NO_WRAP

    val mapper = Mapper<String, ByteArray>(
            direct = { Base64.decode(it, base64Flags) },
            reverse = { Base64.encodeToString(it, base64Flags) }
    )

}