package org.hnau.android.base.extensions.view.group.recycle

import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import org.hnau.android.base.data.course.Course
import org.hnau.android.base.data.course.checkCourse
import org.hnau.base.extensions.boolean.checkBoolean


val Course.linearLayoutManagerOrientation
    get() = checkCourse({ LinearLayoutManager.HORIZONTAL }, { LinearLayoutManager.VERTICAL })

var LinearLayoutManager.course: Course
    get() = (orientation == LinearLayoutManager.HORIZONTAL).checkBoolean(
            ifTrue = { Course.horizontal },
            ifFalse = { Course.vertical }
    )
    set(value) {
        orientation = value.gridLayoutManagerOrientation
    }

inline fun RecyclerView.setLinearLayoutManager(
        course: Course,
        reverseLayout: Boolean = false,
        configurator: LinearLayoutManager.() -> Unit = {}
) = setLayoutManager(
        layoutManager = LinearLayoutManager(context, course.linearLayoutManagerOrientation, reverseLayout),
        configurator = configurator
)

val RecyclerView.linearLayoutManager
    get() = existenceLayoutManager as? LinearLayoutManager
            ?: throw IllegalStateException("layoutManager is not LinearLayoutManager")