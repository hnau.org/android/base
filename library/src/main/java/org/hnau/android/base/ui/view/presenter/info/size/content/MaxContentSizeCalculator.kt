package org.hnau.android.base.ui.view.presenter.info.size.content

import org.hnau.android.base.ui.view.presenter.info.size.content.base.ContentSizeCalculator
import org.hnau.android.base.ui.view.presenter.view.lifecycle.ShowingMeasurable
import org.hnau.android.base.ui.view.presenter.view.lifecycle.ViewMeasuredSizeGetter
import kotlin.math.max


object MaxContentSizeCalculator :
    ContentSizeCalculator {

    override fun calcContentSize(
        measureds: Collection<ShowingMeasurable>,
        measuredSizeExtractor: (ViewMeasuredSizeGetter) -> Int?
    ): Int {
        var max = 0
        measureds.forEach { measurable ->
            val size = measuredSizeExtractor(measurable) ?: return@forEach
            max = max(max, size)
        }
        return max
    }

}

val ContentSizeCalculator.Companion.max get() = MaxContentSizeCalculator