package org.hnau.android.base.data.insets.typed

import org.hnau.android.base.data.edges.Edges
import org.hnau.android.base.data.insets.Insets
import org.hnau.android.base.data.insets.MutableInsets
import org.hnau.android.base.data.insets.applyInsets
import org.hnau.android.base.data.insets.compare
import org.hnau.android.base.data.insets.horizontalSum
import org.hnau.android.base.data.insets.uniqueInsets
import org.hnau.android.base.data.insets.verticalSum
import org.hnau.android.base.extensions.emitter.uniqueMutable
import org.hnau.emitter.Emitter


val Insets<Int>.horizontalSum
    get() = horizontalSum(Int::plus)

val Insets<Int>.verticalSum
    get() = verticalSum(Int::plus)

fun Edges<Int>.applyInsets(
        insets: Insets<Int>
) = applyInsets(
        insets = insets,
        plus = Int::plus,
        minus = Int::minus
)

fun Emitter<Edges<Int>>.applyInsets(
        insets: Insets<Int>
) = applyInsets(
        insets = insets,
        plus = Int::plus,
        minus = Int::minus
)

fun Edges<Int>.applyInsets(
        insets: Emitter<Insets<Int>>
) = applyInsets(
        insets = insets,
        plus = Int::plus,
        minus = Int::minus
)

fun Emitter<Edges<Int>>.applyInsets(
        insets: Emitter<Insets<Int>>
) = applyInsets(
        insets = insets,
        plus = Int::plus,
        minus = Int::minus
)

fun Emitter<Insets<Int>>.uniqueInsets() =
        uniqueInsets { first, second -> first == second }