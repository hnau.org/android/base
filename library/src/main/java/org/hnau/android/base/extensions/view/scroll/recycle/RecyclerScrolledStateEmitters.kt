package org.hnau.android.base.extensions.view.scroll.recycle

import androidx.recyclerview.widget.RecyclerView
import org.hnau.emitter.extensions.makeAlways


class RecyclerScrolledStateEmitters(
        view: RecyclerView
) {

    val onScrolledNotifier by lazy {
        @Suppress("DEPRECATION")
        view.createOnRecyclerScrolledEmitter()
    }

    private val onScrolledAlwaysNotifier by lazy {
        onScrolledNotifier.makeAlways { Unit }
    }

    val isScrolledToLeft by lazy {
        @Suppress("DEPRECATION")
        view.createRecycleIsScrolledToLeftEmitter(onScrolledAlwaysNotifier)
    }

    val isScrolledToTop by lazy {
        @Suppress("DEPRECATION")
        view.createRecycleIsScrolledToTopEmitter(onScrolledAlwaysNotifier)
    }

    val isScrolledToRight by lazy {
        @Suppress("DEPRECATION")
        view.createRecycleIsScrolledToRightEmitter(onScrolledAlwaysNotifier)
    }

    val isScrolledToBottom by lazy {
        @Suppress("DEPRECATION")
        view.createRecycleIsScrolledToBottomEmitter(onScrolledAlwaysNotifier)
    }

}

fun RecyclerView.createRecyclerScrolledStateEmitters() =
        RecyclerScrolledStateEmitters(this)