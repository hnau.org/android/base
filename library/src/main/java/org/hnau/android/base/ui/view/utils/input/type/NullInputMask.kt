package org.hnau.android.base.ui.view.utils.input.type

import android.text.InputType
import org.hnau.android.base.ui.view.utils.input.type.base.ClassInputMask


object NullInputMask : ClassInputMask(InputType.TYPE_NULL)