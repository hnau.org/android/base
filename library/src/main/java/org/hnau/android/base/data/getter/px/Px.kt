package org.hnau.android.base.data.getter.px

import android.content.Context
import androidx.annotation.DimenRes
import org.hnau.base.data.possible.getOrInitialize
import org.hnau.base.data.possible.lateinit.Lateinit
import org.hnau.base.data.possible.lateinit.synchronized

data class Px(
        private val calcRawPx: (Context) -> Float
) : (Context) -> Float {

    companion object {

        val zero = 0.px
    }

    private val cache = Lateinit.synchronized<Float>()

    fun getRawPx(context: Context) =
            cache.getOrInitialize { calcRawPx(context) }

    override fun invoke(p1: Context) =
            getRawPx(p1) * ScaleManager.scale

}

@Suppress("FunctionName")
fun Px(@DimenRes dimenResId: Int) =
        Px { it.resources.getDimension(dimenResId) }