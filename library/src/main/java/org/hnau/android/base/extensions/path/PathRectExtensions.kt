package org.hnau.android.base.extensions.path

import android.graphics.Path
import android.graphics.Rect
import android.graphics.RectF
import kotlin.math.min


fun Path.addRoundSidesRect(
        rect: RectF
) = addRoundSidesRect(
        rectLeft = rect.left,
        rectTop = rect.top,
        rectRight = rect.right,
        rectBottom = rect.bottom
)

fun Path.addRoundSidesRect(
        rect: Rect
) = addRoundSidesRect(
        rectLeft = rect.left,
        rectTop = rect.top,
        rectRight = rect.right,
        rectBottom = rect.bottom
)

fun Path.addRoundSidesRect(
        rectLeft: Number,
        rectTop: Number,
        rectRight: Number,
        rectBottom: Number
) = addRoundCornerRect(
        rectLeft = rectLeft,
        rectTop = rectTop,
        rectRight = rectRight,
        rectBottom = rectBottom,
        cornerRadius = min(
                rectRight.toFloat() - rectLeft.toFloat(),
                rectBottom.toFloat() - rectTop.toFloat()
        ) / 2
)

fun Path.addRoundCornerRect(
        rect: RectF,
        cornerRadius: Number
) = addRoundCornerRect(
        rectLeft = rect.left,
        rectTop = rect.top,
        rectRight = rect.right,
        rectBottom = rect.bottom,
        cornerRadius = cornerRadius
)

fun Path.addRoundCornerRect(
        rect: Rect,
        cornerRadius: Number
) = addRoundCornerRect(
        rectLeft = rect.left,
        rectTop = rect.top,
        rectRight = rect.right,
        rectBottom = rect.bottom,
        cornerRadius = cornerRadius
)

fun Path.addRoundCornerRect(
        rectLeft: Number,
        rectTop: Number,
        rectRight: Number,
        rectBottom: Number,
        cornerRadius: Number
) {

    val left = rectLeft.toFloat()
    val top = rectTop.toFloat()
    val right = rectRight.toFloat()
    val bottom = rectBottom.toFloat()
    val radius = cornerRadius.toFloat()

    val leftC = left + radius
    val topC = top + radius
    val bottomC = bottom - radius
    val rightC = right - radius

    moveTo(left, topC)
    ellipseArcQuarterTo(left, topC, leftC, top, false)
    lineTo(rightC, top)
    ellipseArcQuarterTo(rightC, top, right, topC, true)
    lineTo(right, bottomC)
    ellipseArcQuarterTo(right, bottomC, rightC, bottom, false)
    lineTo(leftC, bottom)
    ellipseArcQuarterTo(leftC, bottom, left, bottomC, true)
    lineTo(left, topC)

}