package org.hnau.android.base.ui.animation.emergence.offset

import org.hnau.android.base.ui.animation.emergence.EmergenceInfo


fun EmergencesInfo.Companion.symetric(
        info: EmergenceInfo = EmergenceInfo()
) = EmergencesInfo(
        show = info,
        hide = info
)