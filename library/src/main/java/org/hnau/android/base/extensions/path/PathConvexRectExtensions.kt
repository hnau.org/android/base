package org.hnau.android.base.extensions.path

import android.graphics.Path
import org.hnau.base.extensions.boolean.checkBoolean
import org.hnau.base.extensions.boolean.ifTrue

private const val convexArmAlongDeltaByLength = 8f / 32f
private const val convexArmAcrossDeltaByLength = 4f / 32f
private const val convexAmountByLength = 3f / 32f

fun Path.addConvexSidesRect(
        left: Number,
        top: Number,
        right: Number,
        bottom: Number
) {

    val xMin = left.toFloat()
    val xMax = right.toFloat()
    val yMin = top.toFloat()
    val yMax = bottom.toFloat()

    val height = yMax - yMin

    val armDx = height * convexArmAcrossDeltaByLength
    val armDy = height * convexArmAlongDeltaByLength
    val offsetX = height * convexAmountByLength

    moveTo(xMax - offsetX, yMin)
    cubicTo(
            xMax - offsetX + armDx, yMin + armDy,
            xMax - offsetX + armDx, yMax - armDy,
            xMax - offsetX, yMax
    )
    lineTo(xMin + offsetX, yMax)
    cubicTo(
            xMin + offsetX - armDx, yMax - armDy,
            xMin + offsetX - armDx, yMin + armDy,
            xMin + offsetX, yMin
    )
    close()

}