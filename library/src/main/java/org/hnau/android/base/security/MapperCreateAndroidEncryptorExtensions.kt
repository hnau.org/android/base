@file:Suppress("DEPRECATION")

package org.hnau.android.base.security

import android.content.Context
import android.os.Build
import android.security.KeyPairGeneratorSpec
import android.security.keystore.KeyGenParameterSpec
import android.security.keystore.KeyProperties
import androidx.annotation.RequiresApi
import org.hnau.base.data.delegate.mutable.MutableDelegate
import org.hnau.base.data.time.Time
import org.hnau.base.data.mapper.Mapper
import org.hnau.base.extensions.boolean.ifFalse
import org.hnau.base.extensions.checkNullable
import org.hnau.base.data.time.now
import org.hnau.base.data.time.toDate
import java.math.BigInteger
import java.security.Key
import java.security.KeyPairGenerator
import java.security.KeyStore
import java.security.SecureRandom
import javax.crypto.KeyGenerator
import javax.crypto.spec.SecretKeySpec
import javax.security.auth.x500.X500Principal


fun Mapper.Companion.createAndroidEncryptor(
    context: Context,
    keyAlias: String,
    publicKeyProperty: MutableDelegate<ByteArray?>,
    keyStoreName: String = "AndroidKeyStore",
    keyValidityPeriod: Time = Time.day * 1000000
) = if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
    createAndroidEncryptorAfterM(
        keyStoreName = keyStoreName,
        keyAlias = keyAlias
    )
} else {
    createAndroidEncryptorBeforeM(
        context = context,
        keyStoreName = keyStoreName,
        keyAlias = keyAlias,
        keyValidityPeriod = keyValidityPeriod,
        publicKeyProperty = publicKeyProperty
    )
}

@RequiresApi(Build.VERSION_CODES.M)
private fun Mapper.Companion.createAndroidEncryptorAfterM(
    keyStoreName: String,
    keyAlias: String
): Mapper<ByteArray, ByteArray> = getKey<Key>(
    keyStoreName = keyStoreName,
    keyAlias = keyAlias,
    extractKey = { getKey(keyAlias, null) },
    generateKey = {
        KeyGenerator
            .getInstance(AESEncryptor.algorithm, keyStoreName)
            .apply {
                init(
                    KeyGenParameterSpec
                        .Builder(
                            keyAlias,
                            KeyProperties.PURPOSE_ENCRYPT or KeyProperties.PURPOSE_DECRYPT
                        )
                        .setBlockModes(AESEncryptor.blockMode)
                        .setEncryptionPaddings(AESEncryptor.padding)
                        .setRandomizedEncryptionRequired(false)
                        .build()
                )
            }
            .generateKey()
    }
).let { key ->
    createAESEncryptor(key)
}

private fun Mapper.Companion.createAndroidEncryptorBeforeM(
    context: Context,
    keyStoreName: String,
    keyAlias: String,
    keyValidityPeriod: Time,
    publicKeyProperty: MutableDelegate<ByteArray?>
): Mapper<ByteArray, ByteArray> {

    val rsaKey = getKey(
        keyStoreName = keyStoreName,
        keyAlias = keyAlias,
        extractKey = { getEntry(keyAlias, null) as KeyStore.PrivateKeyEntry },
        generateKey = {
            KeyPairGenerator
                .getInstance(KeyProperties.KEY_ALGORITHM_RSA, keyStoreName)
                .apply {
                    initialize(
                        Time.now().let { start ->
                            @Suppress("DEPRECATION")
                            KeyPairGeneratorSpec.Builder(context)
                                .setAlias(keyAlias)
                                .setSubject(X500Principal("CN=$keyAlias"))
                                .setSerialNumber(BigInteger.TEN)
                                .setStartDate(start.toDate())
                                .setEndDate(start.plus(keyValidityPeriod).toDate())
                                .build()
                        }
                    )
                }
                .generateKeyPair()
        }
    )

    val rsaEncryptor = createRSAEncryptor(rsaKey)

    val aesKey = publicKeyProperty.get()
        .checkNullable(
            ifNotNull = { encryptedKey ->
                rsaEncryptor.direct(encryptedKey)
            },
            ifNull = {
                ByteArray(16) { 0.toByte() }.also { key ->
                    SecureRandom().nextBytes(key)
                    rsaEncryptor
                        .reverse(key)
                        .let(publicKeyProperty::set)
                }
            }
        )
        .let { key ->
            SecretKeySpec(key, "AES")
        }

    return createAESEncryptor(aesKey)
}


private inline fun <K> getKey(
    keyStoreName: String,
    keyAlias: String,
    generateKey: KeyStore.() -> Unit,
    extractKey: KeyStore.() -> K
) = KeyStore
    .getInstance(keyStoreName)
    .apply { load(null) }
    .run<KeyStore, K> {
        containsAlias(keyAlias).ifFalse { generateKey() }
        extractKey()
    }