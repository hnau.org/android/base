package org.hnau.android.base.ui.path.svg


interface SvgCommandsReceiver {

    fun l(x: Number, y: Number)

    fun m(x: Number, y: Number)

    fun q(x1: Number, y1: Number, x: Number, y: Number)

    fun c(x1: Number, y1: Number, x2: Number, y2: Number, x: Number, y: Number)

}