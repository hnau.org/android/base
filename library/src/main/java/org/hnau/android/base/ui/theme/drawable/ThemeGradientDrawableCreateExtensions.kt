package org.hnau.android.base.ui.theme.drawable

import org.hnau.android.base.ui.theme.ThemeGradient
import org.hnau.emitter.Emitter
import org.hnau.emitter.extensions.map


@Suppress("FunctionName")
fun ThemeGradientDrawable(
        gradient: Emitter<ThemeGradient>
) = ThemeGradientDrawable().let { result ->
    gradient.map { gradientValue ->
        result.also { drawable ->
            drawable.gradient = gradientValue
        }
    }
}