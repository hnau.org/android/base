package org.hnau.android.base.ui.view.header

import org.hnau.android.base.data.getter.picture.Picture
import org.hnau.android.base.data.getter.text.Text
import org.hnau.android.base.ui.lightness.Lightness
import org.hnau.emitter.Emitter


data class HeaderAction(
        val icon: Picture,
        val hint: Text,
        val onClick: () -> Unit
)