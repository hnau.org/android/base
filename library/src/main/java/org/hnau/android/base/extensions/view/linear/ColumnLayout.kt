package org.hnau.android.base.extensions.view.linear

import android.content.Context
import android.view.ViewGroup
import org.hnau.android.base.data.course.Course
import org.hnau.android.base.extensions.view.group.children.view


open class ColumnLayout(
        context: Context
) : ManualLinearLayout(
        context = context,
        course = Course.vertical
)

inline fun ViewGroup.column(
        childConfigurator: ColumnLayout.() -> Unit = {}
) = view(
        view = ColumnLayout(context),
        childConfigurator = childConfigurator
)