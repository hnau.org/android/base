package org.hnau.android.base.system.lifecycle

import androidx.lifecycle.GenericLifecycleObserver
import androidx.lifecycle.Lifecycle
import org.hnau.emitter.observing.push.always.AlwaysEmitter


class LifecycleStateEmitter(
    private val lifecycle: Lifecycle
) : AlwaysEmitter<LifecycleState>() {

    override val value: LifecycleState
        get() = when (lifecycle.currentState) {
            Lifecycle.State.DESTROYED,
            Lifecycle.State.INITIALIZED -> LifecycleState.nonexistent
            Lifecycle.State.CREATED -> LifecycleState.created
            Lifecycle.State.STARTED -> LifecycleState.started
            Lifecycle.State.RESUMED -> LifecycleState.resumed
        }

    private val lifecycleObserver =
            GenericLifecycleObserver { _, _ -> onChanged() }

    init {
        lifecycle.addObserver(lifecycleObserver)
    }

}