package org.hnau.android.base.ui.decoration.ripple.shape
import org.hnau.android.base.data.edges.Edges
import org.hnau.android.base.data.edges.contains
import org.hnau.android.base.data.edges.typed.centerX
import org.hnau.android.base.data.edges.typed.centerY
import org.hnau.base.data.possible.lateinit.Lateinit
import org.hnau.base.data.possible.lateinit.unsafe
import org.hnau.base.data.possible.valueOrThrow
import org.hnau.base.extensions.number.asPercentageInter
import org.hnau.emitter.Emitter
import org.hnau.emitter.extensions.map


fun RippleShape.Companion.borderless(
        bounds: Emitter<Edges<Int>>
) = Lateinit
        .unsafe<Edges<Int>>()
        .let { boundsCache ->
            var maxRadius = 0f
            RippleShape(
                    calcCircleCenter = { touchPoint, growingPercentage, result ->
                        val boundsValue = boundsCache.valueOrThrow
                        result.set(
                                growingPercentage.asPercentageInter(touchPoint.x, boundsValue.centerX),
                                growingPercentage.asPercentageInter(touchPoint.y, boundsValue.centerY)
                        )
                    },
                    getMaxRadius = { maxRadius },
                    clip = { },
                    checkContains = { x, y ->
                       boundsCache.valueOrThrow.contains(x.toInt(), y.toInt())
                    }
            ).let { rippleShape ->
                bounds.map { boundsValue ->
                    maxRadius = RippleShapeUtils.calcBoundsDiagonal(boundsValue) / 2
                    boundsCache.set(boundsValue)
                    rippleShape
                }
            }
        }