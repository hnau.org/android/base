package org.hnau.android.base.ui.view.header.layers

import org.hnau.android.base.data.getter.picture.Picture
import org.hnau.android.base.data.getter.text.Text
import org.hnau.android.base.ui.rawcolor.RawColor
import org.hnau.emitter.Emitter


data class HeaderInfo(
        val topInfo: Emitter<LayerTopInfo>,
        val title: Emitter<Text>,
        val foreground: Emitter<RawColor>,
        val leftAction: Emitter<Triple<(RawColor) -> Picture, Text, () -> Unit>?>,
        val rightAction: Emitter<Triple<(RawColor) -> Picture, Text, () -> Unit>?>
)