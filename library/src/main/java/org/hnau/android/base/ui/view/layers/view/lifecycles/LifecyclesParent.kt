package org.hnau.android.base.ui.view.layers.view.lifecycles

import android.view.View


interface LifecyclesParent {

    fun onLifecycleLayout()

    fun addLifecycleView(view: View)
    fun removeLifecycleView(view: View)

    fun refresh()

}