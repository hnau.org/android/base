package org.hnau.android.base.ui.view.recycler.adapter.utils.separations

import android.content.Context
import android.view.View
import org.hnau.android.base.data.getter.px.Px
import org.hnau.android.base.extensions.view.measureSpecFit
import org.hnau.android.base.ui.view.recycler.adapter.item.various.ViewFactory
import org.hnau.android.base.ui.view.utils.state.createStateEmitters
import org.hnau.emitter.Emitter
import org.hnau.emitter.extensions.useWhen


class RecyclerViewSeparatorItemView(
    context: Context,
    content: Emitter<Int>
) : View(context) {

    companion object : ViewFactory<Int>(::RecyclerViewSeparatorItemView)

    private val state = createStateEmitters()

    private var size = 0
        set(value) {
            field = value
            requestLayout()
        }

    init {
        content
            .useWhen(state.isActive)
            .observe { size = it }
    }

    override fun onMeasure(
        widthMeasureSpec: Int,
        heightMeasureSpec: Int
    ) = setMeasuredDimension(
        measureSpecFit(widthMeasureSpec, size),
        measureSpecFit(heightMeasureSpec, size)
    )

}