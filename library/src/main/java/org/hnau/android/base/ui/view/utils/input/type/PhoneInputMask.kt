package org.hnau.android.base.ui.view.utils.input.type

import android.text.InputType
import org.hnau.android.base.ui.view.utils.input.type.base.ClassInputMask


object PhoneInputMask : ClassInputMask(InputType.TYPE_CLASS_PHONE)