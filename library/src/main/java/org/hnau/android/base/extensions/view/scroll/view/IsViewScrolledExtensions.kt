package org.hnau.android.base.extensions.view.scroll.view

import android.view.View


val View.isScrolledToLeft: Boolean
    get() = canScrollHorizontally(-1)

val View.isScrolledToTop: Boolean
    get() = canScrollVertically(-1)

val View.isScrolledToRight: Boolean
    get() = canScrollHorizontally(1)

val View.isScrolledToBottom: Boolean
    get() = canScrollVertically(1)