package org.hnau.android.base.extensions.view.base

import android.view.View
import androidx.annotation.ColorInt
import org.hnau.android.base.data.getter.picture.Picture
import org.hnau.android.base.data.getter.tone.Tone
import org.hnau.base.extensions.checkNullable

fun View.setBackgroundColor(
    @ColorInt color: Int?
) = color.checkNullable(
    ifNotNull = ::setBackgroundColor,
    ifNull = ::setNoBackground
)

fun View.setBackgroundTone(tone: Tone?) =
    setBackgroundColor(tone?.invoke(context))

fun View.setNoBackground() =
    setBackground(null)

fun View.setBackgroundPicture(picture: Picture?) =
    setBackground(picture?.invoke(context))