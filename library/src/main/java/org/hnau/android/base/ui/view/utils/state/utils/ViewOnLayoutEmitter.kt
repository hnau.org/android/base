package org.hnau.android.base.ui.view.utils.state.utils

import android.graphics.Rect
import android.view.View
import org.hnau.emitter.observing.notifier.Notifier


class ViewOnLayoutEmitter(
        view: View
) : Notifier() {

    private val onLayoutChangedListener = object : View.OnLayoutChangeListener {
        override fun onLayoutChange(
                p0: View?,
                left: Int, top: Int, right: Int, bottom: Int,
                p5: Int, p6: Int, p7: Int, p8: Int
        ) {
            notifyObservers()
        }
    }

    init {
        view.addOnLayoutChangeListener(onLayoutChangedListener)
    }


}