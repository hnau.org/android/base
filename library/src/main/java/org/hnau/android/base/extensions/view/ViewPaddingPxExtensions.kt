package org.hnau.android.base.extensions.view

import android.view.View
import org.hnau.android.base.data.getter.px.Px


fun View.setPaddingLeft(paddingLeft: Px) =
        setPaddingLeft(paddingLeft(context))

fun View.setPaddingTop(paddingTop: Px) =
        setPaddingTop(paddingTop(context))

fun View.setPaddingRight(paddingRight: Px) =
        setPaddingRight(paddingRight(context))

fun View.setPaddingBottom(paddingBottom: Px) =
        setPaddingBottom(paddingBottom(context))

fun View.setPaddingStart(paddingStart: Px) =
        setPaddingStart(paddingStart(context))

fun View.setPaddingEnd(paddingEnd: Px) =
        setPaddingEnd(paddingEnd(context))


fun View.setPaddingHorizontal(paddingHorizontal: Px) =
        setPaddingHorizontal(paddingHorizontal(context))

fun View.setPaddingVertical(paddingVertical: Px) =
        setPaddingVertical(paddingVertical(context))