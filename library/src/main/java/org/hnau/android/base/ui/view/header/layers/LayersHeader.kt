package org.hnau.android.base.ui.view.header.layers

import android.content.Context
import org.hnau.android.base.ui.view.layers.Layer
import org.hnau.emitter.Emitter
import org.hnau.emitter.extensions.map


class LayersHeader<L : Layer>(
        context: Context,
        layers: Emitter<Collection<L>>,
        getInfo: L.() -> HeaderInfo
) {

    private val info =
            layers.map { it.lastOrNull()?.getInfo() }

}