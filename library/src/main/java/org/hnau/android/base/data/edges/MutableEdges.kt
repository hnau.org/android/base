package org.hnau.android.base.data.edges


class MutableEdges<T>(
        override var xMin: T,
        override var yMin: T,
        override var xMax: T,
        override var yMax: T
) : Edges<T> {

    constructor(
            source: Edges<T>
    ) : this(
            xMin = source.xMin,
            yMin = source.yMin,
            xMax = source.xMax,
            yMax = source.yMax
    )

    constructor(
            horizontal: T,
            vertical: T
    ) : this(
            xMin = horizontal,
            yMin = vertical,
            xMax = horizontal,
            yMax = vertical
    )

    constructor(
            value: T
    ) : this(
            horizontal = value,
            vertical = value
    )

    fun set(
            xMin: T,
            yMin: T,
            xMax: T,
            yMax: T,
    ) {
        this.xMin = xMin
        this.yMin = yMin
        this.xMax = xMax
        this.yMax = yMax
    }

    fun set(
            source: Edges<T>
    ) = set(
            xMin = source.xMin,
            yMin = source.yMin,
            xMax = source.xMax,
            yMax = source.yMax
    )

    fun set(
            horizontal: T,
            vertical: T
    ) = set(
            xMin = horizontal,
            yMin = vertical,
            xMax = horizontal,
            yMax = vertical
    )

    fun set(
            value: T
    ) = set(
            horizontal = value,
            vertical = value
    )

}