package org.hnau.android.base.ui.view.layers.view.lifecycles

import android.content.Context
import android.graphics.Canvas
import android.graphics.Rect
import android.view.View
import org.hnau.android.base.ui.animation.emergence.offset.EmergenceDirection
import org.hnau.android.base.ui.animation.emergence.offset.Offset
import org.hnau.android.base.ui.view.layers.view.lifecycles.lifecycle.Lifecycle
import org.hnau.android.base.ui.view.layers.view.lifecycles.transaction.Transaction
import org.hnau.android.base.ui.view.layers.view.lifecycles.transaction.TransactionBuilder
import org.hnau.android.base.ui.view.layers.view.presenting.PresentingLayer
import org.hnau.base.extensions.boolean.ifTrue
import org.hnau.base.extensions.checkNullable
import org.hnau.base.extensions.container.ifNotEmpty
import org.hnau.base.extensions.elvis
import org.hnau.emitter.Emitter


class Lifecycles(
    private val context: Context,
    private val isAnimationsAvailable: Emitter<Boolean>,
    private val layers: Emitter<List<PresentingLayer>>,
    private val lifecyclesParent: LifecyclesParent
) {

    companion object {

        private val zeroOffset = Offset()

    }

    private val lifecyclesToHide = HashSet<Lifecycle>()
    private val lifecycles = ArrayList<Lifecycle>()
    private val transactions = ArrayList<Transaction>()

    val isAnimating get() = transactions.isNotEmpty()

    init {
        layers.observe(::onLayersChanged)
    }

    private fun onLayersChanged(
        layers: List<PresentingLayer>
    ) {
        val hideTransactions = hideOldLayers(layers)
        val showTransactions = showNewLayers(layers)
        (hideTransactions.isNotEmpty() || showTransactions.isNotEmpty())
            .ifTrue { updateLifecyclesVisibility() }
        hideTransactions.forEach { it.start() }
        showTransactions.forEach { it.start() }
    }

    private fun hideOldLayers(
        layers: List<PresentingLayer>
    ) = addTransactions(
        direction = EmergenceDirection.hide
    ) {
        lifecycles.forEach { lifecycle ->
            (lifecycle.layer in layers).ifTrue { return@forEach }
            lifecycle.transaction?.direction.checkNullable(
                ifNotNull = { direction ->
                    (direction == EmergenceDirection.show).ifTrue {
                        lifecyclesToHide.add(lifecycle)
                    }
                },
                ifNull = { addLifecycleToTransaction(lifecycle) }
            )
        }
    }

    private fun showNewLayers(
        layers: List<PresentingLayer>
    ) = addTransactions(
        direction = EmergenceDirection.show
    ) {
        layers.forEach { layer ->
            lifecycles.any { it.layer == layer }.ifTrue { return@forEach }
            val lifecycle = Lifecycle(
                context = context,
                layer = layer,
                lifecyclesParent = lifecyclesParent
            )
            lifecycles.add(lifecycle)
            addLifecycleToTransaction(lifecycle)
        }
    }

    private inline fun addTransactions(
        direction: EmergenceDirection,
        configure: TransactionBuilder.() -> Unit
    ) = TransactionBuilder(
        direction = direction,
        onTransactionFinished = ::onTransactionFinished,
        isAnimationsAvailable = isAnimationsAvailable,
        lifecyclesParent = lifecyclesParent
    )
        .apply(configure)
        .build()
        .also { transactions.addAll(it) }

    private fun onTransactionFinished(
        transaction: Transaction
    ) {
        transactions.remove(transaction)
        when (transaction.direction) {
            EmergenceDirection.show -> onShowTransactionFinished(transaction)
            EmergenceDirection.hide -> onHideTransactionFinished(transaction)
        }
        updateLifecyclesVisibility()
        lifecyclesParent.refresh()
    }

    private fun onHideTransactionFinished(
        transaction: Transaction
    ) {
        transaction.lifecycles.forEach { it.setVisible(false) }
        lifecycles.removeAll(transaction.lifecycles)
    }

    private fun onShowTransactionFinished(
        transaction: Transaction
    ) {
        val hideTransactions = addTransactions(
            direction = EmergenceDirection.hide
        ) {
            transaction.lifecycles.forEach { lifecycle ->
                lifecyclesToHide.remove(lifecycle).ifTrue { addLifecycleToTransaction(lifecycle) }
            }
        }
        hideTransactions.ifNotEmpty { updateLifecyclesVisibility() }
        hideTransactions.forEach { it.start() }
    }

    private fun updateLifecyclesVisibility() {
        var staticCovered = false
        val coveredTransactions = HashSet<Transaction>()
        lifecycles.asReversed().forEach { lifecycle ->
            val transaction = lifecycle.transaction
            val covered = staticCovered ||
                    transaction?.let { it in coveredTransactions }.elvis { false }
            lifecycle.setVisible(!covered)
            lifecycle.layer.opaque.ifTrue {
                transaction.checkNullable<Transaction, Unit>(
                    ifNotNull = { coveredTransactions.add(it) },
                    ifNull = { staticCovered = true }
                )
            }
        }
    }

    fun draw(
        canvas: Canvas,
        drawView: (Canvas, View) -> Unit,
        rect: Rect
    ) {
        transactions.forEach(Transaction::prepareOffset)
        lifecycles.forEach { drawLifecycle(canvas, drawView, it, rect) }
    }

    private fun drawLifecycle(
        canvas: Canvas,
        drawView: (Canvas, View) -> Unit,
        lifecycle: Lifecycle,
        rect: Rect
    ) = lifecycle.transaction.checkNullable(
        ifNull = {
            lifecycle.draw(
                canvas = canvas,
                drawView = drawView,
                offset = zeroOffset,
                offsetAmount = 1f,
                direction = EmergenceDirection.show,
                rect = rect
            )
        },
        ifNotNull = { transaction ->
            transaction.draw(
                canvas = canvas,
                drawView = drawView,
                lifecycle = lifecycle,
                rect = rect
            )
        }
    )

}