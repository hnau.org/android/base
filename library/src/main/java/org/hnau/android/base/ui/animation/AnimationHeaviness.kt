package org.hnau.android.base.ui.animation


@Suppress("EXPERIMENTAL_FEATURE_WARNING")
inline class AnimationHeaviness(
        val value: Float
) {

    companion object {

        val easy = AnimationHeaviness(0f)
        val medium = AnimationHeaviness(0.5f)
        val hard = AnimationHeaviness(1f)

    }

}