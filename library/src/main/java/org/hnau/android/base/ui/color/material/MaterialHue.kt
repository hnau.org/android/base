package org.hnau.android.base.ui.color.material


enum class MaterialHue(
    val hasAdditionalColors: Boolean = true
) {
    red,
    pink,
    purple,
    deepPurple,
    indigo,
    blue,
    lightBlue,
    cyan,
    teal,
    green,
    lightGreen,
    lime,
    yellow,
    amber,
    orange,
    deepOrange,
    brown(hasAdditionalColors = false),
    grey(hasAdditionalColors = false),
    blueGrey(hasAdditionalColors = false)
}