package org.hnau.android.base.extensions.view.layout.params.builders

import android.content.Context
import android.view.ViewGroup
import org.hnau.android.base.data.getter.px.Px
import org.hnau.android.base.data.getter.px.pxInt


open class LayoutParamsBuilder(
    val context: Context
) {

    val zero = Px.zero
    val matchParent = Px { ViewGroup.LayoutParams.MATCH_PARENT.toFloat() }
    val wrapContent = Px { ViewGroup.LayoutParams.WRAP_CONTENT.toFloat() }

    var width = wrapContent
    var height = wrapContent

    protected val widthValue get() = width.pxInt(context)
    protected val heightValue get() = height.pxInt(context)

    open fun build() =
        ViewGroup.LayoutParams(widthValue, heightValue)

}