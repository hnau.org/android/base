package org.hnau.android.base.data.getter.base

import android.content.Context


inline fun <I, O> ContextGetter<I>.mapFull(
    crossinline converter: (Context, I) -> O
) = CachedContextGetter { i ->
    val io = this(i)
    converter(i, io)
}

inline fun <I, O> ContextGetter<I>.map(
    crossinline converter: (I) -> O
) = mapFull { _, io ->
    converter(io)
}

inline fun <A, B, O> ContextGetter<A>.combineFull(
    crossinline other: ContextGetter<B>,
    crossinline combinator: (Context, A, B) -> O
) = CachedContextGetter<O> { context ->
    val a = this(context)
    val b = other(context)
    combinator(context, a, b)
}

inline fun <A, B, O> ContextGetter<A>.combine(
    crossinline other: ContextGetter<B>,
    crossinline combinator: (A, B) -> O
) = combineFull(other) { _, a, b ->
    combinator(a, b)
}

fun <T> ContextGetter<T>.compare(
    context: Context,
    other: ContextGetter<T>,
    valuesComparator: (first: T, other: T) -> Boolean = { first, second -> first == second }
) = valuesComparator(
    this(context),
    other(context)
)

fun <T> ContextGetter<T>.compare(
    other: ContextGetter<T>,
    valuesComparator: (first: T, other: T) -> Boolean = { first, second -> first == second }
) = CachedContextGetter { context ->
    compare(context, other, valuesComparator)
}