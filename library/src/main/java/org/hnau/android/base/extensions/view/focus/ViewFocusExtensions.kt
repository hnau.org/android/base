package org.hnau.android.base.extensions.view.focus

import android.view.View
import org.hnau.base.extensions.boolean.ifTrue
import org.hnau.base.extensions.castOrNull
import org.hnau.emitter.observing.push.always.AlwaysEmitter


fun View.addOnFocusChangeListener(
        listener: View.OnFocusChangeListener
) {
    val existenceListener = onFocusChangeListener

    if (existenceListener == null) {
        onFocusChangeListener = listener
        return
    }

    if (existenceListener is CompositeOnFocusChangeListener) {
        existenceListener.add(listener)
        return
    }

    onFocusChangeListener = CompositeOnFocusChangeListener().apply {
        add(existenceListener)
        add(listener)
    }
}

fun View.removeOnFocusChangeListener(
        listener: View.OnFocusChangeListener
) {
    val existenceListener = onFocusChangeListener
    (existenceListener == listener).ifTrue {
        onFocusChangeListener = null
        return
    }
    existenceListener
            .castOrNull<CompositeOnFocusChangeListener>()
            ?.let { compositeOnFocusChangeListener ->
                compositeOnFocusChangeListener.remove(listener)
                compositeOnFocusChangeListener.isEmpty
                        .ifTrue { onFocusChangeListener = null }

            }
}

fun View.createIsFocusedEmitter() = object : AlwaysEmitter<Boolean>() {

    override val value: Boolean
        get() = isFocused

    init {
        addOnFocusChangeListener(
                View.OnFocusChangeListener { _, _ -> onChanged() }
        )
    }

}