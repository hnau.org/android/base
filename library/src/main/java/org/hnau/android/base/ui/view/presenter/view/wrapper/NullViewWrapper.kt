package org.hnau.android.base.ui.view.presenter.view.wrapper

import android.graphics.Canvas
import android.graphics.Rect
import android.view.View
import android.view.ViewGroup
import org.hnau.android.base.ui.animation.emergence.EmergenceInfo
import org.hnau.android.base.ui.animation.emergence.offset.EmergenceDirection
import org.hnau.android.base.ui.animation.emergence.offset.EmergencesInfo
import org.hnau.android.base.ui.animation.emergence.offset.calculator.OffsetCalculator
import org.hnau.base.data.time.Time


object NullViewWrapper : ViewWrapper {

    override val emergencesInfo = EmergencesInfo(
            show = EmergenceInfo(
                    duration = Time.zero
            )
    )

    override val measuredWidth = null
    override val measuredHeight = null

    override fun measure(
            widthMeasureSpec: Int,
            heightMeasureSpec: Int
    ) {
    }

    override fun layout(slot: Rect) {}

    override fun setAlpha(
            direction: EmergenceDirection,
            offsetAmount: Float
    ) {
    }

    override fun draw(
            slot: Rect,
            canvas: Canvas,
            childDrawer: (canvas: Canvas, child: View) -> Unit,
            emergenceInfo: EmergenceInfo,
            visibilityPercentage: Float
    ) {
    }

    override fun addView(parent: ViewGroup) {}
    override fun removeView(parent: ViewGroup) {}

}

val ViewWrapper.Companion.empty get() = NullViewWrapper