package org.hnau.android.base.data.getter.text

import androidx.annotation.PluralsRes
import androidx.annotation.StringRes


fun Text(existenceString: String) =
    Text { existenceString }

fun Text(@StringRes resId: Int, vararg params: Any) =
    Text { context -> context.getString(resId, *params) }

fun Texts.fromPlurals(
    @PluralsRes resId: Int,
    quantity: Int,
    vararg params: Any
) = Text { context ->
    context.resources.getQuantityString(resId, quantity, *params)
}

fun String.toText() = Text(this)