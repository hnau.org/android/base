package org.hnau.android.base.ui.animation.emergence


data class EmergenceFadeInfo(
    val offsetFactor: Float = 1f,
    val minAlpha: Float = 1f
) {

    companion object;

}