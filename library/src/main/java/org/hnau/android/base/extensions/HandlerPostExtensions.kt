package org.hnau.android.base.extensions

import android.os.Handler
import org.hnau.base.data.time.Time


fun Handler.postDelayed(pause: Time, action: Runnable) =
    postDelayed(action, pause.milliseconds)

fun Handler.postDelayed(pause: Time, action: () -> Unit) =
    postDelayed(action, pause.milliseconds)