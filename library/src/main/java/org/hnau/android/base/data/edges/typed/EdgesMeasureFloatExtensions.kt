package org.hnau.android.base.data.edges.typed

import org.hnau.android.base.data.edges.Edges
import org.hnau.android.base.data.edges.centerX
import org.hnau.android.base.data.edges.centerY
import org.hnau.android.base.data.edges.height
import org.hnau.android.base.data.edges.width


val Edges<Float>.width
    get() = width(Float::minus)

val Edges<Float>.height
    get() = height(Float::minus)

val Edges<Float>.centerX
    get() = centerX(Float::plus) { it / 2f }

val Edges<Float>.centerY
    get() = centerY(Float::plus) { it / 2f }