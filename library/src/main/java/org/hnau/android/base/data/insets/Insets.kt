package org.hnau.android.base.data.insets


interface Insets<out T> {

    companion object;

    val left: T
    val top: T
    val right: T
    val bottom: T

}