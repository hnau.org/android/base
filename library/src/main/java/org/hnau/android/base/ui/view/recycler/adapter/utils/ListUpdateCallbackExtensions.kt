package org.hnau.android.base.ui.view.recycler.adapter.utils

import androidx.recyclerview.widget.ListUpdateCallback


fun ListUpdateCallback.offset(delta: Int) = object : ListUpdateCallback {

    override fun onChanged(position: Int, count: Int, payload: Any?) =
        this@offset.onChanged(position + delta, count, payload)

    override fun onMoved(fromPosition: Int, toPosition: Int) =
        this@offset.onMoved(fromPosition + delta, toPosition + delta)

    override fun onInserted(position: Int, count: Int) =
        this@offset.onInserted(position + delta, count)

    override fun onRemoved(position: Int, count: Int) =
        this@offset.onRemoved(position + delta, count)

}