package org.hnau.android.base.extensions.view.group.children

import android.content.Context
import android.view.View
import android.view.ViewGroup


inline fun <V : View> ViewGroup.view(
        view: V,
        childConfigurator: V.() -> Unit = {}
) = view
        .apply(childConfigurator)
        .also(::addView)

inline fun <V : View> ViewGroup.view(
        viewCreator: (Context) -> V,
        childConfigurator: V.() -> Unit = {}
) = view(
        view = viewCreator(context),
        childConfigurator = childConfigurator
)