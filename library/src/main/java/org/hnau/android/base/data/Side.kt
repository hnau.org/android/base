package org.hnau.android.base.data

import android.graphics.Point
import org.hnau.android.base.data.course.Course


@Suppress("EnumEntryName")
enum class Side(
        val singleOffset: Point,
        val course: Course
) {

    left(
            singleOffset = Point(-1, 0),
            course = Course.horizontal
    ),

    top(
            singleOffset = Point(0, -1),
            course = Course.vertical
    ),

    right(
            singleOffset = Point(1, 0),
            course = Course.horizontal
    ),

    bottom(
            singleOffset = Point(0, 1),
            course = Course.vertical
    );

    companion object

    val opposite
        get() = when (this) {
            left -> right
            top -> bottom
            right -> left
            bottom -> top
        }

}