package org.hnau.android.base.ui.view.lister.adapter

import android.view.View
import androidx.recyclerview.widget.RecyclerView
import org.hnau.emitter.Emitter
import org.hnau.emitter.observing.push.lateinit.LateinitEmitterSimple


class AutoViewHolder<in T>(
    view: View,
    private val setContent: (T) -> Unit
) : RecyclerView.ViewHolder(
    view
) {

    companion object {

        fun <T> create(
            view: (content: Emitter<T>) -> View
        ) = LateinitEmitterSimple<T>().let { emitter ->
            AutoViewHolder<T>(
                view = view(emitter),
                setContent = { emitter.set(it) }
            )
        }

    }

    fun setContent(content: T) = setContent.invoke(content)

}