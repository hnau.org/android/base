package org.hnau.android.base.ui.animation.curver

import android.view.animation.BounceInterpolator
import android.view.animation.OvershootInterpolator
import androidx.interpolator.view.animation.FastOutLinearInInterpolator
import androidx.interpolator.view.animation.FastOutSlowInInterpolator
import androidx.interpolator.view.animation.LinearOutSlowInInterpolator
import org.hnau.base.extensions.it


object Curvers {

    val linear: Curver = ::it
    val accelerateDecelerate = FastOutSlowInInterpolator().curver
    val accelerate = FastOutLinearInInterpolator().curver
    val decelerate = LinearOutSlowInInterpolator().curver
    val overshoot = OvershootInterpolator().curver
    val bounce = BounceInterpolator().curver

}