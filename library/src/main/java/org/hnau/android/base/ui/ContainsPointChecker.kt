package org.hnau.android.base.ui


interface ContainsPointChecker {

    fun checkContainsPoint(
            x: Float, y: Float
    ): Boolean

}