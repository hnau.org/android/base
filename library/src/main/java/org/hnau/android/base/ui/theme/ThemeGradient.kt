package org.hnau.android.base.ui.theme

import org.hnau.android.base.ui.rawcolor.RawColor


interface ThemeGradient {

    companion object

    val start: RawColor
    val end: RawColor
}