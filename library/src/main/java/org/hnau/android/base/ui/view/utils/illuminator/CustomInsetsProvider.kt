package org.hnau.android.base.ui.view.utils.illuminator

import org.hnau.android.base.data.insets.Insets
import org.hnau.emitter.Emitter


interface CustomInsetsProvider {

    val windowToContentInsets: Emitter<Insets<Int>>

}