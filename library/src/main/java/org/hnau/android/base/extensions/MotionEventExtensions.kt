package org.hnau.android.base.extensions

import android.view.MotionEvent


val MotionEvent.isDown: Boolean
    get() = action == MotionEvent.ACTION_DOWN

val MotionEvent.isMove: Boolean
    get() = action == MotionEvent.ACTION_MOVE

val MotionEvent.isUp: Boolean
    get() = action == MotionEvent.ACTION_UP

val MotionEvent.isCancel: Boolean
    get() = action == MotionEvent.ACTION_CANCEL

val MotionEvent.isUpOrCancel: Boolean
    get() = isUp || isCancel