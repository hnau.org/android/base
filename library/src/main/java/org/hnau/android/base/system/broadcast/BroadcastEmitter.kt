package org.hnau.android.base.system.broadcast

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import org.hnau.base.extensions.boolean.ifFalse
import org.hnau.emitter.Emitter
import org.hnau.emitter.observing.ObservingEmitter


abstract class BroadcastEmitter(
    intentFilter: IntentFilter,
    register: BroadcastRegister
) : BroadcastReceiver() {

    private class HandleEmitter(
        private val receiver: BroadcastReceiver,
        private val intentFilter: IntentFilter,
        private val register: BroadcastRegister
    ) : ObservingEmitter<Intent>() {

        override fun beforeFirstAttached() {
            super.beforeFirstAttached()
            register.register(receiver, intentFilter)
        }

        override fun afterLastDetached() {
            super.afterLastDetached()
            register.unregister(receiver)
        }

        fun onIntentReceived(intent: Intent) = call(intent)

    }

    private val onBroadcastInner = HandleEmitter(this, intentFilter, register)

    val onBroadcast: Emitter<Intent>
        get() = onBroadcastInner

    protected abstract fun validateBroadcast(intent: Intent): Boolean

    override final fun onReceive(context: Context, intent: Intent) {
        validateBroadcast(intent).ifFalse { return }
        onBroadcastInner.onIntentReceived(intent)
    }

}