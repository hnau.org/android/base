package org.hnau.android.base.data.getter.px

import org.hnau.emitter.Emitter
import org.hnau.emitter.observing.push.always.content.ManualEmitter


object ScaleManager {

    private val scaleEmitterInner = ManualEmitter(1f)
    val scaleEmitter: Emitter<Float> get() = scaleEmitterInner

    var scale: Float by scaleEmitterInner

}