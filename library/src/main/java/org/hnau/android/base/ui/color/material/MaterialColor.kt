package org.hnau.android.base.ui.color.material


object MaterialColor {

    val values = listOf(
        MaterialList(
            MaterialHue.red,
            MaterialLightness.v50 to "#ffebee",
            MaterialLightness.v100 to "#ffcdd2",
            MaterialLightness.v200 to "#ef9a9a",
            MaterialLightness.v300 to "#e57373",
            MaterialLightness.v400 to "#ef5350",
            MaterialLightness.v500 to "#f44336",
            MaterialLightness.v600 to "#e53935",
            MaterialLightness.v700 to "#d32f2f",
            MaterialLightness.v800 to "#c62828",
            MaterialLightness.v900 to "#b71c1c",
            MaterialLightness.a100 to "#ff8a80",
            MaterialLightness.a200 to "#ff5252",
            MaterialLightness.a400 to "#ff1744",
            MaterialLightness.a700 to "#d50000"
        ),

        MaterialList(
            MaterialHue.pink,
            MaterialLightness.v50 to "#fce4ec",
            MaterialLightness.v100 to "#f8bbd0",
            MaterialLightness.v200 to "#f48fb1",
            MaterialLightness.v300 to "#f06292",
            MaterialLightness.v400 to "#ec407a",
            MaterialLightness.v500 to "#e91e63",
            MaterialLightness.v600 to "#d81b60",
            MaterialLightness.v700 to "#c2185b",
            MaterialLightness.v800 to "#ad1457",
            MaterialLightness.v900 to "#880e4f",
            MaterialLightness.a100 to "#ff80ab",
            MaterialLightness.a200 to "#ff4081",
            MaterialLightness.a400 to "#f50057",
            MaterialLightness.a700 to "#c51162"
        ),

        MaterialList(
            MaterialHue.purple,
            MaterialLightness.v50 to "#f3e5f5",
            MaterialLightness.v100 to "#e1bee7",
            MaterialLightness.v200 to "#ce93d8",
            MaterialLightness.v300 to "#ba68c8",
            MaterialLightness.v400 to "#ab47bc",
            MaterialLightness.v500 to "#9c27b0",
            MaterialLightness.v600 to "#8e24aa",
            MaterialLightness.v700 to "#7b1fa2",
            MaterialLightness.v800 to "#6a1b9a",
            MaterialLightness.v900 to "#4a148c",
            MaterialLightness.a100 to "#ea80fc",
            MaterialLightness.a200 to "#e040fb",
            MaterialLightness.a400 to "#d500f9",
            MaterialLightness.a700 to "#aa00ff"
        ),

        MaterialList(
            MaterialHue.deepPurple,
            MaterialLightness.v50 to "#ede7f6",
            MaterialLightness.v100 to "#d1c4e9",
            MaterialLightness.v200 to "#b39ddb",
            MaterialLightness.v300 to "#9575cd",
            MaterialLightness.v400 to "#7e57c2",
            MaterialLightness.v500 to "#673ab7",
            MaterialLightness.v600 to "#5e35b1",
            MaterialLightness.v700 to "#512da8",
            MaterialLightness.v800 to "#4527a0",
            MaterialLightness.v900 to "#311b92",
            MaterialLightness.a100 to "#b388ff",
            MaterialLightness.a200 to "#7c4dff",
            MaterialLightness.a400 to "#651fff",
            MaterialLightness.a700 to "#6200ea"
        ),

        MaterialList(
            MaterialHue.indigo,
            MaterialLightness.v50 to "#e8eaf6",
            MaterialLightness.v100 to "#c5cae9",
            MaterialLightness.v200 to "#9fa8da",
            MaterialLightness.v300 to "#7986cb",
            MaterialLightness.v400 to "#5c6bc0",
            MaterialLightness.v500 to "#3f51b5",
            MaterialLightness.v600 to "#3949ab",
            MaterialLightness.v700 to "#303f9f",
            MaterialLightness.v800 to "#283593",
            MaterialLightness.v900 to "#1a237e",
            MaterialLightness.a100 to "#8c9eff",
            MaterialLightness.a200 to "#536dfe",
            MaterialLightness.a400 to "#3d5afe",
            MaterialLightness.a700 to "#304ffe"
        ),

        MaterialList(
            MaterialHue.blue,
            MaterialLightness.v50 to "#e3f2fd",
            MaterialLightness.v100 to "#bbdefb",
            MaterialLightness.v200 to "#90caf9",
            MaterialLightness.v300 to "#64b5f6",
            MaterialLightness.v400 to "#42a5f5",
            MaterialLightness.v500 to "#2196f3",
            MaterialLightness.v600 to "#1e88e5",
            MaterialLightness.v700 to "#1976d2",
            MaterialLightness.v800 to "#1565c0",
            MaterialLightness.v900 to "#0d47a1",
            MaterialLightness.a100 to "#82b1ff",
            MaterialLightness.a200 to "#448aff",
            MaterialLightness.a400 to "#2979ff",
            MaterialLightness.a700 to "#2962ff"
        ),

        MaterialList(
            MaterialHue.lightBlue,
            MaterialLightness.v50 to "#e1f5fe",
            MaterialLightness.v100 to "#b3e5fc",
            MaterialLightness.v200 to "#81d4fa",
            MaterialLightness.v300 to "#4fc3f7",
            MaterialLightness.v400 to "#29b6f6",
            MaterialLightness.v500 to "#03a9f4",
            MaterialLightness.v600 to "#039be5",
            MaterialLightness.v700 to "#0288d1",
            MaterialLightness.v800 to "#0277bd",
            MaterialLightness.v900 to "#01579b",
            MaterialLightness.a100 to "#80d8ff",
            MaterialLightness.a200 to "#40c4ff",
            MaterialLightness.a400 to "#00b0ff",
            MaterialLightness.a700 to "#0091ea"
        ),

        MaterialList(
            MaterialHue.cyan,
            MaterialLightness.v50 to "#e0f7fa",
            MaterialLightness.v100 to "#b2ebf2",
            MaterialLightness.v200 to "#80deea",
            MaterialLightness.v300 to "#4dd0e1",
            MaterialLightness.v400 to "#26c6da",
            MaterialLightness.v500 to "#00bcd4",
            MaterialLightness.v600 to "#00acc1",
            MaterialLightness.v700 to "#0097a7",
            MaterialLightness.v800 to "#00838f",
            MaterialLightness.v900 to "#006064",
            MaterialLightness.a100 to "#84ffff",
            MaterialLightness.a200 to "#18ffff",
            MaterialLightness.a400 to "#00e5ff",
            MaterialLightness.a700 to "#00b8d4"
        ),

        MaterialList(
            MaterialHue.teal,
            MaterialLightness.v50 to "#e0f2f1",
            MaterialLightness.v100 to "#b2dfdb",
            MaterialLightness.v200 to "#80cbc4",
            MaterialLightness.v300 to "#4db6ac",
            MaterialLightness.v400 to "#26a69a",
            MaterialLightness.v500 to "#009688",
            MaterialLightness.v600 to "#00897b",
            MaterialLightness.v700 to "#00796b",
            MaterialLightness.v800 to "#00695c",
            MaterialLightness.v900 to "#004d40",
            MaterialLightness.a100 to "#a7ffeb",
            MaterialLightness.a200 to "#64ffda",
            MaterialLightness.a400 to "#1de9b6",
            MaterialLightness.a700 to "#00bfa5"
        ),

        MaterialList(
            MaterialHue.green,
            MaterialLightness.v50 to "#e8f5e9",
            MaterialLightness.v100 to "#c8e6c9",
            MaterialLightness.v200 to "#a5d6a7",
            MaterialLightness.v300 to "#81c784",
            MaterialLightness.v400 to "#66bb6a",
            MaterialLightness.v500 to "#4caf50",
            MaterialLightness.v600 to "#43a047",
            MaterialLightness.v700 to "#388e3c",
            MaterialLightness.v800 to "#2e7d32",
            MaterialLightness.v900 to "#1b5e20",
            MaterialLightness.a100 to "#b9f6ca",
            MaterialLightness.a200 to "#69f0ae",
            MaterialLightness.a400 to "#00e676",
            MaterialLightness.a700 to "#00c853"
        ),

        MaterialList(
            MaterialHue.lightGreen,
            MaterialLightness.v50 to "#f1f8e9",
            MaterialLightness.v100 to "#dcedc8",
            MaterialLightness.v200 to "#c5e1a5",
            MaterialLightness.v300 to "#aed581",
            MaterialLightness.v400 to "#9ccc65",
            MaterialLightness.v500 to "#8bc34a",
            MaterialLightness.v600 to "#7cb342",
            MaterialLightness.v700 to "#689f38",
            MaterialLightness.v800 to "#558b2f",
            MaterialLightness.v900 to "#33691e",
            MaterialLightness.a100 to "#ccff90",
            MaterialLightness.a200 to "#b2ff59",
            MaterialLightness.a400 to "#76ff03",
            MaterialLightness.a700 to "#64dd17"
        ),

        MaterialList(
            MaterialHue.lime,
            MaterialLightness.v50 to "#f9fbe7",
            MaterialLightness.v100 to "#f0f4c3",
            MaterialLightness.v200 to "#e6ee9c",
            MaterialLightness.v300 to "#dce775",
            MaterialLightness.v400 to "#d4e157",
            MaterialLightness.v500 to "#cddc39",
            MaterialLightness.v600 to "#c0ca33",
            MaterialLightness.v700 to "#afb42b",
            MaterialLightness.v800 to "#9e9d24",
            MaterialLightness.v900 to "#827717",
            MaterialLightness.a100 to "#f4ff81",
            MaterialLightness.a200 to "#eeff41",
            MaterialLightness.a400 to "#c6ff00",
            MaterialLightness.a700 to "#aeea00"
        ),

        MaterialList(
            MaterialHue.yellow,
            MaterialLightness.v50 to "#fffde7",
            MaterialLightness.v100 to "#fff9c4",
            MaterialLightness.v200 to "#fff59d",
            MaterialLightness.v300 to "#fff176",
            MaterialLightness.v400 to "#ffee58",
            MaterialLightness.v500 to "#ffeb3b",
            MaterialLightness.v600 to "#fdd835",
            MaterialLightness.v700 to "#fbc02d",
            MaterialLightness.v800 to "#f9a825",
            MaterialLightness.v900 to "#f57f17",
            MaterialLightness.a100 to "#ffff8d",
            MaterialLightness.a200 to "#ffff00",
            MaterialLightness.a400 to "#ffea00",
            MaterialLightness.a700 to "#ffd600"
        ),

        MaterialList(
            MaterialHue.amber,
            MaterialLightness.v50 to "#fff8e1",
            MaterialLightness.v100 to "#ffecb3",
            MaterialLightness.v200 to "#ffe082",
            MaterialLightness.v300 to "#ffd54f",
            MaterialLightness.v400 to "#ffca28",
            MaterialLightness.v500 to "#ffc107",
            MaterialLightness.v600 to "#ffb300",
            MaterialLightness.v700 to "#ffa000",
            MaterialLightness.v800 to "#ff8f00",
            MaterialLightness.v900 to "#ff6f00",
            MaterialLightness.a100 to "#ffe57f",
            MaterialLightness.a200 to "#ffd740",
            MaterialLightness.a400 to "#ffc400",
            MaterialLightness.a700 to "#ffab00"
        ),

        MaterialList(
            MaterialHue.orange,
            MaterialLightness.v50 to "#fff3e0",
            MaterialLightness.v100 to "#ffe0b2",
            MaterialLightness.v200 to "#ffcc80",
            MaterialLightness.v300 to "#ffb74d",
            MaterialLightness.v400 to "#ffa726",
            MaterialLightness.v500 to "#ff9800",
            MaterialLightness.v600 to "#fb8c00",
            MaterialLightness.v700 to "#f57c00",
            MaterialLightness.v800 to "#ef6c00",
            MaterialLightness.v900 to "#e65100",
            MaterialLightness.a100 to "#ffd180",
            MaterialLightness.a200 to "#ffab40",
            MaterialLightness.a400 to "#ff9100",
            MaterialLightness.a700 to "#ff6d00"
        ),

        MaterialList(
            MaterialHue.deepOrange,
            MaterialLightness.v50 to "#fbe9e7",
            MaterialLightness.v100 to "#ffccbc",
            MaterialLightness.v200 to "#ffab91",
            MaterialLightness.v300 to "#ff8a65",
            MaterialLightness.v400 to "#ff7043",
            MaterialLightness.v500 to "#ff5722",
            MaterialLightness.v600 to "#f4511e",
            MaterialLightness.v700 to "#e64a19",
            MaterialLightness.v800 to "#d84315",
            MaterialLightness.v900 to "#bf360c",
            MaterialLightness.a100 to "#ff9e80",
            MaterialLightness.a200 to "#ff6e40",
            MaterialLightness.a400 to "#ff3d00",
            MaterialLightness.a700 to "#dd2c00"
        ),

        MaterialList(
            MaterialHue.brown,
            MaterialLightness.v50 to "#efebe9",
            MaterialLightness.v100 to "#d7ccc8",
            MaterialLightness.v200 to "#bcaaa4",
            MaterialLightness.v300 to "#a1887f",
            MaterialLightness.v400 to "#8d6e63",
            MaterialLightness.v500 to "#795548",
            MaterialLightness.v600 to "#6d4c41",
            MaterialLightness.v700 to "#5d4037",
            MaterialLightness.v800 to "#4e342e",
            MaterialLightness.v900 to "#3e2723"
        ),

        MaterialList(
            MaterialHue.grey,
            MaterialLightness.v50 to "#fafafa",
            MaterialLightness.v100 to "#f5f5f5",
            MaterialLightness.v200 to "#eeeeee",
            MaterialLightness.v300 to "#e0e0e0",
            MaterialLightness.v400 to "#bdbdbd",
            MaterialLightness.v500 to "#9e9e9e",
            MaterialLightness.v600 to "#757575",
            MaterialLightness.v700 to "#616161",
            MaterialLightness.v800 to "#424242",
            MaterialLightness.v900 to "#212121"
        ),

        MaterialList(
            MaterialHue.blueGrey,
            MaterialLightness.v50 to "#eceff1",
            MaterialLightness.v100 to "#cfd8dc",
            MaterialLightness.v200 to "#b0bec5",
            MaterialLightness.v300 to "#90a4ae",
            MaterialLightness.v400 to "#78909c",
            MaterialLightness.v500 to "#607d8b",
            MaterialLightness.v600 to "#546e7a",
            MaterialLightness.v700 to "#455a64",
            MaterialLightness.v800 to "#37474f",
            MaterialLightness.v900 to "#263238"
        )

    ).associateBy { materialList -> materialList.hue }

    operator fun get(hue: MaterialHue) =
        values.getValue(hue)

    val red get() = get(MaterialHue.red)
    val pink get() = get(MaterialHue.pink)
    val purple get() = get(MaterialHue.purple)
    val deepPurple get() = get(MaterialHue.deepPurple)
    val indigo get() = get(MaterialHue.indigo)
    val blue get() = get(MaterialHue.blue)
    val lightBlue get() = get(MaterialHue.lightBlue)
    val cyan get() = get(MaterialHue.cyan)
    val teal get() = get(MaterialHue.teal)
    val green get() = get(MaterialHue.green)
    val lightGreen get() = get(MaterialHue.lightGreen)
    val lime get() = get(MaterialHue.lime)
    val yellow get() = get(MaterialHue.yellow)
    val amber get() = get(MaterialHue.amber)
    val orange get() = get(MaterialHue.orange)
    val deepOrange get() = get(MaterialHue.deepOrange)
    val brown get() = get(MaterialHue.brown)
    val grey get() = get(MaterialHue.grey)
    val blueGrey get() = get(MaterialHue.blueGrey)

}