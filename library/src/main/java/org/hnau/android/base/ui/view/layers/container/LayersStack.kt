package org.hnau.android.base.ui.view.layers.container

import org.hnau.android.base.ui.view.layers.Layer
import org.hnau.android.base.ui.view.utils.GoBackHandler
import org.hnau.base.extensions.boolean.ifFalse
import org.hnau.base.extensions.boolean.ifTrue
import org.hnau.emitter.observing.push.always.AlwaysEmitter


open class LayersStack<L : Layer> : AlwaysEmitter<List<L>>(), GoBackHandler {

    companion object;

    private val layersInner = ArrayList<L>()

    override val value get() = layersInner

    val layers: List<L> get() = layersInner

    fun canPop() = layersInner.size > 1

    fun pop(): Boolean {
        canPop().ifFalse { return false }
        val layer = layersInner.removeAt(layersInner.size - 1)
        layer.onDestroy()
        onChanged()
        return true
    }

    fun push(
        layer: L,
        clearStack: Boolean = false
    ) {
        clearStack.ifTrue {
            layersInner.asReversed().forEach(Layer::onDestroy)
            layersInner.clear()
        }
        layer.onCreate()
        layersInner.add(layer)
        onChanged()
    }

    override fun handleGoBack(): Boolean {
        layersInner.lastOrNull()?.handleGoBack()?.ifTrue { return true }
        pop().ifTrue { return true }
        return false
    }

}