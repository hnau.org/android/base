package org.hnau.android.base.ui.decoration

import android.graphics.Canvas
import org.hnau.emitter.Emitter
import org.hnau.emitter.observing.push.always.line

private class CompositeViewDecoration : ViewDecoration {

    var decorations: Iterable<ViewDecoration?> = emptyList()

    override fun invoke(
            canvas: Canvas
    ) = decorations.forEach { decoration ->
        decoration?.invoke(canvas)
    }

}

fun ViewDecorations.composite(
        decorations: Iterable<Emitter<ViewDecoration>>
): Emitter<ViewDecoration> = CompositeViewDecoration().let { viewDecoration ->
    decorations.line { decorationsList ->
        viewDecoration.apply { this.decorations = decorationsList }
    }
}

fun ViewDecorations.composite(
        vararg decorations: Emitter<ViewDecoration>
) = composite(
        decorations = decorations.toList()
)