package org.hnau.android.base.ui.view.menu

import org.hnau.android.base.data.getter.picture.Picture
import org.hnau.android.base.data.getter.text.Text
import org.hnau.android.base.data.getter.tone.Tone
import org.hnau.base.extensions.boolean.checkBoolean
import org.hnau.emitter.Emitter


data class BottomMenuItem(
        val design: Design,
        val behavior: Behavior
) {

    companion object;

    enum class State(
            val isActive: Boolean
    ) {

        inactive(false),
        active(true);

        companion object {

            operator fun get(
                    isActive: Boolean
            ) = isActive.checkBoolean(
                    ifTrue = { active },
                    ifFalse = { inactive }
            )

        }
    }

    data class Design(
            val createIcon: (State) -> Picture,
            val title: Title
    ) {

        data class Title(
                val tone: Tone,
                val text: Text
        )

    }

    data class Behavior(
            val state: Emitter<State>,
            val onClick: () -> Unit
    )

}