package org.hnau.android.base.extensions.view.base.edgesOld

import android.os.Build
import android.view.View
import android.widget.EdgeEffect
import androidx.annotation.RequiresApi
import org.hnau.base.extensions.boolean.ifFalse
import org.hnau.base.extensions.container.ifNotEmpty


object SetEdgesOldColorUtils {

    @RequiresApi(Build.VERSION_CODES.LOLLIPOP)
    inline fun <reified T : View> createEdgeEffectsColorsSetter(
            fieldsNames: List<String>
    ): T.(Int) -> Unit {
        val fields = run {
            val names = fieldsNames.toHashSet()
            val result = T::class.java.fields
                    .mapNotNull { field ->
                        names.remove(field.name).ifFalse { return@mapNotNull null }
                        (EdgeEffect::class.java.isAssignableFrom(field.declaringClass)).ifFalse {
                            throw IllegalStateException("Field ${field.name} class is not EdgeEffect")
                        }
                        field.apply { isAccessible = true }
                    }
            names.ifNotEmpty {
                throw IllegalStateException("Fields ${names} not found in class ${T::class.java.name}")
            }
            result
        }
        return { color ->
            fields.forEach { field ->
                (field.get(this) as EdgeEffect).color = color
            }
        }
    }

}