package org.hnau.android.base.ui.view.utils.illuminator

import android.annotation.SuppressLint
import android.view.View
import android.view.ViewGroup
import org.hnau.android.base.data.insets.Insets
import org.hnau.android.base.data.insets.typed.horizontalSum
import org.hnau.android.base.data.insets.typed.verticalSum
import org.hnau.android.base.extensions.view.changeMeasureSpecSize
import org.hnau.android.base.extensions.view.measureSpecFit
import org.hnau.android.base.ui.view.utils.state.createStateEmitters
import org.hnau.base.extensions.boolean.ifFalse
import org.hnau.emitter.Emitter
import org.hnau.emitter.extensions.callIfTrue
import org.hnau.emitter.extensions.listen
import org.hnau.emitter.extensions.useWhen
import org.hnau.emitter.observing.push.always.content.ManualEmitter


abstract class ApplyInsetsContainer(
        private val content: View
) : ViewGroup(
        content.context
) {

    private var currentInsets: Insets<Int> = Insets(0)
        set(value) {
            field = value
            post { requestLayout() }
        }

    protected val state = createStateEmitters()

    private val waitingForInsetsCreation = ManualEmitter(true)

    init {
        addView(content)
        state
                .isActive
                .callIfTrue()
                .useWhen(waitingForInsetsCreation)
                .listen {
                    createInsets().observe { currentInsets = it }
                    waitingForInsetsCreation.value = false
                }

    }

    protected abstract fun createInsets(): Emitter<Insets<Int>>

    override fun onLayout(
            changed: Boolean,
            l: Int, t: Int,
            r: Int, b: Int
    ) = content.layout(
            currentInsets.left,
            currentInsets.top,
            width - currentInsets.right,
            height - currentInsets.bottom
    )

    override fun onMeasure(
            widthMeasureSpec: Int,
            heightMeasureSpec: Int
    ) {
        content.measure(
                changeMeasureSpecSize(widthMeasureSpec, -currentInsets.horizontalSum),
                changeMeasureSpecSize(heightMeasureSpec, -currentInsets.verticalSum)
        )
        setMeasuredDimension(
                measureSpecFit(widthMeasureSpec, content.measuredWidth + currentInsets.horizontalSum),
                measureSpecFit(heightMeasureSpec, content.measuredHeight + currentInsets.verticalSum)
        )
    }

}

@SuppressLint("ViewConstructor")
open class ApplySelfInsetsContainer(
        content: View
) : ApplyInsetsContainer(
        content = content
) {

    final override fun createInsets() = resolveInsets(
            isActive = state.isActive,
            onLayout = state.onLayout
    )

}

@Suppress("FunctionName")
inline fun ApplyInsetsContainer(
        content: View,
        crossinline createInsets: View.() -> Emitter<Insets<Int>>
) = object : ApplyInsetsContainer(
        content = content
) {

    override fun createInsets() =
            createInsets.invoke(this)

}

fun ApplyInsetsContainer(
        content: View,
        insets: Emitter<Insets<Int>>
) = ApplyInsetsContainer(
        content = content,
        createInsets = { insets }
)