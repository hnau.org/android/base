package org.hnau.android.base.ui.view.recycler.adapter.item.various

import android.content.Context
import android.view.View
import org.hnau.emitter.Emitter


abstract class ViewFactory<in T>(
    val createView: (Context, Emitter<T>) -> View
)