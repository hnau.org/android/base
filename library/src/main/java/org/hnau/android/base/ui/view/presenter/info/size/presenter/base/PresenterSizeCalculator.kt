package org.hnau.android.base.ui.view.presenter.info.size.presenter.base

import org.hnau.android.base.ui.view.presenter.view.lifecycle.ShowingMeasurable
import org.hnau.android.base.ui.view.presenter.view.lifecycle.ViewMeasuredSizeGetter


interface PresenterSizeCalculator {

    companion object

    fun calcPresenterSize(
        measureSpec: Int,
        measureds: Collection<ShowingMeasurable>,
        measuredSizeExtractor: (ViewMeasuredSizeGetter) -> Int?
    ): Int

}