package org.hnau.android.base.system.preferences.adapter

import android.content.SharedPreferences
import org.hnau.base.extensions.boolean.ifTrue
import org.hnau.base.extensions.checkNullable
import org.hnau.base.extensions.sureNotNull


fun <T> SharedPreferencesAdapter.Companion.create(
        read: SharedPreferences.(key: String) -> T,
        write: SharedPreferences.Editor.(key: String, value: T) -> Unit
) = SharedPreferencesAdapter<T?>(
        read = { key ->
            contains(key).ifTrue { read(this, key) }
        },
        write = { key, valueOrNull ->
            valueOrNull.checkNullable(
                    ifNull = { remove(key) },
                    ifNotNull = { value -> write(key, value) }
            )
        }
)

private val intSharedPreferencesAdapter = SharedPreferencesAdapter.create(
        read = { key -> getInt(key, 0) },
        write = { key, value -> putInt(key, value) }
)

val SharedPreferencesAdapter.Companion.int
    get() = intSharedPreferencesAdapter

private val longSharedPreferencesAdapter = SharedPreferencesAdapter.create(
        read = { key -> getLong(key, 0L) },
        write = { key, value -> putLong(key, value) }
)

val SharedPreferencesAdapter.Companion.long
    get() = longSharedPreferencesAdapter

private val floatSharedPreferencesAdapter = SharedPreferencesAdapter.create(
        read = { key -> getFloat(key, 0f) },
        write = { key, value -> putFloat(key, value) }
)

val SharedPreferencesAdapter.Companion.float
    get() = floatSharedPreferencesAdapter

private val booleanSharedPreferencesAdapter = SharedPreferencesAdapter.create(
        read = { key -> getBoolean(key, false) },
        write = { key, value -> putBoolean(key, value) }
)

val SharedPreferencesAdapter.Companion.boolean
    get() = booleanSharedPreferencesAdapter

private val stringSharedPreferencesAdapter = SharedPreferencesAdapter.create(
        read = { key -> getString(key, null).sureNotNull() },
        write = { key, value -> putString(key, value) }
)

val SharedPreferencesAdapter.Companion.string
    get() = stringSharedPreferencesAdapter

private val stringSetSharedPreferencesAdapter = SharedPreferencesAdapter.create<Set<String>>(
        read = { key -> getStringSet(key, null).sureNotNull() },
        write = { key, value -> putStringSet(key, value) }
)

val SharedPreferencesAdapter.Companion.stringSet
    get() = stringSetSharedPreferencesAdapter