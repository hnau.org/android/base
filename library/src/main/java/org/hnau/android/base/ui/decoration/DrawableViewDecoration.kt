package org.hnau.android.base.ui.decoration

import android.graphics.drawable.Drawable
import org.hnau.android.base.data.edges.Edges
import org.hnau.android.base.extensions.setBounds
import org.hnau.emitter.Emitter
import org.hnau.emitter.extensions.map


fun ViewDecorations.drawable(
        bounds: Emitter<Edges<Int>>,
        drawable: Drawable
) = ViewDecorations
        .create(drawable::draw)
        .let { viewDecoration ->
            bounds.map { boundsValue ->
                drawable.setBounds(boundsValue)
                viewDecoration
            }
        }