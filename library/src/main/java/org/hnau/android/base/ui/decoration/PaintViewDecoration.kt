package org.hnau.android.base.ui.decoration

import android.graphics.Canvas
import android.graphics.Paint
import org.hnau.android.base.ui.shape.CanvasShape
import org.hnau.base.data.possible.lateinit.Lateinit
import org.hnau.base.data.possible.lateinit.unsafe
import org.hnau.base.data.possible.valueOrThrow
import org.hnau.emitter.Emitter
import org.hnau.emitter.extensions.map
import org.hnau.emitter.observing.push.lateinit.combine


class PaintViewDecoration : ViewDecoration {

    var shape: CanvasShape? = null
    var paint: Paint? = null

    override fun invoke(
            canvas: Canvas
    ) {
        paint?.let { shape?.draw(canvas, it) }
    }

}

fun ViewDecorations.paint(
        shape: Emitter<CanvasShape>,
        paint: Paint
) = PaintViewDecoration().let { viewDecoration ->
    viewDecoration.paint = paint
    shape.map { canvasShape ->
        viewDecoration.shape = canvasShape
        viewDecoration
    }
}

fun ViewDecorations.paint(
        shape: Emitter<CanvasShape>,
        paint: Emitter<Paint>
) = PaintViewDecoration().let { viewDecoration ->
    Emitter.combine(
            firstSource = shape,
            secondSource = paint
    ) { shapeValue, paintValue ->
        viewDecoration.apply {
            this.shape = shapeValue
            this.paint = paintValue
        }
    }
}