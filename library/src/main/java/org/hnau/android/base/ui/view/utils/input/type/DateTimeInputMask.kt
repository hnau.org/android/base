package org.hnau.android.base.ui.view.utils.input.type

import android.text.InputType
import org.hnau.android.base.ui.view.utils.input.type.base.ClassInputMask
import org.hnau.android.base.ui.view.utils.input.type.base.setVariation


class DateTimeInputMask() : ClassInputMask(InputType.TYPE_CLASS_DATETIME) {

    fun setTimeVariation() =
        setVariation(InputType.TYPE_DATETIME_VARIATION_TIME)

    fun setDateVariation() =
        setVariation(InputType.TYPE_DATETIME_VARIATION_DATE)

}