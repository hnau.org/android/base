package org.hnau.android.base.data.course


@Suppress("EnumEntryName")
enum class Course {
    horizontal,
    vertical
}