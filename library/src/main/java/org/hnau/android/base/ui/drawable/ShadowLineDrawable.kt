package org.hnau.android.base.ui.drawable

import android.content.Context
import android.graphics.Canvas
import android.graphics.ColorFilter
import android.graphics.Paint
import android.graphics.PixelFormat
import android.graphics.drawable.Drawable
import org.hnau.android.base.data.Side
import org.hnau.android.base.data.course.Course
import org.hnau.android.base.data.course.checkCourse
import org.hnau.android.base.data.getter.tone.Tone
import org.hnau.android.base.data.getter.tone.Tones
import org.hnau.android.base.data.getter.tone.replaceAlpha
import org.hnau.android.base.extensions.color.alphaPercentage
import org.hnau.android.base.extensions.color.clearAlpha
import org.hnau.android.base.extensions.color.shiftTo
import org.hnau.android.base.extensions.inState
import org.hnau.base.extensions.boolean.checkBoolean
import org.hnau.base.extensions.number.asPercentageInter
import org.hnau.base.extensions.number.takeIfPositive
import kotlin.math.min
import kotlin.math.sqrt


open class ShadowLineDrawable(
        context: Context,
        info: Info
) : Drawable() {

    data class Info(
            val toSide: Side = Side.bottom,
            val fromTone: Tone = Tones.black,
            val toTone: Tone = fromTone.replaceAlpha(0f),
            val alphaCurver: (Float) -> Float = { 1f - sqrt(1f - it * it) }
    )

    private val fromSide = info.toSide.opposite
    private val fromColor = info.fromTone(context).clearAlpha()
    private val toColor = info.toTone(context).clearAlpha()
    private val fromAlpha = info.fromTone(context).alphaPercentage
    private val toAlpha = info.toTone(context).alphaPercentage
    private val alphaCurver = info.alphaCurver

    private var drawAlpha = 255

    private val paint = Paint()

    private val course = fromSide.course

    private val fromRightOrTop =
            fromSide == Side.right || fromSide == Side.top

    override fun draw(canvas: Canvas) = canvas.inState {

        val width = bounds.width().takeIfPositive() ?: return@inState
        val height = bounds.height().takeIfPositive() ?: return@inState

        translate(bounds.left.toFloat(), bounds.top.toFloat())

        if (course == Course.vertical) {
            val rotateOffset = min(width, height) / 2f
            val rotateOffsetX =
                    fromRightOrTop.checkBoolean({ width - rotateOffset }, { rotateOffset })
            val rotateOffsetY =
                    fromRightOrTop.checkBoolean({ height - rotateOffset }, { rotateOffset })
            rotate(-90f, rotateOffsetX, rotateOffsetY)
        }

        if (fromRightOrTop) {
            scale(-1f, -1f, width / 2f, height / 2f)
        }

        val along = course.checkCourse({ width }, { height })
        val across = course.checkCourse({ height }, { width })
        repeat(along) { i ->
            val rawPercentage = i.toFloat() / along.toFloat()
            val percentage = 1f - alphaCurver(1f - rawPercentage)
            paint.color = fromColor.shiftTo(toColor, percentage)
            paint.alpha = (drawAlpha * percentage.asPercentageInter(fromAlpha, toAlpha)).toInt()
            drawLine(i.toFloat(), 0f, i.toFloat(), across.toFloat(), paint)
        }

    }

    override fun getOpacity() =
            PixelFormat.TRANSLUCENT

    override fun setAlpha(alpha: Int) {
        drawAlpha = alpha
    }

    override fun setColorFilter(colorFilter: ColorFilter?) {
        paint.setColorFilter(colorFilter)
    }

}