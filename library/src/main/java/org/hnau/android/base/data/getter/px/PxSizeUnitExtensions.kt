package org.hnau.android.base.data.getter.px

import android.content.Context
import android.util.DisplayMetrics

@Suppress("EnumEntryName")
enum class SizeUnitType(
        scaleToPxFactor: (DisplayMetrics) -> Float
) {

    px({ 1f }),
    dp({ it.density }),
    sp({ it.scaledDensity }),
    pt({ it.xdpi / 72f }),
    inch({ it.xdpi }),
    mm({ it.xdpi / 25.4f }),
    cm({ it.xdpi / 2.54f });

    val scaleToPxFactor = { context: Context ->
        scaleToPxFactor(context.resources.displayMetrics)
    }

}

fun SizeUnitType.scaleToPixels(context: Context, value: Float) =
        scaleToPxFactor(context) * value

@Suppress("FunctionName")
fun Px(value: Number, sizeUnitType: SizeUnitType) =
        Px { context -> value.toFloat() * sizeUnitType.scaleToPxFactor(context) }

fun px(value: Number) = Px(value, SizeUnitType.px)
fun dp(value: Number) = Px(value, SizeUnitType.dp)
fun sp(value: Number) = Px(value, SizeUnitType.sp)
fun inch(value: Number) = Px(value, SizeUnitType.inch)
fun pt(value: Number) = Px(value, SizeUnitType.pt)
fun mm(value: Number) = Px(value, SizeUnitType.mm)
fun cm(value: Number) = Px(value, SizeUnitType.cm)

val Number.px get() = px(this)
val Number.dp get() = dp(this)
val Number.sp get() = sp(this)
val Number.inch get() = inch(this)
val Number.pt get() = pt(this)
val Number.mm get() = mm(this)
val Number.cm get() = cm(this)

fun Px.get(context: Context, sizeUnitType: SizeUnitType) =
        this(context) / sizeUnitType.scaleToPxFactor(context)

fun Px.px(context: Context) = get(context, SizeUnitType.px)
fun Px.dp(context: Context) = get(context, SizeUnitType.dp)
fun Px.sp(context: Context) = get(context, SizeUnitType.sp)
fun Px.inch(context: Context) = get(context, SizeUnitType.inch)
fun Px.pt(context: Context) = get(context, SizeUnitType.pt)
fun Px.mm(context: Context) = get(context, SizeUnitType.mm)
fun Px.cm(context: Context) = get(context, SizeUnitType.cm)

fun Px.pxInt(context: Context) = px(context).toInt()
fun Px.dpInt(context: Context) = dp(context).toInt()
fun Px.spInt(context: Context) = sp(context).toInt()
fun Px.inchInt(context: Context) = inch(context).toInt()
fun Px.ptInt(context: Context) = pt(context).toInt()
fun Px.mmInt(context: Context) = mm(context).toInt()
fun Px.cmInt(context: Context) = cm(context).toInt()