package org.hnau.android.base.ui.view.input

import android.annotation.SuppressLint
import android.content.Context
import android.graphics.Canvas
import android.widget.EditText
import org.hnau.android.base.data.LocaleDirection
import org.hnau.android.base.data.getter.text.Text
import org.hnau.android.base.data.insets.Insets
import org.hnau.android.base.extensions.view.base.setNoBackground
import org.hnau.android.base.extensions.view.createBounds
import org.hnau.android.base.extensions.view.focus.createIsFocusedEmitter
import org.hnau.android.base.extensions.view.text.createTextEmitter
import org.hnau.android.base.extensions.view.text.setCursorColor
import org.hnau.android.base.ui.view.createViewScroll
import org.hnau.android.base.ui.view.input.delegate.BorderDelegate
import org.hnau.android.base.ui.view.input.delegate.InsetsDelegate
import org.hnau.android.base.ui.view.input.delegate.TitleDelegate
import org.hnau.android.base.ui.view.input.info.InputViewInfo
import org.hnau.android.base.ui.view.input.tone.InputViewStateTones
import org.hnau.android.base.ui.view.input.tone.InputViewTones
import org.hnau.android.base.ui.view.utils.state.createStateEmitters
import org.hnau.emitter.Emitter
import org.hnau.emitter.extensions.map
import org.hnau.emitter.extensions.single
import org.hnau.emitter.extensions.useWhen
import org.hnau.emitter.observing.push.lateinit.combine


@SuppressLint("ViewConstructor")
class InputView(
        context: Context,
        titleText: Text,
        tones: Emitter<InputViewStateTones> = Emitter.single(InputViewStateTones()),
        info: InputViewInfo = InputViewInfo()
) : EditText(
        context
) {

    companion object;

    private val bounds = createBounds()
    private val state = createStateEmitters()
    private val isFocusedEmitter = createIsFocusedEmitter()

    private val tonesLocal = Emitter.combine(
            firstSource = tones,
            secondSource = isFocusedEmitter
    ) { tones, isFocused ->
        tones.choose(isFocused)
    }

    private val getInsets: () -> Insets<Float> =
            Insets(0f).let { emptyInsets ->
                { (insets as Insets<Float>?) ?: emptyInsets }
            }

    private val viewScroll = createViewScroll()

    private val title = TitleDelegate(
            context = context,
            titleText = titleText,
            tone = tonesLocal.map(InputViewTones::title),
            isActive = state.isActive,
            invalidate = ::invalidate,
            bounds = bounds,
            info = info.title,
            textIsNotEmpty = createTextEmitter().map(CharSequence::isNotEmpty),
            isFocused = isFocusedEmitter,
            isForeground = state.isForeground,
            font = info.font,
            getInsets = getInsets,
            getTextSize = { textSize },
            viewScroll = viewScroll
    )

    private val border = BorderDelegate(
            context = context,
            borderInfo = info.border,
            boundsEmitter = bounds,
            titleTopHeight = title.topHeight,
            invalidate = ::invalidate,
            isActive = state.isActive,
            tone = tonesLocal.map(InputViewTones::border),
            titleTopWidth = title.topWidth,
            titleToTopPercentageSafe = title.toTopPercentageSafe,
            getInsets = getInsets,
            viewScroll = viewScroll
    )

    private val insets = InsetsDelegate(
            context = context,
            borderWidth = border.borderWidth,
            horizontalSpace = info.horizontalSpace,
            titleTopHeight = title.topHeight
    )

    init {
        setNoBackground()
        includeFontPadding = false
        typeface = info.font(context)

        tones
                .map { it.focused.text }
                .useWhen(state.isActive)
                .observe { textTone ->
                    setCursorColor(textTone(context))
                }

        tonesLocal
                .map(InputViewTones::text)
                .useWhen(state.isActive)
                .observe { textTone ->
                    setTextColor(textTone(context))
                }

        setPadding(0, 0, 0, 0)
    }

    override fun draw(canvas: Canvas) {
        border.draw(canvas)
        title.draw(canvas)
        super.draw(canvas)
    }

    override fun setPaddingRelative(
            start: Int,
            top: Int,
            end: Int,
            bottom: Int
    ) = super.setPadding(
            LocaleDirection.chooseLeft({ start }, { end }) + insets.int.left,
            top + insets.int.top,
            LocaleDirection.chooseRight({ start }, { end }) - insets.int.right,
            bottom - insets.int.bottom
    )

    override fun setPadding(
            left: Int,
            top: Int,
            right: Int,
            bottom: Int
    ) = super.setPadding(
            left + insets.int.left,
            top + insets.int.top,
            right - insets.int.right,
            bottom - insets.int.bottom
    )

    override fun getPaddingLeft() =
            super.getPaddingLeft() - insets.int.left

    override fun getPaddingTop() =
            super.getPaddingTop() - insets.int.top

    override fun getPaddingRight() =
            super.getPaddingRight() + insets.int.right

    override fun getPaddingBottom() =
            super.getPaddingBottom() + insets.int.bottom

    override fun getPaddingStart() =
            LocaleDirection.chooseStart({ paddingLeft }, { paddingRight })

    override fun getPaddingEnd() =
            LocaleDirection.chooseEnd({ paddingLeft }, { paddingRight })


}