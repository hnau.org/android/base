package org.hnau.android.base.ui.drawable.partout

import android.graphics.Canvas
import android.graphics.Rect
import android.graphics.drawable.Drawable
import org.hnau.android.base.ui.drawable.DrawableWrapper


class PartoutDrawable(
    content: Drawable,
    private val partoutCalculator: PartoutCalculator
) : DrawableWrapper(
    content = content,
    wrapperSizeCalculator = partoutCalculator
) {

    private val contentBounds = Rect()

    override fun setBounds(left: Int, top: Int, right: Int, bottom: Int) {
        super.setBounds(left, top, right, bottom)
        partoutCalculator.calcContentBounds(
            parentWidth = right - left,
            parentHeight = bottom - top,
            contentPreferredWidth = content.intrinsicWidth,
            contentPreferredHeight = content.intrinsicHeight,
            result = contentBounds
        )
        contentBounds.offset(left, top)
        content.bounds = contentBounds
    }

    override fun draw(canvas: Canvas) =
        content.draw(canvas)

}