package org.hnau.android.base.ui.theme

import org.hnau.android.base.extensions.color.HSV
import org.hnau.android.base.extensions.color.toHSV
import org.hnau.android.base.extensions.color.toRGB
import org.hnau.android.base.ui.rawcolor.RawColor


data class ThemeGradientBuilder(
        override val start: RawColor,
        val customEnd: RawColor? = null
) : ThemeGradient {

    companion object {

        private const val hueOffset = -30f
        private const val saturationOffset = 0f
        private const val valueOffset = -0.3f

        private fun startColorToDefaultEndColor(
                start: RawColor
        ): RawColor {
            val (hue, saturation, value) = start.value.toHSV()
            val hsv = HSV(
                    hue = ((hue + hueOffset).toInt() % 360).toFloat(),
                    saturation = (saturation + saturationOffset).coerceIn(0f, 1f),
                    value = (value + valueOffset).coerceIn(0f, 1f)
            )
            val rgb = hsv.toRGB()
            return RawColor(rgb)
        }

    }

    override val end =
            customEnd ?: startColorToDefaultEndColor(start)

}