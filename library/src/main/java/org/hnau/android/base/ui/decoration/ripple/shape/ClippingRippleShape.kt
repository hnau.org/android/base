package org.hnau.android.base.ui.decoration.ripple.shape

import org.hnau.android.base.data.edges.Edges
import org.hnau.android.base.data.edges.contains
import org.hnau.android.base.ui.shape.BoundsCanvasShape
import org.hnau.android.base.ui.shape.CanvasShape
import org.hnau.base.data.possible.lateinit.Lateinit
import org.hnau.base.data.possible.lateinit.unsafe
import org.hnau.base.data.possible.valueOrThrow
import org.hnau.base.extensions.it
import org.hnau.emitter.Emitter
import org.hnau.emitter.extensions.map


inline fun <S> RippleShape.Companion.clipping(
        source: Emitter<S>,
        crossinline extractCanvasShape: S.() -> CanvasShape,
        crossinline extractBounds: S.() -> Edges<Int>
) = Lateinit
        .unsafe<Edges<Int>>()
        .let { boundsCache ->
            var maxRadius = 0f
            Lateinit
                    .unsafe<CanvasShape>()
                    .let { canvasShapeCache ->
                        RippleShape(
                                calcCircleCenter = { touchPoint, _, result ->
                                    result.set(touchPoint)
                                },
                                getMaxRadius = { maxRadius },
                                clip = {
                                    canvasShapeCache.valueOrThrow.clip(this)
                                },
                                checkContains = { x, y ->
                                    boundsCache.valueOrThrow.contains(x.toInt(), y.toInt())
                                }
                        ).let { rippleShape ->
                            source.map { sourceValue ->
                                canvasShapeCache.set(sourceValue.extractCanvasShape())
                                val boundsValue = sourceValue.extractBounds()
                                maxRadius = RippleShapeUtils.calcBoundsDiagonal(boundsValue)
                                boundsCache.set(boundsValue)
                                rippleShape
                            }
                        }
                    }
        }

fun RippleShape.Companion.clipping(
        canvasShape: Emitter<BoundsCanvasShape>
) = clipping(
        source = canvasShape,
        extractCanvasShape = ::it,
        extractBounds = BoundsCanvasShape::bounds
)

