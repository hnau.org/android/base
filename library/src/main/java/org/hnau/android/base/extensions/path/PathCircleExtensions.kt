package org.hnau.android.base.extensions.path

import android.graphics.Path
import android.graphics.Rect
import android.graphics.RectF
import kotlin.math.min


fun Path.addEllipse(
        rect: RectF
) = addEllipse(
        ellipseLeft = rect.left,
        ellipseTop = rect.top,
        ellipseRight = rect.right,
        ellipseBottom = rect.bottom
)

fun Path.addEllipse(
        rect: Rect
) = addEllipse(
        ellipseLeft = rect.left,
        ellipseTop = rect.top,
        ellipseRight = rect.right,
        ellipseBottom = rect.bottom
)

fun Path.addEllipse(
        ellipseLeft: Number,
        ellipseTop: Number,
        ellipseRight: Number,
        ellipseBottom: Number
) {
    val left = ellipseLeft.toFloat()
    val top = ellipseTop.toFloat()
    val right = ellipseRight.toFloat()
    val bottom = ellipseBottom.toFloat()
    val cx = (right + left) / 2f
    val cy = (bottom + top) / 2f

    moveTo(left, cy)
    ellipseArcQuarterTo(left, cy, cx, top, false)
    ellipseArcQuarterTo(cx, top, right, cy, true)
    ellipseArcQuarterTo(right, cy, cx, bottom, false)
    ellipseArcQuarterTo(cx, bottom, left, cy, true)
}

fun Path.addCenterEllipse(
        cx: Number,
        cy: Number,
        radiusX: Number,
        radiusY: Number
) = addEllipse(
        ellipseLeft = cx.toFloat() - radiusX.toFloat(),
        ellipseTop = cy.toFloat() - radiusY.toFloat(),
        ellipseRight = cx.toFloat() + radiusX.toFloat(),
        ellipseBottom = cy.toFloat() + radiusY.toFloat()
)

fun Path.addCircle(
        cx: Number,
        cy: Number,
        radius: Number
) = addCenterEllipse(
        cx = cx,
        cy = cy,
        radiusX = radius,
        radiusY = radius
)

fun Path.addCircle(
        bounds: RectF
) = addCircle(
        circleBoundsLeft = bounds.left,
        circleBoundsTop = bounds.top,
        circleBoundsRight = bounds.right,
        circleBoundsBottom = bounds.bottom
)

fun Path.addCircle(
        bounds: Rect
) = addCircle(
        circleBoundsLeft = bounds.left,
        circleBoundsTop = bounds.top,
        circleBoundsRight = bounds.right,
        circleBoundsBottom = bounds.bottom
)

fun Path.addCircle(
        circleBoundsLeft: Number,
        circleBoundsTop: Number,
        circleBoundsRight: Number,
        circleBoundsBottom: Number
) = addCircle(
        cx = (circleBoundsLeft.toFloat() + circleBoundsRight.toFloat()) / 2,
        cy = (circleBoundsTop.toFloat() + circleBoundsBottom.toFloat()) / 2,
        radius = min(
                circleBoundsRight.toFloat() - circleBoundsLeft.toFloat(),
                circleBoundsBottom.toFloat() - circleBoundsTop.toFloat()
        ) / 2
)