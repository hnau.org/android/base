package org.hnau.android.base.ui.view.presenter.view.lifecycle


interface ViewMeasuredSizeGetter {

    val measuredWidth: Int?
    val measuredHeight: Int?

}