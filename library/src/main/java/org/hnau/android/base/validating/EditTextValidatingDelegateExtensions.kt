package org.hnau.android.base.validating

import org.hnau.base.data.maybe.Maybe
import org.hnau.base.data.maybe.isSuccess
import org.hnau.emitter.extensions.map


val EditTextValidatingDelegate.isCorrect
    get() = validatedText.map(Maybe<*>::isSuccess)