package org.hnau.android.base.ui.decoration.ripple

import android.content.Context
import org.hnau.android.base.extensions.color.alpha
import org.hnau.android.base.extensions.color.fullAlpha
import org.hnau.android.base.extensions.inState
import org.hnau.android.base.extensions.view.touch.TouchEvent
import org.hnau.android.base.extensions.view.touch.TouchEventType
import org.hnau.android.base.ui.decoration.ViewDecoration
import org.hnau.android.base.ui.decoration.ViewDecorations
import org.hnau.android.base.ui.decoration.create
import org.hnau.android.base.ui.decoration.ripple.shape.RippleShape
import org.hnau.android.base.ui.shape.BoundsCanvasShape
import org.hnau.android.base.ui.shape.CanvasShape
import org.hnau.base.extensions.boolean.ifFalse
import org.hnau.base.extensions.boolean.ifTrue
import org.hnau.base.extensions.elvis
import org.hnau.base.extensions.it
import org.hnau.emitter.Emitter
import org.hnau.emitter.extensions.listen
import org.hnau.emitter.extensions.map
import org.hnau.emitter.extensions.useWhen
import org.hnau.emitter.observing.push.always.AlwaysEmitter
import org.hnau.emitter.observing.push.always.content.ManualEmitter
import org.hnau.emitter.observing.push.lateinit.collapse


class RippleViewDecoration(
        context: Context,
        touchEvents: Emitter<TouchEvent>,
        private val info: RippleInfo,
        maxCirclesCount: Int = 16
) : AlwaysEmitter<ViewDecoration>() {

    private val ripples = run {
        val color = info.tone(context)
        val maxAlpha = color.alpha
        val colorWithoutAlpha = color.fullAlpha()
        Array(maxCirclesCount) {
            Ripple(
                    maxAlpha = maxAlpha,
                    colorWithoutAlpha = colorWithoutAlpha,
                    info = info
            )
        }
    }

    var shape: RippleShape? = null

    private val isObserving = ManualEmitter(false)

    override val value = ViewDecorations.create {
        shape?.let { shape ->
            inState {
                shape.clip(this)
                val maxRadius = shape.getMaxRadius()
                ripples.forEach { circle ->
                    circle.draw(this, shape, maxRadius)
                }
            }
        }
    }

    init {
        ripples.forEach { circle ->
            circle.needRefreshNotifier.useWhen(isObserving).listen(::onChanged)
        }
        touchEvents.useWhen(isObserving).observe(::onTouchEvent)
    }

    override fun beforeFirstAttached() {
        super.beforeFirstAttached()
        isObserving.value = true
    }

    override fun afterLastDetached() {
        isObserving.value = false
        super.afterLastDetached()
    }

    private fun onTouchEvent(touchEvent: TouchEvent) {

        fun RippleShape?.checkContains(touchEvent: TouchEvent) = this
                ?.checkContains
                ?.invoke(touchEvent.x, touchEvent.y)
                .elvis { false }

        fun onDown(touchEvent: TouchEvent) {
            shape.checkContains(touchEvent).ifFalse { return }
            ripples.forEach { ripple ->
                ripple.tryStart(touchEvent.x, touchEvent.y).ifTrue { return }
            }
        }

        fun onCancel() =
                ripples.forEach(Ripple::cancel)

        fun onMove(touchEvent: TouchEvent) {
            shape.checkContains(touchEvent).ifFalse { onCancel() }
        }

        when (touchEvent.type) {
            TouchEventType.down -> onDown(touchEvent)
            TouchEventType.move -> onMove(touchEvent)
            TouchEventType.up,
            TouchEventType.cancel -> onCancel()
        }
    }

}

fun ViewDecorations.ripple(
        context: Context,
        shape: Emitter<RippleShape>,
        touchEvents: Emitter<TouchEvent>,
        info: RippleInfo,
        maxCirclesCount: Int = 16
) = RippleViewDecoration(
        context = context,
        info = info,
        maxCirclesCount = maxCirclesCount,
        touchEvents = touchEvents
)
        .let { rippleViewDecoration ->
            shape.map { shapeValue ->
                rippleViewDecoration.shape = shapeValue
                rippleViewDecoration
            }
        }
        .collapse()