package org.hnau.android.base.ui.view.waiter.material

import android.content.Context
import android.graphics.Canvas
import android.graphics.Paint
import android.graphics.RectF
import android.view.ViewGroup
import org.hnau.android.base.extensions.color.alpha
import org.hnau.android.base.extensions.paint
import org.hnau.android.base.extensions.view.group.children.view
import org.hnau.android.base.extensions.view.paddingHorizontalSum
import org.hnau.android.base.extensions.view.paddingVerticalSum
import org.hnau.android.base.ui.view.waiter.Waiter
import org.hnau.android.base.ui.view.waiter.material.params.MaterialWaiterAnimationParams
import org.hnau.android.base.ui.view.waiter.material.params.MaterialWaiterColor
import org.hnau.android.base.ui.view.waiter.material.params.MaterialWaiterSize
import org.hnau.emitter.Emitter


class MaterialWaiter(
        context: Context,
        isVisible: Emitter<Boolean>,
        color: MaterialWaiterColor = MaterialWaiterColor(),
        size: MaterialWaiterSize = MaterialWaiterSize.default,
        animationParams: MaterialWaiterAnimationParams = MaterialWaiterAnimationParams()
) : Waiter(
        context = context,
        isVisible = isVisible,
        backgroundTone = color.background,
        visibilitySwitchingTime = animationParams.visibilitySwitchingTime,
        preferredHeight = size.preferredSize,
        preferredWidth = size.preferredSize
) {

    companion object;

    private val radius = size.radius(context)

    private val lineAlpha = color.foreground(context).alpha
    private val linePaint = paint {
        style = Paint.Style.STROKE
        strokeCap = size.lineCap
        strokeWidth = size.lineWidth(context)
        this.color = color.foreground(context)
    }


    private val lineArcOvalRect = RectF()

    private val helper = MaterialWaiterHelper(animationParams)
    private val arcParams = MaterialWaiterHelper.ArcParams()

    override fun drawContent(
            canvas: Canvas,
            visibilityPercentage: Float
    ) {
        helper.calculateArcParams(arcParams)
        linePaint.alpha = (lineAlpha * visibilityPercentage).toInt()
        canvas.drawArc(lineArcOvalRect, arcParams.start, arcParams.sweep, false, linePaint)
    }

    override fun onLayout(
            changed: Boolean,
            left: Int,
            top: Int,
            right: Int,
            bottom: Int
    ) {
        super.onLayout(changed, left, top, right, bottom)
        val centerX = paddingLeft + (width - paddingHorizontalSum) / 2f
        val centerY = paddingTop + (height - paddingVerticalSum) / 2f
        lineArcOvalRect.set(
                centerX - radius,
                centerY - radius,
                centerX + radius,
                centerY + radius
        )
    }

}

inline fun ViewGroup.materialWaiter(
        visibility: Emitter<Boolean>,
        color: MaterialWaiterColor = MaterialWaiterColor(),
        size: MaterialWaiterSize = MaterialWaiterSize.default,
        animationParams: MaterialWaiterAnimationParams = MaterialWaiterAnimationParams(),
        childConfigurator: MaterialWaiter.() -> Unit = {}
) = view(
        MaterialWaiter(
                context = context,
                isVisible = visibility,
                color = color,
                size = size,
                animationParams = animationParams
        ),
        childConfigurator
)