package org.hnau.android.base.extensions.view.text

import android.widget.TextView
import androidx.annotation.ColorInt

fun TextView.setShadowInfo(radius: Number, dx: Number, dy: Number, @ColorInt shadowColor: Int) =
     setShadowLayer(radius.toFloat(), dx.toFloat(), dy.toFloat(), shadowColor)