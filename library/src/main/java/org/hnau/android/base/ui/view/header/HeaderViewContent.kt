package org.hnau.android.base.ui.view.header

import android.annotation.SuppressLint
import android.content.Context
import org.hnau.android.base.data.Side
import org.hnau.android.base.data.getter.picture.Picture
import org.hnau.android.base.data.getter.px.Px
import org.hnau.android.base.data.getter.px.constants
import org.hnau.android.base.data.getter.px.dp
import org.hnau.android.base.data.getter.text.Text
import org.hnau.android.base.data.getter.tone.Tone
import org.hnau.android.base.extensions.view.group.children.view
import org.hnau.android.base.extensions.view.layout.params.linearParams
import org.hnau.android.base.extensions.view.linear.RowLayout
import org.hnau.android.base.extensions.view.makeExactlyMeasureSpec
import org.hnau.android.base.extensions.view.measureSpecFit
import org.hnau.android.base.extensions.view.measureSpecMax
import org.hnau.android.base.ui.rawcolor.RawColor
import org.hnau.emitter.Emitter


@SuppressLint("ViewConstructor")
class HeaderViewContent(
        context: Context,
        title: Emitter<Pair<Text, Side>>,
        foreground: Emitter<RawColor>,
        leftAction: Emitter<Triple<(RawColor) -> Picture, Text, () -> Unit>?>,
        rightAction: Emitter<Triple<(RawColor) -> Picture, Text, () -> Unit>?>
) : RowLayout(
        context = context
) {

    init {
        view(
                HeaderActionViewPresenter(
                        context = context,
                        foreground = foreground,
                        action = leftAction,
                        emergenceSide = Side.left
                )
        ) {
            linearParams {
                width = Px.constants.headerHeight
                height = matchParent
            }
        }
        view(
                HeaderTitleViewPresenter(
                        context = context,
                        foreground = foreground,
                        title = title
                )
        ) {
            linearParams {
                weight = 1
                width = zero
                height = matchParent
            }
        }
        view(
                HeaderActionViewPresenter(
                        context = context,
                        foreground = foreground,
                        action = rightAction,
                        emergenceSide = Side.right
                )
        ) {
            linearParams {
                width = Px.constants.headerHeight
                height = matchParent
            }
        }
    }


    override fun onMeasure(
            widthMeasureSpec: Int,
            heightMeasureSpec: Int
    ) = super.onMeasure(
            makeExactlyMeasureSpec(measureSpecMax(widthMeasureSpec)),
            makeExactlyMeasureSpec(measureSpecFit(heightMeasureSpec, Px.constants.headerHeight))
    )

}