package org.hnau.android.base.ui.view.utils.state.utils

import org.hnau.android.base.system.lifecycle.LifecycleState
import org.hnau.emitter.Emitter
import org.hnau.emitter.extensions.alwaysFalse
import org.hnau.emitter.extensions.map
import org.hnau.emitter.extensions.unique
import org.hnau.emitter.extensions.useWhen


fun ViewIsActiveEmitter(
    lifecycleState: Emitter<LifecycleState>,
    isAttachedToWindow: Emitter<Boolean>
) = lifecycleState
    .map { it.level >= LifecycleState.created.level }
    .unique()
    .useWhen(
        isNeedUse = isAttachedToWindow,
        placeholder = Emitter.alwaysFalse
    )
    .unique()