package org.hnau.android.base.ui.view

import android.graphics.Canvas
import android.view.View


interface ViewScroll {
    val scrollX: Int
    val scrollY: Int
}

fun Canvas.scroll(
        viewScroll: ViewScroll
) = translate(
        viewScroll.scrollX.toFloat(),
        viewScroll.scrollY.toFloat()
)

fun View.createViewScroll() = object : ViewScroll {

    override val scrollX: Int
        get() = this@createViewScroll.scrollX

    override val scrollY: Int
        get() = this@createViewScroll.scrollY

}