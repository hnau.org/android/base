package org.hnau.android.base.extensions.view.touch


@Suppress("EnumEntryName")
enum class TouchEventType {

    down,
    move,
    up,
    cancel

}