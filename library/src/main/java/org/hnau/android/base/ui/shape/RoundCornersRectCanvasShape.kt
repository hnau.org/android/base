package org.hnau.android.base.ui.shape

import android.content.Context
import android.graphics.Canvas
import android.graphics.Paint
import android.graphics.Path
import org.hnau.android.base.data.edges.Edges
import org.hnau.android.base.data.getter.px.Px
import org.hnau.android.base.extensions.path.addRoundCornerRect
import org.hnau.emitter.Emitter


class RoundCornersRectCanvasShape(
        context: Context,
        cornersRadius: Px
) : BoundsCanvasShape() {

    private val cornersRadius = cornersRadius(context)

    private val path = Path()

    private val pathCanvasShape = PathCanvasShape.create { path }

    override var bounds: Edges<Int>
        get() = super.bounds
        set(value) {
            super.bounds = value
            updatePath()
        }

    private fun updatePath() {
        path.reset()
        path.addRoundCornerRect(boundsRect, cornersRadius)
    }

    override fun draw(canvas: Canvas, paint: Paint) =
            pathCanvasShape.draw(canvas, paint)

    override fun clip(canvas: Canvas) =
            pathCanvasShape.clip(canvas)

}

fun CanvasShape.Companion.roundCornersRect(
        context: Context,
        cornersRadius: Px
) = RoundCornersRectCanvasShape(
        context = context,
        cornersRadius = cornersRadius
)

fun CanvasShape.Companion.roundCornersRect(
        context: Context,
        cornersRadius: Px,
        bounds: Emitter<Edges<Int>>
) = roundCornersRect(
        context = context,
        cornersRadius = cornersRadius
).withBounds(bounds)