package org.hnau.android.base.ui.shadow

import android.content.Context
import org.hnau.android.base.data.getter.px.Px
import org.hnau.android.base.data.getter.px.min
import org.hnau.android.base.data.insets.Insets
import org.hnau.android.base.data.insets.combine
import org.hnau.android.base.extensions.color.shiftTo
import org.hnau.android.base.ui.animation.animation
import org.hnau.base.data.time.Time
import org.hnau.base.extensions.min
import org.hnau.base.extensions.number.asPercentageInter


data class ButtonShadowInfo(
        val up: ShadowInfo,
        val down: ShadowInfo,
        val switchingDuration: Time = Time.animation.small
) {

    companion object

    val insets = Insets.combine(
            first = up.insets,
            second = down.insets
    ) { first, second ->
        Px.min(first, second)
    }

    fun getRadius(
            context: Context,
            upToDownPercentage: Float
    ) = upToDownPercentage.asPercentageInter(
            up.radius(context),
            down.radius(context)
    )

    fun getDx(
            context: Context,
            upToDownPercentage: Float
    ) = upToDownPercentage.asPercentageInter(
            up.dx(context),
            down.dx(context)
    )

    fun getDy(
            context: Context,
            upToDownPercentage: Float
    ) = upToDownPercentage.asPercentageInter(
            up.dy(context),
            down.dy(context)
    )

    fun getShadowColor(
            context: Context,
            upToDownPercentage: Float
    ) = up.tone(context).shiftTo(
            target = down.tone(context),
            percentage = upToDownPercentage
    )

}