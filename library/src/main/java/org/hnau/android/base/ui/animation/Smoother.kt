package org.hnau.android.base.ui.animation

import org.hnau.base.data.single.Single
import org.hnau.base.data.time.Time
import org.hnau.base.data.time.now
import org.hnau.base.extensions.boolean.checkBoolean
import org.hnau.base.extensions.boolean.ifFalse
import org.hnau.base.extensions.elvis
import org.hnau.emitter.Emitter
import org.hnau.emitter.extensions.listen
import org.hnau.emitter.observing.push.always.content.ManualEmitter
import org.hnau.emitter.observing.push.lateinit.LateinitEmitter


fun <T> Emitter<T>.smooth(
        animatingTime: Time = Time.animation.default,
        copy: T.() -> T,
        calcSmoothedValue: (from: T, to: T, percentage: Float) -> T
): Emitter<T> = object : LateinitEmitter<T>() {

    private var from: Single<T>? = null
    private var to: Single<T>? = null
    private var animationStartedTimestamp = Time.zero

    private val isObserving = ManualEmitter(false)
    private var isAnimationsAvailable = false

    private val ticker = AnimationTicker()

    init {
        this@smooth.observe { newTo ->
            checkContains(
                    ifContains = { current ->
                        from = Single(current.copy())
                        to = Single(newTo)
                        animationStartedTimestamp = Time.now()
                        isAnimationsAvailable.checkBoolean(
                                ifTrue = ticker::start,
                                ifFalse = ::onTic
                        )
                    },
                    ifNotContains = {
                        from = Single(newTo)
                        to = from
                        set(newTo)
                    }
            )
        }
        ticker.listen(::onTic)
    }

    private fun stop() {
        ticker.stop()
        to?.let { set(it.value) }
    }

    private fun onTic() {
        fun handleTic(): Boolean {
            isAnimationsAvailable.ifFalse { return false }
            val from = from.elvis { return false }.value
            val to = to.elvis { return false }.value
            val duration = (Time.now() - animationStartedTimestamp)
                    .takeIf { it < animatingTime }
                    ?: return false
            val percentage =
                    (duration.milliseconds.toDouble() / animatingTime.milliseconds.toDouble()).toFloat()
            set(calcSmoothedValue(from, to, percentage))
            return true
        }
        handleTic().ifFalse(::stop)
    }

    override fun beforeFirstAttached() {
        super.beforeFirstAttached()
        isObserving.value = true
        isAnimationsAvailable = true
    }

    override fun afterLastDetached() {
        isAnimationsAvailable = false
        isObserving.value = false
        stop()
        super.afterLastDetached()
    }


}