package org.hnau.android.base.ui.view.presenter.info.size.content

import org.hnau.android.base.ui.animation.curver.Curver
import org.hnau.android.base.ui.view.presenter.info.size.content.base.ContentSizeCalculator
import org.hnau.android.base.ui.view.presenter.view.lifecycle.ShowingMeasurable
import org.hnau.android.base.ui.view.presenter.view.lifecycle.ViewMeasuredSizeGetter
import org.hnau.base.extensions.number.takeIfPositive


abstract class CombiningContentSizeCalculator(
    private val curver: Curver
) : ContentSizeCalculator {

    override fun calcContentSize(
        measureds: Collection<ShowingMeasurable>,
        measuredSizeExtractor: (ViewMeasuredSizeGetter) -> Int?
    ): Int {
        var count = 0
        var percentageSum = 0f
        val sum = measureds.fold(0f) { acc, measurable ->
            val viewSize = measuredSizeExtractor(measurable)
            val visibilityPercentageRaw = measurable.visibilityPercentage
            val visibilityPercentage = curver(visibilityPercentageRaw)
            val significantSize = calculateViewSignificantSize(viewSize, visibilityPercentage)
            significantSize?.let {
                percentageSum += visibilityPercentage
                count += 1
            }
            return@fold acc + (significantSize ?: 0f)
        }
        count.takeIfPositive() ?: return 0
        if (count == 1) {
            return sum.toInt()
        }
        percentageSum.takeIfPositive() ?: return 0
        return (sum / percentageSum).toInt()
    }

    protected abstract fun calculateViewSignificantSize(
        viewSize: Int?,
        percentage: Float
    ): Float?

}