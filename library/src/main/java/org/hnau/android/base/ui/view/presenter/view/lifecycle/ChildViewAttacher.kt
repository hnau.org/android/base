package org.hnau.android.base.ui.view.presenter.view.lifecycle

import android.view.ViewGroup


interface ChildViewAttacher {

    fun addView(parent: ViewGroup)
    fun removeView(parent: ViewGroup)

}