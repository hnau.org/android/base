package org.hnau.android.base.ui.view.input.info

import org.hnau.android.base.data.getter.px.Px
import org.hnau.android.base.data.getter.px.dp
import org.hnau.base.data.time.Time
import org.hnau.base.data.time.asSeconds


data class InputViewTitleInfo(
        val topTextSize: Px = 12.dp,
        val toTopAnimationTime: Time = 0.2.asSeconds,
        val bottomAlpha: Float = 0.5f
) {

    companion object

}