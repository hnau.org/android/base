package org.hnau.android.base.ui.lightness.values


data class MutableLightnessValues<T>(
        override var light: T,
        override var dark: T
) : LightnessValues<T> {

    fun set(
            light: T,
            dark: T
    ) {
        this.light = light
        this.dark = dark
    }

    fun set(
            source: LightnessValues<T>
    ) = set(
            light = source.light,
            dark = source.dark
    )

}