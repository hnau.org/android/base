package org.hnau.android.base.data.insets

import org.hnau.android.base.extensions.emitter.uniqueMutable
import org.hnau.emitter.Emitter


inline fun <A, B> Insets.Companion.compare(
        first: Insets<A>,
        second: Insets<B>,
        compare: (A, B) -> Boolean
) = compare(first.left, second.left) &&
        compare(first.top, second.top) &&
        compare(first.right, second.right) &&
        compare(first.bottom, second.bottom)

inline fun <T> Emitter<Insets<T>>.uniqueInsets(
        crossinline compare: (T, T) -> Boolean
) = uniqueMutable(
        createCache = ::MutableInsets,
        updateCache = MutableInsets<T>::set,
        comparator = { first, second ->
            Insets.compare(first, second, compare)
        }
)