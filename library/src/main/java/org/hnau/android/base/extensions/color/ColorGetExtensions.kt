package org.hnau.android.base.extensions.color

import android.graphics.Color
import androidx.annotation.ColorInt


val @receiver:ColorInt Int.alpha: Int
    get() = Color.alpha(this)

val @receiver:ColorInt Int.red: Int
    get() = Color.red(this)

val @receiver:ColorInt Int.green: Int
    get() = Color.green(this)

val @receiver:ColorInt Int.blue: Int
    get() = Color.blue(this)

val @receiver:ColorInt Int.alphaPercentage: Float
    get() = ColorUtils.valueToPercentage(alpha)

val @receiver:ColorInt Int.redPercentage: Float
    get() = ColorUtils.valueToPercentage(red)

val @receiver:ColorInt Int.greenPercentage: Float
    get() = ColorUtils.valueToPercentage(green)

val @receiver:ColorInt Int.bluePercentage: Float
    get() = ColorUtils.valueToPercentage(blue)