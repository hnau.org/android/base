package org.hnau.android.base.ui.drawable.partout.calculators

import android.graphics.Point
import android.graphics.Rect
import org.hnau.android.base.data.direction.Direction
import org.hnau.android.base.data.direction.apply
import org.hnau.android.base.ui.drawable.partout.PartoutCalculator


abstract class DirectionPartoutCalculator(
        private val direction: Direction
) : PartoutCalculator {

    private val contentSize = Point()

    final override fun calcContentBounds(
            parentWidth: Int,
            parentHeight: Int,
            contentPreferredWidth: Int,
            contentPreferredHeight: Int,
            result: Rect
    ) {
        calcContentSize(parentWidth, parentHeight, contentPreferredWidth, contentPreferredHeight, contentSize)
        direction.apply(
                width = contentSize.x,
                height = contentSize.y,
                parentLeft = 0,
                parentTop = 0,
                parentRight = parentWidth,
                parentBottom = parentHeight,
                result = result
        )
    }

    protected abstract fun calcContentSize(
            parentWidth: Int,
            parentHeight: Int,
            contentPreferredWidth: Int,
            contentPreferredHeight: Int,
            result: Point
    )

}