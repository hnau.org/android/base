package org.hnau.android.base.ui

import android.os.Handler
import android.os.Looper
import org.hnau.base.extensions.boolean.checkBoolean
import org.hnau.base.extensions.boolean.ifFalse


object UiThreadUtils {

    internal val mainLooper = Looper.getMainLooper()!!

    @PublishedApi
    internal val isInMainThread: Boolean
        get() = Looper.myLooper() == mainLooper

    private val mainThreadHandler = Handler(mainLooper)

    inline fun <T> runUiOrElse(
        elseAction: () -> T,
        action: () -> T
    ) = isInMainThread.checkBoolean(
        ifTrue = action,
        ifFalse = elseAction
    )

    inline fun <T> runUiOrNull(action: () -> T) = runUiOrElse(
            action = action,
            elseAction = { null }
    )

    inline fun runUiOrFalse(action: () -> Unit) = runUiOrElse(
            action = { action(); true },
            elseAction = { false }
    )

    inline fun <T> runUiOrThrow(action: () -> T) = runUiOrElse(
            action = action,
            elseAction = UiThreadUtils::throwNotInMainThreadException
    )

    fun runUiOrPost(action: () -> Unit) = runUiOrElse(
            action = action,
            elseAction = { mainThreadHandler.post(action) }
    )

    fun throwExceptionIfNotInMainThread() {
        isInMainThread.ifFalse(UiThreadUtils::throwNotInMainThreadException)
    }

    @PublishedApi
    internal fun throwNotInMainThreadException(): Nothing =
        throw IllegalStateException("Not in main thread")

}