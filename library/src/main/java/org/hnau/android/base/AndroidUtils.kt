package org.hnau.android.base

import java.util.*

object AndroidUtils {

    val random = Random(System.currentTimeMillis())

    fun generateId() = random.nextInt() and 0x0000ffff

}