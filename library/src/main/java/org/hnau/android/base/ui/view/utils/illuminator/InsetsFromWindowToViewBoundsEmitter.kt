package org.hnau.android.base.ui.view.utils.illuminator

import android.view.View
import android.view.Window
import org.hnau.android.base.data.edges.Edges
import org.hnau.android.base.data.edges.MutableEdges
import org.hnau.android.base.data.insets.Insets
import org.hnau.android.base.data.insets.MutableInsets
import org.hnau.base.data.possible.lateinit.Lateinit
import org.hnau.base.data.possible.lateinit.unsafe
import org.hnau.base.data.possible.updateMutable
import org.hnau.base.extensions.boolean.ifTrue
import org.hnau.emitter.Emitter
import org.hnau.emitter.extensions.filterNotNull
import org.hnau.emitter.extensions.map
import org.hnau.emitter.observing.notifier.Notifier
import org.hnau.emitter.observing.push.lateinit.combine


fun InsetsFromWindowToViewBoundsEmitter(
        view: View,
        window: Emitter<Window>,
        onLayout: Emitter<Unit>
): Emitter<Insets<Int>> = Lateinit.unsafe<MutableInsets<Int>>().let { resultCache ->
    val positionCache = IntArray(2) { 0 }
    Emitter.combine(
            firstSource = window,
            secondSource = onLayout
    ) { window, _ ->
        view.getLocationInWindow(positionCache)
        val (left, top) = positionCache
        val right = window.decorView.width - (left + view.width)
        val bottom = window.decorView.height - (top + view.height)
        resultCache.updateMutable(
                createInitialValue = { MutableInsets(left, top, right, bottom) },
                updateValue = { set(left, top, right, bottom) }
        )
    }
}