package org.hnau.android.base.data.edges

import org.hnau.android.base.data.edges.MutableEdges
import org.hnau.base.data.possible.lateinit.Lateinit
import org.hnau.base.data.possible.lateinit.unsafe
import org.hnau.base.data.possible.updateMutable
import org.hnau.emitter.Emitter
import org.hnau.emitter.extensions.map


inline fun <I, O> Edges<I>.map(
        transform: (I) -> O
) = Edges(
        xMin = transform(xMin),
        yMin = transform(yMin),
        xMax = transform(xMax),
        yMax = transform(yMax)
)

inline fun <I, O> Emitter<Edges<I>>.mapMemSave(
        crossinline transform: (I) -> O
): Emitter<Edges<O>> = Lateinit.unsafe<MutableEdges<O>>().let { resultCache ->
    map { source ->
        resultCache.updateMutable(
                createInitialValue = {
                    MutableEdges(
                            xMin = transform(source.xMin),
                            yMin = transform(source.yMin),
                            xMax = transform(source.xMax),
                            yMax = transform(source.yMax)
                    )
                },
                updateValue = {
                    xMin = transform(source.xMin)
                    yMin = transform(source.yMin)
                    xMax = transform(source.xMax)
                    yMax = transform(source.yMax)
                }
        )
    }
}