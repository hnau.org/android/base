package org.hnau.android.base.extensions.emitter

import org.hnau.base.data.possible.lateinit.Lateinit
import org.hnau.base.data.possible.lateinit.unsafe
import org.hnau.base.extensions.boolean.ifTrue
import org.hnau.emitter.Emitter
import org.hnau.emitter.extensions.create


inline fun <T, C> Emitter<T>.uniqueMutable(
        crossinline createCache: (T) -> C,
        crossinline updateCache: C.(T) -> Unit,
        crossinline comparator: (first: C, second: T) -> Boolean = { first, second -> first == second }
) = Emitter.create<T> { observer ->
    val cache = Lateinit.unsafe<C>()
    observe { newValue ->
        val changed = synchronized(cache) {
            cache.checkPossible(
                    ifValueExists = { cached ->
                        val changed = comparator(cached, newValue).not()
                        changed.ifTrue { cached.updateCache(newValue) }
                        changed
                    },
                    ifValueNotExists = {
                        cache.set(createCache(newValue))
                        true
                    }
            )
        }
        changed.ifTrue { observer(newValue) }
    }
}