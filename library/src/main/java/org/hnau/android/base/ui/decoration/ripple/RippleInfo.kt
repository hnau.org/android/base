package org.hnau.android.base.ui.decoration.ripple

import org.hnau.android.base.data.getter.tone.Tone
import org.hnau.android.base.data.getter.tone.Tones
import org.hnau.android.base.data.getter.tone.replaceAlpha
import org.hnau.android.base.ui.animation.animation
import org.hnau.android.base.ui.animation.curver.Curver
import org.hnau.android.base.ui.animation.curver.Curvers
import org.hnau.base.data.time.Time
import org.hnau.base.extensions.boolean.checkBoolean


data class RippleInfo(
        val tone: Tone,
        val growingDuration: Time = Time.animation.default,
        val fadingDuration: Time = Time.animation.default,
        val growingSizeCurver: Curver = Curvers.accelerateDecelerate,
        val minRadiusPercentage: Float = 0.4f
) {

    companion object;

}