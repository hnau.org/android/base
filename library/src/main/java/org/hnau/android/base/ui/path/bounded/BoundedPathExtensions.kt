package org.hnau.android.base.ui.path.bounded

import android.graphics.Matrix
import android.graphics.Path
import android.graphics.PointF


fun BoundedPath.scale(
        scaleX: Float,
        scaleY: Float
) = copy(
        size = PointF(
                size.x * scaleX,
                size.y * scaleY
        ),
        path = Path().also { scaledPath ->
            path.transform(
                    Matrix().apply { setScale(scaleX, scaleY) },
                    scaledPath
            )
        }

)

fun BoundedPath.scale(
        scale: Float
) = scale(
        scaleX = scale,
        scaleY = scale
)