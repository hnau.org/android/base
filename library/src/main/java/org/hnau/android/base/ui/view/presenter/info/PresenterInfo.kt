package org.hnau.android.base.ui.view.presenter.info

import org.hnau.android.base.ui.view.presenter.info.size.presenter.base.PresenterSizeCalculator
import org.hnau.android.base.ui.view.presenter.info.size.presenter.contentSizeIndependent
import org.hnau.android.base.ui.view.presenter.info.size.presenter.matchParent


data class PresenterInfo(
    val width: PresenterSizeCalculator = PresenterSizeCalculator.matchParent(),
    val height: PresenterSizeCalculator = PresenterSizeCalculator.matchParent()
) {

    val contentSizeIndependent =
        width.contentSizeIndependent && height.contentSizeIndependent


}