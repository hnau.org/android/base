package org.hnau.android.base.ui

import android.content.Context
import android.widget.Toast
import org.hnau.android.base.data.getter.text.Text


class Toaster(
    private val context: Context
) {

    companion object;

    enum class Length(val key: Int) {
        short(Toast.LENGTH_SHORT),
        long(Toast.LENGTH_LONG)
    }

    private var currentToast: Toast? = null
        set(value) {
            field?.cancel()
            field = value
            value?.show()
        }

    fun show(
        text: Text,
        length: Length
    ): Toast = Toast.makeText(
        context,
        text(context),
        length.key
    ).also { newToast ->
        currentToast = newToast
    }

    fun short(
        text: Text
    ) = show(
        text = text,
        length = Length.short
    )

    fun long(
        text: Text
    ) = show(
        text = text,
        length = Length.long
    )

}