package org.hnau.android.base.ui.view.presenter.info.size.presenter

import android.content.Context
import org.hnau.android.base.data.getter.px.Px
import org.hnau.android.base.ui.MeasureUtils
import org.hnau.android.base.ui.view.presenter.info.size.presenter.base.PresenterSizeCalculator
import org.hnau.android.base.ui.view.presenter.view.lifecycle.ShowingMeasurable
import org.hnau.android.base.ui.view.presenter.view.lifecycle.ViewMeasuredSizeGetter


abstract class IndependentPresenterSizeCalculator : PresenterSizeCalculator {

    companion object {

        inline fun create(
            crossinline calcPresenterSize: (measureSpec: Int) -> Int
        ) = object :
            IndependentPresenterSizeCalculator() {
            override fun calcPresenterSize(measureSpec: Int) =
                calcPresenterSize.invoke(measureSpec)
        }

    }

    override final fun calcPresenterSize(
        measureSpec: Int,
        measureds: Collection<ShowingMeasurable>,
        measuredSizeExtractor: (ViewMeasuredSizeGetter) -> Int?
    ) = calcPresenterSize(
        measureSpec = measureSpec
    )

    protected abstract fun calcPresenterSize(measureSpec: Int): Int

}

val PresenterSizeCalculator.contentSizeIndependent: Boolean
    get() = this is IndependentPresenterSizeCalculator

inline fun PresenterSizeCalculator.Companion.independent(
    crossinline calcPresenterSize: (measureSpec: Int) -> Int
) =
    IndependentPresenterSizeCalculator.create(
        calcPresenterSize
    )

fun PresenterSizeCalculator.Companion.matchParent(
    forUnspecified: Number = 0
) = IndependentPresenterSizeCalculator.create { measureSpec ->
    MeasureUtils.max(measureSpec, forUnspecified)
}

fun PresenterSizeCalculator.Companion.matchParent(
    context: Context,
    forUnspecified: Px
) = IndependentPresenterSizeCalculator.create { measureSpec ->
    MeasureUtils.max(context, measureSpec, forUnspecified)
}

fun PresenterSizeCalculator.Companion.fixed(
    size: Number
) = IndependentPresenterSizeCalculator.create { measureSpec ->
    MeasureUtils.fit(measureSpec, size)
}

fun PresenterSizeCalculator.Companion.fixed(
    context: Context,
    size: Px
) = IndependentPresenterSizeCalculator.create { measureSpec ->
    MeasureUtils.fit(context, measureSpec, size)
}