package org.hnau.android.base.ui.drawable.partout.calculators

import org.hnau.android.base.ui.drawable.DrawableWrapper


interface NoResizeWrapperSizeCalculator : DrawableWrapper.WrapperSizeCalculator {

    override fun calcIntrinsicWrapperWidth(contentIntrinsicWidth: Int) =
        contentIntrinsicWidth

    override fun calcIntrinsicWrapperHeight(contentIntrinsicHeight: Int) =
        contentIntrinsicHeight

}