package org.hnau.android.base.data.getter.picture

import android.content.Context
import android.graphics.drawable.Drawable
import org.hnau.android.base.data.getter.base.ContextGetter
import org.hnau.android.base.data.getter.base.CachedContextGetter
import org.hnau.android.base.ui.drawable.EmptyDrawable


typealias Picture = ContextGetter<Drawable>

@Suppress("FunctionName")
fun Picture(get: (Context) -> Drawable): Picture =
    CachedContextGetter(get)

object Pictures {

    val empty = Picture(EmptyDrawable)

}