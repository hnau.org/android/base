package org.hnau.android.base.ui.view.presenter.view.lifecycle

import android.graphics.Canvas
import android.graphics.Rect
import android.view.View
import org.hnau.android.base.ui.animation.curver.Curvers
import org.hnau.android.base.ui.animation.emergence.offset.EmergenceDirection
import org.hnau.android.base.ui.animation.emergence.offset.EmergencesInfo
import org.hnau.android.base.ui.animation.tasks.AnimationTaskPercentage
import org.hnau.android.base.ui.animation.tasks.AnimationTasks
import org.hnau.android.base.ui.view.presenter.transaction.OffsetTask
import org.hnau.android.base.ui.view.presenter.transaction.Transaction
import org.hnau.android.base.ui.view.presenter.view.wrapper.ViewWrapper
import org.hnau.base.extensions.boolean.ifFalse
import org.hnau.emitter.extensions.listen


class ViewLifecycle(
        private val viewWrapper: ViewWrapper,
        private val parent: LifecycleParent
) : ViewMeasurer by viewWrapper,
        ViewMeasuredSizeGetter by viewWrapper,
        ChildViewAttacher by viewWrapper,
        VisibilityPercentageGetter,
        ShowingMeasurable {

    companion object {

        fun create(
                parent: LifecycleParent,
                transaction: Transaction
        ) = ViewLifecycle(
                parent = parent,
                viewWrapper = transaction.view
        )


    }

    private val tasksQueue = AnimationTasks(
            initialTask = createOffsetTask(EmergenceDirection.show),
            isAnimateAvailable = parent.isAnimationsAvailable
    )

    private var targetDirection = EmergenceDirection.show

    private val currentTaskDirection
        get() = state?.task?.direction ?: EmergenceDirection.show

    private val offsetAmount
        get() = state?.percentage ?: 0f

    private var state: AnimationTaskPercentage<OffsetTask>? = null
        set(value) {
            field = value
            viewWrapper.setAlpha(currentTaskDirection, offsetAmount)
            parent.onLifecycleChanged()
        }

    override val visibilityPercentage
        get() = when (currentTaskDirection) {
            EmergenceDirection.show -> offsetAmount
            EmergenceDirection.hide -> 1 - offsetAmount
        }.let(Curvers.decelerate)

    init {
        tasksQueue.observe { state = it }
        tasksQueue.onFinished.listen(::onQueueFinished)
    }

    private fun createOffsetTask(
            direction: EmergenceDirection
    ) = OffsetTask(
            direction = direction,
            duration = viewWrapper.emergencesInfo[direction].duration
    )

    private fun onQueueFinished() {
        (targetDirection == EmergenceDirection.hide).ifFalse { return }
        parent.onLifecycleFinished(this)
    }

    fun hide() {
        (targetDirection == EmergenceDirection.show).ifFalse { return }
        targetDirection = EmergenceDirection.hide
        tasksQueue.addTask(createOffsetTask(EmergenceDirection.hide))
    }

    fun layout(
            slot: Rect
    ) = viewWrapper.layout(
            slot = slot
    )

    fun draw(
            slot: Rect,
            canvas: Canvas,
            emergencesInfo: EmergencesInfo,
            childDrawer: (canvas: Canvas, child: View) -> Unit
    ) {
        viewWrapper.draw(
                slot = slot,
                canvas = canvas,
                childDrawer = childDrawer,
                visibilityPercentage = visibilityPercentage,
                emergenceInfo = emergencesInfo[currentTaskDirection]
        )
    }

}