package org.hnau.android.base.data.edges


inline fun <T> Edges<T>.width(
        minus: (T, T) -> T
) = minus(
        xMax,
        xMin
)

inline fun <T> Edges<T>.height(
        minus: (T, T) -> T
) = minus(
        yMax,
        yMin
)

inline fun <T> Edges<T>.centerX(
        plus: (T, T) -> T,
        half: (T) -> T
) = half(
        plus(
                xMax,
                xMin
        )
)

inline fun <T> Edges<T>.centerY(
        plus: (T, T) -> T,
        half: (T) -> T
) = half(
        plus(
                yMax,
                yMin
        )
)

inline fun <T> Edges<T>.contains(
        x: T,
        y: T,
        isFirstLargeOrEqualsThanSecond: (T, T) -> Boolean
) = isFirstLargeOrEqualsThanSecond(x, xMin) &&
        isFirstLargeOrEqualsThanSecond(xMax, x) &&
        isFirstLargeOrEqualsThanSecond(y, yMin) &&
        isFirstLargeOrEqualsThanSecond(yMax, y)

fun <T : Comparable<T>> Edges<T>.contains(
        x: T,
        y: T
) = contains(
        x = x,
        y = y,
        isFirstLargeOrEqualsThanSecond = { a, b -> a >= b }
)