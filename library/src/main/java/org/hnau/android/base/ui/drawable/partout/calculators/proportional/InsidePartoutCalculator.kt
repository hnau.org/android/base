package org.hnau.android.base.ui.drawable.partout.calculators.proportional

import org.hnau.android.base.data.direction.Direction
import org.hnau.android.base.ui.drawable.partout.PartoutCalculator
import org.hnau.base.extensions.number.takeIfPositive
import kotlin.math.min


class InsidePartoutCalculator(
    direction: Direction = Direction.center
) : ProportionalPartoutCalculator(direction) {

    override fun calcScaleFactor(
        parentWidth: Int,
        parentHeight: Int,
        contentPreferredWidth: Int,
        contentPreferredHeight: Int
    ): Float {
        val contentWidth = contentPreferredWidth.takeIfPositive()?.toFloat() ?: return 1f
        val contentHeight = contentPreferredHeight.takeIfPositive()?.toFloat() ?: return 1f
        val width = parentWidth.takeIfPositive()?.toFloat() ?: return 1f
        val height = parentHeight.takeIfPositive()?.toFloat() ?: return 1f
        return min(width / contentWidth, height / contentHeight)
    }

}

fun PartoutCalculator.Companion.inside(
        direction: Direction = Direction.center
) = InsidePartoutCalculator(
        direction = direction
)