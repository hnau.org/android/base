package org.hnau.android.base.ui.view.utils.illuminator

import android.content.Context
import android.view.View
import android.widget.FrameLayout
import org.hnau.android.base.data.insets.typed.uniqueInsets
import org.hnau.emitter.Emitter

inline fun IlluminatorContainerView(
        context: Context,
        background: View,
        buildForeground: (illuminatorView: View) -> View,
        isActive: Emitter<Boolean>,
        onLayout: Emitter<Unit>
): View {

    val illuminatorView = View(context)
    val foreground = buildForeground(illuminatorView)

    val backgroundWrapper = object : FrameLayout(context), CustomInsetsProvider {

        override val windowToContentInsets = InsetsFromWindowToViewBoundsEmitter(
                view = illuminatorView,
                window = ViewWindowEmitter(
                        view = illuminatorView,
                        isActive = isActive
                ),
                onLayout = onLayout
        ).uniqueInsets()

        init {
            addView(background)
        }

    }

    return FrameLayout(context).apply {
        addView(backgroundWrapper)
        addView(foreground)
    }
}

