package org.hnau.android.base.extensions

import android.content.Context
import android.graphics.Canvas
import android.graphics.Path
import android.graphics.Region
import android.os.Build
import org.hnau.android.base.data.getter.px.Px
import org.hnau.android.base.data.getter.px.SizeUnitType


inline fun Canvas.inState(
        action: Canvas.() -> Unit
) {
    save()
    action(this)
    restore()
}

fun Canvas.clipPathOut(
        path: Path
) = if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
    clipOutPath(path)
} else {
    @Suppress("DEPRECATION")
    clipPath(path, Region.Op.DIFFERENCE)
}

fun Canvas.scale(unit: Float) =
        scale(unit, unit)

fun Canvas.scale(
        context: Context,
        unit: Px
) = scale(
        unit = unit(context)
)

fun Canvas.scale(context: Context, sizeUnitType: SizeUnitType) =
        scale(sizeUnitType.scaleToPxFactor(context))

fun Canvas.scaleToDp(context: Context) =
        scale(context, SizeUnitType.dp)

fun Canvas.scaleToSp(context: Context) =
        scale(context, SizeUnitType.sp)

fun Canvas.scaleToPt(context: Context) =
        scale(context, SizeUnitType.pt)

fun Canvas.scaleToInch(context: Context) =
        scale(context, SizeUnitType.inch)

fun Canvas.scaleToMM(context: Context) =
        scale(context, SizeUnitType.mm)

fun Canvas.scaleToCM(context: Context) =
        scale(context, SizeUnitType.cm)