package org.hnau.android.base.ui.view.recycler.adapter.content.emitter.notifier.diff

import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListUpdateCallback
import org.hnau.android.base.ui.view.recycler.adapter.content.emitter.notifier.NewContentNotifier
import org.hnau.android.base.ui.view.recycler.adapter.content.emitter.notifier.diff.matcher.Matcher


class NewContentDiffNotifier<T>(
    adapter: ListUpdateCallback,
    matcher: Matcher<T>,
    private val detectItemsMoves: Boolean = true
) : NewContentNotifier<T>(
    adapter = adapter
) {

    companion object;

    private var oldContent = emptyList<T>()
    private var newContent = emptyList<T>()

    private val callback = object : DiffUtil.Callback() {

        override fun getOldListSize() = oldContent.size
        override fun getNewListSize() = newContent.size

        override fun areItemsTheSame(
            oldItemPosition: Int,
            newItemPosition: Int
        ) = matcher.itemsMatcher.itemsAreSame(
            item1 = oldContent[oldItemPosition],
            item2 = newContent[newItemPosition]
        )

        override fun areContentsTheSame(
            oldItemPosition: Int,
            newItemPosition: Int
        ) = matcher.contentsMatcher.contentsAreSame(
            item1 = oldContent[oldItemPosition],
            item2 = newContent[newItemPosition]
        )

    }

    override fun onContentChanged(oldContent: List<T>, newContent: List<T>) {
        this.oldContent = oldContent
        this.newContent = newContent

        val diff = DiffUtil.calculateDiff(callback, detectItemsMoves)
        diff.dispatchUpdatesTo(adapter)

        this.oldContent = emptyList()
        this.newContent = emptyList()
    }

}

fun <T> NewContentNotifier.Companion.diff(
    matcher: Matcher<T>,
    detectItemsMoves: Boolean = true
): (ListUpdateCallback) -> NewContentNotifier<T> = { adapter ->
    NewContentDiffNotifier(
        adapter = adapter,
        matcher = matcher,
        detectItemsMoves = detectItemsMoves
    )
}