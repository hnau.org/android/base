package org.hnau.android.base.ui.theme.drawable

import android.graphics.Canvas
import android.graphics.ColorFilter
import android.graphics.LinearGradient
import android.graphics.Paint
import android.graphics.PixelFormat
import android.graphics.Point
import android.graphics.Shader
import android.graphics.drawable.Drawable
import org.hnau.android.base.extensions.inState
import org.hnau.android.base.ui.theme.ThemeGradient
import org.hnau.base.extensions.ifNotNull
import org.hnau.base.extensions.ifNull
import org.hnau.base.extensions.number.takeIfPositive


class ThemeGradientDrawable : Drawable() {

    var gradient: ThemeGradient? = null
        set(value) {
            value.ifNotNull { existenceValue ->
                field = existenceValue
                updateGradient(width, height, existenceValue)
            }
        }

    private val currentGradientSize = Point()

    private val paint = Paint()

    private val width
        get() = bounds.right - bounds.left

    private val height
        get() = bounds.bottom - bounds.top

    override fun setBounds(left: Int, top: Int, right: Int, bottom: Int) {
        super.setBounds(left, top, right, bottom)
        val width = this.width.takeIfPositive() ?: return
        val height = this.height.takeIfPositive() ?: return
        if (currentGradientSize.x != width || currentGradientSize.y != height) {
            gradient.ifNotNull { gradient ->
                updateGradient(width, height, gradient)
            }
        }
    }

    private fun updateGradient(
            width: Int,
            height: Int,
            gradient: ThemeGradient
    ) {
        paint.shader = LinearGradient(
                0f, 0f,
                width.toFloat(), height.toFloat(),
                gradient.start.value, gradient.end.value,
                Shader.TileMode.CLAMP
        )
    }

    override fun draw(
            canvas: Canvas
    ) {
        gradient.ifNull { return }
        canvas.inState {
            translate(-bounds.left.toFloat(), -bounds.top.toFloat())
            drawRect(0f, 0f, width.toFloat(), height.toFloat(), paint)
        }
    }

    override fun setAlpha(alpha: Int) {
        paint.alpha = alpha
    }

    override fun setColorFilter(colorFilter: ColorFilter?) {
        paint.colorFilter = colorFilter
    }

    override fun getOpacity() =
            PixelFormat.OPAQUE

}