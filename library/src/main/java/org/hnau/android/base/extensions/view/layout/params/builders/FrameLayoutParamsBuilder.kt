package org.hnau.android.base.extensions.view.layout.params.builders

import android.content.Context
import android.widget.FrameLayout
import org.hnau.android.base.data.direction.gravity


class FrameLayoutParamsBuilder(
    context: Context
) : MarginGravityLayoutParamsBuilder(
    context = context
) {

    override fun build() =
        FrameLayout.LayoutParams(
            widthValue, heightValue
        ).apply {
            applyMargins()
            gravity = layoutParamsDirection.gravity
        }

}