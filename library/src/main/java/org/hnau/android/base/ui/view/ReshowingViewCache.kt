package org.hnau.android.base.ui.view

import android.view.View
import org.hnau.base.data.associator.Associator
import org.hnau.base.data.associator.byHashMap
import org.hnau.base.data.associator.filterValues
import org.hnau.base.data.associator.resolveWeakReference
import org.hnau.base.data.associator.toAutoAssociator
import java.lang.ref.WeakReference


class ReshowingViewCache<K, V : View>(
        getView: (K) -> V
) : (K) -> V {

    companion object;

    private val cache = Associator
            .byHashMap<K, WeakReference<V>>()
            .resolveWeakReference()
            .filterValues { it?.parent == null }
            .toAutoAssociator(getView)

    override fun invoke(key: K) = cache(key)

}

fun <K> ReshowingViewCache.Companion.create(
        getView: (K) -> View
) = ReshowingViewCache<K, View>(
        getView = getView
)

fun <V : View> ReshowingViewCache.Companion.singleTyped(
        getView: () -> V
) = ReshowingViewCache<Unit, V>(
        getView = { getView() }
)

fun ReshowingViewCache.Companion.single(getView: () -> View) =
        singleTyped(getView)

operator fun <V : View> ReshowingViewCache<Unit, V>.invoke() = invoke(Unit)