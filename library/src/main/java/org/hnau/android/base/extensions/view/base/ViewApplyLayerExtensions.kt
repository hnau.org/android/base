package org.hnau.android.base.extensions.view.base

import android.graphics.Paint
import android.view.View


fun View.setLayerTypeNone(layerPaint: Paint? = null) =
    setLayerType(View.LAYER_TYPE_NONE, layerPaint)

fun View.setLayerTypeSoftware(layerPaint: Paint? = null) =
    setLayerType(View.LAYER_TYPE_SOFTWARE, layerPaint)

fun View.setLayerTypeHardware(layerPaint: Paint? = null) =
    setLayerType(View.LAYER_TYPE_HARDWARE, layerPaint)