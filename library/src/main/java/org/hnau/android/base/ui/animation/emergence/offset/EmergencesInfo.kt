package org.hnau.android.base.ui.animation.emergence.offset

import org.hnau.android.base.ui.animation.emergence.EmergenceInfo


data class EmergencesInfo(
    val show: EmergenceInfo = EmergenceInfo(),
    val hide: EmergenceInfo = show.reverse()
) {

    companion object;

    operator fun get(
        direction: EmergenceDirection
    ) = when (direction) {
        EmergenceDirection.show -> show
        EmergenceDirection.hide -> hide
    }

}