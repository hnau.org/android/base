package org.hnau.android.base.ui.view.layers

import org.hnau.base.extensions.setUnique
import org.hnau.emitter.Emitter
import org.hnau.emitter.observing.push.always.content.ManualEmitter


abstract class StateLayer: Layer {

    private val isActiveInner =
        ManualEmitter(false)

    protected val isActive: Emitter<Boolean>
        get() = isActiveInner

    override fun onCreate() {
        super.onCreate()
        isActiveInner::value.setUnique(true)
    }

    override fun onDestroy() {
        super.onDestroy()
        isActiveInner::value.setUnique(false)
    }

}