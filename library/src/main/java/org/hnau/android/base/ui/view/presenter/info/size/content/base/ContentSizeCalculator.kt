package org.hnau.android.base.ui.view.presenter.info.size.content.base

import org.hnau.android.base.ui.view.presenter.view.lifecycle.ShowingMeasurable
import org.hnau.android.base.ui.view.presenter.view.lifecycle.ViewMeasuredSizeGetter


interface ContentSizeCalculator {

    companion object;

    fun calcContentSize(
        measureds: Collection<ShowingMeasurable>,
        measuredSizeExtractor: (ViewMeasuredSizeGetter) -> Int?
    ): Int

}