package org.hnau.android.base.system.lifecycle


enum class LifecycleState(
    val level: Int
) {

    nonexistent(0),
    created(1),
    started(2),
    resumed(3)

}