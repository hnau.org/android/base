package org.hnau.android.base.ui.view

import android.view.View
import android.widget.LinearLayout
import org.hnau.android.base.data.getter.px.Px
import org.hnau.android.base.extensions.view.group.children.view
import org.hnau.android.base.extensions.view.layout.params.linearParams


@PublishedApi
internal fun View.createLinearLayoutSeparator(
    weight: Number,
    width: Px,
    height: Px
) = View(context).apply {
    linearParams {
        this.weight = weight.toFloat()
        this.width = width
        this.height = height
    }
}

inline fun LinearLayout.separator(
        weight: Number = 1,
        width: Px = Px.zero,
        height: Px = Px.zero,
        childConfigurator: View.() -> Unit = {}
) = view(
    createLinearLayoutSeparator(weight, width, height),
    childConfigurator
)