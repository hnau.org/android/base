package org.hnau.android.base.data.course


inline fun <R> Course.checkCourse(
        ifHorizontal: () -> R,
        ifVertical: () -> R
) = when (this) {
    Course.horizontal -> ifHorizontal()
    Course.vertical -> ifVertical()
}